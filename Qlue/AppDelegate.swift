//
//  AppDelegate.swift
//  Qlue
//
//  Created by Nurul on 5/25/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import Fabric
import TwitterKit
import IQKeyboardManagerSwift
import GoogleMaps
import Firebase
import SwiftyJSON
//import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FIRApp.configure()
        
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification(_:)),
                                                         name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
      
        // Custom Navigation Bar
        UINavigationBar.appearance().backgroundColor = kBgNavBar
        UINavigationBar.appearance().barTintColor = kBgNavBar
        UINavigationBar.appearance().tintColor = kCreamColor
        UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "bgNavbar"), for: .default)
        UINavigationBar.appearance().isTranslucent = false
        
        
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:kCreamColor, NSFontAttributeName:UIFont.init(name: QlueFont.ProximaNovaSoft, size: 17)!]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:kCreamColor, NSFontAttributeName:UIFont.init(name: QlueFont.ProximaNovaSoft, size: 16)!], for: UIControlState())
        
        setTabBarAppearance()
        
        // Facebook Delegate
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Google SignIn
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        //Twitter
        Twitter.sharedInstance().start(withConsumerKey: kConsumerKeyTwitter, consumerSecret: kConsumerSecretTwitter)
        Fabric.with([Twitter.self])
        
        // Keyboard Handle
        IQKeyboardManager.sharedManager().enable = true
        
        
        // GoogleMaps
        GMSServices.provideAPIKey(kGoogleMaps)
        
        UserDefaults.standard.set(["us"], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        
        // Check profile
        if (User.currentUser() != nil){
            showMainViewController()
        }
//
        // Remote notification
        if let receiveNotification = UserDefaults.standard.object(forKey: kReceiveNotification) as? NSNumber {
            if receiveNotification.boolValue {
                registerForRemoteNotifications()
            }
        }
        else {
            UserDefaults.standard.set(NSNumber(value: true as Bool), forKey: kReceiveNotification)
            UserDefaults.standard.synchronize()
            
            registerForRemoteNotifications()
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Push Notifications
    
    func registerForRemoteNotifications() {
        
        let application = UIApplication.shared
        
//        if #available(iOS 10.0, *) {
//            let center  = UNUserNotificationCenter.currentNotificationCenter()
//            center.delegate = self
//            center.requestAuthorizationWithOptions([.Sound, .Alert, .Badge], completionHandler: { (granted, error) in
//                if error == nil {
//                    application.registerForRemoteNotifications()
//                }
//            })
//        }
//        else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
//        }
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            
            Engine.shared.registerNotif(refreshedToken).responseString { (response) in
                
                if response.result.isSuccess {
                    if let result = response.result.value {
                        print(result)
                    }
                }
                else if let error = response.result.error {
                    print("error: \(error.localizedDescription)")
                }
                else {
                    
                }
            }
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        if let deviceTokenString = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(deviceTokenString)")
            
            Engine.shared.registerNotif(deviceTokenString).responseString { (response) in
                
                if response.result.isSuccess {
                    if let result = response.result.value {
                        print(result)
                    }
                }
                else if let error = response.result.error {
                    print("error: \(error.localizedDescription)")
                }
                else {
                    
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Did fail to register for remote notifications with rrror: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        self.application(application, didReceiveRemoteNotification: userInfo) { (backgroundFetchResult) in }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        handleRemoteNotificationWithUserInfo(userInfo, fetchCompletionHandler: completionHandler)
    }
    
    // MARK: - Helpers
    
    func handleRemoteNotificationWithUserInfo(_ userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        
        if UIApplication.shared.applicationState != .active {
            
            let data = JSON(userInfo)
            let notif = QlueNotif.notifWithData(data)
            
            if let type = notif.type {
                
                switch type {
                case NotificationType.Message.rawValue:
                    viewController?.showChatDetail(notif)
                    
                case NotificationType.Forum.rawValue:
                    viewController?.showForumDetail(notif)
                    
                case NotificationType.ForumDetail.rawValue:
                    viewController?.showForumDetail(notif)
                    
                case NotificationType.Follow.rawValue:
                    viewController?.showOtherProfile(notif)
                    
                case NotificationType.Admin.rawValue:
                    viewController?.showForumDetail(notif)
                    
                case NotificationType.MentionPost.rawValue:
                    viewController?.showFeedDetail(notif)
                    
                case NotificationType.MentionForum.rawValue:
                    viewController?.showForumDetail(notif)
                    
//                case NotificationType.EditKelurahan.rawValue:
//                    break
//                    
//                case NotificationType.ApproveAvatar.rawValue:
//                    break // Main
//                    
//                case NotificationType.Like.rawValue:
//                    break
//                    
//                case NotificationType.ProgressUpdate.rawValue:
//                    break
//                    
//                case NotificationType.FAQReject.rawValue:
//                    break // FAQ page
//                    
//                case NotificationType.PromoCode.rawValue:
//                    break
                    
                case NotificationType.ForumKelurahan.rawValue:
                    viewController?.showForumDetail(notif)
                    
                case NotificationType.UserComment.rawValue:
                    viewController?.showComment(notif)
                    
                case NotificationType.Conversation.rawValue:
                    viewController?.showChatDetail(notif)
                    
                case NotificationType.ReportInNeighborhood.rawValue:
                    viewController?.showFeedDetail(notif)
                    
                default:
                    if let viewController = window?.rootViewController as? SWRevealViewController {
                        if let navigationController = viewController.frontViewController as? UINavigationController {
                            
                            if let menuViewController = navigationController.viewControllers.first as? QlueMenuViewController {
                                
                                if menuViewController.isViewLoaded {
                                    menuViewController.tabTapped(menuViewController.notifButton)
                                }
                                else {
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
                                        menuViewController.tabTapped(menuViewController.notifButton)
                                    })
                                }
                            }
                        }
                    }
                }
                completionHandler(.newData)
            }
            else {
                completionHandler(.noData)
            }
        }
    }
    
    func showMainViewController() {
        
        let viewController = menuStoryboard.instantiateViewController(withIdentifier: "main")
        Util.setRootViewController(viewController)
    }
    
    var viewController: UIViewController? {
        return window?.rootViewController
    }
    
    // MARK: - URL
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if url.scheme == kUrlSchemeFb {
            let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
            return handled
        }
        
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.qlue.Qlue" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "Qlue", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the  saved data."
        
        do {
            
            // Declare Options
            let options = [ NSMigratePersistentStoresAutomaticallyOption : true, NSInferMappingModelAutomaticallyOption : true ]
            
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    
    // MARK: - Tab Bar Appearance
    
    func setTabBarAppearance(){
        let selectedBG = UIImage(named:"selectedTabBarItem.png")?.resizableImage(withCapInsets: UIEdgeInsetsMake(0, 0, 0, 0))
        UITabBar.appearance().selectionIndicatorImage = selectedBG
        
        let appearance = UITabBarItem.appearance()
        let font = UIFont(name: QlueFont.ProximaNovaSoft, size: 10)!
        let attributes: [String : AnyObject] = [NSFontAttributeName: font,NSForegroundColorAttributeName: kInactiveTabBar]
        
        let attributesSelected: [String : AnyObject] = [NSFontAttributeName: font,NSForegroundColorAttributeName: kActiveTabBar]
        
        appearance.setTitleTextAttributes(attributes, for: UIControlState())
        appearance.setTitleTextAttributes(attributesSelected, for: .selected)
        
        appearance.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5)
        
        UITabBar.appearance().backgroundColor = kBgTabBar
        UITabBar.appearance().isOpaque = false
        //        UITabBar.appearance().ba
        
        UITabBar.appearance().alpha = 1
    }
    
    
}


