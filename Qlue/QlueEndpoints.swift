//
//  QlueEndpoints.swift
//  Qlue
//
//  Created by Nurul on 6/1/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation

let BASE_URL = "http://services.qlue.id"

let PRIVACY_URL = "http://services.qluein.org/Drafting_PrivacyPolicy.html"
//let PRIVACY_URL = "http://services.qluein.org/Drafting_PrivacyPolicy_eng.html"

let LOGIN_URL = "/qluein_signin_v3.php"
let FORGOT_PWD_URL = "/api/v2_ios/mobile_forgot_password.php"

// MARK: - Registration
let SIGN_UP_URL = "/mobile_signup_v3.php"//"/api/V2.5/mobile_signup.php"
let REGISTRATION_URL = "/api/V3/mobile_signup.php"
let GET_LIST_COUNTRY_URL = "/api/V3/mobile_neighborhood_list_country.php"
let GET_LIST_CITY_URL = "/api/V3/mobile_neighborhood_list_city.php"
let GET_LIST_KELURAHAN_URL = "/api/V3/mobile_neighborhood_list_kelurahan.php"

let GET_WEATHER_URL = "/api/V2.5/mobile_weather.php"
let GET_PROVINSI = "/mobile_update_provinsi_v2.php"

let FEED_FUSHION_PATH_URL = "/api/V2.5/mobile_feed_fusion.php"
let FEED_URL = "/api/V2.5/mobile_feed.php"
let FEED_DETAIL_URL = "/api/V2.5/mobile_clusterDetail.php"
let FEED_PROGRESS_DETAIL_URL = "/V2/mobile_detail_progress.php"
let FEED_PROGRESS_GET_RATING_URL = "/V2/mobile_review_progress.php"
let FEED_PROGRESS_POST_RATING_URL = "/V2/mobile_post_review_progress.php"
let FEED_BY_CATEGORY = "/api/V2.5/mobile_search_label.php"
let FEED_BY_HASHTAG_URL = "/mobile_search_v4.php"
let SEARCH_URL = "/api/V2.5/mobile_search_all.php"

let POST_LIKE_URL = "/api/V2.5/mobile_post_like.php"
let POST_FAVORITE_URL = "/mobile_post_history.php"

let GET_LIST_PLACE_SWASTA = "/api/V2.5/mobile_listPoi.php"
let GET_CATEGORY_PLACE = "/api/V2.5/mobile_suggest_promosi_placemark.php"
let POST_PLACE_SWASTA = "/mobile_post_placemark.php"

let GET_LIST_TOPIC_SWASTA = "/mobile_suggest_promosi_v3.php"
let GET_LIST_TOPIC_GOVERMENT = "/mobile_suggest1_v3.php"
let POST_REPORT_URL = "/api/V2.5/mobile_post_qlue.php"

// MARK: - Profile
let LIST_REPORT_USER = "/api/V2.5/mobile_showUserPost.php"
let LIST_TROPHY_USER = "/V2/mobile_rank_trophy_user.php"
let LIST_FORUM_USER = "/V2/mobile_forum.php"
let GET_PROFILE_DETAIL = "/api/V2.5/mobile_showUserProfile.php"
let GET_PROFILE_FOLLOWERS = "/mobile_showListUserFollower_v2.php"
let LIST_AVATAR_USER = "/mobile_showItemUser.php"
let LIST_AVATAR_CURRENT_USER = "/mobile_showItem1_v2.php"
let BLOCK_USER_URL = "/V2/mobile_post_user_blocked.php"
let LIST_CHOOSE_REPORT_URL = "/mobile_list_report.php"
let POST_CHOOSE_REPORT_URL = "/mobile_post_report.php"

// MARK: - Kelurahan
let JOIN_KELURAHAN_URL = "/V2/mobile_followKelurahan.php"
let PROFILE_KELURAHAN_URL = "/api/V2.5/mobile_profile_kelurahan.php"
let CHECK_KELURAHAN_RATING_URL = "/V2/mobile_rate_kelurahan.php"
let POST_RATING_KELURAHAN = "/V2/mobile_post_rate_kelurahan.php"
let LIST_MEMBER_KELURAHAN_URL = "/mobile_list_member_kelurahan_v3.php"
let LIST_LAPORAN_KELURAHAN_URL = "/api/V2.5/mobile_myqlueKelurahan.php"
let LAPORAN_GRID_KELURAHAN = "/api/V2.5/mobile_myqlueKelurahanDetail.php"
let LIST_TROPHY_KELURAHAN_URL = "/api/V2.5/mobile_rank_trophy_kelurahan.php"

let FOLLOW_MEMBER_URL = "/mobile_followUser.php"

// MARK: - Forum
let LIST_DISKUSI_KELURAHAN_URL = "/api/V2.5/mobile_collect_forum_kelurahan.php"
let FAVORITE_FORUM_URL = "/V2/mobile_post_fav_forum.php"
let LIST_SEEN_URL = "/mobile_list_seen.php"
let DETAIL_FORUM_URL = "/V2/mobile_detail_wall.php"
let LIST_COMMENT_FORUM = "/V2/mobile_detail_forum_comment.php"
let LIST_FORUM = "/api/V2.5/mobile_collect_forum.php"
let GET_TOPIC_FORUM = "/api/V2.5/mobile_forum_category.php"
let POST_FORUM_URL = "/V2/mobile_post_wall.php"
let POST_VOTE_FORUM = "/V2/mobile_post_vote_forum.php"
let EDIT_FORUM_URL = "/mobile_action_wall_v2.php"

// MARK: - Ranking
let LIST_RANKING_USER_URL = "/V2/mobile_rank_user.php"
let LIST_RANKING_SWASTA_URL = "/V2/mobile_rank_business.php"
let LIST_RANKING_GOV_URL = "/V2/mobile_rank_kelurahan.php"
let LIST_RANKING_GOV_KECAMATAN_URL = "/V2/mobile_rank_kecamatan.php"
let LIST_RANKING_GOV_KOTAMADYA_URL = "/V2/mobile_rank_kota.php"
let LIST_RANKING_GOV_STAFF_URL = "/V2/mobile_rank_staff_dinas.php"
let LIST_RANKING_GOV_DINAS_URL = "/mobile_rank_dinas_v3.php"
let LIST_RANKING_KECAMATAN_BY_KOTAMADYA_URL = "/V2/mobile_rank_kecamatan_by_kota.php"
let LIST_RANKING_KELURAHAN_BY_KECAMATAN_URL = "/V2/mobile_rank_kelurahan_by_kecamatan.php"
let LIST_RANKING_DINAS_DETAIL = "/mobile_rank_dinas_detail.php"
let LIST_RANKING_SWASTA_DETAIL_URL = "/qluein_rank_detail_v2.php"

// MARK: - Comment
let GET_LIST_COMMENT = "/api/V2.5/mobile_clusterDetail_comment.php"
let POST_COMMENT = "/api/V2.5/mobile_post_comment.php"
let POST_FORUM_COMMENT = "/mobile_post_comment_wall_v2.php"
let DELETE_COMMENT = "/mobile_action_comment.php"

// MARK: - Chat
let GET_LIST_CHAT = "/V2/mobile_list_message_group.php"
let GET_CHAT_DETAIL = "/mobile_list_message_group_detail_v2.php"
let CHAT_READ = "/mobile_update_status_message.php"
let POST_CHAT = "/mobile_post_message.php"

// MARK: - Search
let SEARCH_ALL_URL = "/api/V2.5/mobile_search_all.php"
let SEARCH_KELURAHAN_URL = "/api/V2.5/mobile_search_kelurahan.php"
let SEARCH_USERNAME = "/api/V2.5/mobile_search_username.php"
let SEARCH_FORUM_URL = "/api/V2.5/mobile_search_forum.php"
let SEARCH_QLUE_URL = "/api/V2.5/mobile_search_qlue.php"
let SEARCH_POPULAR_URL = "/api/V3/mobile_search_popular_filtered.php"
let SEARCH_LIVE_URL = "/api/V3/mobile_search_live_filtered.php"

// MARK: - Notif
let NOTIFICATION_URL = "/api/V2.5/mobile_showNotif.php"
let READ_NOTIFICATION_URL = "/mobile_readNotif.php"
let REGISTER_NOTIFICATION_URL = "/mobile_send_message.php"
let UNREGISTER_NOTIFICATION_URL = "/mobile_delete_message.php"

// MARK: - Avatar Shop
let AVATAR_LIST_URL = "/api/V2.5/mobile_list_avatar_shop.php"
let CHANGE_AVATAR_URL = "/mobile_changeItem_v2.php"
let BUY_AVATAR_URL = "/V2/mobile_post_avatar_shop.php"

// MARK: - List Favorite
let LIST_FAVORITE_URL = "/mobile_history_v2.php"

// MARK: - Map
let POI_LIST_URL = "/api/V2.5/mobile_qlue_maps.php"
let POI_FILTER_URL = "/api/V2.5/mobile_qlue_maps_filter.php"


