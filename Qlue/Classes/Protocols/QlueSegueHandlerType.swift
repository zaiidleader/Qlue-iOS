//
//  QlueSegueHandlerTypeProtocol.swift
//  Qlue
//
//  Created by Nurul on 5/28/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import UIKit

protocol QlueSegueHandlerType {
    associatedtype SegueIdentifier: RawRepresentable
}

extension QlueSegueHandlerType where Self: UIViewController, SegueIdentifier.RawValue == String{

    func performSegueWithIdentifier(_ segueIdentifier: SegueIdentifier, sender: AnyObject?){
        performSegue(withIdentifier: segueIdentifier.rawValue, sender: sender
        )
    }
    
    func segueIdentifierForSegue(_ segue: UIStoryboardSegue) -> SegueIdentifier{
        
        guard let identifier = segue.identifier, let segueIdentifier = SegueIdentifier(rawValue: identifier) else{
            fatalError("Invalid segue identifier \(segue.identifier)")
        }
        
        return segueIdentifier
        
    }
    
}
