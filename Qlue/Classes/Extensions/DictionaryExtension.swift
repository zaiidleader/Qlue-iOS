//
//  DictionaryExtension.swift
//  Qlue
//
//  Created by Nurul on 7/13/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
extension Dictionary {
    mutating func update(_ other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}
