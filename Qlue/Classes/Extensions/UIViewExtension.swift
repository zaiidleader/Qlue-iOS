//
//  UIViewExtension.swift
//  Qlue
//
//  Created by Nurul on 5/25/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
     public class func fromNib(_ nibNameOrNil: String? = nil) -> Self {
          return fromNib(nibNameOrNil, type: self)
     }
     
     public class func fromNib<T : UIView>(_ nibNameOrNil: String? = nil, type: T.Type) -> T {
          let v: T? = fromNib(nibNameOrNil, type: T.self)
          return v!
     }
     
     public class func fromNib<T : UIView>(_ nibNameOrNil: String? = nil, type: T.Type) -> T? {
          var view: T?
          let name: String
          if let nibName = nibNameOrNil {
               name = nibName
          } else {
               // Most nibs are demangled by practice, if not, just declare string explicitly
               name = nibName
          }
          let nibViews = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
          for v in nibViews! {
               if let tog = v as? T {
                    view = tog
               }
          }
          return view
     }
     
     public class var nibName: String {
          let name = "\(self)".components(separatedBy: ".").first ?? ""
          return name
     }
     public class var nib: UINib? {
          if let _ = Bundle.main.path(forResource: nibName, ofType: "nib") {
               return UINib(nibName: nibName, bundle: nil)
          } else {
               return nil
          }
     }

    func border(_ width: CGFloat, color: UIColor) {
        
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }
     
     func cornerRadius(withRadius rad: CGFloat, andBorderWithColor color: UIColor, borderWidth: CGFloat){
          self.cornerRadius(rad)
          self.border(borderWidth, color: color)
     }

    
    func cornerRadius(_ radius: CGFloat) {
        
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func cornerRadiusWithShadow(){
        self.layer.cornerRadius = 5.0
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 2
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        

    }
    
     func shadowWithColor(_ color: UIColor){
          self.layer.shadowColor = color.cgColor
          self.layer.shadowOpacity = 0.5
          self.layer.shadowRadius = 2
          self.layer.shadowOffset = CGSize(width: 1, height: 2)

     }
     
     
     
    func firstResponder() -> UIView? {
        
        if isFirstResponder {
            return self
        }
        for subview in subviews {
            let responder = subview.firstResponder();
            if responder != nil {
                return responder
            }
        }
        return nil;
    }
    
    func superviewLevel(_ level: UInt) -> UIView {
        
        if level == 0 {
            return self
        }
        
        var view = self
        for _ in 1...level {
            if let sv = view.superview {
                view = sv
            }
            else {
                break
            }
        }
        
        return view
    }


}
