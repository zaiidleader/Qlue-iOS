//
//  UIViewControllerExtension.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

extension UIViewController {
     
     func replaceViewController(_ viewController: UIViewController?, withViewController newViewController: UIViewController, inContainerView containerView: UIView) {
          
          addChildViewController(newViewController)
          newViewController.view.frame = containerView.bounds
          containerView.addSubview(newViewController.view)
          
          newViewController.view.translatesAutoresizingMaskIntoConstraints = false
          containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["view": newViewController.view]))
          containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["view": newViewController.view]))
          
          newViewController.didMove(toParentViewController: self)
          
          viewController?.willMove(toParentViewController: nil)
          viewController?.view.removeFromSuperview()
          viewController?.removeFromParentViewController()
     }
     
     func showFeedDetail(_ notif: QlueNotif) {
          if let notifId = notif.notifId {
               presentDetailFeed(withFeedId: Int(notifId), nav: navigationController)
          }
     }
     
     func showListForum(_ notif: QlueNotif) {
          if let notifId = notif.notifId {
               presentListForum(withType: ForumType.kelurahan(codeKel: notifId), nav: navigationController)
          }
     }
     
     func showListForumGeneral(_ notif: QlueNotif) {
          presentListForum(withType: ForumType.general, nav: navigationController)
     }
     
     func showComment(_ notif: QlueNotif) {
          if let notifId = notif.notifId {
               presentComment(withId: notifId, nav: navigationController)
          }
     }
     
     func showOtherProfile(_ notif: QlueNotif) {
          if let notifId = notif.notifId {
               presentOtherProfile(notifId)
          }
     }
     
     func showChatDetail(_ notif: QlueNotif) {
          if let notifId = notif.notifId, let userId = User.currentUser()?.userId {
               
               let memberDict = [
                    "avatar": "",
                    "follow": false,
                    "user_id": notifId,
                    "username": "",
                    "isBlocked": false,
                    "curr_level": ""
               ] as [String : Any]
               let member = QlueMember.init(fromJson: JSON(memberDict))
               
               presentChatDetail(withGroupId: notifId + "," + userId, isGroup: false, members: [member])
          }
     }
     
     func showForumDetail(_ notif: QlueNotif) {
          if let notifId = notif.notifId {
               presentForumDetail(forumId: notifId, forumType: .general)
          }
     }
}
