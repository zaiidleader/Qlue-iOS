//
//  NSDateExtension.swift
//  Qlue
//
//  Created by Nurul on 6/7/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation

// MARK: - Format View 
extension Date{
     
     func getTimestamp() -> String?{
        
           let  timestamp = NSString.init(format: "%f", Date().timeIntervalSince1970)
          
         return String(timestamp)
     }
    func getDateFromTimestamp(_ string: String?)-> Date?{
    
        if let str = string, let dbl = Double(str){
            return Date(timeIntervalSince1970: dbl)
        }
        return nil
    }
    func formatDateReadable(_ short: Bool = false) -> String{
        let dateFormatter = DateFormatter()
        
        if short{
            
            dateFormatter.dateFormat = "d MMM, yyyy"
        }else{
            
            dateFormatter.dateFormat = "d MMMM, yyyy"
        }
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        
        
        return dateFormatter.string(from: self)
        
    }
    func formatShortStyle() -> String? {
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        
        
        return dateFormatter.string(from: self)
    
    }
     
     func formatDate(withStyle style: DateFormatter.Style) -> String? {
          
          let dateFormatter = DateFormatter()
          dateFormatter.dateStyle = style
          dateFormatter.timeZone = TimeZone.autoupdatingCurrent
          
          
          return dateFormatter.string(from: self)
          
     }
     
     func formatTime(withStyle style: DateFormatter.Style) -> String? {
          
          let dateFormatter = DateFormatter()
          dateFormatter.timeStyle = style
          dateFormatter.timeZone = TimeZone.autoupdatingCurrent
          
          
          return dateFormatter.string(from: self)
          
     }
    func formatToAPI() -> String{
        let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        
        
        return dateFormatter.string(from: self)
    }
    
    func timeAgoWithNumericDates(_ numericDates:Bool) -> String {
        let calendar : Calendar = Calendar.current
        let now: Date = Date()
        let earliest: Date = (now as NSDate).earlierDate(self)
        let latest: Date = (earliest == now) ? self : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second) seconds ago"
        } else {
            return "Just now"
        }
        
    }
     
     func formatChatTime() -> String?{
     let calendar : Calendar = Calendar.current
     let now: Date = Date()
     let earliest: Date = (now as NSDate).earlierDate(self)
     let latest: Date = (earliest == now) ? self : now
     let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.day], from: earliest, to: latest, options: NSCalendar.Options() )
          
          let dateFormatter = DateFormatter()
          dateFormatter.timeZone = TimeZone.autoupdatingCurrent
          

          
          if (components.day! >= 1){
               dateFormatter.dateFormat = "M/d"
               return dateFormatter.string(from: self)
          } else{
               dateFormatter.dateFormat = "hh:mm a"
               return dateFormatter.string(from: self)
          }
     }

}
