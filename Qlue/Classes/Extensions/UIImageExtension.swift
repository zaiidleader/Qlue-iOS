//
//  UIImageExtension.swift
//  Qlue
//
//  Created by Nurul on 6/4/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func imageWithColor(_ color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        
        let context = UIGraphicsGetCurrentContext()! as CGContext
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
        context.clip(to: rect, mask: self.cgImage!)
        color1.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func drawOutlineWithColor(_ color: UIColor) -> UIImage{
    
        let newImageKoef:CGFloat = 1.08
        
        let outlinedImageRect = CGRect(x: 0.0, y: 0.0, width: self.size.width * newImageKoef, height: self.size.height * newImageKoef)
        
        let imageRect = CGRect(x: self.size.width * (newImageKoef - 1) * 0.5, y: self.size.height * (newImageKoef - 1) * 0.5, width: self.size.width, height: self.size.height)
        
        UIGraphicsBeginImageContextWithOptions(outlinedImageRect.size, false, newImageKoef)
        
        self.draw(in: outlinedImageRect)
        
        let context = UIGraphicsGetCurrentContext()
        context?.setBlendMode(CGBlendMode.sourceIn)
        
        context?.setFillColor(color.cgColor)
        context?.fill(outlinedImageRect)
        self.draw(in: imageRect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
     
     func imageWithWidth(_ width: CGFloat) -> UIImage {
          
          let scale = width / size.width
          let height = size.height * scale
          UIGraphicsBeginImageContext(CGSize(width: width, height: height))
          draw(in: CGRect(x: 0, y: 0, width: width, height: height))
          let image = UIGraphicsGetImageFromCurrentImageContext()
          UIGraphicsEndImageContext()
          
          return image!
     }
     
     func cropCenterAndResize(to size: CGSize) -> UIImage? {
          guard let cgimage = self.cgImage else { return nil }
          
          let contextImage: UIImage = UIImage(cgImage: cgimage)
          
          let contextSize: CGSize = contextImage.size
          
          //Set to square
          var posX: CGFloat = 0.0
          var posY: CGFloat = 0.0
          let cropAspect: CGFloat = size.width / size.height
          
          var cropWidth: CGFloat = size.width
          var cropHeight: CGFloat = size.height
          
          if size.width > size.height { //Landscape
               cropWidth = contextSize.width
               cropHeight = contextSize.width / cropAspect
               posY = (contextSize.height - cropHeight) / 2
          } else if size.width < size.height { //Portrait
               cropHeight = contextSize.height
               cropWidth = contextSize.height * cropAspect
               posX = (contextSize.width - cropWidth) / 2
          } else { //Square
               if contextSize.width >= contextSize.height { //Square on landscape (or square)
                    cropHeight = contextSize.height
                    cropWidth = contextSize.height * cropAspect
                    posX = (contextSize.width - cropWidth) / 2
               }else{ //Square on portrait
                    cropWidth = contextSize.width
                    cropHeight = contextSize.width / cropAspect
                    posY = (contextSize.height - cropHeight) / 2
               }
          }
          
          // Create bitmap image from context using the rect
          if let cgImage = contextImage.cgImage {
               
               let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)
               let imageRef: CGImage = cgImage.cropping(to: rect)!
               
               // Create a new image based on the imageRef and rotate back to the original orientation
               let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
               
               UIGraphicsBeginImageContextWithOptions(size, true, self.scale)
               cropped.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
               
               if let resized = UIGraphicsGetImageFromCurrentImageContext() {
                    UIGraphicsEndImageContext()
                    return resized
               }
          }
          
          return nil
     }
     
     func fixImageOrientation() -> UIImage {
          
          if self.imageOrientation == UIImageOrientation.up {
               return self
          }
          
          var transform: CGAffineTransform = CGAffineTransform.identity
          
          switch self.imageOrientation {
          case UIImageOrientation.down, UIImageOrientation.downMirrored:
               transform = transform.translatedBy(x: self.size.width, y: self.size.height)
               transform = transform.rotated(by: CGFloat(M_PI))
               break
          case UIImageOrientation.left, UIImageOrientation.leftMirrored:
               transform = transform.translatedBy(x: self.size.width, y: 0)
               transform = transform.rotated(by: CGFloat(M_PI_2))
               break
          case UIImageOrientation.right, UIImageOrientation.rightMirrored:
               transform = transform.translatedBy(x: 0, y: self.size.height)
               transform = transform.rotated(by: CGFloat(-M_PI_2))
               break
          case UIImageOrientation.up, UIImageOrientation.upMirrored:
               break
          }
          
          switch self.imageOrientation {
          case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
               transform.translatedBy(x: self.size.width, y: 0)
               transform.scaledBy(x: -1, y: 1)
               break
          case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
               transform.translatedBy(x: self.size.height, y: 0)
               transform.scaledBy(x: -1, y: 1)
          case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
               break
          }
          
          let ctx:CGContext = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: self.cgImage!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
          
          ctx.concatenate(transform)
          
          switch self.imageOrientation {
          case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
               ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: self.size.height, height: self.size.width))
               break
          default:
               ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
               break
          }
          
          let cgimg: CGImage = ctx.makeImage()!
          let img: UIImage = UIImage(cgImage: cgimg)
          
          return img
     }
}
