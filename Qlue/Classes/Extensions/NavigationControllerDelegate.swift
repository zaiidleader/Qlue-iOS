//
//  NavigationControllerDelegate.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 1/1/17.
//  Copyright © 2017 Qlue. All rights reserved.
//

import UIKit

class NavigationControllerDelegate: NSObject, UINavigationControllerDelegate {
     
     func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
          
          if (fromVC is QlueMenuViewController && toVC is QlueFABMenuController) || (fromVC is QlueFABMenuController && toVC is QlueMenuViewController) {
               return CircleTransitionAnimator()
          }
          return nil
     }
}

class CircleTransitionAnimator: NSObject, CAAnimationDelegate, UIViewControllerAnimatedTransitioning {
     
     weak var transitionContext: UIViewControllerContextTransitioning?
     
     func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
          return 0.25
     }
     
     func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
          
          //1
          self.transitionContext = transitionContext
          
          //2
          let containerView = transitionContext.containerView
          let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
          let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
          
          var button: UIButton!
          if let fromViewController = fromViewController as? QlueMenuViewController {
               button = fromViewController.fabButton
          }
          else if let fromViewController = fromViewController as? QlueFABMenuController {
               button = fromViewController.selectedButton
          }
          
          //3
          containerView.addSubview(toViewController.view)
          
          //4
          let circleMaskPathInitial = UIBezierPath(ovalIn: button.frame)
          let extremePoint = CGPoint(x: button.center.x - 0, y: button.center.y + toViewController.view.bounds.height)
          let radius = sqrt((extremePoint.x*extremePoint.x) + (extremePoint.y*extremePoint.y))
          let circleMaskPathFinal = UIBezierPath(ovalIn: button.frame.insetBy(dx: -radius, dy: -radius))
          
          //5
          let maskLayer = CAShapeLayer()
          maskLayer.path = circleMaskPathFinal.cgPath
          toViewController.view.layer.mask = maskLayer
          
          //6
          let maskLayerAnimation = CABasicAnimation(keyPath: "path")
          maskLayerAnimation.fromValue = circleMaskPathInitial.cgPath
          maskLayerAnimation.toValue = circleMaskPathFinal.cgPath
          maskLayerAnimation.duration = self.transitionDuration(using: transitionContext)
          // FIXME: - to Swift 3
          maskLayerAnimation.delegate = self
          maskLayer.add(maskLayerAnimation, forKey: "path")
     }
     
     // FIXME: - to Swift 3
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        self.transitionContext?.completeTransition(!self.transitionContext!.transitionWasCancelled)
        self.transitionContext?.viewController(forKey: UITransitionContextViewControllerKey.from)?.view.layer.mask = nil
    }
}
