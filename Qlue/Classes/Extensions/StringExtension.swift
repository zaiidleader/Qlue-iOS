//
//  String+HeightCalculate.swift
//  Qlue
//
//  Created by Nurul on 6/4/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation

import UIKit

extension String{
    var md5: String {
        
        var digest = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
        if let data = self.data(using: String.Encoding.utf8) {
          data.withUnsafeBytes { bytes in
               CC_MD5(bytes, CC_LONG(data.count), &digest)
          }
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }
    
     func convertEmojiToUnicode() -> String{
          var message = self
          if let data = self.data(using: String.Encoding.nonLossyASCII)
               ,let goodValue = String.init(data: data, encoding: String.Encoding.utf8)
          {
               message = goodValue.replacingOccurrences(of: "\\", with: "\\\\")
               
               
          }
          if let msg = message.addingPercentEscapes(using: String.Encoding.utf8){
               message = msg
          }
          return message
     }
     
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
    
    func encodeURL() -> URL?{
        
        if let formatString: String = self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed){
            
            if let url = URL(string: formatString){
                
                return url
                
            }
        }
        
        return nil
    }
     
     func getLabelTopic() -> String{
          var label = ""
          
          if let rangeOfCat = self.range(of: "category/",
                                                  options: NSString.CompareOptions.backwards) {
               let suffix = String(self.characters.suffix(from: rangeOfCat.upperBound))
               if let rangeOfPng = suffix.range(of: ".png",
                                                        options: NSString.CompareOptions.backwards) {
                    label = String(suffix.characters.prefix(upTo: rangeOfPng.lowerBound))
               }
          }
          
          return label

     }
}

extension NSAttributedString{
    func heightWithConstrainedWidth(_ width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
}
