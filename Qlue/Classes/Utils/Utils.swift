//
//  Utils.swift
//  Qlue
//
//  Created by Nurul on 6/2/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import AVFoundation

class Util {
     
     static func textfieldPlaceholderWithColor(_ color: UIColor, font: UIFont?, textfield: UITextField){
          if let pc = textfield.placeholder, let font = font{
               textfield.attributedPlaceholder = NSAttributedString(string: pc, attributes: [NSFontAttributeName : font,
                    NSForegroundColorAttributeName: color])
          }
     }
     
     static func thumbnailImageFromVideoURL(_ videoUrl: URL) -> UIImage?{
     
          let asset = AVURLAsset.init(url: videoUrl)
          let generator = AVAssetImageGenerator.init(asset: asset)
          
          let requestedImage = CMTime.init(value: 1, timescale: 60)
          do {
          let imgRef = try generator.copyCGImage(at: requestedImage, actualTime: nil)
               
               let thumbnailImage = UIImage.init(cgImage: imgRef)
               return thumbnailImage
          } catch let error as NSError{
               print("error: \(error.localizedDescription)")
          }
          
          return nil
     }
     
    static func showSimpleAlert(_ text: String?, vc: UIViewController, completion: ((_ action: UIAlertAction) -> Void)?){
        
        let alert = UIAlertController(title: "", message: text, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: completion)
        
        alert.addAction(okAction)
        vc.present(alert, animated: true, completion: nil)
    
    }
     
     static func showSimpleCancelAlert(_ text: String?, vc: UIViewController, completion: ((_ action: UIAlertAction) -> Void)?){
          
          let alert = UIAlertController(title: "", message: text, preferredStyle: UIAlertControllerStyle.alert)
          
          let okAction = UIAlertAction(title: "OK", style: .default, handler: completion)
          
          alert.addAction(okAction)
          
          let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
          alert.addAction(cancelAction)
          
          vc.present(alert, animated: true, completion: nil)
          
     }
    
     
     
    static func getCommentAttributedString(_ username: String?, comment: String?) -> NSMutableAttributedString{
        let text = NSMutableAttributedString.init()
        
        if let username = username{
            let title = NSAttributedString.init(string: username + " : ", attributes: [
                NSFontAttributeName : UIFont.init(name: QlueFont.ProximaNovaSoft, size: 14)!,
               NSForegroundColorAttributeName : UIColor.darkGray
                
                ])
            text.append(title)
        }
        
//        if let comment = comment/*,
//            let commentData = comment.dataUsingEncoding(NSUTF8StringEncoding),
//            let commentEncode = String.init(data: commentData, encoding: NSNonLossyASCIIStringEncoding)*/{
//        
//            let commentTxt = NSMutableAttributedString.init(string: Util.convertEmojiFromUnicode(<#T##str: String?##String?#>), attributes: [
//                NSFontAttributeName: UIFont.init(name: QlueFont.HelveticaNeue, size: 13)!
//                ])
//            
////            let commentStr = commentEncode as NSString
//            
////                let words = commentStr.componentsSeparatedByString("")
////                for word in words{
////                    if word.hasPrefix("@"){
////                        let range = commentStr.rangeOfString(word)
////                        commentTxt.addAttributes([
////                            NSForegroundColorAttributeName : kQlueBlue
////                            ], range: range )
////                    }
////                }
//            
//            
//            text.appendAttributedString(commentTxt)
//        }
     let commentTxt = NSMutableAttributedString.init(string: Util.convertEmojiFromUnicode(comment), attributes: [
          NSFontAttributeName: UIFont.init(name: QlueFont.HelveticaNeue, size: 13)!,
          NSForegroundColorAttributeName : UIColor.darkGray
          ])
     text.append(commentTxt)
        
        return text

    }

    static func getHeightCommentInFeedList(_ text: NSMutableAttributedString) -> CGFloat{
        let commentText = text.heightWithConstrainedWidth(UIScreen.main.bounds.width - 82)
        return commentText + 16 + 45

    }
    
    static func setRootViewController(_ viewController: UIViewController) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        UIView.transition(with: appDelegate.window!, duration: 0.3, options: [.transitionCrossDissolve, .allowAnimatedContent], animations: { () -> Void in
            
            let oldState = UIView.areAnimationsEnabled
            
            UIView.setAnimationsEnabled(false)
            appDelegate.window?.rootViewController = viewController
            UIView.setAnimationsEnabled(oldState)
            
            }, completion: nil)
    }
     
     static func validateEmail(_ candidate: String?) -> Bool {
          let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
          return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
     }
     
     static func convertToEmoji(_ str: NSString) -> NSString{
     
          var unicodeString: NSString!
          
          let charCode = str.substring(from: 2)
          var unicodeInt: UInt32 = 0
          
          //convert unicode character to int
          (Scanner.localizedScanner(with: charCode) as AnyObject).scanHexInt32(&unicodeInt)
          
          //convert this integer to a char array (bytes)
          var chars: [UInt32]!
          let len: Int = 4
          
          chars[0] = (unicodeInt >> 24) & (1 << 24) - 1
          chars[1] = (unicodeInt >> 16) & (1 << 16) - 1
          chars[2] = (unicodeInt >> 8) & (1 << 8) - 1
          chars[3] = unicodeInt & (1 << 8) - 1
          
          unicodeString = NSString.init(bytes: chars,
                                        length: len,
                                        encoding: String.Encoding.utf32.rawValue)
          
          return unicodeString
          
     }
     
     static func convertEmojiFromUnicode(_ str: String? ) -> String{
          
          var completeString: String = ""
          
          let string  = str ?? ""
          let separateBySpace = string.components(separatedBy: " ")
          for strSpace in separateBySpace{
               
               let separateByDash = strSpace.components(separatedBy: "\\")
               if separateByDash.count > 1{
                    var completeDash: String = ""
                    for strDash in separateByDash{
                         if (strDash.hasPrefix("u1"))  && (strDash.characters.count > 5){
                              
                              let getStringU = String(strDash.characters.prefix(6))
                              let getStringNum = String(getStringU.characters.dropFirst(1))
                              
                              if let charCode = UInt32(getStringNum, radix: 16) {
                                   // Create string from Unicode code point:
                                   let str = String(describing: UnicodeScalar(charCode))
                                   completeDash.append(str)
                              } else {
                                   print("invalid input")
                              }
                              
                              let restStr = String(strDash.characters.dropFirst(6))
                              completeDash.append(restStr)
                         }
                         else if (strDash.hasPrefix("u2"))  && (strDash.characters.count > 4){
                              let getStringU = String(strDash.characters.prefix(5))
                              let getStringNum = String(getStringU.characters.dropFirst(1))
                              
                              if let charCode = UInt32(getStringNum, radix: 16) {
                                   // Create string from Unicode code point:
                                   let str = String(describing: UnicodeScalar(charCode))
                                   completeDash.append(str)
                              } else {
                                   print("invalid input")
                              }
                              
                              let restStr = String(strDash.characters.dropFirst(5))
                              completeDash.append(restStr)
                         }
                         else{
                              completeDash.append(strDash)
                         }
                    }
                    
                    completeString.append(completeDash + " ")
               } else {
                    completeString.append(strSpace + " ")
               }
               
               
          }
          
          return completeString
          
     }
     
     static func replaceIdWithUsername(_ comment: QlueComment) -> String{
          
          let ats : [QlueCommentUser] = comment.at
          
          let string  = comment.comment ?? ""
          let separated = string.components(separatedBy: CharacterSet.init(charactersIn: "@ "))
          var dinamic = separated
          
          var i = 0
          
          for str in separated{
               for at in ats{
                    if let atUsername = at.username, str == at.idno{
                         let username = "@" + atUsername
                         dinamic[i] = username
                         
                    }
               }
               
               i += 1
          }
          
          return dinamic.joined(separator: " ")
          
     }

     
     static func setStatusBarBackgroundColor(_ color: UIColor) {
          
          guard  let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView else {
               return
          }
          
          statusBar.backgroundColor = color
     }
}
