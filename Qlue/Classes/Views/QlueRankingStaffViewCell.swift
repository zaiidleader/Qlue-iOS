//
//  QlueRankingStaffViewCell.swift
//  Qlue
//
//  Created by Nurul on 8/15/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueRankingStaffViewCell: UITableViewCell {

    @IBOutlet weak var cellRankView: UIView!
    @IBOutlet weak var cellRankLabel: UILabel!
    @IBOutlet weak var cellDescription: UILabel!
     @IBOutlet weak var cellLabel: UILabel!
     @IBOutlet weak var cellPoint: UILabel!
     @IBOutlet weak var cellImage: UIImageView!
     @IBOutlet weak var cellTrophy: UIImageView!
     
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
