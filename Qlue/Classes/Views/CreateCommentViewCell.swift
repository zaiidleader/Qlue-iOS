//
//  CreateCommentViewCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol CreateCommentViewCellDelegate{
    func createTapped(_ cell: CreateCommentViewCell)
}

class CreateCommentViewCell: UITableViewCell {

    var delegate: CreateCommentViewCellDelegate?
    
    @IBOutlet weak var createButton: UIButton!{
        didSet{
            createButton.cornerRadius(20)
            createButton.border(1, color: kQlueBlue)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Action

    @IBAction func createTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.createTapped(self)
        }
    }
}
