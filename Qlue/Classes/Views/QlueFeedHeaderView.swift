//
//  QlueFeedHeaderView.swift
//  Qlue
//
//  Created by Nurul on 6/1/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueFeedHeaderViewDelegate {
    func seenByTapped(_ view: QlueFeedHeaderView)
    func videoPlayTapped(_ view: QlueFeedHeaderView, button: UIButton)
    func fullscreenVideoTapped(_ view: QlueFeedHeaderView)
    func likeTapped(_ view: QlueFeedHeaderView)
    func mainImageTapped(_ view: QlueFeedHeaderView)
    func favoriteTapped(_ view: QlueFeedHeaderView)
    func kelurahanTapped(_ view: QlueFeedHeaderView)
    func commentTapped(_ view: QlueFeedHeaderView)
    func lokasiTapped(_ view: QlueFeedHeaderView)


}

class QlueFeedHeaderView: UIView {

    var delegate : QlueFeedHeaderViewDelegate?
    
    // MARK: - Outlets
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellImageButton: UIButton!
    @IBOutlet weak var cellKelurahan: UILabel!
    @IBOutlet weak var cellStarButton: UIButton!{
        didSet{
            cellStarButton.cornerRadius(cellStarButton.bounds.width / 2)
           
        }
    }
    @IBOutlet weak var cellCategoryImage: UIImageView!
    @IBOutlet weak var cellPlaceBgButton: UIButton!{
        didSet{
            cellPlaceBgButton.cornerRadius(5)
        }
    }
    @IBOutlet weak var cellSeenBy: UIButton!{
        didSet{
            cellSeenBy.cornerRadius(5)
        }
    }
     @IBOutlet weak var cellLikeButton: UIButton!
     
     var isLike: Bool = true
     var buttonLiked: Bool = false{
          didSet{
               if buttonLiked == true{
                    if isLike == true{
                         cellLikeButton.setImage(UIImage.init(named: "ic_like_new")?.imageWithColor(kDarkColor), for: UIControlState())
                    } else{
                         cellLikeButton.setImage(UIImage.init(named: "ic_support")?.imageWithColor(kDarkColor), for: UIControlState())
                    }
               } else{
                    if isLike == true{
                         cellLikeButton.setImage(UIImage.init(named: "ic_like_new")?.imageWithColor(kQlueBlue), for: UIControlState())
                    } else{
                         cellLikeButton.setImage(UIImage.init(named: "ic_support")?.imageWithColor(kQlueBlue), for: UIControlState())
                    }
               }
          }
     }
    @IBOutlet weak var cellLikeLabel: UILabel!
    
    @IBOutlet weak var cellLocationButton: UIButton!{
        didSet{
            cellLocationButton.setImage(UIImage.init(named: "ic_place_new")?.imageWithColor(kQlueBlue), for: UIControlState())
        }
    }
    @IBOutlet weak var cellCommentButton: UIButton!{
        didSet{
            cellCommentButton.setImage(UIImage.init(named: "ic_comment_new")?.imageWithColor(kQlueBlue), for: UIControlState())
        }
    }
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var fullscreenVideoButton: UIButton!
    
    override func awakeFromNib() {
        fullscreenVideoButton.isHidden = true
    }
    
    // MARK: - Action
    @IBAction func likeButtonTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.likeTapped(self)
        }
    }
    @IBAction func seenByTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.seenByTapped(self)
        }
    }
    @IBAction func videoPlayTapped(_ sender: UIButton) {
        
        if let delegate = delegate{
            delegate.videoPlayTapped(self, button: sender)
        }
    }
    @IBAction func fullscreenVideoTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.fullscreenVideoTapped(self)
        }
    }
    @IBAction func imageTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.mainImageTapped(self)
        }
    }
    @IBAction func favoriteButtonTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.favoriteTapped(self)
        }

    }

    @IBAction func kelurahanTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.kelurahanTapped(self)
        }

    }
    @IBAction func commentTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.commentTapped(self)
        }
    }
    @IBAction func lokasiTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.lokasiTapped(self)
        }
    }
}
