
//
//  QlueFabView.swift
//  Qlue
//
//  Created by Nurul on 7/3/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueFabView: UIView {
     
     
     @IBOutlet weak var fabWrapView: UIView!
     @IBOutlet weak var cityProblemsButton: UIButton!
     var cityActivated: Bool!{
          didSet{
               
               cityProblemsButton.cornerRadius(cityProblemsButton.bounds.width / 2)
               
               if cityActivated == true{
                    cityProblemsButton.backgroundColor = UIColor(red: 0.153, green: 0.196, blue: 0.216, alpha: 0.5)
               } else{
                    cityProblemsButton.backgroundColor = UIColor.clear
               }
               
          }
     }
     @IBOutlet weak var companyButton: UIButton!
     var companyActivated: Bool!{
          didSet{
               
               companyButton.cornerRadius(companyButton.bounds.width / 2)
               
               if companyActivated == true{
                    companyButton.backgroundColor = UIColor(red: 0.153, green: 0.196, blue: 0.216, alpha: 0.5)
               } else{
                    companyButton.backgroundColor = UIColor.clear
               }
               
          }
     }
     
     @IBOutlet weak var qluebi: UIImageView!
     
     var animator: UIDynamicAnimator!
     
     var parentController: QlueViewController!
     
     override func awakeFromNib() {
          
          
          
          
          qluebi.frame = self.frame
          qluebi.isUserInteractionEnabled = true
          let pan = UIPanGestureRecognizer.init(target: self, action: #selector(QlueFabView.panAction(_:)))
          pan.cancelsTouchesInView = false
          qluebi.addGestureRecognizer(pan)
          
          
          //        fabWrapView.hidden = true
          fabWrapView.alpha = 0
          animator = UIDynamicAnimator(referenceView: self)
          
     }
     
     override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
          for subview in subviews as [UIView] {
               if !subview.isHidden && subview.alpha > 0 && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                    return true
               }
          }
          return false
     }
     
     
     // MARK: - Private Func
     func hideWrapView(_ state: Bool){
          
          var alpha: CGFloat = 0
          if state == true {alpha = 1}
          
          fabWrapView.alpha = alpha
          
          cityProblemsButton.transform = CGAffineTransform(scaleX: 0, y: 0)
          companyButton.transform = CGAffineTransform(scaleX: 0, y: 0)
          
          UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseIn, animations: {
               
               var curAlpha: CGFloat = 1
               if state == true {curAlpha = 0}
               self.fabWrapView.alpha = curAlpha
               //self.parentController.tabBarController?.tabBar.hidden = !state
               //self.parentController.navigationController?.navigationBarHidden = !state
               
               }, completion: { (finished) in
                    
                    UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseIn, animations: {
                         self.cityProblemsButton.transform = CGAffineTransform(scaleX: 1, y: 1)
                         self.companyButton.transform = CGAffineTransform(scaleX: 1, y: 1)
                         }, completion: nil)
                    //                self.fabWrapView.hidden = state
          })
     }
     
     fileprivate func activateButtonWithQluebie(_ qFrame: CGRect, bFrame: CGRect) -> Bool{
          let qOrigin = qFrame.origin
          
          let minButtonX = bFrame.origin.x - qFrame.size.width + 15
          let maxButtonX = bFrame.origin.x + qFrame.size.width - 15
          
          let minButtonY = bFrame.origin.y - qFrame.size.height + 15
          let maxButtonY = bFrame.origin.y + qFrame.size.height - 15
          
          return  (qOrigin.x > minButtonX && qOrigin.x < maxButtonX)
               && (qOrigin.y > minButtonY && qOrigin.y < maxButtonY)
     }
     
     // MARK: - Helper
     fileprivate func presentCamera(){
          Engine.shared.currentReport = QlueReport()
          Engine.shared.currentReport?.qlueType = .goverment
          
          parentController.presentCameraView(parentController.navigationController, completion:  {
               
               self.hideWrapView(true)
               self.animator.removeAllBehaviors()
               self.setNeedsLayout()
          })
     }
     fileprivate func presentSearchPlace(){
          Engine.shared.currentReport = QlueReport()
          Engine.shared.currentReport?.qlueType = .business
          parentController.presentReportSwastaSearchPlace(parentController.navigationController, completion: {
               
               
               
               self.hideWrapView(true)
               self.animator.removeAllBehaviors()
               self.setNeedsLayout()
          })
     }
     
     // MARK: - Action
     @IBAction func cityProblemsTapped(_ sender: UIButton) {
          presentCamera()
     }
     
     @IBAction func companyTapped(_ sender: UIButton) {
          presentSearchPlace()
     }
     
     @IBAction func closeTapped(_ sender: UIButton) {
          hideWrapView(true)
          animator.removeAllBehaviors()
          self.setNeedsLayout()
     }
     func panAction(_ sender: UIPanGestureRecognizer) {
          
          switch sender.state {
          case .began:
               hideWrapView(false)
          case .changed:
               if let view = sender.view {
                    
                    let translation = sender.translation(in: view)
                    
                    companyActivated = false
                    cityActivated = false
                    
                    if activateButtonWithQluebie(view.frame, bFrame: cityProblemsButton.frame){
                         cityActivated = true
                    } else if activateButtonWithQluebie(view.frame, bFrame: companyButton.frame){
                         companyActivated = true
                    }
                    
                    view.center = CGPoint(x: view.center.x + translation.x, y: view.center.y + translation.y)
                    view.layoutIfNeeded()
                    
                    sender.setTranslation(CGPoint.zero, in: view)
               }
               
          case .ended:
               
               if cityActivated == true{
                    self.presentCamera()
               } else if companyActivated == true{
                    self.presentSearchPlace()
               } else{
                    
                    animator.removeAllBehaviors()
                    
                    let gravity = UIGravityBehavior(items: [qluebi])
                    gravity.gravityDirection = CGVector(dx: 0, dy: 5)
                    animator.addBehavior(gravity)
               }
          default:
               return
          }
          
          
     }
     
     
}

extension QlueViewController{
     func showFabView(_ parentController: QlueViewController) -> QlueFabView{
          let fabView = Bundle.main.loadNibNamed("QlueFabView", owner: nil, options: nil)?.first as! QlueFabView
          
          let hotspotBarHeight = UIApplication.shared.statusBarFrame.size.height - 20
          
          fabView.frame = CGRect(origin: CGPoint(x: 0, y: -1 * hotspotBarHeight ), size: UIScreen.main.bounds.size)
          fabView.parentController = parentController
          
          view.addSubview(fabView)
          
          return fabView
     }
}
