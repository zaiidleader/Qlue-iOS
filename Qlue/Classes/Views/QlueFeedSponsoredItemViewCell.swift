//
//  QlueFeedSponsoredItemViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueFeedSponsoredItemViewCell: UICollectionViewCell {

    @IBOutlet weak var cellImage: UIImageView!{
        didSet{
            cellImage.cornerRadius(5)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
