//
//  RadioButton.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/20/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import UIKit

class RadioButton : UIButton {
     
     var myAlternateButton:Array<RadioButton>?
     
     var backgroundColorSelected: UIColor?
     var backgroundColorNormal: UIColor?
     
     // for gender registration
     var buttonLabel: UILabel?
     var buttonLabelColorNormal: UIColor?
     
     var downStateImage:String? = "ic_radio_button_checked.png"{
          
          didSet{
               
               if downStateImage != nil {
                    
                    self.setImage(UIImage(named: downStateImage!), for: UIControlState.selected)
               }
          }
     }
     
     required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          self.setImage(UIImage(named: downStateImage!), for: UIControlState.selected)
     }
     
     func unselectAlternateButtons(){
          
          if myAlternateButton != nil {
               
               self.isSelected = true
               if let color = backgroundColorSelected{
                    self.backgroundColor = color
                    self.buttonLabel?.textColor = color
               }
               
               for aButton:RadioButton in myAlternateButton! {
                    
                    aButton.isSelected = false
                    if let color = backgroundColorNormal{
                         aButton.backgroundColor = color
                         aButton.buttonLabel?.textColor = buttonLabelColorNormal
                         
                    }
               }
               
          }else{
               
               toggleButton()
          }
     }
     
     override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
          unselectAlternateButtons()
          super.touchesBegan(touches, with: event)
     }
     
     func toggleButton(){
          
          if self.isSelected==false{
               
               self.isSelected = true
               if let color = backgroundColorSelected{
                    self.backgroundColor = color
                    self.buttonLabel?.textColor = color
               }
          }else {
               
               self.isSelected = false
               if let color = backgroundColorNormal{
                    self.backgroundColor = color
                    self.buttonLabel?.textColor = buttonLabelColorNormal
               }
          }
     }
}
