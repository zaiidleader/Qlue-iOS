//
//  QlueKLDetailViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueKLDetailViewCell: UICollectionViewCell {

    @IBOutlet weak var cellImageFeed: UIImageView!
    @IBOutlet weak var cellImage: UIImageView!{
        didSet{
            cellImage.cornerRadius(cellImage.bounds.width / 2)
            cellImage.border(2, color: UIColor.white)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
