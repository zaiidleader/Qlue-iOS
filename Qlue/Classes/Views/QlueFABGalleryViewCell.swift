//
//  QlueFABGalleryViewCell.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 12/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueFABGalleryViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 4
        layer.masksToBounds = true
        
        numberLabel.layer
        
        let path = UIBezierPath(roundedRect:numberLabel.bounds, byRoundingCorners:[.bottomLeft], cornerRadii: CGSize(width: 4, height: 4))
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        numberLabel.layer.mask = maskLayer
    }
}
