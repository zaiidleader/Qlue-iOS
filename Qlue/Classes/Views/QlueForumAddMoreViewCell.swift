//
//  QlueForumAddMoreViewCell.swift
//  Qlue
//
//  Created by Andi on 8/20/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueForumAddMoreViewCellDelegate {
    func addMoreTapped(_ cell: QlueForumAddMoreViewCell)
}

class QlueForumAddMoreViewCell: UITableViewCell {

    var delegate: QlueForumAddMoreViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Action
    @IBAction func addMoreButton(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.addMoreTapped(self)
        }
    }
    
}
