//
//  CommentHeaderViewCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol CommentHeaderViewCellDelegate {
     func sortTapped(_ cell: CommentHeaderViewCell, button: UIButton)
}

class CommentHeaderViewCell: UITableViewHeaderFooterView {

    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var sortIndicator: UIActivityIndicatorView!
     var delegate: CommentHeaderViewCellDelegate?
     @IBOutlet weak var cellTitle: UILabel!
     
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

     @IBAction func sortTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.sortTapped(self, button: sender)
          }
     }
}
