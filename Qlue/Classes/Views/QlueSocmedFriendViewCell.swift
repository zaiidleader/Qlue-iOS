//
//  QlueSocmedFriendViewCell.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 12/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueSocmedFriendViewCellDelegate {
    func socmedFriendViewCellInviteButtonTapped(_ cell: QlueSocmedFriendViewCell)
}

class QlueSocmedFriendViewCell: UITableViewCell {
    var delegate: QlueSocmedFriendViewCellDelegate?
    
    @IBOutlet weak var friendImageView: UIImageView!
    @IBOutlet weak var friendNameLabel: UILabel!
    @IBOutlet weak var inviteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        friendImageView.layer.cornerRadius = 4.0
        friendImageView.layer.masksToBounds = true
        
        inviteButton.layer.cornerRadius = 4.0
        inviteButton.layer.masksToBounds = true
        inviteButton.layer.borderColor = kQlueBlue.cgColor
        inviteButton.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func inviteButtonTapped(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.socmedFriendViewCellInviteButtonTapped(self)
        }
    }
}
