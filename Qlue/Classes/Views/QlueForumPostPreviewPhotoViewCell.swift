//
//  QlueForumPostPreviewPhotoViewCell.swift
//  Qlue
//
//  Created by Andi on 8/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueForumPostPreviewPhotoViewCellDelegate {
    func cancelPhotoTapped(_ cell: QlueForumPostPreviewPhotoViewCell)
}

class QlueForumPostPreviewPhotoViewCell: UICollectionViewCell {

    @IBOutlet weak var cellPhoto: UIImageView!
     var delegate: QlueForumPostPreviewPhotoViewCellDelegate?
     
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Action
    
    @IBAction func cancelTapped(_ sender: UIButton) {
     if let delegate = delegate{
          delegate.cancelPhotoTapped(self)
     }
    }

}
