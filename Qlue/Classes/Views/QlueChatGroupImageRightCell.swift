//
//  QlueChatGroupImageRightCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueChatGroupImageRightCell: UITableViewCell {
     
     @IBOutlet weak var cellUsername: UILabel!
     @IBOutlet weak var cellTime: UILabel!
     @IBOutlet weak var cellImage: UIImageView!{
          didSet{
               cellImage.cornerRadius(10)
          }
     }
     @IBOutlet weak var cellImageWrapper: UIView!{
          didSet{
               cellImageWrapper.cornerRadius(10)
          }
     }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
