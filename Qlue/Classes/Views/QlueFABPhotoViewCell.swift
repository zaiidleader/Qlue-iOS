//
//  QlueFABPhotoViewCell.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 12/22/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueFABPhotoViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var moreLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 4
        layer.masksToBounds = true
    }
}
