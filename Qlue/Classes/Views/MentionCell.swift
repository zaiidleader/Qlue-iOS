//
//  MentionCell.swift
//  cobaemoji
//
//  Created by Nurul Rachmawati on 8/27/16.
//  Copyright © 2016 DyCode. All rights reserved.
//

import UIKit

class MentionCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
