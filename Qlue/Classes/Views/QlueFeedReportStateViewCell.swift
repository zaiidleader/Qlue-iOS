//
//  QlueFeedReportStateViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueFeedReportStateViewCellDelegate{
    func processTapped(_ cell: QlueFeedReportStateViewCell)
    func doneTapped(_ cell: QlueFeedReportStateViewCell)
}

class QlueFeedReportStateViewCell: UITableViewCell {

    var delegate: QlueFeedReportStateViewCellDelegate?
    
    @IBOutlet weak var cellUpButton: UIButton!
    @IBOutlet weak var cellWaitingButton: UIButton!
    @IBOutlet weak var cellProgressButton: UIButton!
    @IBOutlet weak var cellDoneButton: UIButton!
    
    var roundedButton: [UIButton]!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        roundedButton = [cellUpButton, cellWaitingButton, cellProgressButton, cellDoneButton]

        for button in roundedButton{
            button.cornerRadius(5)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func processTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.processTapped(self)
        }
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.doneTapped(self)
        }
    }
}
