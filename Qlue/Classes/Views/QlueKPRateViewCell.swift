//
//  QlueKPRateViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/8/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import GoogleMaps

class QlueKPRateViewCell: UITableViewCell {

    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellPhone: UILabel!
    @IBOutlet weak var cellEmail: UILabel!
    @IBOutlet weak var cellDesc: UILabel!
    @IBOutlet weak var cellRating: QlueRateView!
    @IBOutlet weak var cellStarRating: QlueRateView!
    @IBOutlet weak var cellCountReview: UIButton!
    @IBOutlet weak var cellMapsView: GMSMapView!{
        didSet{
            cellMapsView.cornerRadius(5)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
