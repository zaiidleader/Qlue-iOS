//
//  QlueKHeaderNewCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import FLAnimatedImage

protocol QlueKHeaderNewCellDelegate {
     func laporanTapped(_ cell: QlueKHeaderNewCell)
     func rankingTapped(_ cell: QlueKHeaderNewCell)
    func membersTapped(_ cell: QlueKHeaderNewCell)
    func discussionTapped(_ cell: QlueKHeaderNewCell)
}

class QlueKHeaderNewCell: UITableViewCell {

     var delegate: QlueKHeaderNewCellDelegate?
     
    @IBOutlet weak var cellDiscussionLabel: UILabel!
     @IBOutlet weak var cellReportLabel: UILabel!
     @IBOutlet weak var cellRankingLabel: UILabel!
     @IBOutlet weak var cellMemberLabel: UILabel!
     
    @IBOutlet weak var cellLurah: UILabel!
    @IBOutlet weak var cellCodeKel: UILabel!
     @IBOutlet weak var cellKelurahanName: UILabel!
     @IBOutlet weak var cellRating: QlueRateView!
     
     @IBOutlet weak var cellGifImage: FLAnimatedImageView!
     
     // MARK: - View Lifecycle
     override func awakeFromNib() {
          super.awakeFromNib()
          // Initialization code
     }
     
     // MARK: - Action
     
     @IBAction func reportTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.laporanTapped(self)
          }
     }
     
     @IBAction func rankingTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.rankingTapped(self)
          }
     }
     
     @IBAction func memberTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.membersTapped(self)
          }
     }
    @IBAction func discussionTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.discussionTapped(self)
        }
    }

}
