//
//  BgViewPostForum.swift
//  Qlue
//
//  Created by Nurul on 8/14/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol BgViewPostForumDelegate {
     func sendForumTapped(_ view: BgViewPostForum, vote: String)
    func closeForumTapped(_ view: BgViewPostForum)
}

class BgViewPostForum: UIView {
     
     enum PostForumType{
          case content
          case photo
          case defaultChoice
          case choice
          case addMore
          
          var cellIdentifier: String{
               switch self {
               case .content:
                    return "ContentCell"
               case .photo:
                    return "PhotoCell"
               case .defaultChoice:
                    return "DefaultChoiceCell"
               case .choice:
                    return "ChoiceCell"
               case .addMore:
                    return "AddMoreCell"
               }
          }
     }
     
     var delegate: BgViewPostForumDelegate?
     
     @IBOutlet weak var avatar: UIImageView!
     @IBOutlet weak var nameLabel: UILabel!
     @IBOutlet weak var titleTextField: UITextField!{
          didSet{
               Util.textfieldPlaceholderWithColor(UIColor.darkGray, font: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 16), textfield: titleTextField)
          }
     }
     
     @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
     
     var textHeight: CGFloat = 30
     var textHeightBefore: CGFloat = 30
     
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var votingButton: UIButton!
     var votingSelected: Bool = false
     
     var forumContent: [PostForumType] = [.content, .photo]
     
     var photos: [UIImage] = []
     var photoCollectionView: UICollectionView?
     var vote: [String] = []
    @IBOutlet weak var sendButton: UIButton!
     
    @IBOutlet weak var forumTopicName: UILabel!
     
     var contentTextView : UITextView?
     var descText: String?
     
     // MARK: Forum Send Var
     var title: String = ""
     var desc: String = ""
     
     // MARK: - View Lifecycle
     override func awakeFromNib() {
          configureTableView()
        sendButton.isEnabled = false
          
          if let ava = User.currentUser()?.avatar?.encodeURL(){
               avatar.sd_setImage(with: ava as URL!)
          }
          
          nameLabel.text = User.currentUser()?.username
          checkSendEnable()
     }
     
     // MARK: - Configure
     
     fileprivate func configureTableView(){
          tableView.register(UINib.init(nibName: "QlueForumTextViewCell", bundle: nil), forCellReuseIdentifier: PostForumType.content.cellIdentifier)
          tableView.register(UINib.init(nibName: "QlueForumPostPhotoViewCell", bundle: nil), forCellReuseIdentifier: PostForumType.photo.cellIdentifier)
          tableView.register(UINib.init(nibName: "QlueForumDefaultChoiceViewCell", bundle: nil), forCellReuseIdentifier: PostForumType.defaultChoice.cellIdentifier)
          tableView.register(UINib.init(nibName: "QlueForumChoiceViewCell", bundle: nil), forCellReuseIdentifier: PostForumType.choice.cellIdentifier)
          tableView.register(UINib.init(nibName: "QlueForumAddMoreViewCell", bundle: nil), forCellReuseIdentifier: PostForumType.addMore.cellIdentifier)
     }
    
     // MARK: - Action
    @IBAction func votingTapped(_ sender: UIButton) {
        votingSelected = !votingSelected
        
        if votingSelected == true{
            forumContent.append(.defaultChoice)
            forumContent.append(.defaultChoice)
            forumContent.append(.addMore)
        } else{
            forumContent.removeLast(forumContent.count - 2)
        }
        tableView.reloadData()
          tableView.scrollToRow(at: IndexPath.init(row: tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: true)

        
    }
     @IBAction func sendTapped(_ sender: UIButton) {
          
          var votes = ""
          if votingSelected == true{
               votes = vote.joined(separator: ",")
          }
          title = titleTextField.text ?? ""
          
          if let delegate = delegate{
               delegate.sendForumTapped(self, vote:  votes)
          }
          
     }
    
    @IBAction func closeTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.closeForumTapped(self)
        }
        
        
        
    }
    fileprivate func checkSendEnable(){
//        if (titleTextField.text != "") && (contentTextView?.text != ""){
            sendButton.isEnabled = true
//        } else{
//            sendButton.enabled = false
//        }
    }
     
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension BgViewPostForum: UITableViewDataSource, UITableViewDelegate{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
          return forumContent.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
          let forum =  forumContent[indexPath.row]
          switch forum{
          case .content:
               let cell = tableView.dequeueReusableCell(withIdentifier: forum.cellIdentifier, for: indexPath) as! QlueForumTextViewCell
               if let descText = descText{
                   cell.textView.text = descText
               }
               contentTextView = cell.textView
               cell.textView.delegate = self
               
               checkSendEnable()
               return cell
          case .photo:
               let cell = tableView.dequeueReusableCell(withIdentifier: forum.cellIdentifier, for: indexPath) as! QlueForumPostPhotoViewCell
               photoCollectionView = cell.cellCollectionView
               photoCollectionView?.dataSource = self
               photoCollectionView?.delegate = self
               
               photoCollectionView?.register(UINib.init(nibName: "QlueForumPostPreviewPhotoViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
               return cell
          case .defaultChoice:
               let cell = tableView.dequeueReusableCell(withIdentifier: forum.cellIdentifier, for: indexPath) as! QlueForumDefaultChoiceViewCell
               cell.cellChoice.placeholder = "Choice \(indexPath.item - 1)"
               if vote.count >= (indexPath.item - 1){
                    cell.cellChoice.text = vote[indexPath.item - 2]
               }
               cell.delegate = self
               return cell
          case .choice: // voting
               let cell = tableView.dequeueReusableCell(withIdentifier: forum.cellIdentifier, for: indexPath) as! QlueForumChoiceViewCell
               cell.cellChoice.placeholder = "Choice \(indexPath.item - 1)"
               if vote.count >= (indexPath.item - 1){
                    cell.cellChoice.text = vote[indexPath.item - 2]
               }
               cell.delegate = self
               
               return cell
          case .addMore:
               let cell = tableView.dequeueReusableCell(withIdentifier: forum.cellIdentifier, for: indexPath) as! QlueForumAddMoreViewCell
               
               cell.delegate = self
               
               return cell
          }
          
          
     }
     
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let forum =  forumContent[indexPath.row]
        switch forum{
        case .content:
            return textHeight + 16
        case .photo:
          if photos.count > 0{
               return 100
          }else{
               return 0
          }
        default:
            return 45
        }
        
    }
}

// MARK: - UITextFieldDelegate
extension BgViewPostForum: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        title = textField.text ?? ""
        checkSendEnable()
        
        return true
    }
}

// MARK: - UITextViewDelegate
extension BgViewPostForum:  UITextViewDelegate {
     func textViewDidChange(_ textView: UITextView) {
          let height = textView.contentSize.height
          
          if height == textHeight || height < 15{
               return
          }
          
          desc = textView.text
        checkSendEnable()
          
          textHeight  = height
          tableView.beginUpdates()
          tableView.endUpdates()
          
     }
}

// MARK: - QlueForumAddMoreViewCellDelegate
extension BgViewPostForum: QlueForumAddMoreViewCellDelegate{
    func addMoreTapped(_ cell: QlueForumAddMoreViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else{
            return
        }
        
        forumContent.insert(.choice, at: indexPath.row)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
}

// MARK: - QlueForumChoiceViewCellDelegate
extension BgViewPostForum: QlueForumChoiceViewCellDelegate{
    func cancelTapped(_ cell: QlueForumChoiceViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else{
            return
        }
        forumContent.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
     
     func choiceDidChange(_ cell: QlueForumChoiceViewCell, text: String?) {
          guard let indexPath = tableView.indexPath(for: cell) else { return }
          let index = indexPath.row - 2
          if vote.count >= indexPath.row - 1{
               vote.remove(at: index)
          }
          vote.insert(text ?? "", at: index)
     }
}

// MARK: - QlueForumDefaultChoiceViewCellDelegate
extension BgViewPostForum: QlueForumDefaultChoiceViewCellDelegate{
     func defaultChoiceDidChange(_ cell: QlueForumDefaultChoiceViewCell, text: String?) {
          guard let indexPath = tableView.indexPath(for: cell) else { return }
          let index = indexPath.row - 2
          if vote.count >= indexPath.row - 1{
               vote.remove(at: index)
          }
          vote.insert(text ?? "", at: index)
     }
}


// MARK: - UICollectionDataSource, UICollectionViewDelegate
extension BgViewPostForum: UICollectionViewDataSource, UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return photos.count
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! QlueForumPostPreviewPhotoViewCell
          
          cell.cellPhoto.image = photos[indexPath.item]
          cell.delegate = self
          
          return cell
     }
}

// MARK: - QlueForumPostPreviewPhotoViewCellDelegate
extension BgViewPostForum: QlueForumPostPreviewPhotoViewCellDelegate{
     func addPhoto(withImage image: UIImage){
          photos.append(image)
          tableView.reloadRows(at: [IndexPath.init(row: 1, section: 0)], with: .automatic)
          photoCollectionView?.reloadData()
     }
     func getPhoto() -> [UIImage]{
          return photos
     }
     
     func cancelPhotoTapped(_ cell: QlueForumPostPreviewPhotoViewCell) {
          guard let indexPath = photoCollectionView?.indexPath(for: cell) else { return }
          photos.remove(at: indexPath.item)
          photoCollectionView?.deleteItems(at: [indexPath])
          tableView.beginUpdates()
          tableView.endUpdates()
          
     }
}
