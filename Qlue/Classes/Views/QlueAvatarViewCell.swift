//
//  QlueAvatarViewCell.swift
//  Qlue
//
//  Created by Nurul on 7/1/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueAvatarViewCell: UICollectionViewCell {
    @IBOutlet weak var cellLockLabel: UILabel!
    @IBOutlet weak var cellBgLock: UIView!

    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    
    @IBOutlet weak var checklistImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
     
     override func prepareForReuse() {
          checklistImage.isHidden = true
     }

}
