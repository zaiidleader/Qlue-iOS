//
//  QlueKLViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/8/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueKLViewCellDelegate {
    func moreTapped(_ view: QlueKLViewCell)
}

class QlueKLViewCell: UICollectionViewCell {

    var delegate: QlueKLViewCellDelegate?
    
    @IBOutlet weak var cellNotif: UIImageView!
    @IBOutlet weak var cellCollectionView: UICollectionView!
    @IBOutlet weak var cellTitle: UILabel!
     
     var laporan: [QlueFeed] = []
     
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellCollectionView.register(UINib.init(nibName: "QluePhotoSquareViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCell")
    }

}

// MARK: - UICollectionViewDataSource
extension QlueKLViewCell: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return laporan.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! QluePhotoSquareViewCell
     
     if let thumb  = laporan[indexPath.row].fileUrl?.encodeURL(){
          cell.cellImage.sd_setImage(with: thumb as URL!)
     }
     
     if indexPath.item + 1 == laporan.count{
          let next = UIImageView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize(width: 30, height: 30)))
          next.center = CGPoint(x: cell.bounds.width / 2, y: cell.bounds.height / 2)
          next.image  = UIImage.init(named: "play-symbol")
          next.contentMode = .scaleAspectFit
          cell.addSubview(next)
     }
        
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension QlueKLViewCell: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let delegate = delegate, (indexPath.row + 1) == laporan.count{
            delegate.moreTapped(self)
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueKLViewCell: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size: CGFloat = (collectionView.bounds.width - 26) / 3
        
        return CGSize(width: size, height: size)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 12, left: 8, bottom: 12, right: 8)
    }

    
}
