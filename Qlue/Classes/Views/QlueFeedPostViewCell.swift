//
//  QlueFeedPostViewCell.swift
//  Qlue
//ß
//  Created by Nurul on 6/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import GoogleMaps
import ActiveLabel

class QlueFeedPostViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellPost: ActiveLabel!
    @IBOutlet weak var cellStreetView: GMSPanoramaView!{
        didSet{
            cellStreetView.cornerRadius(5)
        }
    }
    var lat: Double?
    var lng: Double?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let lat = lat, let lng = lng{
            cellStreetView.moveNearCoordinate(CLLocationCoordinate2D(latitude: lat, longitude: lng))
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
