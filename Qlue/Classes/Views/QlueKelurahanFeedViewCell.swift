//
//  QlueKelurahanFeedViewCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 8/28/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import FLAnimatedImage

protocol QlueKelurahanFeedViewCellDelegate {
     func leaveTapped(_ cell: QlueKelurahanFeedViewCell )
     func diskusiTapped(_ cell: QlueKelurahanFeedViewCell)
     func laporanTapped(_ cell: QlueKelurahanFeedViewCell)
     func rankingTapped(_ cell: QlueKelurahanFeedViewCell)
     func membersTapped(_ cell: QlueKelurahanFeedViewCell)
}

class QlueKelurahanFeedViewCell: UICollectionViewCell {
    
     var delegate: QlueKelurahanFeedViewCellDelegate?
     
    @IBOutlet weak var cellReportLabel: UILabel!
    @IBOutlet weak var cellDiscussionLabel: UILabel!
    @IBOutlet weak var cellRankingLabel: UILabel!
    @IBOutlet weak var cellMemberLabel: UILabel!
    
    @IBOutlet weak var cellKelurahanName: UILabel!
    @IBOutlet weak var cellRating: QlueRateView!
     
     @IBOutlet weak var cellGifImage: FLAnimatedImageView!
     
    // MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
     
    // MARK: - Action
     @IBAction func leaveTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.leaveTapped(self)
          }
     }

     @IBAction func reportTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.laporanTapped(self)
          }
     }
     
     @IBAction func duscussionTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.diskusiTapped(self)
          }
     }
     
     @IBAction func rankingTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.rankingTapped(self)
          }
     }
     
     @IBAction func memberTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.membersTapped(self)
          }
     }
     
     // MARK: - Helpers
     
}
