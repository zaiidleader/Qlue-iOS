//
//  QlueSearchCitizenCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/27/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueSearchCitizenCell: UITableViewCell {

     @IBOutlet weak var cellLabel: UILabel!
     @IBOutlet weak var cellImage: UIImageView!
     
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
