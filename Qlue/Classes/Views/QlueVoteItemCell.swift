//
//  QlueVoteItemCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueVoteItemCellDelegate {
    func voteTapped(_ cell: QlueVoteItemCell)
}

class QlueVoteItemCell: UITableViewCell {

    var delegate: QlueVoteItemCellDelegate?
    
    @IBOutlet weak var trailingProgress: NSLayoutConstraint!
    @IBOutlet weak var percent: UILabel!
    @IBOutlet weak var voteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - ACTION
    @IBAction func buttonTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.voteTapped(self)
        }
    }
    
}
