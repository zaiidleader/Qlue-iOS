//
//  QlueChatMemberCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/13/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueChatMemberCellDelegate {
    func followTapped(_ cell: QlueChatMemberCell, button: UIButton)
}
class QlueChatMemberCell: UITableViewCell {

    var delegate: QlueChatMemberCellDelegate?
    
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellAddButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: - Action
    @IBAction func addMemberTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.followTapped(self, button: sender)
        }
    }
}
