//
//  QlueRankingReportViewCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/16/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueRankingReportViewCell: UITableViewCell {

    @IBOutlet weak var cellProgress: UILabel!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellWrapperView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellWrapperView.cornerRadiusWithShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
