//
//  QlueMPointHeaderView.swift
//  Qlue
//
//  Created by Nurul on 6/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueMPointHeaderView: UIView {

    @IBOutlet weak var waitingButton: UIButton!
    @IBOutlet weak var viewGemButton: UIButton!
    @IBOutlet weak var viewPointButton: UIButton!
    @IBOutlet weak var progressButton: UIButton!
    @IBOutlet weak var completedButton: UIButton!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
