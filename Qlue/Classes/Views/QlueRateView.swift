//
//  QlueRateView.swift
//  Qlue
//
//  Created by Nurul on 7/23/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import UIKit

protocol QlueRateViewDelegate{
     func rating(_ cell: QlueRateView, didRatingSelected rating: Double)
}


class QlueRateView: UIView {
     
     var delegate: QlueRateViewDelegate?
     
     @IBOutlet weak var star1Button: UIButton!
     @IBOutlet weak var star2Button: UIButton!
     @IBOutlet weak var star3Button: UIButton!
     @IBOutlet weak var star4Button: UIButton!
     @IBOutlet weak var star5Button: UIButton!
     
     var emptyStar = UIImage(named: "ic_star_bad")!
     let halfStar = UIImage(named: "ic_star_half")!
     let fullStar = UIImage(named: "ic_star_happy")!
     
     var rate: Double = 0 {
          didSet {
               star1Button.setImage(emptyStar, for: UIControlState())
               star2Button.setImage(emptyStar, for: UIControlState())
               star3Button.setImage(emptyStar, for: UIControlState())
               star4Button.setImage(emptyStar, for: UIControlState())
               star5Button.setImage(emptyStar, for: UIControlState())
               
               if rate > 0 {
                    star1Button.setImage(halfStar, for: UIControlState())
               }
               if rate > 0.5{
                    star1Button.setImage(fullStar, for: UIControlState())
               }
               if rate > 1 {
                    star2Button.setImage(halfStar, for: UIControlState())
               }
               if rate > 1.5 {
                    star2Button.setImage(fullStar, for: UIControlState())
               }
               if rate > 2 {
                    star3Button.setImage(halfStar, for: UIControlState())
               }
               if rate > 2.5{
                    star3Button.setImage(fullStar, for: UIControlState())
               }
               if rate > 3 {
                    star4Button.setImage(halfStar, for: UIControlState())
               }
               if rate > 3.5 {
                    star4Button.setImage(fullStar, for: UIControlState())
               }
               if rate > 4 {
                    star5Button.setImage(halfStar, for: UIControlState())
               }
               if rate > 4.5 {
                    star5Button.setImage(fullStar, for: UIControlState())
               }
          }
     }
     
     var ratingEnable: Bool = true {
          didSet {
            emptyStar = UIImage(named: "ic_star_netral")!
               star1Button.isEnabled = true
               star2Button.isEnabled = true
               star3Button.isEnabled = true
               star4Button.isEnabled = true
               star5Button.isEnabled = true
               
               if !ratingEnable {
                
                emptyStar = UIImage(named: "ic_star_bad")!
                    star1Button.isEnabled = false
                    star2Button.isEnabled = false
                    star3Button.isEnabled = false
                    star4Button.isEnabled = false
                    star5Button.isEnabled = false
               }
          }
     }
     
     var rateBeforeButton: UIButton?
    
    override func awakeFromNib() {
        star1Button.imageView?.contentMode = .scaleAspectFit
        star2Button.imageView?.contentMode = .scaleAspectFit
        star3Button.imageView?.contentMode = .scaleAspectFit
        star4Button.imageView?.contentMode = .scaleAspectFit
        star5Button.imageView?.contentMode = .scaleAspectFit
    }
    
     // MARK: - Actions
     @IBAction func rateButtonClicked(_ sender: UIButton) {
          
          
          
          var _rate: Double = 0
          
          if sender == star1Button {
               _rate = 1
          }
          if sender == star2Button {
               _rate = 2
          }
          if sender == star3Button {
               _rate = 3
          }
          if sender == star4Button {
               _rate = 4
          }
          if sender == star5Button {
               _rate = 5
          }
          
          
          if rateBeforeButton != sender{
               _rate -= 0.5
          }
          
          rate = _rate
          rateBeforeButton = sender
          
          if let delegate = delegate{
               delegate.rating(self, didRatingSelected: rate)
          }
          
     }
}
