//
//  QlueMemberItemViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/8/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueMemberItemViewCellDelegate {
     func followButtonTapped(_ cell: QlueMemberItemViewCell, button: UIButton)
}

class QlueMemberItemViewCell: UITableViewCell {

     var delegate : QlueMemberItemViewCellDelegate?
     
    @IBOutlet weak var memberWidthConstant: NSLayoutConstraint!
    
    @IBOutlet weak var cellUsername: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellFollowButton: UIButton!{
        didSet{
            cellFollowButton.cornerRadius(5)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Action
    @IBAction func followButton(_ sender: UIButton) {
     
     if let delegate = delegate{
          delegate.followButtonTapped(self, button: sender)
     }
     
    }
    
    
}
