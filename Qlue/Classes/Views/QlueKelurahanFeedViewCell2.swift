//
//  QlueKelurahanFeedViewCell2.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 11/28/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import FLAnimatedImage

protocol QlueKelurahanFeedViewCell2Delegate {
    func leaveTapped(_ cell: QlueKelurahanFeedViewCell2)
}

class QlueKelurahanFeedViewCell2: UICollectionViewCell {
    var delegate: QlueKelurahanFeedViewCell2Delegate?
    
    @IBOutlet weak var leaveButton: UIButton!
    @IBOutlet weak var cellKelurahanName: UILabel!
    @IBOutlet weak var cellRating: QlueRateView!
    
    @IBOutlet weak var cellGifImage: FLAnimatedImageView!
    
    // MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }
    
    // MARK: - Helpers
    func setupViews() {
        leaveButton.layer.cornerRadius = 4.0
        leaveButton.layer.masksToBounds = true
        leaveButton.layer.borderColor = UIColor.white.cgColor
        leaveButton.layer.borderWidth = 1.0
    }
    
    // MARK: - Action
    @IBAction func leaveTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.leaveTapped(self)
        }
    }
}
