//
//  SearchTabCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/27/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class SearchTabCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var activeView: UIView!
    var isActive = false{
        didSet{
            if isActive == true{
                activeView.isHidden = false
                label.textColor = UIColor.white
            }else{
                activeView.isHidden = true
                label.textColor = UIColor.lightGray
            }
        }
    }
   
}
