//
//  QlueRankingCell.swift
//  Qlue
//
//  Created by Nurul on 8/14/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueRankingCell: UITableViewCell {

    @IBOutlet weak var cellRankLabel: UILabel!
    @IBOutlet weak var cellRankView: UIView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellGreenCount: UILabel!
    @IBOutlet weak var cellYellowCount: UILabel!
    @IBOutlet weak var cellRedCount: UILabel!
    @IBOutlet weak var cellPoint: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellRankView.cornerRadius(cellRankView.bounds.width / 2)
     
//     cellImage.cornerRadius(cellImage.bounds.width / 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
