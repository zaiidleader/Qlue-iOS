//
//  QlueSwitchViewCell.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 10/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueSwitchViewCellDelegate {
    func qlueSwitchViewCellOnOffSwitchValueChanged(_ cell: QlueSwitchViewCell)
}

class QlueSwitchViewCell: UITableViewCell {
    var delegate: QlueSwitchViewCellDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var onOffSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onOffSwitchValueChanged(_ sender: UISwitch) {
        
        if let delegate = delegate {
            delegate.qlueSwitchViewCellOnOffSwitchValueChanged(self)
        }
    }
}
