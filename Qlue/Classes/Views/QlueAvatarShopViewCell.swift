//
//  QlueAvatarShopViewCell.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 9/25/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueAvatarShopViewCellDelegate {
    func qlueAvatarShopViewCellBuyButtonTapped(_ cell: QlueAvatarShopViewCell)
    func qlueAvatarShopViewCellApplyButtonTapped(_ cell: QlueAvatarShopViewCell)
}

class QlueAvatarShopViewCell: UITableViewCell {
    var delegate: QlueAvatarShopViewCellDelegate?

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var avatarNameLabel: UILabel!
    @IBOutlet weak var avatarTypeLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var applyButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupViews() {
        
        buyButton.layer.cornerRadius = 4
        buyButton.layer.masksToBounds = true
        buyButton.layer.borderColor = UIColor.lightGray.cgColor
        buyButton.layer.borderWidth = 1
        
        applyButton.layer.cornerRadius = 4
        applyButton.layer.masksToBounds = true
        applyButton.layer.borderColor = UIColor.lightGray.cgColor
        applyButton.layer.borderWidth = 1
    }
    
    // MARK: - Actions
    
    @IBAction func buyButtonTapped(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.qlueAvatarShopViewCellBuyButtonTapped(self)
        }
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.qlueAvatarShopViewCellApplyButtonTapped(self)
        }
    }
}
