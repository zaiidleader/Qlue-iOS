//
//  QlueForumViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/8/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueForumViewCellDelegate{
    func starDidTapped(_ cell: UICollectionViewCell, button: UIButton)
    func seenDidTapped(_ cell: UICollectionViewCell, button: UIButton)
}

class QlueForumViewCell: UICollectionViewCell {

    var delegate : QlueForumViewCellDelegate?
    
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var cellStarWidth: NSLayoutConstraint!
    @IBOutlet weak var cellStar: UIButton!
    @IBOutlet weak var cellComment: UIButton!
    @IBOutlet weak var cellSeen: UIButton!
    @IBOutlet weak var cellUsername: UIButton!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellDate: UILabel!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Action
    @IBAction func starTapped(_ sender: UIButton) {
        
        if let delegate = delegate{
            delegate.starDidTapped(self, button: sender)
        }
        
    }
    
    @IBAction func seenByTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.seenDidTapped(self, button: sender)
        }
    }

}
