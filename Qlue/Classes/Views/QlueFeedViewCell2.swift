//
//  QlueFeedViewCell2.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 11/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import PBJVideoPlayer
import ActiveLabel

protocol QlueFeedViewCell2Delegate {
     func countForCommentTableView(_ cell: QlueFeedViewCell2) -> Int
     func seeAllCommentTapped(_ cell: QlueFeedViewCell2)
     func commentData(_ cell: QlueFeedViewCell2) -> [QlueComment]
     func poiTapped(_ cell: QlueFeedViewCell2)
     func likeTapped(_ cell: QlueFeedViewCell2, button: UIButton, label: UILabel)
     func seenByTapped(_ cell: QlueFeedViewCell2)
     func kelurahanTapped(_ cell: QlueFeedViewCell2)
     func moreTapped(_ cell: QlueFeedViewCell2)
     func progressTapped(_ cell: QlueFeedViewCell2, button: UIButton)
     func avatarCommentTapped(_ feedCell: QlueFeedViewCell2, indexComment: Int)
     func avatarFeedTapped(_ cell: QlueFeedViewCell2)
     func categoryTapped(_ cell: QlueFeedViewCell2)
     func videoTapped(_ cell: QlueFeedViewCell2, videoPlayer: PBJVideoPlayerController)
}

class QlueFeedViewCell2: UICollectionViewCell,  QlueCommentViewCellDelegate {
    
    var delegate: QlueFeedViewCell2Delegate?
    
    @IBOutlet weak var cellAvatar: UIImageView!{
        didSet{
            cellAvatar.cornerRadius(10)
        }
    }
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellTime: UILabel!
    
    @IBOutlet weak var cellDesc: ActiveLabel!
    
    @IBOutlet weak var cellPlaceName: UILabel!
    @IBOutlet weak var cellCategoryImage: UIImageView!{
        didSet{
            cellCategoryImage.cornerRadius(10)
        }
    }
    @IBOutlet weak var cellVideoButton: UIButton!{
        didSet{
            cellVideoButton.cornerRadius(30)
        }
    }
    @IBOutlet weak var cellMainImage: UIImageView!
    
    @IBOutlet weak var cellPoiButton: UIButton!
    
    @IBOutlet weak var cellLikeButton: UIButton!
    
    @IBOutlet weak var cellCommentButton: UIButton!
    @IBOutlet weak var cellCommentHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cellLikeCommentTitle: UILabel!
    
    @IBOutlet weak var cellShareButton: UIButton!
    
    var countComment = 0
    var comments: [QlueComment] = []
    
    @IBOutlet weak var cellCommentWrapperView: UIView!{
        didSet{
            cellCommentWrapperView.layer.cornerRadius = 10
            cellCommentWrapperView.layer.shadowColor = UIColor.darkGray.cgColor
            cellCommentWrapperView.layer.shadowOffset = CGSize(width: 1, height: 2)
            cellCommentWrapperView.layer.shadowRadius = 10
        }
    }
    @IBOutlet weak var cellTableView: UITableView!
    
    var videoPlayer: PBJVideoPlayerController = PBJVideoPlayerController()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //tableView
        registerNib()
        if let delegate = delegate{
            comments = delegate.commentData(self)
            cellTableView.reloadData()
        }
        
        cellMainImage.layoutIfNeeded()
        videoPlayer.view.frame = cellMainImage.bounds
        cellVideoButton.isHidden = true
        
    }
    
    override func prepareForReuse() {
        cellVideoButton.isHidden = true
        videoPlayer.stop()
        videoPlayer.removeFromParentViewController()
    }
    
    // MARK: - Private Func
    fileprivate func registerNib(){
        cellTableView.register(UINib.init(nibName: "QlueCommentViewCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
    }
    
    // MARK: - Action
    @IBAction func avatarFeedTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.avatarFeedTapped(self)
        }
    }
    @IBAction func seeAllCommentTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.seeAllCommentTapped(self)
        }
    }
    @IBAction func poiTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.poiTapped(self)
        }
    }
    @IBAction func likeTapped(_ sender: UIButton) {
        
        if let delegate = delegate{
            delegate.likeTapped(self, button: sender, label: cellLikeCommentTitle)
        }
        
    }
    func commentAvatarTapped(_ cell: QlueCommentViewCell) {
        guard let commentIndex = cellTableView.indexPath(for: cell) else { return }
        
        if let delegate = delegate{
            delegate.avatarCommentTapped(self, indexComment: commentIndex.row)
        }
    }
    @IBAction func categoryTapped(_ sender: UIButton) {
        if let delegate  = delegate{
            delegate.categoryTapped(self)
        }
    }
    @IBAction func videoFeedTapped(_ sender: UIButton) {
        if let delegate  = delegate{
            delegate.videoTapped(self, videoPlayer: self.videoPlayer)
        }
    }
    
}

// MARK: - UITableViewCellDataSource
extension QlueFeedViewCell2: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
        //        return countComment
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! QlueCommentViewCell
        
        let data = comments[indexPath.row]
        
        
        cell.delegate = self
        cell.cellAvatar.image = nil
        if let avatar = data.avatar?.encodeURL(){
            cell.cellAvatar.sd_setImage(with: avatar as URL!)
        }
        
        cell.cellLabel.attributedText = Util.getCommentAttributedString(data.username, comment: data.comment)
        cell.cellLabel.resolveHashTags()
        
        
        cell.cellTime.text = data.timestamp?.timeAgoWithNumericDates(true)
        
        return cell
    }
}

// MARK: - UITableViewCellDelegate
extension QlueFeedViewCell2: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let data = comments[indexPath.row]
        
        return Util.getHeightCommentInFeedList(Util.getCommentAttributedString(data.username, comment: data.comment))
    }
    
}
