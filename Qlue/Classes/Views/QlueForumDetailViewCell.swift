//
//  QlueForumDetailViewCell.swift
//  Qlue
//
//  Created by Nurul on 8/3/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueForumDetailViewCellDelegate {
    func leftArrowTapped(_ cell: QlueForumDetailViewCell, button: UIButton)
    func rightArrowTapped(_ cell: QlueForumDetailViewCell, button: UIButton)
    func seenByTapped(_ cell: QlueForumDetailViewCell)
}

class QlueForumDetailViewCell: UITableViewCell {
    @IBOutlet weak var cellAvatar: UIImageViewAligned!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellHeightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var cellCollectionView: UICollectionView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellDesc: UILabel!
    @IBOutlet weak var cellArrowLeft: UIButton!
    @IBOutlet weak var cellArrowRight: UIButton!
    @IBOutlet weak var cellSeenBy: UIButton!{
        didSet{
            cellSeenBy.cornerRadius(5)
        }
    }
    
    @IBOutlet weak var cellVoteHeight: NSLayoutConstraint!
    @IBOutlet weak var cellVoteTableView: UITableView!
    @IBOutlet weak var cellVoteTotal: UILabel!
    var delegate: QlueForumDetailViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Action
    @IBAction func seenByTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.seenByTapped(self)
        }
    }
    @IBAction func cellLeftArrowTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.leftArrowTapped(self, button: sender)
        }
    }
    
    @IBAction func cellRightArrowTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.rightArrowTapped(self, button: sender)
        }

    }
    
     override func prepareForReuse() {
          cellArrowRight.isHidden = false
          cellArrowLeft.isHidden = false
     }
}
