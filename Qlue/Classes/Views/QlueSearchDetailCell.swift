//
//  QlueSearchDetailCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueSearchDetailCell: UITableViewCell {

     @IBOutlet weak var cellName: UILabel!
     @IBOutlet weak var cellImage: UIImageView!
     override func awakeFromNib() {
          super.awakeFromNib()
          // Initialization code
     }
     
     override func setSelected(_ selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
          
          // Configure the view for the selected state
     }
     
     override func prepareForReuse() {
          cellName.text = ""
          cellImage.image = nil
          cellImage.contentMode = .scaleAspectFill
     }

}
