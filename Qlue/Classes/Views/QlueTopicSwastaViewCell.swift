//
//  QlueTopicSwastaViewCell.swift
//  Qlue
//
//  Created by Nurul on 7/14/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueTopicSwastaViewCellDelegate {
     func topicSwastaData(_ cell: QlueTopicSwastaViewCell ) -> [QlueTopic]
}

class QlueTopicSwastaViewCell: UICollectionViewCell {
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellCollectionView: UICollectionView!
     
     
//    override func awakeFromNib() {
//        
//        setLayoutCollection()
//     collectionView.registerNib(UINib.init(nibName: "QlueSelectTopicViewCell", bundle: nil), forCellWithReuseIdentifier: "TopicCell")
//    }
//    
//     // MARK: - Helpers
//     func setLayoutCollection(){
//          
//          let layout = UICollectionViewFlowLayout()
//          layout.minimumLineSpacing = 5
//          layout.minimumInteritemSpacing = 5
//          layout.sectionInset = UIEdgeInsetsMake(15, 15, 0, 15)
//          
//          collectionView.setCollectionViewLayout(layout, animated: true)
//          
//          let alignedLayout = FSQCollectionViewAlignedLayout.init()
//          
//          collectionView.collectionViewLayout = alignedLayout
//     }
//
//    
//}
//
//// MARK: - UICollectionViewDataSource
//extension QlueTopicSwastaViewCell: UICollectionViewDataSource{
//     
//     func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//          if let delegate = delegate{
//               return dele
//          }
//          return topics.count
//     }
//     func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//          let cell = collectionView.dequeueReusableCellWithReuseIdentifier("TopicCell", forIndexPath: indexPath) as! QlueSelectTopicViewCell
//          
//          cell.layer.shouldRasterize = true
//          cell.layer.rasterizationScale = UIScreen.mainScreen().scale
//          
//          let topic = topics[indexPath.item]
//          
//          cell.cellTitle.text = topic.suggestName
//          if let image = topic.suggestIcon?.encodeURL(){
//               cell.cellImage.sd_setImageWithURL(image)
//          }
//          
//          return cell
//     }
//}
//
//// MARK: - UICollectionViewDelegate
//extension QlueTopicSwastaViewCell: UICollectionViewDelegate{
//     
//}
//
//// MARK: - UICollectionViewDelegateFlowLayout
//extension QlueTopicSwastaViewCell: FSQCollectionViewDelegateAlignedLayout{
//     
//     func collectionView(collectionView: UICollectionView!, layout collectionViewLayout: FSQCollectionViewAlignedLayout!, sizeForItemAtIndexPath indexPath: NSIndexPath!, remainingLineSpace: CGFloat) -> CGSize {
//          let size: CGFloat = ((collectionView.bounds.width - 45) / 4 )
//          var height = size + 16
//          if let label = topics[indexPath.item].suggestName{
//               
//               height += label.heightWithConstrainedWidth(size, font: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 12)!)
//          }
//          
//          
//          
//          return CGSize(width: size, height: height)
//          
//     }
//     func collectionView(collectionView: UICollectionView!, layout collectionViewLayout: FSQCollectionViewAlignedLayout!, attributesForSectionAtIndex sectionIndex: Int) -> FSQCollectionViewAlignedLayoutSectionAttributes! {
//          return FSQCollectionViewAlignedLayoutSectionAttributes.topCenterAlignment()
//     }
    
}

