//
//  CommentListCell
//  Qlue
//
//  Created by Nurul Rachmawati on 9/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import ActiveLabel

protocol CommentListCellDelegate {
    func commentImageTapped(_ cell: CommentListCell)
    func commentLongTapped(_ cell: CommentListCell)
}
class CommentListCell: UITableViewCell {
    @IBOutlet weak var cellImage: UIImageView!
     @IBOutlet weak var cellComment: ActiveLabel!
     @IBOutlet weak var cellTime: UILabel!
     @IBOutlet weak var cellName: UILabel!
     @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellImageHeight: NSLayoutConstraint!
    
     var delegate: CommentListCellDelegate?
     
    // MARK: - View Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let longGesture = UILongPressGestureRecognizer.init(target: self, action: #selector(CommentListCell.longTappped))
        self.addGestureRecognizer(longGesture)
    }
    
    override func prepareForReuse() {
        cellImageHeight.constant = 0
    }
    
    // MARK: - Action

    @IBAction func imageTapped(_ sender: UIButton) {
     if let delegate = delegate{
          delegate.commentImageTapped(self)
     }
    }
    func longTappped(){
     if let delegate = delegate{
          delegate.commentLongTapped(self)
     }
        print("long pressed")
    }
    
}
