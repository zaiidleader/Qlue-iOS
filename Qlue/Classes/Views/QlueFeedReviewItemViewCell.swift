//
//  QlueFeedReviewItemViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import ActiveLabel

class QlueFeedReviewItemViewCell: UITableViewCell {
    @IBOutlet weak var cellComment: ActiveLabel!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
