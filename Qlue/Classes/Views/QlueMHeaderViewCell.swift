//
//  QlueMHeaderViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueMHeaderViewCell: UITableViewCell {

    @IBOutlet weak var cellGemButton: UIButton!{
        didSet{
            cellGemButton.cornerRadiusWithShadow()
        }
    }
    @IBOutlet weak var cellBanner: UIImageView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellViewImage: UIView!{
        didSet{
            cellViewImage.cornerRadius(cellViewImage.bounds.width / 2)
        }
    }
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellLevel: UILabel!
    @IBOutlet weak var cellFollowersButton: UIButton!
    @IBOutlet weak var cellFollowingButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
