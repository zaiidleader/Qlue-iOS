//
//  QlueNotifViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/8/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueNotifViewCell: UITableViewCell {

    @IBOutlet weak var cellDate: UILabel!
    @IBOutlet weak var cellDesc: UILabel!
    @IBOutlet weak var cellImage: UIImageView! {
        didSet{
            cellImage.cornerRadius(10)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
