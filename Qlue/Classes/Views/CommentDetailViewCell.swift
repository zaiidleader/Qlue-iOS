//
//  CommentDetailViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import ActiveLabel

class CommentDetailViewCell: UITableViewCell {

    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellComment: ActiveLabel!
    @IBOutlet weak var cellUsername: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
