//
//  QlueChatGroupTextRightCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueChatGroupTextRightCell: UITableViewCell {
     
     @IBOutlet weak var cellUsername: UILabel!
     @IBOutlet weak var cellTextLabel: UILabel!
     @IBOutlet weak var cellTime: UILabel!
     @IBOutlet weak var cellTextWrapper: UIView!{
          didSet{
               cellTextWrapper.cornerRadius(10)
          }
     }
     
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
