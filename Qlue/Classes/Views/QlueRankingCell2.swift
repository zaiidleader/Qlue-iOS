//
//  QlueRankingCell2.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 12/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueRankingCell2: UITableViewCell {
    
    @IBOutlet weak var cellRankView: UIView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellRankLabel: UILabel!
    @IBOutlet weak var cellKelurahanLabel: UILabel!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellPoint: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellRankView.cornerRadius(cellRankView.bounds.width / 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
