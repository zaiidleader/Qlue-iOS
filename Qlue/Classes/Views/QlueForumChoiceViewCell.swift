//
//  QlueForumChoiceViewCell.swift
//  Qlue
//
//  Created by Andi on 8/20/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueForumChoiceViewCellDelegate {
    func cancelTapped(_ cell: QlueForumChoiceViewCell)
    func choiceDidChange(_ cell: QlueForumChoiceViewCell, text: String?)

}
class QlueForumChoiceViewCell: UITableViewCell, UITextFieldDelegate {

    var delegate : QlueForumChoiceViewCellDelegate?
    @IBOutlet weak var cellChoice: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Action
    @IBAction func cancelButton(_ sender: UIButton) {
        
        if let delegate = delegate{
            delegate.cancelTapped(self)
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let delegate = delegate{
          let fullText = textField.text ?? ""
          delegate.choiceDidChange(self,text: fullText + string)
        }
        
        return true
    }
    
}
