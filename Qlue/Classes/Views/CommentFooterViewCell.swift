//
//  CommentFooterViewCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
protocol CommentFooterViewCellDelegate {
     func readAllReview(_ cell: CommentFooterViewCell)
}

class CommentFooterViewCell: UITableViewCell {

     var delegate: CommentFooterViewCellDelegate?
     
     @IBOutlet weak var readAll: UIButton!{
          didSet{
               readAll.cornerRadius(20)
          }
     }
     
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
     
     @IBAction func allReviewTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.readAllReview(self)
          }
     }

}
