//
//  QlueSearchViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/15/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueSearchViewCell: UITableViewCell {

    @IBOutlet weak var cellDesc: UILabel!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
     override func prepareForReuse() {
          cellDesc.text = ""
          cellName.text = ""
          cellImage.image = nil
          cellImage.contentMode = .scaleAspectFill
          cellImage.cornerRadius(5)
     }
}
