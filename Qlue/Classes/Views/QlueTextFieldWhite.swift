//
//  QlueTextFieldWhite.swift
//  Qlue
//
//  Created by Nurul on 5/27/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueTextFieldWhite: UITextField {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        self.textColor = UIColor.white
        self.font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 15)
        
        // placeholder
        if let pc = placeholder, let font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 15){
            self.attributedPlaceholder = NSAttributedString(string: pc, attributes: [NSFontAttributeName : font,
                NSForegroundColorAttributeName: UIColor.white])
        }
        
        
        //bottom line
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.height - 1, width: self.frame.width, height: 1)
        bottomLine.backgroundColor = UIColor.white.cgColor
        self.borderStyle = .none
        self.layer.addSublayer(bottomLine)
        
    }
    
    
    
}
