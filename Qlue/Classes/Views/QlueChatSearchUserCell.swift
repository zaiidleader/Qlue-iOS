//
//  QlueChatSearchUserCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/11/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueChatSearchUserCell: UITableViewCell {

     @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellIndicator: UIView!{
        didSet{
            cellIndicator.border(1, color: kQlueBlue)
            
        }
    }
     @IBOutlet weak var cellImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        cellIndicator.backgroundColor = UIColor.clear
    }

}
