//
//  QlueCommentViewCell.swift
//  Qlue
//
//  Created by Nurul on 5/29/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import ActiveLabel

protocol QlueCommentViewCellDelegate{
    func commentAvatarTapped(_ cell: QlueCommentViewCell)
}

class QlueCommentViewCell: UITableViewCell, UITextViewDelegate {

    var delegate: QlueCommentViewCellDelegate?
    
    @IBOutlet weak var cellLabel: UITextView!
    @IBOutlet weak var cellAvatar: UIImageView!
    @IBOutlet weak var cellTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        cellLabel.fo nt = UIFont.init(name: QlueFont.HelveticaNeue, size: 15)
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Action
    @IBAction func avatarTapped(_ sender: UIButton) {
        
        if let delegate = delegate{
            delegate.commentAvatarTapped(self)
        }
    }
    
     // MARK: - UITextViewDelegate
     func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
          
          if let scheme = URL.scheme {
               switch scheme {
               case "hash":
                    print("hastag")
               case "mention":
                    print("mention")
               default:
                    print("just a regular url")
               }
          }
          
          return true
    }
    
}
