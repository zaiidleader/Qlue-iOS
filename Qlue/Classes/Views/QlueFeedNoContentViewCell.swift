//
//  QlueFeedNoContentViewCell.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 12/2/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueFeedNoContentViewCellDelegate {
    func qlueFeedNoContentViewCellPostButtonTapped(_ cell: QlueFeedNoContentViewCell)
}

class QlueFeedNoContentViewCell: UICollectionViewCell {
    var delegate: QlueFeedNoContentViewCellDelegate?
    
    @IBAction func postButtonTapped(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.qlueFeedNoContentViewCellPostButtonTapped(self)
        }
    }
}
