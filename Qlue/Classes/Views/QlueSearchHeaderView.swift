//
//  QlueSearchHeaderView.swift
//  Qlue
//
//  Created by Nurul on 6/15/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueSearchHeaderViewDelegate {
    func searchAllTapped(_ cell: QlueSearchHeaderView)
}

class QlueSearchHeaderView: UIView {
    
    var delegate: QlueSearchHeaderViewDelegate?
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func seeAllTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.searchAllTapped(self)
        }
    }

}
