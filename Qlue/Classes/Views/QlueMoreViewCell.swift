//
//  QlueMoreViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueMoreViewCell: UITableViewCell {

    @IBOutlet weak var cellTimer: UILabel!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
