//
//  QlueSwastaSearchViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/25/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueSwastaSearchViewCell: UITableViewCell {
    
    @IBOutlet weak var cellDesc: UILabel!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
     override func prepareForReuse() {
          cellDesc.text = ""
          cellLabel.text = ""
          cellImage.image = nil
     }
}
