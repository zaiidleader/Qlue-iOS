//
//  QlueFeedReviewViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueFeedReviewViewCellDelegate {
     func sortTapped(_ cell: QlueFeedReviewViewCell, button: UIButton)
    func readAllReview(_ cell: QlueFeedReviewViewCell)
}

class QlueFeedReviewViewCell: UITableViewCell {

    var delegate: QlueFeedReviewViewCellDelegate?
    
    @IBOutlet weak var readAll: UIButton!{
        didSet{
            readAll.cornerRadius(20)
        }
    }
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellTableView: UITableView!
    @IBOutlet weak var cellTableViewHeight: NSLayoutConstraint!
     @IBOutlet weak var createButton: UIButton!{
          didSet{
               createButton.cornerRadius(20)
               createButton.border(1, color: kQlueBlue)
          }
     }
     
     var comments : [QlueComment] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cellTableView.register(UINib.init(nibName: "QlueFeedReviewItemViewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Actions
    @IBAction func allReviewTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.readAllReview(self)
        }
    }
    @IBAction func sortTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.sortTapped(self, button: sender)
        }
    }
     
     @IBAction func createTapped(_ sender: UIButton) {
          if let delegate = delegate{
               delegate.readAllReview(self)
          }
     }
    
    
}

// MARK: - UITableViewDataSource
extension QlueFeedReviewViewCell: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! QlueFeedReviewItemViewCell
     
     let comment = comments[indexPath.row]
     
     if let image = comment.avatar?.encodeURL(){
          cell.cellImageView.sd_setImage(with: image as URL!)
     }
     
     cell.cellName.text = comment.username
     cell.cellTime.text = comment.timestamp?.timeAgoWithNumericDates(false)
     
     let  mentionText = Util.replaceIdWithUsername(comment)
     cell.cellComment.text = Util.convertEmojiFromUnicode(mentionText)
     
     cell.cellComment.handleMentionTap { (username) in
          let atSelected = comment.at.filter({$0.username == username}).first
          let appDelegate = UIApplication.shared.delegate as! AppDelegate
          if let vc = appDelegate.window?.rootViewController{
               vc.presentOtherProfile(atSelected?.idno)
          }
     }
     
        return cell
    }
    
    
    
}

// MARK: - UITableViewDelegate
extension QlueFeedReviewViewCell: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
}
