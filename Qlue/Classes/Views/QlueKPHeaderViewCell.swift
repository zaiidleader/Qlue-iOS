//
//  QlueKPHeaderViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/8/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueKPHeaderViewCellDelegate{
    func leaveTapped(_ cell: QlueKPHeaderViewCell )
    func diskusiTapped(_ cell: QlueKPHeaderViewCell)
    func laporanTapped(_ cell: QlueKPHeaderViewCell)
    func membersTapped(_ cell: QlueKPHeaderViewCell)
}

class QlueKPHeaderViewCell: UITableViewCell {

    var delegate: QlueKPHeaderViewCellDelegate?
    
    @IBOutlet weak var cellBanner: UIImageView!
    @IBOutlet weak var cellFullname: UILabel!
    @IBOutlet weak var cellKelurahanName: UILabel!
    @IBOutlet weak var cellTrophy: UIButton!{
        didSet{
            cellTrophy.border(1, color: UIColor.lightGray)
            cellTrophy.cornerRadius(5)
        }
    }
    @IBOutlet weak var cellMemberLabel: UILabel!
    @IBOutlet weak var cellDiskusiLabel: UILabel!
    @IBOutlet weak var cellLaporanLabel: UILabel!
    @IBOutlet weak var cellLeaveButton: UIButton!{
        didSet{
            cellLeaveButton.cornerRadius(5)
        }
    }
    @IBOutlet weak var cellProfileImage: UIImageView!{
        didSet{
            cellProfileImage.cornerRadius(10)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Action
    @IBAction func leaveTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.leaveTapped(self)
        }
        
    }
    
    @IBAction func diskusiTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.diskusiTapped(self)
        }
        
    }
    @IBAction func laporanTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.laporanTapped(self)
        }
        
    }
    @IBAction func membersTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.membersTapped(self)
        }
    }
    
}
