//
//  MentionTableView.swift
//  cobaemoji
//
//  Created by Nurul Rachmawati on 8/27/16.
//  Copyright © 2016 DyCode. All rights reserved.
//

import UIKit

protocol MentionTableViewDelegate {
    func mentionDidSelect(_ view: MentionTableView, text: String)
}

class MentionTableView: UITableView , UITableViewDataSource, UITableViewDelegate{
    
    var mentionDelegate: MentionTableViewDelegate?
    
    var mentionOri = ["ana", "aka", "b", "c", "daa", "sdfer", "hil", "laui"]
    var mentions : [String] = []
    
    override func awakeFromNib() {
        self.dataSource = self
        self.delegate = self
        
        self.register(UINib.init(nibName: "MentionCell", bundle: nil), forCellReuseIdentifier: "Cell")
    }

    
    // MARK: - Helper

//    func show(){
//        UIView.animateWithDuration(0.5) { 
//            self.hidden = false
//        }
//    }
//    
//    func hide(){
//        UIView.animateWithDuration(0.5) { 
//            self.hidden = true
//        }
//    }
    
    func reloadTableWithMention(_ text: String, isStart: Bool = false){
        let txt = text.replacingOccurrences(of: "@", with: "")
        let mentionsFilter = mentionOri.filter { (mention) -> Bool in
            if mention.hasPrefix(txt){
                return true
            }
            return false
        }
        
        if isStart {
            mentions = mentionOri
        } else{
            mentions = mentionsFilter
        }
        
        self.reloadData()
        
    }
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mentions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MentionCell
        
        cell.label.text = mentions[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let delegate = mentionDelegate{
            delegate.mentionDidSelect(self, text: mentions[indexPath.row] + " ")
        }
    }

}
