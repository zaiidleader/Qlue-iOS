//
//  QlueChatGroupCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/11/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueChatGroupCell: UITableViewCell {

    @IBOutlet weak var cellImage1: UIImageView!
    @IBOutlet weak var cellImage2: UIImageView!
    @IBOutlet weak var cellImage3: UIImageView!
    @IBOutlet weak var cellImage4: UIImageView!
    
    @IBOutlet weak var cellUsername: UILabel!
    @IBOutlet weak var cellTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
