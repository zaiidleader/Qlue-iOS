//
//  QlueFavoritCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/2/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueFavoritCell: UITableViewCell {
     
     @IBOutlet weak var cellImage: UIImageView!
     @IBOutlet weak var cellTime: UILabel!
     @IBOutlet weak var cellTitle: UILabel!
     
     override func awakeFromNib() {
          super.awakeFromNib()
          // Initialization code
          self.layoutIfNeeded()
          cellImage.cornerRadius(cellImage.bounds.width / 2)
     }
     
     override func setSelected(_ selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
          
          // Configure the view for the selected state
     }
     
}
