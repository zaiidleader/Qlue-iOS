//
//  QlueSelectTopicViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/19/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueSelectTopicViewCell: UICollectionViewCell {

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellImage: UIImageView!{
        didSet{
            cellImage.cornerRadius(10)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        cellTitle.text = ""
        cellImage.image = nil
    }

}
