//
//  ChatSearchToken.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/11/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import NWSTokenView

open class ChatSearchToken: NWSToken {
     @IBOutlet weak var titleLabel: UILabel!
     
     
     open class func initWithTitle(_ title: String?) -> ChatSearchToken?
     {
          if let token = UINib(nibName: "ChatSearchToken", bundle:nil).instantiate(withOwner: nil, options: nil)[0] as? ChatSearchToken
          {
               token.cornerRadius(5)
               token.backgroundColor = kQlueBlue
               let oldTextWidth = token.titleLabel.bounds.width
               token.titleLabel.text = title
               token.titleLabel.sizeToFit()
               token.titleLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
               let newTextWidth = token.titleLabel.bounds.width

               // Resize to fit text
               token.frame.size = CGSize(width: token.frame.size.width+(newTextWidth-oldTextWidth), height: token.frame.height)
               token.setNeedsLayout()
               token.frame = token.frame
               
               return token
          }
          return nil
     }
}
