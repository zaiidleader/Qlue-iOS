//
//  QlueKTrophyViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueKTrophyViewCell: UICollectionViewCell {

    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellPoint: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
