//
//  QlueChatViewCell.swift
//  Qlue
//
//  Created by Nurul on 6/13/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueChatViewCell: UITableViewCell {

    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellUsername: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
