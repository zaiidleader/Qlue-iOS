//
//  QlueChatImageLeftCell.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueChatImageLeftCell: UITableViewCell {
     @IBOutlet weak var cellImage: UIImageView!{
          didSet{
               cellImage.cornerRadius(10)
          }
     }
     @IBOutlet weak var cellTime: UILabel!
     @IBOutlet weak var cellImageWrapper: UIView!{
          didSet{
               cellImageWrapper.cornerRadius(10)
          }
     }
     @IBOutlet weak var cellAvatar: UIImageView!
     @IBOutlet weak var cellAvatarWrapper: UIView!{
          didSet{
               cellAvatarWrapper.layer.cornerRadius = 45 / 2
               cellAvatarWrapper.layer.shadowColor = UIColor.darkGray.cgColor
               cellAvatarWrapper.layer.shadowOffset = CGSize(width: 1, height: 2)
               cellAvatarWrapper.layer.shadowOpacity = 0.5
          }
     }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
