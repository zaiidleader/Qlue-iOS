//
//  QlueKLDetailController.swift
//  Qlue
//
//  Created by Nurul on 6/10/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON

class QlueKLDetailController: QlueViewController {
     
     @IBOutlet weak var collectionView: UICollectionView!
     var code: String = ""
     var district: String = ""
     var category: String  = ""
     
     var page = 0
     
     var feeds: [QlueFeed] = []
     override func viewDidLoad() {
          super.viewDidLoad()
          
          // Do any additional setup after loading the view.
          collectionView.register(UINib.init(nibName: "QlueKLDetailViewCell", bundle: nil), forCellWithReuseIdentifier: "DetailCell")
          
          downloadData()
          DejalActivityView.addActivityView(for: self.view)
          collectionView.addInfiniteScroll { (_) in
               self.loadMore()
          }
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
     
     override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
          collectionView.contentOffset = CGPoint.zero
          collectionView.contentInset = UIEdgeInsets.zero

     }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     func initWithCode(_ code: String?, district: String?, category: String?){
          self.code = code ?? ""
          self.district = district ?? ""
          self.category = category ?? ""
     }
     
     // MARK: - Data
     func loadMore(){
          page += 1
          downloadData()
     }
     fileprivate func downloadData(){
          let start = page * 25
          let finish = (page + 1) * 25
          
          let req = Engine.shared.gridLaporanKelurahan(withCodeKel: code, district: district, category: category, start: start, finish: finish)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               self.collectionView.finishInfiniteScroll()
               DejalActivityView.remove()
               }, completion:  { (data, success, fail) in
                    if success == true{
                         
                         if self.page == 0{
                              self.feeds = []
                         }
                         
                         for d in data.arrayValue{
                              self.feeds.append(QlueFeed.feedWithData(d))
                         }
                         
                         self.collectionView.reloadData()
                    }
          })
     }
     
}

// MARK: - UICollectionViewDataSource
extension QlueKLDetailController: UICollectionViewDataSource{
     func numberOfSections(in collectionView: UICollectionView) -> Int {
          return 1
     }
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return feeds.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailCell", for: indexPath) as! QlueKLDetailViewCell
          
          let feed = feeds[indexPath.item]
          
          if let state = feedState(rawValue: feed.progress ?? "process"){
               cell.cellImage.backgroundColor = state.colorGrid
               cell.cellImage.alpha = 0.6
          }
          
          if let file = feed.fileUrl?.encodeURL(){
               cell.cellImageFeed.sd_setImage(with: file as URL!)
               
          }
          
          return cell
     }
}

// MARK: - UICollectionViewDelegate
extension QlueKLDetailController: UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          presentDetailFeed(withFeedId: feeds[indexPath.item].feedId, nav: self.navigationController)
     }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueKLDetailController: UICollectionViewDelegateFlowLayout{
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          let size: CGFloat = (collectionView.bounds.width / 3) - 1
          
          return CGSize(width: size, height: size)
          
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return 1
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 1
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsets.zero
     }
     
     
}
