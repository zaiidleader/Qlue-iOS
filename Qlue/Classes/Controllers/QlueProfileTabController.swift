//
//  QlueProfileTabController.swift
//  Qlue
//
//  Created by Nurul on 7/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CarbonKit

class QlueProfileTabController: QlueViewController, CarbonTabSwipeNavigationDelegate {
     
    @IBOutlet weak var gemButton: UIButton!{
        didSet{
            gemButton.cornerRadiusWithShadow()
        }
    }
    
    
     let items: [UIImage] = [
          UIImage.init(named: "ic_p_profile")!,
          UIImage.init(named: "ic_p_qlues")!,
          UIImage.init(named: "ic_p_trophy")!,
          UIImage.init(named: "ic_p_forum")!
     ]
     override func viewDidLoad() {
          super.viewDidLoad()
          
          setupTabView()
          
          // Do any additional setup after loading the view.
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Private Func
     fileprivate func setupTabView(){
          let carbonTabSwipeNavigation = CarbonTabSwipeNavigation.init(items: items, delegate: self)
          
          carbonTabSwipeNavigation.insert(intoRootViewController: self)
          carbonTabSwipeNavigation.toolbarHeight.constant = 50.0
          carbonTabSwipeNavigation.setIndicatorColor(kCreamColor)
          
          carbonTabSwipeNavigation.toolbar.setBackgroundImage(UIImage(named: "bgNavbar"), forToolbarPosition: UIBarPosition.any, barMetrics: UIBarMetrics.default)
          carbonTabSwipeNavigation.toolbar.shadowWithColor(UIColor.clear)
          
          for i in 0..<items.count{
               carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width / 4, forSegmentAt: i)
          }
          
          carbonTabSwipeNavigation.carbonSegmentedControl?.imageNormalColor = kCreamColor
          carbonTabSwipeNavigation.carbonSegmentedControl?.imageSelectedColor = kCreamColor
          
         
     }
     
     // MARK: - CarbonTabSwipeNavigationDelegate
     func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
          
          
          switch index {
          case 0:
               if let userProfile = profileStoryboard.instantiateViewController(withIdentifier: "ProfileUser") as? QlueProfileViewController{
                    return userProfile
               }
          case 1:
               if let report = profileStoryboard.instantiateViewController(withIdentifier: "ProfileReport") as? QlueProfileReportController{
                    
                    report.userId = User.currentUser()?.userId
                    return report
               }
          case 2:
               if let thropy = profileStoryboard.instantiateViewController(withIdentifier: "ProfileThropy") as? QlueProfileThropyController{
                    
                    thropy.userId = User.currentUser()?.userId
                    return thropy
               }
          default:
          if let forum = profileStoryboard.instantiateViewController(withIdentifier: "ProfileForum") as? QlueProfileForumController{
               
               forum.userId = User.currentUser()?.userId
               
               return forum
               }
          }
          
          return UIViewController()
          
     }
}

extension QlueViewController{
    func presentProfileDetail(_ nav: UINavigationController? = nil){
    
        if let profile = profileStoryboard.instantiateViewController(withIdentifier: "ProfileMain") as? QlueProfileTabController{
            
            if let nav = nav{
                nav.pushViewController(profile, animated: true)
            } else{
                present(profile, animated: true, completion: nil)
            }
        }
        
    }
}
