//
//  QlueFABMenuController.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 12/26/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueFABMenuControllerDelegate {
    func qlueFABMenuControllerFABGeneralButtonTapped(_ viewController: QlueFABMenuController)
    func qlueFABMenuControllerFABHelpButtonTapped(_ viewController: QlueFABMenuController)
    func qlueFABMenuControllerFABAskButtonTapped(_ viewController: QlueFABMenuController)
    func qlueFABMenuControllerFABReportButtonTapped(_ viewController: QlueFABMenuController)
    func qlueFABMenuControllerFABReviewButtonTapped(_ viewController: QlueFABMenuController)
    func qlueFABMenuControllerFABMarketButtonTapped(_ viewController: QlueFABMenuController)
}

class QlueFABMenuController: UIViewController {
    var delegate: QlueFABMenuControllerDelegate?
    
    @IBOutlet weak var fabGeneralButton: UIButton!
    @IBOutlet weak var fabHelpButton: UIButton!
    @IBOutlet weak var fabAskButton: UIButton!
    @IBOutlet weak var fabReportButton: UIButton!
    @IBOutlet weak var fabReviewButton: UIButton!
    @IBOutlet weak var fabMarketButton: UIButton!
    
    @IBOutlet weak var closeButton: UIButton!
    var selectedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - Helpers
    
    func setupViews() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        selectedButton = closeButton
    }
    
    // MARK: - Actions
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        selectedButton = sender
        
        if let navigationController = navigationController {
            navigationController.popViewController(animated: true)
        }
        else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func fabButtonTapped(_ sender: UIButton) {
        selectedButton = sender
        
        if let delegate = delegate {
            
            switch sender {
            case fabGeneralButton:
                delegate.qlueFABMenuControllerFABGeneralButtonTapped(self)
            case fabHelpButton:
                delegate.qlueFABMenuControllerFABHelpButtonTapped(self)
            case fabAskButton:
                delegate.qlueFABMenuControllerFABAskButtonTapped(self)
            case fabReportButton:
                delegate.qlueFABMenuControllerFABReportButtonTapped(self)
            case fabReviewButton:
                delegate.qlueFABMenuControllerFABReviewButtonTapped(self)
            case fabMarketButton:
                delegate.qlueFABMenuControllerFABMarketButtonTapped(self)
            default:
                break
            }
        }
    }
    
}

// MARK: - UIViewController
extension UIViewController {
    
    func presentFABMenu(_ nav: UINavigationController?, delegate: QlueFABMenuControllerDelegate) {
        
        if let fabMenu = fabStoryboard.instantiateViewController(withIdentifier: "FABMenu") as? QlueFABMenuController {
            fabMenu.delegate = delegate
            fabMenu.modalTransitionStyle = .crossDissolve
            
            if let nav = nav {
                nav.pushViewController(fabMenu, animated: true)
                return
            }
            present(fabMenu, animated: true, completion: nil)
        }
    }
}
