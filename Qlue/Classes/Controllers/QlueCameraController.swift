//
//  QlueCameraController.swift
//  Qlue
//
//  Created by Nurul on 6/19/16.
//  Copyright © 2016 Qlue. All rights reserved.
//
import UIKit
import CameraManager

class QlueCameraController: QlueViewController{
    
    // MARK: - Outlets
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var devicePositionButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var progressBar: UIProgressView!
    
    // MARK: - Vars
    fileprivate var cameraManager =  CameraManager()
    var isRecording: Bool = false
    var didBackTapped: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCamera()
        
        // gesture capture button
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(QlueCameraController.captureImage))
        let longGesture = UILongPressGestureRecognizer.init(target: self, action: #selector(QlueCameraController.captureVideo))
        tapGesture.numberOfTapsRequired = 1
        captureButton.addGestureRecognizer(tapGesture)
        captureButton.addGestureRecognizer(longGesture)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cameraManager.cameraOutputMode = .stillImage
        DejalActivityView.remove()
//        DejalBezelActivityView.removeViewAnimated(true)
        
    }
    // MARK: - Helper
    func stopRecording(){
        self.progressBar.progress = 0
        
        isRecording = false
        cameraManager.stopVideoRecording({ (videoURL, error) -> Void in
            DejalBezelActivityView.remove(animated: true)
            DejalActivityView.remove()
            
            if let url = videoURL {
                self.presentPreviewImageWithMedia(Media.video(url: url), nav: self.navigationController)
            }
            
            if let errorOccured = error {
                self.cameraManager.showErrorBlock("Error occurred", errorOccured.localizedDescription)
            }
        })

    }
    func setupCamera(){
        
        let currentCameraState = cameraManager.currentCameraStatus()
        
        if currentCameraState == .notDetermined {
          cameraManager.askUserForCameraPermission({ permissionGranted in
               if permissionGranted {
                    self.addCameraToView()
               }
          })
        } else if (currentCameraState == .ready) {     
            self.addCameraToView()
                    }
        if !cameraManager.hasFlash {
                        flashButton.isEnabled = false
            //            flashModeButton.setTitle("No flash", forState: UIControlState.Normal)
        }
        
    }
     
     func addCameraToView(){
          cameraManager.addPreviewLayerToView(previewView)
          
          cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
               
               let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
               alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alertAction) -> Void in  }))
               
               self?.present(alertController, animated: true, completion: nil)
          }

     }
    
    // MARK: - Action
    override func backTapped(_ sender: AnyObject) {
        super.backTapped(sender)
        if let didBackTapped = didBackTapped{
            didBackTapped()
        }
    }
    func captureImage(){
        
        if isRecording == true{
            DejalBezelActivityView.addActivityView(for: self.view)
            stopRecording()
            return
            
        } else{
            captureButton.isEnabled = false
        }
        
        cameraManager.cameraOutputMode = .stillImage
        cameraManager.capturePictureWithCompletion({ (image, error) -> Void in
            
            self.captureButton.isEnabled = true
            
            if let errorOccured = error {
                self.cameraManager.showErrorBlock("Error occurred", errorOccured.localizedDescription)
            }
            else if let image = image {
               let fixedImage = image.fixImageOrientation()
               self.presentPreviewImageWithMedia(Media.photo(image: fixedImage), nav: self.navigationController)
            }
        })
        
        
    }
    func captureVideo(){
        if isRecording == false{
            cameraManager.cameraOutputMode = .videoOnly
            isRecording = true
            
            cameraManager.startRecordingVideo()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(10 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
                DejalBezelActivityView.addActivityView(for: self.view)
                self.stopRecording()
            })
            self.progressBar.setProgress(0, animated: true)
            UIView.animate(withDuration: 10, animations: {
                self.progressBar.setProgress(10, animated: true)
            })
            
        }
        
    }
    
    @IBAction func flipTapped(_ sender: UIButton) {
        cameraManager.cameraDevice = cameraManager.cameraDevice == CameraDevice.front ? CameraDevice.back : CameraDevice.front
        switch (cameraManager.cameraDevice) {
        case .front:
            sender.setImage(UIImage.init(named: "ic_camerafront"), for: UIControlState())
        case .back:
            sender.setImage(UIImage.init(named: "ic_camerarear"), for: UIControlState())
        }

    }
    
    @IBAction func flashToggle(_ sender: UIButton) {
        switch (cameraManager.changeFlashMode()) {
        case .off:
            sender.setImage(UIImage.init(named: "ic_flashoff"), for: UIControlState())
        case .on:
            sender.setImage(UIImage.init(named: "ic_flashon"), for: UIControlState())
        case .auto:
            sender.setImage(UIImage.init(named: "ic_flashauto"), for: UIControlState())
        }
    }
}

extension QlueViewController{
    func presentCameraView(_ nav: UINavigationController?, completion: (()-> Void)? = nil) {
        if let camera = reportStoryboard.instantiateViewController(withIdentifier: "Camera") as? QlueCameraController{
            
            camera.didBackTapped = completion
            
            if let nav = nav{
                nav.pushViewController(camera, animated: true)
                return
            }
            
            present(camera, animated: true, completion: nil)
        }
    }
}

