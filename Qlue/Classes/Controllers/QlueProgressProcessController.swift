//
//  QlueProgressProcessController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 8/29/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueProgressProcessController: QlueViewController {

     
    @IBOutlet weak var wrapView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
     
    var progress: QlueFeedProgress?
    var feedId: Int = 0
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        for v in wrapView.subviews{
            if let label = v as? UILabel{
                label.text = ""
            }
        }
        
     downloadData()
     DejalActivityView.addActivityView(for: wrapView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     // MARK: - Data
     fileprivate func downloadData(){
          
          let req = Engine.shared.getFeedProgressDetail(feedId, type: "process")
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalBezelActivityView.remove()
               },completion:  { (data, success, fail) in
                    if success == true{
                         self.progress = QlueFeedProgress.init(fromJson: data)
                         self.configureView()
                    }
          })
          
     }
     
     // MARK: - Configure
     fileprivate func configureView(){
          nameLabel.text = progress?.name
          usernameLabel.text = progress?.username
          descriptionLabel.text = progress?.descriptionField
          timeLabel.text = Date().getDateFromTimestamp(progress?.timestamp)?.timeAgoWithNumericDates(true)
     }


    
}

extension QlueViewController{
     func presentProgressProcess(withFeedId id: Int){
          let progressVc = QlueProgressProcessController()
          progressVc.feedId = id
          semiModalWithViewController(progressVc)
     }
}

