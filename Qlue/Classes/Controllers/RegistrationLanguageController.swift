//
//  RegistrationLanguageController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/20/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class RegistrationLanguageController: RegistrationController {
    
    // MARK: Outlets
    @IBOutlet weak var indonesiaButton: RadioButton!
    @IBOutlet weak var englishButton: RadioButton!
     @IBOutlet weak var japanButton: RadioButton!
     var languageButtons : [RadioButton] = []
    
    var languageSelected = false

    // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
          configureButton()
        
    }
     
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Configure
    func configureButton(){
        nextButton.isEnabled = false
        
        indonesiaButton.cornerRadius(5)
        englishButton.cornerRadius(5)
        japanButton.cornerRadius(5)
     
        languageButtons = [indonesiaButton, englishButton, japanButton]
        for button in languageButtons{
            button.downStateImage = nil
            button.myAlternateButton = languageButtons.filter{$0 != button}
            button.backgroundColorNormal = kGreyEEE
            button.backgroundColorSelected = kBlue0988EEColor
        }
    }
    
    // MARK: - Action
    @IBAction func languageTapped(_ sender: RadioButton) {
        switch sender {
        case indonesiaButton:
            UserDefaults.standard.set(["id"], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
        default:
            UserDefaults.standard.set(["us"], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
        }
        
        nextButton.isEnabled = true
    }

}
