//
//  RegistrationCompleteController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class RegistrationCompleteController: RegistrationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action
    override func nextTapped(_ sender: UIButton) {
        print("\(Engine.shared.createUser)")
     DejalBezelActivityView.addActivityView(for: self.view)
     
     let req = Engine.shared.registration(Engine.shared.createUser)
     
     Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
          DejalActivityView.remove()
          }, completion:  { (data, success, fail) in
               if success == true{
                    Engine.shared.createUser = QlueRegistration()
               Util.showSimpleAlert(data["message"].string, vc: self, completion: { (action) in
                    self.performSegue(withIdentifier: "backToLogin", sender: self)
               })
               }
     })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
