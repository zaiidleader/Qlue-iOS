//
//  ReportUserModalController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON

class ReportUserModalController: QlueViewController {

     // MARK: Var
     var lists : [ReportUser] = []
     var userId: String?
     
     // MARK: Outlet
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.cornerRadius(5)
            tableView.border(1, color: UIColor.lightGray)
        }
    }
     
     // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        viewHeight.constant = 90
          downloadData()
     tableView.delegate = self
     tableView.dataSource = self
     tableView.rowHeight = 40
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     // MARK: - Data
     fileprivate func downloadData(){
          let req = Engine.shared.listChooseReportUser()
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion: { (data, success, fail) in
                    if success == true{
                         self.lists = []
                         for list in data.arrayValue{
                              self.lists.append(ReportUser.init(fromJson: list))
                         }
                         
                         self.tableView.reloadData()
                        let listCount = self.lists.count * 40
                        var heightList = CGFloat(listCount)
                        if CGFloat(listCount - 120) > UIScreen.main.bounds.height{
                            heightList = UIScreen.main.bounds.height - 120
                        }
                        self.viewHeight.constant = 90 + heightList
                    }
          })
     }
     
     fileprivate func reportUser(_ reportId: String?){
          guard let userId = userId, let reportId = reportId else{
               DejalActivityView.remove()
               return
          }
          
          let req = Engine.shared.postChooseReportUser(userId: userId, reportId: reportId)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               },completion:  { (data, success, fail) in
                    if success == true{
                         self.dismiss(animated: true, completion: nil)
                    }
          })
          
          
     }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Table View
extension ReportUserModalController: UITableViewDelegate, UITableViewDataSource{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return lists.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QlueReportUserCell
        
        let list = lists[indexPath.row]
        cell.cellLabel.text = list.reportName
          
          return cell
     }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          DejalBezelActivityView.addActivityView(for: view)
          reportUser(lists[indexPath.row].reportId)
     }
}

//MARK: - navigation
extension QlueViewController{
     func presentReportUserModal(userId pid: String?){
          if let report = profileStoryboard.instantiateViewController(withIdentifier: "ReportUser") as? ReportUserModalController{
               report.userId = pid
               semiModalWithViewController(report)
          }
     }
}
