//
//  QlueTipsController.swift
//  Qlue
//
//  Created by Nurul on 6/4/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class QlueTipsController: QlueViewController {

    @IBOutlet weak var tutorialImage: UIImageView!
    var i = 1
    fileprivate let imageName = [
        "tips_new_ind_6+_1",
        "tips_new_ind_6+_2",
        "tips_new_ind_6+_3",
        "tips_new_ind_6+_4"
    ]
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var getStartedButton: UIButton!{
        didSet{
            getStartedButton.cornerRadius(10)
        }
    }
    
    var dismissWhenDone = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        if dismissWhenDone {
            getStartedButton.setTitle("Close", for: UIControlState())
        }
        
        // Page control
        pageControl.numberOfPages = imageName.count
        
        // Collection View
        registerNib()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.navigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private Func
    fileprivate func registerNib(){

        collectionView.register(UINib.init(nibName: "QlueTipsViewCell", bundle: nil), forCellWithReuseIdentifier: "TipsCell")
        
    }
    
    
    fileprivate func setImageTutorial(){
        tutorialImage.image = UIImage.init(named: "tips_new_ind_6+_\(i)")
    }
    
    
    // MARK: - Action
    @IBAction func getStartedTappes(_ sender: UIButton) {
        if dismissWhenDone {
            dismiss(animated: true, completion: nil)
        }
        else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showMainViewController()
        }
    }
    
    @IBAction func pageValueChanged(_ sender: UIPageControl) {
        
        var scrollPosition = UICollectionViewScrollPosition.right
        
        
        let indexPath = collectionView.indexPathsForVisibleItems.first
        
        if indexPath?.item > sender.currentPage{
            scrollPosition = UICollectionViewScrollPosition.left
        }
        
        self.collectionView.scrollToItem(at: IndexPath(item: sender.currentPage, section: 0), at: scrollPosition, animated: true)
        
    }
    
    override func tap(_ sender: UITapGestureRecognizer) {
        
        
        if i == 4{
            i = 1
            if dismissWhenDone {
                dismiss(animated: true, completion: nil)
            }
            else {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showMainViewController()
            }
            return
        }
        
        i += 1
        setImageTutorial()
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - UICollectioViewDataSource
extension QlueTipsController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageName.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TipsCell", for: indexPath) as! QlueTipsViewCell
        
        cell.cellImage.image = UIImage(named: imageName[indexPath.item])
        
        return cell
    }
    
    
}

// MARK: - UICollectionViewDelegate
extension QlueTipsController: UICollectionViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentIndex = self.collectionView.contentOffset.x / self.collectionView.frame.size.width
        pageControl.currentPage = Int(currentIndex)
        if Int(currentIndex) == imageName.count - 1{
            getStartedButton.isHidden = false
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueTipsController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height + 20)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
}

extension UIViewController {
    
    func presentTips() {
        
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "Tips") as! QlueTipsController
        viewController.dismissWhenDone = true
        present(viewController, animated: true, completion: nil)
    }
}
