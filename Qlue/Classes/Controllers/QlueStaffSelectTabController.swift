//
//  QlueStaffSelectTabController.swift
//  Qlue
//
//  Created by Nurul on 6/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CarbonKit

class QlueStaffSelectTabController: QlueViewController, CarbonTabSwipeNavigationDelegate {
    
    let items: [String] = ["LURAH", "STAF LURAH", "DINAS"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTabView()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private Func
    fileprivate func setupTabView(){
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation.init(items: items, delegate: self)
        
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.toolbarHeight.constant = 40.0
        carbonTabSwipeNavigation.setIndicatorColor(kCreamColor)
        
        carbonTabSwipeNavigation.toolbar.barTintColor = kQlueBlue
        
        
        for i in 0..<items.count{
            carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width / 3, forSegmentAt: i)
        }
        
        if let barFont = UIFont(name: QlueFont.ProximaNovaSoft, size: 12.0) {
            carbonTabSwipeNavigation.carbonSegmentedControl?.setTitleTextAttributes([NSForegroundColorAttributeName: kCreamColor
                ,NSFontAttributeName:barFont], for: UIControlState())
            carbonTabSwipeNavigation.carbonSegmentedControl?.setTitleTextAttributes([NSForegroundColorAttributeName: kCreamColor
                ,NSFontAttributeName:barFont], for: .selected)
            
        }
    }
    
    // MARK: - CarbonTabSwipeNavigationDelegate
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        //        switch index {
        //        case 0:
        //            if let profil: QlueKProfilController = storyboard?.instantiateViewControllerWithIdentifier("KProfil") as? QlueKProfilController{
        //                return profil
        //            }
        //        case 1:
        //            if let laporan: QlueKLaporanController = storyboard?.instantiateViewControllerWithIdentifier("KLaporan") as? QlueKLaporanController{
        //                return laporan
        //            }
        //        case 2:
        //
        //            if let diskusi: QlueKDiskusiController = storyboard?.instantiateViewControllerWithIdentifier("KDiskusi") as? QlueKDiskusiController{
        //                return diskusi
        //            }
        //        case 3:
        //            if let trophy: QlueKTrophyController = storyboard?.instantiateViewControllerWithIdentifier("KTrophy") as? QlueKTrophyController{
        //                return trophy
        //            }
        //        default:
        //            break
        //        }
        
        return UIViewController()
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
