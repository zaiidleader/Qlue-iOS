//
//  QlueMapsController.swift
//  Qlue
//
//  Created by Nurul on 6/30/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON

class QlueMapsController: QlueViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var currentLocationView: UIView! {
        didSet{
            currentLocationView.cornerRadiusWithShadow()
        }
    }
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var currentAddressLabel: UILabel!
    
    @IBOutlet weak var selectedPoiView: UIView! {
        didSet{
            selectedPoiView.shadowWithColor(UIColor.darkGray)
        }
    }
    @IBOutlet weak var poiImageView: UIImageView!
    @IBOutlet weak var poiNameLabel: UILabel!
    @IBOutlet weak var poiAddressLabel: UILabel!
    
    fileprivate var currentLocation: CLLocation?
    fileprivate var fixLocation: CLLocation?
    fileprivate lazy var locationManager: CLLocationManager = {
        
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.delegate = self
        
        return manager
    }()
    
    var selectedCategoryId: Int = 1
    
    @IBOutlet weak var mapsView: GMSMapView!
    var camera: GMSCameraPosition!
    var marker: GMSMarker!
    
    var pois: [QluePoi] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapsView.isMyLocationEnabled = false
        mapsView.delegate = self
        
        mapsView.settings.myLocationButton = true
        
        if let location = fixLocation {
            currentLocation = location
            marker.position = location.coordinate
            setToCurrentLocation()
        }
        else {
            locationManager.startUpdatingLocation()
        }
        
        downloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        navigationController?.navigationBarHidden = true
//        tabBarController?.tabBar.hidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
//        navigationController?.navigationBarHidden = false
//        tabBarController?.tabBar.hidden = false
    }
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        let newLocation = locations[0]
        
        currentLocation = newLocation
        
        setToCurrentLocation()
        downloadData()
    }
    
    // MARK: - Action
    
    @IBAction func filterTapped(_ sender: UIBarButtonItem) {
        presentFilterViewWithDelegate(self)
    }
    
    @IBAction func currentLocationTapped(_ sender: UIButton) {
        setToCurrentLocation()
    }
    
    @IBAction func poiButtonTapped(_ sender: UIButton) {
        
    }
    
    // MARK: - Helper
    
    func setupMarkers() {
        
        if let location = currentLocation {
            mapsView.clear()
            
            var zoom: Float = 15
            if camera != nil {
                zoom = camera.zoom
            }
            
            camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: zoom)
            mapsView.camera = camera
            
            if marker == nil { marker = GMSMarker() }
            marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            marker.appearAnimation = kGMSMarkerAnimationPop
            marker.map = mapsView
            marker.userData = User.currentUser()
            if let userAvatar = User.currentUser()?.avatar {
                if let url = URL(string: userAvatar) {
                    SDWebImageDownloader.shared().downloadImage(with: url, options: [], progress: { (_, _) -> Void in
                        
                        }, completed: { (image, data, error, _) -> Void in
                            let resizedImage = image?.imageWithWidth(30)
                            DispatchQueue.main.async {
                                self.marker.icon = resizedImage
                            }
                    })
                }
            }
            
            for poi in pois {
                if let lat = poi.lat, let lng = poi.lng {
                    
                    let poiMarker = GMSMarker()
                    poiMarker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    poiMarker.appearAnimation = kGMSMarkerAnimationPop
                    poiMarker.map = mapsView
                    poiMarker.userData = poi
                    if let iconUrl = poi.icon {
                        if let url = URL(string: iconUrl) {
                            SDWebImageDownloader.shared().downloadImage(with: url, options: [], progress: { (_, _) -> Void in
                                
                                }, completed: { (image, data, error, _) -> Void in
                                    if let image = image {
                                        let resizedImage = image.imageWithWidth(22)
                                        DispatchQueue.main.async {
                                           poiMarker.icon = resizedImage
                                        }
                                    }
                            })
                        }
                    }
                }
            }
        }
    }
    
    func downloadData() {
        
        if let location = currentLocation {
            
            DejalBezelActivityView.addActivityView(for: view)
            Engine.shared.listPOI(location.coordinate.latitude, lng: location.coordinate.longitude, categoryId: selectedCategoryId).responseString { (response) in
                DejalBezelActivityView.remove(animated: true)
                
                if response.result.isSuccess {
                    if let result = response.result.value {
                        let data = JSON.parse(result)
                        
                        self.pois.removeAll()
                        for poi in data.arrayValue {
                            if poi == nil { continue }
                            self.pois.append(QluePoi.poiWithData(poi))
                        }
                        self.setupMarkers()
                    }
                }
                else if let error = response.result.error {
                    print("error: \(error.localizedDescription)")
                }
                else {
                    
                }
            }
        }
    }
    
    fileprivate func setToCurrentLocation(){
        
        var location: CLLocation!
        if let current = currentLocation {
            location = current
        }
        else {
            location = CLLocation.init(latitude: -6.886166, longitude: 107.537527)
        }
        
        camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15)
        mapsView.animate(to: camera)
        
    }
    
    fileprivate func showCurrentLocationView(with user: User) {
        
        profileImageView.image = nil
        if let avatar = user.avatar {
            if let url = URL(string: avatar) {
                profileImageView.sd_setImage(with: url)
            }
        }
        currentAddressLabel.text = nil
        
        currentLocationView.alpha = 0
        currentLocationView.isHidden = false
        UIView.animate(withDuration: 0.25, animations: { 
            self.currentLocationView.alpha = 1
        }) 
    }
    
    fileprivate func hideCurrentLocationView() {
        
        UIView.animate(withDuration: 0.25, animations: { 
            self.currentLocationView.alpha = 0
            }, completion: { (finished) in
                self.currentLocationView.isHidden = true
                self.currentLocationView.alpha = 1
        }) 
    }
    
    fileprivate func showSelectedPoiView(with poi: QluePoi) {
        
        poiImageView.image = nil
        if let icon = poi.icon {
            if let url = URL(string: icon) {
                poiImageView.sd_setImage(with: url)
            }
        }
        poiNameLabel.text = poi.title
        poiAddressLabel.text = poi.address
        
        if selectedPoiView.isHidden {
            
            selectedPoiView.alpha = 0
            selectedPoiView.isHidden = false
            UIView.animate(withDuration: 0.25, animations: {
                self.selectedPoiView.alpha = 1
            }) 
        }
    }
    
    fileprivate func hideSelectedPoiView() {
        
        UIView.animate(withDuration: 0.25, animations: {
            self.selectedPoiView.alpha = 0
        }, completion: { (finished) in
            self.selectedPoiView.isHidden = true
            self.selectedPoiView.alpha = 1
        }) 
    }

    
    // MARK: - GMSMapViewDelegate
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        hideCurrentLocationView()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if let user = marker.userData as? User {
            hideSelectedPoiView()
            showCurrentLocationView(with: user)
        }
        else  if let poi = marker.userData as? QluePoi {
            hideCurrentLocationView()
            showSelectedPoiView(with: poi)
        }
        else {
            hideCurrentLocationView()
            hideSelectedPoiView()
        }
        
        return false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension QlueViewController {
    func pushMapsWithLocation(_ location: CLLocation?){
        let maps = QlueMapsController()
        maps.fixLocation = location
        navigationController?.pushViewController(maps, animated: true)
    }
}

extension QlueMapsController: QlueFilterControllerDelegate {
    
    func qlueFilterControllerCompanyButtonTapped(_ viewController: QlueFilterController) {
        selectedCategoryId = 2
        downloadData()
    }
    
    func qlueFilterControllerGovermentButtonTapped(_ viewController: QlueFilterController) {
        selectedCategoryId = 1
        downloadData()
    }
}

// MARK: - Navigation
extension QlueViewController {
    func presentMaps(_ nav: UINavigationController?){
        
        let maps = mainStoryboard.instantiateViewController(withIdentifier: "Maps")
        if let nav = nav {
            nav.pushViewController(maps, animated: true)
        }
        else {
            let root = UINavigationController(rootViewController: maps)
            present(root, animated: false, completion: nil)
        }
    }
}
