//
//  QlueShopTabController.swift
//  Qlue
//
//  Created by Nurul on 6/24/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class QlueShopTabController: QlueViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(QlueShopTabController.refresh(_:)), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        return refreshControl
    }()
    
    var avatars: [QlueAvatar] = []
    lazy var diamondButton: UIButton = {
        let button = UIButton(type: .custom)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        button.setTitleColor(UIColor.darkGray, for: UIControlState())
        button.setImage(UIImage(named: "ic_gem"), for: UIControlState())
        button.setTitle(" 0", for: UIControlState())
        button.frame = CGRect(x: 0, y: 0, width: 76, height: 34)
        
        button.backgroundColor = UIColor.white
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1
        
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let diamondBarButtonItem = UIBarButtonItem(customView: diamondButton)
        navigationItem.rightBarButtonItem = diamondBarButtonItem
        
        if let diamond = User.currentUser()?.diamond {
            diamondButton.setTitle(" \(diamond)", for: UIControlState())
        }
        
        tableView.estimatedRowHeight = 91
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
        
        tableView.addInfiniteScroll { (tableView) in
            self.downloadData(self.avatars.count, refresh: false)
        }
        
        refreshControl.beginRefreshing()
        downloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    // MARK: - Actions
    
    func refresh(_ sender: UIRefreshControl) {
        downloadData(0, refresh: true)
    }
    
    // MARK: - Helpers
     
    func downloadData(_ from: Int = 0, refresh: Bool = false) {
        
        var gender = "M"
        if let sex = User.currentUser()?.sex {
           gender = sex
        }
        
        Engine.shared.getAvatarList(from: from, sex: gender).responseString { (response) in
            
            self.refreshControl.endRefreshing()
            self.tableView.finishInfiniteScroll()
            
            if response.result.isSuccess {
                if let result = response.result.value {
                    let data = JSON.parse(result)
                    
                    if data["success"].string == "false" {
                        Util.showSimpleAlert(data["message"].string, vc: self, completion: nil)
                    }
                    else {
                        
                        if refresh { self.avatars = [] }
                        for avatar in data.arrayValue {
                            if avatar == nil { continue }
                            self.avatars.append(QlueAvatar.avatarWithData(avatar))
                        }
                        
                        self.tableView.reloadData()
                    }
                }
            }
            else if let error = response.result.error {
                print("error: \(error.localizedDescription)")
            }
            else {
                
            }
        }
    }
    
    func changeAvatar(_ avatar: QlueAvatar) {
        
        if let avatarUrl = avatar.fileURL {
            
            DejalBezelActivityView.addActivityView(for: view)
            Engine.shared.changeAvatar(avatarUrl).responseString { (response) in
                DejalBezelActivityView.remove(animated: true)
                
                if response.result.isSuccess {
//                    if let result = response.result.value {
//                        let data = JSON.parse(result)
//                        if let user = data.arrayValue.first {
//                           
//                            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//                            let moc = appDelegate.managedObjectContext
//
//                            User.userWithData(user, inManagedObjectContext: moc)
//                            appDelegate.saveContext()
//                        }
//                    }
                    
                    User.currentUser()?.avatar = avatar.fileURL
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.saveContext()
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUserDidUpdateNotification), object: nil, userInfo: nil)
                    
                    avatar.isBuyed = true
                    self.tableView.reloadData()
                }
                else if let error = response.result.error{
                    print("error: \(error.localizedDescription)")
                }
                else {
                    
                }
            }
        }
    }
    
    func buyAvatar(_ avatar: QlueAvatar) {
        
        if let currLevel = User.currentUser()?.level, let level = avatar.level {
            if Int(currLevel) < Int(level) {
                Util.showSimpleAlert("Level anda tidak cukup", vc: self, completion: nil)
                return
            }
        }
        
        if let avatarUrl = avatar.fileURL {
            
            DejalBezelActivityView.addActivityView(for: view)
            Engine.shared.buyAvatar(avatarUrl).responseString { (response) in
                DejalBezelActivityView.remove(animated: true)
                
                if response.result.isSuccess {
                    if let result = response.result.value {
                        let data = JSON(parseJSON: result)
                        if let diamond = data["diamond"].string {
                            self.diamondButton.setTitle(" \(diamond)", for: .normal)
                        }
                    }
                    
                    User.currentUser()?.avatar = avatar.fileURL
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.saveContext()
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUserDidUpdateNotification), object: nil, userInfo: nil)
                    
                    avatar.isBuyed = true
                    self.tableView.reloadData()
                }
                else if let error = response.result.error{
                    print("error: \(error.localizedDescription)")
                }
                else {
                    
                }
            }
        }
    }
}

extension QlueShopTabController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return avatars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarCellId", for: indexPath) as! QlueAvatarShopViewCell
        
        let avatar = avatars[indexPath.row]
        cell.avatarNameLabel.text = avatar.avatarName
        cell.avatarTypeLabel.text = avatar.type
        cell.buyButton.setTitle(" 0", for: UIControlState())
        if let diamond = avatar.diamond {
            cell.buyButton.setTitle(" \(diamond)", for: UIControlState())
        }
        
        cell.avatarImageView.image = nil
        if let fileUrl = avatar.fileURL?.encodeURL() {
            cell.avatarImageView.sd_setImage(with: fileUrl as URL!)
        }
        
        cell.buyButton.isHidden = true
        cell.applyButton.isHidden = true
        if let isBuyed = avatar.isBuyed {
            if !isBuyed {
                cell.buyButton.isHidden = false
            }
            else {
                
                cell.applyButton.isHidden = false
                
                cell.applyButton.backgroundColor = UIColor.white
                cell.applyButton.setTitleColor(UIColor.darkGray, for: UIControlState())
                cell.applyButton.setTitle("PILIH", for: UIControlState())
                
                if let currentAvatar = User.currentUser()?.avatar, let avatar = avatar.fileURL {
                    if let currentAvatarURL = URL(string: currentAvatar), let avatarURL = URL(string: avatar) {
                        if currentAvatarURL.lastPathComponent == avatarURL.lastPathComponent {
                            cell.applyButton.backgroundColor = kQlueBlue
                            cell.applyButton.setTitleColor(UIColor.white, for: UIControlState())
                            cell.applyButton.setTitle("TERPILIH", for: UIControlState())
                        }
                    }
                }
            }
        }
    
        cell.delegate = self
        
        return cell
    }
}

extension QlueShopTabController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension QlueShopTabController: QlueAvatarShopViewCellDelegate {
    
    func qlueAvatarShopViewCellBuyButtonTapped(_ cell: QlueAvatarShopViewCell) {
        
        if let indexPath = tableView.indexPath(for: cell) {
            let avatar = avatars[indexPath.row]
            buyAvatar(avatar)
        }
    }
    
    func qlueAvatarShopViewCellApplyButtonTapped(_ cell: QlueAvatarShopViewCell) {
        
        if let indexPath = tableView.indexPath(for: cell) {
            let avatar = avatars[indexPath.row]
            changeAvatar(avatar)
        }
    }
}
