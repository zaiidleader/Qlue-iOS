//
//  RegistrationController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/20/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class RegistrationController: QlueViewController {

     
     @IBOutlet weak var nextButton: UIButton!{
          didSet{
               nextButton.cornerRadius(5)
          }
     }
     
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          configureNav()
     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     
     // MARK: - Action
     @IBAction func nextTapped(_ sender: UIButton) {
     }
    
     // MARK: - Configure
     func configureNav(){
          self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(), for: UIBarMetrics.default)
          self.navigationController?.navigationBar.barTintColor = UIColor.white
          self.navigationController?.navigationBar.tintColor = kBlue0988EEColor
          Util.setStatusBarBackgroundColor(kBlue0988EEColor)
          self.navigationController?.navigationBar.shadowWithColor(UIColor.lightGray)
          if let font = UIFont.init(name: QlueFont.ProximaNovaRegular, size: 17){
               self.navigationController?.navigationBar.titleTextAttributes =
                    [NSForegroundColorAttributeName:kBlue0988EEColor, NSFontAttributeName:font]
          }
          
          //          self.navigationController?.toolbar.backgroundColor = kBlue0988EEColor
     }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
