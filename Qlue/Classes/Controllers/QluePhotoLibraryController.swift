//
//  QluePhotoLibraryController.swift
//  Qlue
//
//  Created by Nurul on 7/15/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import Photos
import CameraManager

class QluePhotoLibraryController: QlueViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var previewView: UIView!
    
    // MARK: - Vars
    
    fileprivate var cameraManager =  CameraManager()
    
    let cachingImageManager = PHCachingImageManager()
    var assets: [PHAsset] = [] {
        willSet{
            cachingImageManager.stopCachingImagesForAllAssets()
        }
        
        didSet{
            cachingImageManager.startCachingImages(
                for: self.assets,
                targetSize: PHImageManagerMaximumSize,
                contentMode: .aspectFit,
                options: nil)
        }
    }
    
    fileprivate var didTapped: Bool = false
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCamera()
        setupCollectionViewLayout()
        fetchPhotoFromLibrary()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        didTapped = false
    }
    
    // MARK: - Helper
    func fetchPhotoFromLibrary(){
        let options = PHFetchOptions()
        options.sortDescriptors = [
            NSSortDescriptor(key: "creationDate", ascending: false)
        ]
        
        let results = PHAsset.fetchAssets(with: .image, options: options)
        results.enumerateObjects({ (object, index, stop) in
            if let asset = object as? PHAsset{
                self.assets.append(asset)
            }
            if index > 50 {
                stop.pointee = true
            }
        })
        
        collectionView.reloadData()
    }
    
    func setupCollectionViewLayout(){
        //        collectionView.setNeedsLayout()
        //        view.setNeedsLayout()
        view.layoutIfNeeded()
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize.init(width: (collectionView.bounds.width - 20) / 3 , height: (collectionView.bounds.width - 20) / 3)
        flowLayout.minimumInteritemSpacing = 5
        flowLayout.minimumLineSpacing = 5
        flowLayout.sectionInset = UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
        collectionView.collectionViewLayout = flowLayout
    }
    
    func setupCamera(){
        
        let currentCameraState = cameraManager.currentCameraStatus()
        
        if currentCameraState == .notDetermined {
            
        } else if (currentCameraState == .ready) {
            //addCameraToView
            cameraManager.addPreviewLayerToView(previewView)
            
            cameraManager.showErrorBlock = { [weak self] (erTitle: String, erMessage: String) -> Void in
                
                let alertController = UIAlertController(title: erTitle, message: erMessage, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (alertAction) -> Void in  }))
                
                self?.present(alertController, animated: true, completion: nil)
            }
        }
        
        
    }
    
    // MARK: - Action
    @IBAction func takeTapped(_ sender: UIButton) {
        
    }
    
}

extension QluePhotoLibraryController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assets.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        let manager = PHImageManager.default()
        if cell.tag != 0{
            manager.cancelImageRequest(PHImageRequestID(cell.tag))
        }
        
        let asset = assets[indexPath.item]
        
        cell.tag = Int(manager.requestImage(for: asset,
            targetSize: CGSize.init(width: 200, height: 200),
            contentMode: .aspectFill,
            options: nil,
            resultHandler: { (image, _) in
                if let imageView =  cell.viewWithTag(99) as? UIImageView{
                    let fixedImage = image?.fixImageOrientation()
                    imageView.image = fixedImage
                }
        }))
        
        return cell
    }
}
extension QluePhotoLibraryController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        manager.requestImage(for: assets[indexPath.item],
                                     targetSize: CGSize.init(width: 300, height: 300),
                                     contentMode: .aspectFill,
                                     options: options,
                                     resultHandler: { (image, _) in
                                        if self.didTapped == true{
                                            return
                                        }
                                        let fixedImage = image?.fixImageOrientation()
                                        Engine.shared.currentReport?.image = fixedImage
                                        Engine.shared.currentReport?.format = Media.photo(image: fixedImage!)
                                        self.performSegue(withIdentifier: "Topic", sender: self)
                                        
        })
        
        
        didTapped = true
    }
}
