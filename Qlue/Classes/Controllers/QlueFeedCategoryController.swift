//
//  QlueFeedCategoryController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/16/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage
import SwiftyJSON
import ActionSheetPicker_3_0
import FLAnimatedImage
import Alamofire
import PBJVideoPlayer
import Photos


// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class QlueFeedCategoryController: QlueScrollViewController {
     
     // MARK: Outlets
     @IBOutlet weak var collectionView: UICollectionView!
     
     // MARK: Private Var
     fileprivate var feeds : [QlueFeed] = []
     fileprivate var page = 0
     
     // User
     fileprivate var user: User?
     var category: String = ""
    @IBOutlet weak var titleScreen: UILabel!
     var titleCategory: String = ""
     
     // Mark: Refresh
     let refreshControl = UIRefreshControl()
     
     fileprivate var currentLocation: CLLocation?
     
     var selectedItem = 0
     
     // MARK: asset library var
//     var library: ALAssetsLibrary = ALAssetsLibrary()
     
     
     // true: category, false: hashtag
     var isCategory: Bool = true
     
     
     // MARK: - View Life Cycle
     override func viewDidLoad() {
          super.viewDidLoad()
        
        titleScreen.text = titleCategory
        
          //collectionView
          registerNib()
          
          //refresh
          collectionView.addSubview(refreshControl)
          refreshControl.addTarget(self, action: #selector(QlueFeedCategoryController.refresh(_:)), for: .valueChanged)
          DejalActivityView.addActivityView(for: self.collectionView)
          //        refreshControl.beginRefreshing()
          
          
          // location
          setCurrentLocation()
          
          
          // loadmore
          collectionView.addInfiniteScroll { (collectionView) in
               self.loadMore()
          }
     }
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          // getProfileData
          //          navigationController?.navigationBarHidden = true
          loadUserData()
          
     }
     override func viewWillDisappear(_ animated: Bool) {
          super.viewDidDisappear(animated)
          //          navigationController?.navigationBarHidden = false
     }
     
     
     // MARK: - Private Func
     fileprivate func registerNib(){
          collectionView.register(UINib.init(nibName: "QlueFeedViewCell", bundle: nil), forCellWithReuseIdentifier: "FeedCell")
          collectionView.register(UINib.init(nibName: "QlueKelurahanFeedViewCell", bundle: nil), forCellWithReuseIdentifier: "KelurahanCell")
     }
     fileprivate func loadMore(){
          self.page += 1
          downloadData()
     }
     
     // MARK: - Location
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               currentLocation = location
               
               downloadData()
               
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QlueFeedCategoryController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }
     
     // MARK: - Data
     fileprivate func loadUserData(){
          user = User.currentUser()
          
     }
     
     fileprivate func downloadData(){
     
          let start = page * 25
          let finish = (page + 1) * 25
          
          var req: DataRequest!
          
          if isCategory == true{
               req = Engine.shared.getFeedByCategory(category: self.category, codeKel: User.currentUser()?.kelurahan ?? "", start: start, finish: finish, lat: currentLocation!.coordinate.latitude, lng: currentLocation!.coordinate.longitude)
          } else{
               req = Engine.shared.getFeedByHashtag(hashtag: self.category, codeKel: User.currentUser()?.kelurahan ?? "", start: start, finish: finish, lat: currentLocation!.coordinate.latitude, lng: currentLocation!.coordinate.longitude)
          }
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               self.refreshControl.endRefreshing()
               self.collectionView.finishInfiniteScroll()
               }, completion: { (data, success, fail) in
                    if success == true{
                         if self.page == 0{
                              self.feeds = []
                         }
                         
                         for feed in data.arrayValue {
                              if feed == JSON.null {
                                   continue
                              }
                              self.feeds.append(QlueFeed.feedWithData(feed))
                         }
                         
                         self.collectionView.reloadData()
                         
                    }
          })
          
     }
     
     
     
     // MARK: - Action
    
     func refresh(_ sender: UIRefreshControl){
          page = 0
          downloadData()
     }
     
     // MARK: - Helper
     func saveToImageAlbum(withImage image: UIImage){
          // FIXME: - to Swift 3
          
          PHPhotoLibrary.shared().performChanges({ 
               PHAssetChangeRequest.creationRequestForAsset(from: image)
          }) { (success, error) in
               self.saveDone(nil, error: error)
          }
     }
     
     func saveDone(_ assetURL: URL!, error: Error!){
          Util.showSimpleAlert("Save image done", vc: self, completion: nil)
          // FIXME: - to Swift 3
//          library.asset(for: assetURL, resultBlock: self.saveToAlbum, failureBlock: nil)
          
     }
     
     // FIXME: - to Swift 3
//     func saveToAlbum(_ asset:ALAsset!){
//          
//          library.enumerateGroupsWithTypes(ALAssetsGroupAlbum, usingBlock: { group, stop in
//               stop?.pointee = false
//               if(group != nil){
//                    let str = group?.value(forProperty: ALAssetsGroupPropertyName) as! String
//                    if(str == "MY_ALBUM_NAME"){
//                         group!.add(asset!)
//                         let assetRep:ALAssetRepresentation = asset.defaultRepresentation()
//                         let iref = assetRep.fullResolutionImage().takeUnretainedValue()
//                         let image:UIImage =  UIImage(cgImage:iref)
//                         
//                    }
//                    
//               }
//               
//               },
//                                           failureBlock: { error in
//                                             print("NOOOOOOO")
//          })
//          
//     }
     
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "FeedDetailSegue" {
               let destination = segue.destination as? QlueFeedDetailController
               destination?.initWithFeedId(feeds[selectedItem].feedId)
               
          } else if segue.identifier == "CommentSegue"{
               let destination = segue.destination as? QlueCommentController
               if let id = feeds[selectedItem].feedId{
                    destination?.initWithFeedId(String(id))
               }
          }
     }
     
     
}

// MARK: - UICollectionViewDataSource
extension QlueFeedCategoryController: UICollectionViewDataSource{
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                   
          return feeds.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
          
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCell", for: indexPath) as! QlueFeedViewCell
          
          let data = feeds[indexPath.item]
          
          
          cell.cellAvatar.image = nil
          if let avaUrl = data.avatar?.encodeURL(){
               cell.cellAvatar.sd_setImage(with: avaUrl as URL!)
          }
          
          cell.cellName.text = data.username
          
          let date = Date().getDateFromTimestamp(data.timestamp)
          cell.cellTime.text = date?.timeAgoWithNumericDates(true)
          
          cell.cellTitle.text     = data.title
          cell.cellDesc.text      = data.desc
          cell.cellDesc.handleHashtagTap { (hashtag) in
               self.presentFeedByCategory(hashtag, categoryName: hashtag, isCategory: false)
          }
          
          if (data.reportType == .ReviewNegatif ) || (data.reportType == .ReviewPositif ){
               cell.cellPlaceName.text = data.poi?.title
          } else{
               let kel = data.kelurahan ?? ""
               let kec = data.kecamatan ?? ""
               cell.cellPlaceName.text = kel + ", " + kec
          }
          
          cell.cellMainImage.image = nil
          
          if data.format == "image"{
               if let picUrl = data.fileUrl?.encodeURL(){
                    cell.cellMainImage.sd_setImage(with: picUrl as URL!)
               }
               
          }else{
               if let picUrl = data.thumbUrl?.encodeURL(){
                    cell.cellMainImage.sd_setImage(with: picUrl as URL!)
               }
               
          }
          
          
          cell.cellPoiTitle.text = "0 km"
          if let distance = data.distance{
               cell.cellPoiTitle.text = "\(round(Double(distance))) km"
          }
          
          cell.cellCategoryImage.image = nil
          if let labelUrl = data.labelUrl?.encodeURL(){
               cell.cellCategoryImage.sd_setImage(with: labelUrl as URL!)
          }
          
          if data.like == true{
               cell.cellLikeButton.setImage(data.reportType?.likeImageActive, for: UIControlState())
               //            cell.cellLikeButton.enabled = false
          }else{cell.cellLikeButton.setImage(data.reportType?.likeImageDeactive, for: UIControlState())
               //            cell.cellLikeButton.enabled = true
          }
          
          
          cell.cellLikeTitle.text = "0"
          if let like = data.countLike{
               cell.cellLikeTitle.text = String(like)
               
          }
          
          cell.cellCommentTitle.text = "0"
          if let comment = data.countComment{
               cell.cellCommentTitle.text = String(comment)
          }
          
          var commentHeight: CGFloat = 0
          for comment in data.comments{
               
               commentHeight += Util.getHeightCommentInFeedList(Util.getCommentAttributedString(comment.username, comment: comment.comment))
          }
          
          cell.cellCommentHeightConstraint.constant = commentHeight
          
          cell.comments = data.comments
          
          cell.cellSeenButton.setTitle("0", for: UIControlState())
          if let countView = data.countView{
               cell.cellSeenButton.setTitle(String(countView), for: UIControlState())
          }
          
          if let state = data.progressState{
               cell.progressStateView.isHidden = false
               cell.cellGreenButton.setImage(state.image, for: UIControlState())
               cell.stateDetail.text = state.labelDesc
               
          } else{
               cell.progressStateView.isHidden = true
          }
          
          cell.delegate = self
          cell.cellTableView.reloadData()
          
          return cell
     }
     
}

// MARK: - UICollectionViewDelegate
extension QlueFeedCategoryController: UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          
               selectedItem = indexPath.item
              presentDetailFeed(withFeedId: feeds[indexPath.item].feedId, nav: self.navigationController)
          
     }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueFeedCategoryController: UICollectionViewDelegateFlowLayout{
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
        
          let width = collectionView.bounds.width
          let data = feeds[indexPath.item]
          
          if data.postType == "ads" { return CGSize.zero }
          
          let imageHeight : CGFloat = width
          
          
          var height = imageHeight + 71 + 8 + 35 + 50 + 1
          if let title = data.title{
               height += title.heightWithConstrainedWidth(width - 35, font: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 17)!)
          }
          
          if let desc = data.desc{
               height += desc.heightWithConstrainedWidth(width - 35 , font: UIFont.init(name: QlueFont.HelveticaNeue, size: 13)!)
          }
          
          if data.countComment > 0 {
               
               height += 65
               
               for comment in data.comments{
                    
                    height += Util.getHeightCommentInFeedList(Util.getCommentAttributedString(comment.username, comment: comment.comment))
                    
                    
               }
               
          }
          
          return CGSize(width: collectionView.bounds.width, height: height)
     }
}

// MARK: - QlueFeedViewCellDelegate

extension QlueFeedCategoryController: QlueFeedViewCellDelegate{
     
     func videoTapped(_ cell: QlueFeedViewCell, videoPlayer: PBJVideoPlayerController) {
          
     }
     
     func categoryTapped(_ cell: QlueFeedViewCell) {
          
     }
     
     func avatarFeedTapped(_ cell: QlueFeedViewCell) {
          guard let indexPath = collectionView.indexPath(for: cell) else{
               return
          }
          
          let feed = feeds[indexPath.item]
          presentOtherProfile(feed.userId)
          
     }
     
     func avatarCommentTapped(_ feedCell: QlueFeedViewCell, indexComment: Int) {
          
     }
     func countForCommentTableView(_ cell: QlueFeedViewCell) -> Int {
          
          guard let indexPath = collectionView.indexPath(for: cell) else{
               return 0
          }
          
          if let count = feeds[indexPath.item].countPreviewComment{
               return count
          }
          return 0
          
          
     }
     
     func seeAllCommentTapped(_ cell: QlueFeedViewCell) {
          if let indexPath = collectionView.indexPath(for: cell)
          {
               selectedItem = indexPath.item
               
               if let id = feeds[indexPath.item].feedId{
                    presentComment(withId: String(id), nav: self.navigationController)
               }
          }
     }
     
     func commentData(_ cell: QlueFeedViewCell) -> [QlueComment] {
          if let indexPath = collectionView.indexPath(for: cell) {
               return feeds[indexPath.item].comments
          }
          return []
     }
     
     func poiTapped(_ cell: QlueFeedViewCell) {
          if let indexPath = collectionView.indexPath(for: cell) {
               let feed = feeds[indexPath.item]
               if let lat = feed.lat,
                    let lng = feed.lng {
                    
                    let location = CLLocation.init(latitude: CLLocationDegrees.init(lat), longitude: CLLocationDegrees.init(lng))
                    pushMapsWithLocation(location)
               }
          }
     }
     func likeTapped(_ cell: QlueFeedViewCell, button: UIButton, label: UILabel) {
          if let indexPath = collectionView.indexPath(for: cell) {
               guard let postId = feeds[indexPath.item].feedId  else { return }
               let req = Engine.shared.postLike(withFeedId: String(postId))
               
               Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
                    }, completion: { (data, success, fail) in
                         if self.feeds[indexPath.item].like == false{
                              button.setImage(self.feeds[indexPath.item].reportType?.likeImageActive, for: .normal)
                              self.feeds[indexPath.item].like = true
                              
                              if let countLike = self.feeds[indexPath.item].countLike{
                                   self.feeds[indexPath.item].countLike = countLike + 1
                                   label.text = String(countLike + 1)
                              }
                         } else{
                         button.setImage(self.feeds[indexPath.item].reportType?.likeImageDeactive, for: .normal)
                              self.feeds[indexPath.item].like = false
                              
                              if let countLike = self.feeds[indexPath.item].countLike{
                                   self.feeds[indexPath.item].countLike = countLike - 1
                                   label.text = String(countLike - 1)
                              }
                         }
                    }, falseCompletion: {
                         
                         
               })
          }
     }

     
     func kelurahanTapped(_ cell: QlueFeedViewCell) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          
          let feed = feeds[indexPath.item]
          
          if (feed.reportType == .ReviewNegatif ) || (feed.reportType == .ReviewPositif ){
               
               Util.showSimpleAlert("Detail tidak tersedia", vc: self, completion: nil)
               return
          }

          
          if (feed.kdKel == nil)/* || (feed.reportType)*/{
               Util.showSimpleAlert("Detail tidak tersedia", vc: self, completion: nil)
               
               return
          }
          presentKelurahan(withId: feed.kdKel, district: feed.district, nav: self.navigationController)
     }
     
     func seenByTapped(_ cell: QlueFeedViewCell) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          let feed = feeds[indexPath.item]
          presentSeenBy(withId: feed.feedId?.description ?? "", countSeen: feed.countView?.description ?? "", type:  0)
     }
     
     func moreTapped(_ cell: QlueFeedViewCell) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          let feed = feeds[indexPath.item]
          
          let alert = UIAlertController.init(title: "More", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
          
          let nomorKel = UIAlertAction.init(title: "Nomor Laporan", style: .default) { (action) in
               self.presentNomorLaporan(withNoLap: feed.feedId?.description)
          }
          alert.addAction(nomorKel)
          
          let simpanGambar = UIAlertAction.init(title: "Simpan Gambar", style: .default) { (action) in
               if let image = cell.cellMainImage.image{
                    self.saveToImageAlbum(withImage: image)
               }
          }
          alert.addAction(simpanGambar)
          
          
          let cancel = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
          alert.addAction(cancel)
          
          present(alert, animated: true, completion: nil)
     }
     
     func progressTapped(_ cell: QlueFeedViewCell, button: UIButton) {
          
          guard let indexPath = collectionView.indexPath(for: cell) else{
               return
          }
          
          let feed = feeds[indexPath.item]
          
          if feed.progressState == .Complete{
               presentProgressComplete(withFeedId: feed.feedId ?? 0)
          } else if feed.progressState == .Process{
               presentProgressProcess(withFeedId: feed.feedId ?? 0)
          }
          
     }
}


// MARK: - Navigate
extension QlueViewController{

     func presentFeedByCategory(_ categoryIcon: String, categoryName: String, isCategory: Bool = true){
          if let feed = menuStoryboard.instantiateViewController(withIdentifier: "FeedCategory") as? QlueFeedCategoryController{
               
               feed.category = categoryIcon
               feed.titleCategory = categoryName
               feed.isCategory = isCategory
               
               present(feed, animated: true, completion: nil)
               
          }
     }
}

