//
//  QlueChatSearchController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/11/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import NWSTokenView
import Alamofire

class QlueChatSearchController: QlueViewController {

    @IBOutlet weak var searchTokenView: NWSTokenView!
    @IBOutlet weak var tableView: UITableView!
     
    @IBOutlet weak var loading: UIActivityIndicatorView!
     var searchContent: [QlueMember] = []
     var searchSelected: [QlueMember] = []
     
     var req : DataRequest?
     
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
     searchTokenView.awakeFromNib()
        configureTokenView()
     configureTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    
    }
    
    // MARK: - Data
     func downloadDataWithUsername(_ username : String){
          loading.startAnimating()
          req = Engine.shared.searchUsername(username.lowercased())
          Engine.shared.parseJsonWithReq(req!, controller: self, loadingStop: {
//               DejalActivityView.removeView()
            self.loading.stopAnimating()
               },completion:  { (data, success, fail) in
                    
                    if success == true{
                         self.searchContent = []
                         for d in data.arrayValue{
                              self.searchContent.append(QlueMember.init(fromJson: d))
                              self.tableView.reloadData()
                         }
                    }
          })
          
     }
    
    // MARK: - Action

    @IBAction func doneTapped(_ sender: UIButton) {
        
        var isGroup = false
        if searchSelected.count > 1{
            isGroup = true
        }
        var groupId = ""
        for member in searchSelected{
            groupId += member.userId ?? ""
            groupId += ","
        }
     groupId += User.currentUser()?.userId ?? ""
        
        groupId = groupId.trimmingCharacters(in: CharacterSet(charactersIn: ","))
//     print(groupId)
     
        presentChatDetail(withGroupId: groupId, isGroup: isGroup, members: searchSelected)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailSegue"{
            let destination = segue.destination as? QlueChatDetailController
//            destination?.initWithGroupId(<#T##groupId: String##String#>, isGroup: <#T##Bool#>, members: <#T##[QlueMember]#>)
        }
    }
    

}

// MARK: - TableView
extension QlueChatSearchController: UITableViewDataSource, UITableViewDelegate{
     func configureTableView(){
          tableView.allowsMultipleSelection = true
     }
     
     // MARK: UITableViewDataSource
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return searchContent.count
     }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QlueChatSearchUserCell
          
          let search = searchContent[indexPath.row]
        
        
        if indexPath.row % 2 == 0{
            cell.backgroundColor = kBlueE0F5FDColor
        } else{
            cell.backgroundColor = UIColor.white
        }
        
          if let url = search.avatar?.encodeURL(){
               cell.cellImage.sd_setImage(with: url as URL!)
          }
          cell.cellName.text = search.username
          
          return cell
     }
     
     // MARK: UITableViewDelegate
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          if let cell = tableView.cellForRow(at: indexPath) as? QlueChatSearchUserCell{
               cell.cellIndicator.backgroundColor = kQlueBlue
               let search = searchSelected.filter({ (member) -> Bool in
                    if member.username == searchContent[indexPath.row].username{
                    return true
                    }
                    return false
               })
               
               if search.count < 1{
                    self.searchSelected.append(searchContent[indexPath.row])
                    searchTokenView.textView.text = ""
                    searchTokenView.reloadData()
               }
               
          }
     }
     func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
          if let cell = tableView.cellForRow(at: indexPath) as? QlueChatSearchUserCell{
               cell.cellIndicator.backgroundColor = UIColor.clear
               let searchDeselect = searchContent[indexPath.row]
               searchSelected = searchSelected.filter({ (member) -> Bool in
                    if member.username == searchDeselect.username{
                         return false
                    }
                    return true
               })
               searchTokenView.reloadData()
          }
     }
     
}

// MARK: - NWSTokenView
extension QlueChatSearchController: NWSTokenDataSource, NWSTokenDelegate{
     func configureTokenView(){
          searchTokenView.dataSource = self
          searchTokenView.delegate = self
          searchTokenView.reloadData()
     }
     
    // MARK: NWSTokenDataSource
    func insetsForTokenView(_ tokenView: NWSTokenView) -> UIEdgeInsets? {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func numberOfTokensForTokenView(_ tokenView: NWSTokenView) -> Int {
        return searchSelected.count
    }
    func titleForTokenViewLabel(_ tokenView: NWSTokenView) -> String? {
        return nil
    }
    func titleForTokenViewPlaceholder(_ tokenView: NWSTokenView) -> String? {
        return "Search Here..."
    }
    func tokenView(_ tokenView: NWSTokenView, viewForTokenAtIndex index: Int) -> UIView? {
     let contact = searchSelected[Int(index)]
     if let token = ChatSearchToken.initWithTitle(contact.username)
     {
          return token
     }
     
     return nil
    }
    
    // MARK: NWSTokenDelegate
    func tokenView(_ tokenView: NWSTokenView, didSelectTokenAtIndex index: Int) {
        
    }
    func tokenView(_ tokenView: NWSTokenView, didDeleteTokenAtIndex index: Int) {
        searchSelected.removeLast()
     tokenView.reloadData()
     tokenView.textView.becomeFirstResponder()
    }
    func tokenView(_ tokenView: NWSTokenView, didDeselectTokenAtIndex index: Int) {
     
    }
    func tokenViewDidEndEditing(_ tokenView: NWSTokenView) {
     view.endEditing(true)
        tokenView.textView.resignFirstResponder()
     
    }
    func tokenView(_ tokenViewDidBeginEditing: NWSTokenView) {
        
    }
    func tokenView(_ tokenView: NWSTokenView, didChangeText text: String) {
        req?.cancel()
//     DejalActivityView.addActivityViewForView(self.tableView)
     downloadDataWithUsername(text)
    }
    func tokenView(_ tokenView: NWSTokenView, didEnterText text: String) {
        
    }
    func tokenView(_ tokenView: NWSTokenView, contentSizeChanged size: CGSize) {
        
    }
    func tokenView(_ tokenView: NWSTokenView, didFinishLoadingTokens tokenCount: Int) {
        
    }
    
}

// MARK: - Navigation
extension QlueViewController{
    func presentChatSearch(){
        if let chatSearch = chatStoryboard.instantiateViewController(withIdentifier: "ChatSearch") as? QlueChatSearchController{
          self.present(chatSearch, animated: true, completion: nil)
          
        }
    }
}


