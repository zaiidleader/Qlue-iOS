//
//  QlueNotifController.swift
//  Qlue
//
//  Created by Nurul on 6/8/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class QlueNotifController: QlueViewController {
    
    @IBOutlet weak var tableView: UITableView!
    lazy var refreshControl: UIRefreshControl? = {
        let refreshControl = UIRefreshControl()
        self.tableView.addSubview(refreshControl)
        return refreshControl
    }()
    @IBOutlet weak var chatButton: UIButton!
    
    var notifs: [QlueNotif] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureChatButton(chatButton)
        
        tableView.register(UINib.init(nibName: "QlueNotifViewCell", bundle: nil), forCellReuseIdentifier: "NotifCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 71
        
        tableView.tableFooterView = UIView()
        
        refreshControl?.beginRefreshing()
        refreshControl?.addTarget(self, action: #selector(QlueNotifController.refresh(_:)), for: .valueChanged)
        
        tableView.addInfiniteScroll { (tableView) in
            self.downloadData(self.notifs.count, refresh: false)
        }
        
        refreshControl?.beginRefreshing()
        downloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helper
    
    
    // MARK: - Action
    func refresh(_ sender: UIRefreshControl) {
        downloadData(0, refresh: true)
    }
    
    func downloadData(_ from: Int = 0, refresh: Bool = false) {
        
        let uid = User.currentUser()!.userId!
        
        Engine.shared.getNotif(from: from, id: uid, city: "", deviceName: "").responseString { (res) in
            
            self.refreshControl?.endRefreshing()
            self.tableView.finishInfiniteScroll()
            
            if res.result.isSuccess{
                if let result = res.result.value{
                    let data = JSON.parse(result)
                    
                    if data["success"].string == "false" {
                        Util.showSimpleAlert(data["message"].string, vc: self, completion: nil)
                    }
                    else {
                        
                        if refresh { self.notifs = [] }
                        for notif in data.arrayValue {
                            if notif == nil { continue }
                            self.notifs.append(QlueNotif.notifWithData(notif))
                        }
                        
                        self.tableView.reloadData()
                        
                    }
                }
            }
            else if let error = res.result.error{
                print("error: \(error.localizedDescription)")
            }
            else {
                
            }
        }
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return notifs.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotifCell", for: indexPath) as! QlueNotifViewCell
        
        let data = notifs[indexPath.row]
        
        if data.status == "0" {
            cell.backgroundColor = kBgCellBlue
        }
        else {
            cell.backgroundColor = UIColor.white
        }
        
        cell.cellImage.image = nil
        if let fileUrl = data.fileUrl?.encodeURL(){
            cell.cellImage.sd_setImage(with: fileUrl as URL!)
        }
        
        let attText = NSMutableAttributedString()
        if let username = data.username {
            attText.append(NSAttributedString(string: "\(username) ", attributes: [NSFontAttributeName: UIFont(name: QlueFont.ProximaNovaSoft, size: 13)!, NSForegroundColorAttributeName: UIColor.black]))
        }
        if let desc = data.desc {
            attText.append(NSAttributedString(string: desc, attributes: [NSFontAttributeName: UIFont(name: QlueFont.ProximaNovaRegular, size: 13)!, NSForegroundColorAttributeName: UIColor.darkGray]))
        }
        cell.cellDesc.attributedText = attText
        
        let date = Date().getDateFromTimestamp(data.timestamp)
        cell.cellDate.text = date?.formatDateReadable()
        
        
        return cell
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let data = notifs[indexPath.row]
        if let type = data.type {
            if let id = data.notifId {
                
                switch "\(type)" {
                case NotificationType.Message.rawValue:
                    showChatDetail(data)
                    
                case NotificationType.Forum.rawValue:
                    showForumDetail(data)
                    
                case NotificationType.ForumDetail.rawValue:
                    showForumDetail(data)
                    
                case NotificationType.Follow.rawValue:
                    showOtherProfile(data)
                    
                case NotificationType.Admin.rawValue:
                    showForumDetail(data)
                    
                case NotificationType.MentionPost.rawValue:
                    showFeedDetail(data)
                    
                case NotificationType.MentionForum.rawValue:
                    showForumDetail(data)
                    
//                case NotificationType.EditKelurahan.rawValue:
//                    break
                    
//                case NotificationType.ApproveAvatar.rawValue:
//                    break // Main
                    
//                case NotificationType.Like.rawValue:
//                    break
                    
//                case NotificationType.ProgressUpdate.rawValue:
//                    break
                    
//                case NotificationType.FAQReject.rawValue:
//                    break // FAQ page
                    
//                case NotificationType.PromoCode.rawValue:
//                    break
                    
                case NotificationType.ForumKelurahan.rawValue:
                    showForumDetail(data)
                    
                case NotificationType.UserComment.rawValue:
                    showComment(data)
                    
                case NotificationType.Conversation.rawValue:
                    showChatDetail(data)
                    
                case NotificationType.ReportInNeighborhood.rawValue:
                    showFeedDetail(data)
                    
                default:
                    break
                }
                
                Engine.shared.readNotif(with: id, type: type).responseString(completionHandler: { (response) in
                    
                    if response.result.isSuccess {
                        if let result = response.result.value {
                            let json = JSON(parseJSON: result)
                            
                            if let success = json["success"].string, success == "true" {
                                
                                data.status = "1"
                                self.tableView.reloadRows(at: [indexPath], with: .automatic)
                            }
                        }
                    }
                })
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
