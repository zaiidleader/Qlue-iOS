//
//  QlueFeedFusionController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/27/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage
import SwiftyJSON
import ActionSheetPicker_3_0
import AssetsLibrary
import FLAnimatedImage
import Alamofire
import PBJVideoPlayer
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class QlueFeedFusionController: QlueScrollViewController {
     
     // MARK: Outlets
     @IBOutlet weak var collectionView: UICollectionView!
     
     // MARK: Private Var
      var feeds : [QlueFeed] = []
      var page = 0
     
     // User
      var user: User?
     
     // Mark: Refresh
     let refreshControl = UIRefreshControl()
     
      var currentLocation: CLLocation?
     
     var selectedItem = 0
     
     // MARK: asset library var
     var library: ALAssetsLibrary = ALAssetsLibrary()
     
     
     
     // MARK: - View Life Cycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          //collectionView
          registerNib()
          
          //refresh
          collectionView.addSubview(refreshControl)
          refreshControl.addTarget(self, action: #selector(QlueFeedFusionController.refresh(_:)), for: .valueChanged)
          DejalActivityView.addActivityView(for: self.collectionView)
          //        refreshControl.beginRefreshing()
          
          
          
          // loadmore
          collectionView.addInfiniteScroll { (collectionView) in
               self.loadMore()
          }
     }
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          // getProfileData
          //          navigationController?.navigationBarHidden = true
          loadUserData()
          
     }
     override func viewWillDisappear(_ animated: Bool) {
          super.viewDidDisappear(animated)
          //          navigationController?.navigationBarHidden = false
     }
     
     
     // MARK: - Private Func
      func registerNib(){
          collectionView.register(UINib.init(nibName: "QlueForumFeedViewCell", bundle: nil), forCellWithReuseIdentifier: "ForumCell")
          collectionView.register(UINib.init(nibName: "QlueFeedViewCell2", bundle: nil), forCellWithReuseIdentifier: "FeedCell")
     }
      func loadMore(){
          self.page += 1
          downloadData()
     }
     
          
     // MARK: - Data
      func loadUserData(){
          user = User.currentUser()
          
     }
     
     func downloadData(){
          
     }
     
     
     
     // MARK: - Action
     func refresh(_ sender: UIRefreshControl){
          page = 0
          downloadData()
     }
     
     // MARK: - Helper
     func saveToImageAlbum(withImage image: UIImage){
          
          library.writeImage(toSavedPhotosAlbum: image.cgImage, orientation: ALAssetOrientation(rawValue: image.imageOrientation.rawValue)!) { (url, error) in
               
               Util.showSimpleAlert("Save image done", vc: self, completion: nil)
               self.library.asset(for: url, resultBlock: { (asset) in
                    
                    self.library.enumerateGroupsWithTypes(ALAssetsGroupAlbum, usingBlock: { group, stop in
                         stop?.pointee = false
                         if group != nil {
                              let str = group?.value(forProperty: ALAssetsGroupPropertyName) as! String
                              if str == "MY_ALBUM_NAME" {
                                   group!.add(asset!)
                                   let assetRep:ALAssetRepresentation = asset!.defaultRepresentation()
                                   let iref = assetRep.fullResolutionImage().takeUnretainedValue()
                                   let image: UIImage =  UIImage(cgImage:iref)
                                   
                              }
                              
                         }
                         
                    }, failureBlock: { error in print("NOOOOOOO") })
                    
               }, failureBlock: { (error) in
                    
               })
          }
     }
     
     // MARK: - Navigation
     
     @IBAction func unwindToFeed(_ segue: UIStoryboardSegue) {
          if segue.source.isKind(of: QluePostGovController.self) || segue.source.isKind(of: QluePostSwastaController.self){
               downloadData()
          }
     }
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "FeedDetailSegue" {
               let destination = segue.destination as? QlueFeedDetailController
               destination?.initWithFeedId(feeds[selectedItem].feedId)
               
          } else if segue.identifier == "CommentSegue"{
               let destination = segue.destination as? QlueCommentController
               if let id = feeds[selectedItem].feedId{
                    destination?.initWithFeedId(String(id))
               }
          }
     }
     
     
}
// MARK: - UICollectionViewDataSource
extension QlueFeedFusionController: UICollectionViewDataSource{
     func numberOfSections(in collectionView: UICollectionView) -> Int {
          return 1
     }
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          
          return feeds.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
          let data = feeds[indexPath.item]
          
          if data.postType == "forum" {
               
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForumCell", for: indexPath) as! QlueForumViewCell
               
               cell.cornerRadius(0)
               cell.border(1, color: kGreyEEE)
               
               
               
               if (data.forumIsVote == true) && (data.forumIsFav == false){
                    cell.indicatorView.backgroundColor = kDarkColor
               } else if data.forumIsFav == true{
                    cell.indicatorView.backgroundColor = kCommunityForumIndicatorColor
               } else{
                    cell.indicatorView.backgroundColor = kQlueBlue
               }
               
               
               cell.cellTitle.text = data.qlueTitle
               if let imgUrl = data.fileUrl?.encodeURL(){
                    cell.cellImage.sd_setImage(with: imgUrl as URL!)
               }
               let timestamp = Date().getDateFromTimestamp(data.timestamp)
               cell.cellDate.text = timestamp?.formatDate(withStyle: DateFormatter.Style.full)
               cell.cellTime.text = timestamp?.formatTime(withStyle: DateFormatter.Style.short)
               cell.cellUsername.setTitle(data.username, for: UIControlState())
               cell.cellSeen.setTitle(data.forumViewCount, for: UIControlState())
               cell.cellComment.setTitle(data.forumCommentCount, for: UIControlState())
               cell.delegate = self
               
               return cell
          }
          
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCell", for: indexPath) as! QlueFeedViewCell2
          
          
          
          cell.cellAvatar.image = nil
          if let avaUrl = data.avatar?.encodeURL(){
               cell.cellAvatar.sd_setImage(with: avaUrl as URL!)
          }
          
          cell.cellName.text = data.username
          
          let date = Date().getDateFromTimestamp(data.timestamp)
          cell.cellTime.text = date?.timeAgoWithNumericDates(true)
          
          //          cell.cellTitle.text     = data.title
          cell.cellDesc.text      = data.desc
          cell.cellDesc.handleHashtagTap { (hashtag) in
               self.presentFeedByCategory(hashtag, categoryName: hashtag, isCategory: false)
          }
          
          
          if (data.reportType == .ReviewNegatif ) || (data.reportType == .ReviewPositif ){
               cell.cellPlaceName.text = data.poi?.title
          } else{
               let kel = data.kelurahan ?? ""
               let kec = data.kecamatan ?? ""
               cell.cellPlaceName.text = kel + ", " + kec
          }
          
          
          cell.cellMainImage.image = nil
          cell.cellVideoButton.isHidden = true
          
          cell.videoPlayer.stop()
          cell.videoPlayer.view.isHidden = true
          cell.cellMainImage.image = nil
          if data.format == "image"{
               if let picUrl = data.fileUrl?.encodeURL(){
                    cell.cellMainImage.sd_setImage(with: picUrl as URL!)
               }
               
          }else{
               cell.cellVideoButton.isHidden = false
               cell.videoPlayer.videoPath = data.fileUrl
               cell.videoPlayer.videoFillMode = "AVLayerVideoGravityResizeAspectFill"
               if let picUrl = data.thumbUrl?.encodeURL(){
                    cell.cellMainImage.sd_setImage(with: picUrl as URL!)
               }
               
          }
          
          
          cell.cellCategoryImage.image = nil
          if let labelUrl = data.labelUrl?.encodeURL(){
               cell.cellCategoryImage.sd_setImage(with: labelUrl as URL!)
          }
          
          if data.like == true{
               cell.cellLikeButton.setImage(data.reportType?.likeImageActive2, for: UIControlState())
               //            cell.cellLikeButton.enabled = false
          }else{cell.cellLikeButton.setImage(data.reportType?.likeImageDeactive2, for: UIControlState())
               //            cell.cellLikeButton.enabled = true
          }
          
          cell.cellLikeCommentTitle.text = nil
          if let like = data.countLike, like > 0 {
               cell.cellLikeCommentTitle.text = "\(like) Likes"
          }
          
          if let comment = data.countComment, comment > 0 {
               if let text = cell.cellLikeCommentTitle.text {
                    cell.cellLikeCommentTitle.text = "\(text), \(comment) Comments"
               }
               else {
                    cell.cellLikeCommentTitle.text = "\(comment) Comments"
               }
          }
          
          var commentHeight: CGFloat = 0
          for comment in data.comments{
               
               commentHeight += Util.getHeightCommentInFeedList(Util.getCommentAttributedString(comment.username, comment: comment.comment))
          }
          
          cell.cellCommentHeightConstraint.constant = commentHeight
          
          cell.comments = data.comments
          
          cell.delegate = self
          cell.cellTableView.reloadData()
          
          return cell
     }
     
}

// MARK: - UICollectionViewDelegate
extension QlueFeedFusionController: UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          
               let data = feeds[indexPath.item]
               
               if data.postType == "forum" {
                    presentForumDetail(forumId: data.feedId?.description ?? "", forumType: ForumType.general)
                    return
               }
               
               selectedItem = indexPath.item
          presentDetailFeed(withFeedId: (feeds[selectedItem].feedId), nav: nil)
//               performSegueWithIdentifier("FeedDetailSegue", sender: self)
          
     }
     
     func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
          if let cell = cell as? QlueFeedViewCell2 {
               cell.videoPlayer.removeFromParentViewController()
          }
     }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueFeedFusionController: UICollectionViewDelegateFlowLayout{
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          let width = collectionView.bounds.width
          let data = feeds[indexPath.item]
          
          if data.postType == "forum" {
               return CGSize(width: collectionView.bounds.width, height: 120)
          }
          
          if data.postType == "ads" { return CGSize.zero }
          
          let imageHeight: CGFloat = width / 1.61803
          
          var height = imageHeight + 85 + 29 + 32 + 1 + 51
          //          if let title = data.title {
          //               height += title.heightWithConstrainedWidth(width - 35, font: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 17)!)
          //          }
          
          if let desc = data.desc {
               height += desc.heightWithConstrainedWidth(width - 40 , font: UIFont.init(name: QlueFont.HelveticaNeue, size: 13)!)
          }
          
          if data.countComment > 0 {
               
               height += 69
               
               for comment in data.comments{
                    height += Util.getHeightCommentInFeedList(Util.getCommentAttributedString(comment.username, comment: comment.comment))
               }
               
          }
          
          return CGSize(width: collectionView.bounds.width, height: height)
     }
}

// MARK: - QlueFeedViewCell2Delegate

extension QlueFeedFusionController: QlueFeedViewCell2Delegate {
     
     
     func videoTapped(_ cell: QlueFeedViewCell2, videoPlayer: PBJVideoPlayerController) {
          
          
          cell.videoPlayer.view.isHidden = false
          self.addChildViewController(videoPlayer)
          cell.cellMainImage.addSubview(videoPlayer.view)
          videoPlayer.didMove(toParentViewController: self)
          videoPlayer.playFromBeginning()
          
          
          
     }
     func avatarFeedTapped(_ cell: QlueFeedViewCell2) {
          guard let indexPath = collectionView.indexPath(for: cell) else{
               return
          }
          
          let feed = feeds[indexPath.item]
          presentOtherProfile(feed.userId)
          
     }
     func avatarCommentTapped(_ feedCell: QlueFeedViewCell2, indexComment: Int) {
          
     }
     
     func categoryTapped(_ cell: QlueFeedViewCell2) {
          
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          
          let feed = feeds[indexPath.item]
          presentFeedByCategory(feed.categoryIcon ?? "", categoryName: feed.categoryName ?? "")
          
          
     }
     
     func countForCommentTableView(_ cell: QlueFeedViewCell2) -> Int {
          
          guard let indexPath = collectionView.indexPath(for: cell) else{
               return 0
          }
          
          if let count = feeds[indexPath.item].countPreviewComment{
               return count
          }
          return 0
          
          
     }
     
     func seeAllCommentTapped(_ cell: QlueFeedViewCell2) {
          if let indexPath = collectionView.indexPath(for: cell)
          {
               selectedItem = indexPath.item
               
               if let id = feeds[indexPath.item].feedId{
                    presentComment(withId: String(id), nav: self.navigationController)
               }
          }
     }
     
     func commentData(_ cell: QlueFeedViewCell2) -> [QlueComment] {
          if let indexPath = collectionView.indexPath(for: cell) {
               return feeds[indexPath.item].comments
          }
          
          return []
     }
     
     func poiTapped(_ cell: QlueFeedViewCell2) {
         
          kelurahanTapped(cell)
     }
     
     func likeTapped(_ cell: QlueFeedViewCell2, button: UIButton, label: UILabel) {
          if let indexPath = collectionView.indexPath(for: cell) {
               guard let postId = feeds[indexPath.item].feedId  else { return }
               let req = Engine.shared.postLike(withFeedId: String(postId))
               
               Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
                    }, completion: { (data, success, fail) in
                         if self.feeds[indexPath.item].like == false {
                              button.setImage(self.feeds[indexPath.item].reportType?.likeImageActive2, for: .normal)
                              self.feeds[indexPath.item].like = true
                              
                              if let countLike = self.feeds[indexPath.item].countLike{
                                   self.feeds[indexPath.item].countLike = countLike + 1
                                   //                                   label.text = String(countLike + 1)
                              }
                         } else{
                              button.setImage(self.feeds[indexPath.item].reportType?.likeImageDeactive2, for: .normal)
                              self.feeds[indexPath.item].like = false
                              
                              if let countLike = self.feeds[indexPath.item].countLike{
                                   self.feeds[indexPath.item].countLike = countLike - 1
                                   //                                   label.text = String(countLike - 1)
                              }
                         }
                         
                         cell.cellLikeCommentTitle.text = nil
                         if let like = self.feeds[indexPath.item].countLike, like > 0 {
                              cell.cellLikeCommentTitle.text = "\(like) Likes"
                         }
                         
                         if let comment = self.feeds[indexPath.item].countComment, comment > 0 {
                              if let text = cell.cellLikeCommentTitle.text {
                                   cell.cellLikeCommentTitle.text = "\(text), \(comment) Comments"
                              }
                              else {
                                   cell.cellLikeCommentTitle.text = "\(comment) Comments"
                              }
                         }
                    }, falseCompletion: {
                         
                         
               })
          }
     }
     
     func kelurahanTapped(_ cell: QlueFeedViewCell2) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          
          let feed = feeds[indexPath.item]
          
          if (feed.reportType == .ReviewNegatif ) || (feed.reportType == .ReviewPositif ){
               Util.showSimpleAlert("Detail tidak tersedia", vc: self, completion: nil)
               return
          }
          
          if (feed.kdKel == nil)/* || (feed.reportType)*/{
               Util.showSimpleAlert("Detail tidak tersedia", vc: self, completion: nil)
               
               return
          }
          presentKelurahan(withId: feed.kdKel, district: feed.district, nav: self.navigationController)
     }
     
     func seenByTapped(_ cell: QlueFeedViewCell2) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          let feed = feeds[indexPath.item]
          presentSeenBy(withId: feed.feedId?.description ?? "", countSeen: feed.countView?.description ?? "", type:  0)
     }
     
     func moreTapped(_ cell: QlueFeedViewCell2) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          let feed = feeds[indexPath.item]
          
          let alert = UIAlertController.init(title: "More", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
          
          let nomorKel = UIAlertAction.init(title: "Nomor Laporan", style: .default) { (action) in
               self.presentNomorLaporan(withNoLap: feed.feedId?.description)
          }
          alert.addAction(nomorKel)
          
          let simpanGambar = UIAlertAction.init(title: "Simpan Gambar", style: .default) { (action) in
               if let image = cell.cellMainImage.image{
                    self.saveToImageAlbum(withImage: image)
               }
          }
          alert.addAction(simpanGambar)
          
          let cancel = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
          alert.addAction(cancel)
          
          present(alert, animated: true, completion: nil)
     }
     
     func progressTapped(_ cell: QlueFeedViewCell2, button: UIButton) {
          
          guard let indexPath = collectionView.indexPath(for: cell) else{
               return
          }
          
          let feed = feeds[indexPath.item]
          
          if feed.progressState == .Complete{
               presentProgressComplete(withFeedId: feed.feedId ?? 0)
          } else if feed.progressState == .Process{
               presentProgressProcess(withFeedId: feed.feedId ?? 0)
          }
          
     }
}
// MARK: - QlueForumViewCellDelegate
extension QlueFeedFusionController: QlueForumViewCellDelegate{
     func starDidTapped(_ cell: UICollectionViewCell, button: UIButton) {
          
     }
     
     func seenDidTapped(_ cell: UICollectionViewCell, button: UIButton) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          let forum = feeds[indexPath.item]
          
          presentSeenBy(withId: forum.feedId?.description ?? "", countSeen: forum.forumViewCount ?? "0")
     }
}
