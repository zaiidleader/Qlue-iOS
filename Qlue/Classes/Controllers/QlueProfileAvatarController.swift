//
//  QlueProfileAvatarController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueProfileAvatarController: QlueProfileBaseController {

     
     // MARK: - Vars
     // MARK: Outlets
     @IBOutlet weak var collectionView: UICollectionView!
     
     // MARK: Data
     var avatars: [QlueUserAvatar] = []
     
     // MARK: - View Lifecycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          configureCollectionView()
          DejalActivityView.addActivityView(for: collectionView)
          downloadData()
          
          if userId != User.currentUser()?.userId{
               setupFab()
          }
          
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Configure
     func configureCollectionView(){
          collectionView.register(UINib.init(nibName: "QlueAvatarViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
     }
     
    
     // MARK: - Data
     func downloadData(){
          
          guard let userId = userId else{
               DejalActivityView.remove()
               return
          }
          
          let req = Engine.shared.listAvatarUser(userId: userId)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               DejalActivityView.remove()
               
               }, completion:  { (data, success, fail) in
                    if success == true{
                         
                         self.avatars = []
                         for ava in data.arrayValue{
                              self.avatars.append(QlueUserAvatar.init(fromJson: ava))
                         }
                         
                         self.collectionView.reloadData()
                         
                    }
          })
          
     }
     
     
}


// MARK: - UICollectionViewDataSource
extension QlueProfileAvatarController: UICollectionViewDataSource{
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return avatars.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! QlueAvatarViewCell
          
          let ava = avatars[indexPath.item]
          
          cell.cellImage.image = nil
          if let image = ava.avatar?.encodeURL(){
                cell.cellImage.sd_setImage(with: image as URL!)
          }
         cell.cellLabel.text = ava.type
          
          return cell
     }
}

// MARK: - UICollectionViewDelegate
extension QlueProfileAvatarController: UICollectionViewDelegate{
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueProfileAvatarController: UICollectionViewDelegateFlowLayout{
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          let size: CGFloat = (UIScreen.main.bounds.width / 3) + 0.3
          
          return CGSize(width: size, height: size)
          
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return -2
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return -2
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsets.zero
     }
     
     
}
