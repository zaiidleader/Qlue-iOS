//
//  QlueChatMemberController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/13/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueChatMemberController: QlueViewController {

    @IBOutlet weak var tableView: UITableView!
    var members: [QlueMember] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.rowHeight = 50
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Data
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Table View
extension QlueChatMemberController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QlueChatMemberCell
        
        let member = members[indexPath.row]
        cell.cellName.text = member.username
        if member.follow == true{
            cell.cellAddButton.isHidden = true
        } else{
            cell.cellAddButton.isHidden = false
        }
        if let avaUrl = member.avatar?.encodeURL(){
            cell.cellImage.sd_setImage(with: avaUrl as URL!)
        }
        cell.delegate = self
        return cell
    }
}


extension QlueChatMemberController: QlueChatMemberCellDelegate{
    func followTapped(_ cell: QlueChatMemberCell, button: UIButton) {
     guard let indexPath = tableView.indexPath(for: cell) else { return }
        let req = Engine.shared.followMember(withId: members[indexPath.row].userId ?? "")
     button.isEnabled = false
     Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: { 
          
          }, completion: { (data, success, fail) in
               if success == true{
                    
                    button.isHidden = true
               } else{
                    button.isEnabled = true
               }
          })
    }
}

// MARK: - Navigation
extension QlueViewController{
    func presentListMember(withMembers members: [QlueMember]){
        if let member = chatStoryboard.instantiateViewController(withIdentifier: "Member") as? QlueChatMemberController{
            member.members = members
            present(member, animated: true, completion: nil)
        }
    }
}
