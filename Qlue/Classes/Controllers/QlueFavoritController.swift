//
//  QlueFavoritController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/2/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueFavoritController: QlueViewController {
    
    // MARK: - Vars
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!

     // MARK: Data
     var feeds: [QlueFeed] = []
     var page = 0
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        DejalActivityView.addActivityView(for: tableView)
        tableView.addInfiniteScroll { (tableView) in
          if self.feeds.count >= (self.page + 1) * 25{
               self.loadMore()
          } else{
               self.tableView.finishInfiniteScroll()
          }
        }
        downloadData()
        configureTableView()
    }

     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          
          self.navigationController?.isNavigationBarHidden = true
     }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     
     // MARK: - Data
     func loadMore(){
          page += 1
          downloadData()
     }
     func downloadData(){

          let start = page * 25
          let finish = (page + 1) * 25
          
          let req = Engine.shared.getListFavorite(start, finish: finish)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               self.tableView.finishInfiniteScroll()
               DejalActivityView.remove()
               
               }, completion:  { (data, success, fail) in
                    if success == true{
                         
                         if self.page == 0{
                              self.feeds = []
                         }
                         
                         for feed in data.arrayValue{
                              self.feeds.append(QlueFeed.feedWithData(feed))
                         }
                         
                         self.tableView.reloadData()
                         
                    }
          })
          
     }
}

// MARK: - TableView
extension QlueFavoritController: UITableViewDataSource, UITableViewDelegate{
    fileprivate func configureTableView(){
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QlueFavoritCell
     
          let feed = feeds[indexPath.row]
     
     if let image = feed.thumbUrl?.encodeURL(){
          cell.cellImage.sd_setImage(with: image as URL!)
     }
     
          cell.cellTitle.text = feed.title
          cell.cellTime.text = Date().getDateFromTimestamp(feed.timestamp)?.timeAgoWithNumericDates(true)
     
        return cell
    }
    
    // MARK: UITableViewDelegate
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          presentDetailFeed(withFeedId: feeds[indexPath.row].feedId, nav: navigationController)
     }
}

// MARK: - Navigation
extension QlueViewController{
     func presentListFavorite(_ nav: UINavigationController?){
          let favorite = moreStoryboard.instantiateViewController(withIdentifier: "ListFavorite")
          if let nav = nav {
               nav.pushViewController(favorite, animated: true)
          } else{
               present(favorite, animated: false, completion: nil)
          }
     }
}
