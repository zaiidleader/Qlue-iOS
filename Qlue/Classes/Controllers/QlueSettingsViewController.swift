//
//  QlueSettingsTableViewController.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 10/16/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import Firebase

enum SettingItem {
    case posting
    case bahasa
    case notifikasi
    case blok
    case update
    case tutorial
    case legal
    case version
    case logOut
    
    case faq
    case privacyPolicy
    case eula
    
    case postingSimpanGambar
    
    case notifikasiMention
    case notifikasiKelurahan
    case notifikasiAdmin
    case notifikasiComment
    case notifikasiMessage
    
    case notifikasiOnOff
    
    var description: String {
        
        switch self {
        case .posting:
            return "Posting"
        case .bahasa:
            return "Bahasa"
        case .notifikasi:
            return "Notifikasi"
        case .blok:
            return "User yang Diblok"
        case .update:
            return "Cek untuk Update"
        case .tutorial:
            return "Tutorial"
        case .legal:
            return "Legal"
        case .version:
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                return "2015 © Qlue v.\(version)"
            }
            return "2015 © Qlue"
        case .logOut:
            return "Log Out"
            
        case .faq:
            return "FAQ"
        case .privacyPolicy:
            return "Privacy Policy"
        case .eula:
            return "EULA"
            
        case .postingSimpanGambar:
            return "Simpan gambar ketika posting"
            
        case .notifikasiMention:
            return "Mention"
        case .notifikasiKelurahan:
            return "Kelurahan"
        case .notifikasiAdmin:
            return "Admin"
        case .notifikasiComment:
            return "Comment"
        case .notifikasiMessage:
            return "Message"
        case .notifikasiOnOff:
            return "Notifikasi"
        }
    }
    
    var cellType: Int {
        
        switch self {
        case .postingSimpanGambar:
            return 1
        case .notifikasiMention:
            return 1
        case .notifikasiKelurahan:
            return 1
        case .notifikasiAdmin:
            return 1
        case .notifikasiComment:
            return 1
        case .notifikasiMessage:
            return 1
        case .notifikasiOnOff:
            return 1
        default:
            return 0
        }
    }
}

class QlueSettingsViewController: QlueViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var currentSettingItem: SettingItem?
    lazy var items: [SettingItem] = {
        
        if self.currentSettingItem == .legal {
            return [
                .faq,
                .privacyPolicy,
                .eula
            ]
        }
        else if self.currentSettingItem == .posting {
            return [
                .postingSimpanGambar
            ]
        }
        else if self.currentSettingItem == .notifikasi {
            return [
//                .NotifikasiMention,
//                .NotifikasiKelurahan,
//                .NotifikasiAdmin,
//                .NotifikasiComment,
//                .NotifikasiMessage
                .notifikasiOnOff
            ]
        }
        else {
            return [
                .posting,
//                .Bahasa,
                .notifikasi,
                .blok,
                .update,
                .tutorial,
                .legal,
                .version,
                .logOut
            ]
        }
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

extension QlueSettingsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = items[indexPath.row]
        if item.cellType == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellId", for: indexPath)
            cell.textLabel?.text = item.description
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchCellId", for: indexPath) as! QlueSwitchViewCell
            cell.titleLabel?.text = item.description
            
            if item == .notifikasiOnOff {
                
                cell.onOffSwitch.setOn(true, animated: true)
                if let receiveNotification = UserDefaults.standard.object(forKey: kReceiveNotification) as? NSNumber {
                    if !receiveNotification.boolValue {
                        cell.onOffSwitch.setOn(false, animated: true)
                    }
                }
            }
            
            cell.delegate = self
            
            return cell
        }
    }
}

extension QlueSettingsViewController: QlueSwitchViewCellDelegate {
    
    func qlueSwitchViewCellOnOffSwitchValueChanged(_ cell: QlueSwitchViewCell) {
        
        if let indexPath = tableView.indexPath(for: cell) {
            
            let item = items[indexPath.row]
            if item == .notifikasiOnOff {
                
                UserDefaults.standard.set(NSNumber(value: cell.onOffSwitch.isOn as Bool), forKey: kReceiveNotification)
                UserDefaults.standard.synchronize()
                
                if !cell.onOffSwitch.isOn {
                    if let token = FIRInstanceID.instanceID().token() {
                        Engine.shared.unregisterNotif(token)
                    }
                }
                else {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.registerForRemoteNotifications()
                }
            }
        }
    }
}

extension QlueSettingsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = items[indexPath.row]
        switch item {
            
        case .tutorial:
            presentTips()
            
        case .faq:
            presentWebView(navigationController, htmlFile: kFaqHtmlFile, title: item.description)
            
        case .privacyPolicy:
            presentWebView(navigationController, urlString: PRIVACY_URL, title: item.description)
            
        case .eula:
            presentWebView(navigationController, htmlFile: kEulaHtmlFile, title: item.description)
            
        case .version:
            presentWebView(navigationController, htmlFile: kAboutHtmlFile, title: item.description)
            
        case .logOut:
            Engine.shared.logout()
            
        case .posting, .notifikasi:
            presentSettings(navigationController, settingItem: item, title: item.description)
            
        case .notifikasiOnOff:
            
            if let receiveNotification = UserDefaults.standard.object(forKey: kReceiveNotification) as? NSNumber {
                UserDefaults.standard.set(NSNumber(value: !receiveNotification.boolValue as Bool), forKey: kReceiveNotification)
                UserDefaults.standard.synchronize()
                
                tableView.reloadRows(at: [indexPath], with: .automatic)
                
                if receiveNotification.boolValue {
                    if let token = FIRInstanceID.instanceID().token() {
                        Engine.shared.unregisterNotif(token)
                    }
                }
                else {
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.registerForRemoteNotifications()
                }
            }
            
        default:
            return
        }
    }
}

// MARK: - Navigation
extension QlueViewController {
    func presentSettings(_ nav: UINavigationController?, settingItem: SettingItem?, title: String = "Settings") {
        
        let settings = settingsStoryboard.instantiateViewController(withIdentifier: "Settings") as! QlueSettingsViewController
        settings.currentSettingItem = settingItem
        settings.title = title
        
        if let nav = nav {
            nav.pushViewController(settings, animated: true)
        }
        else {
            let navigationController = UINavigationController(rootViewController: settings)
            present(navigationController, animated: false, completion: nil)
        }
    }
    
}
