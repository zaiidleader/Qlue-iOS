//
//  QlueAvatarPopUpController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/23/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueAvatarPopUpController: QlueViewController {

    var avatarName: String?
    var avaImage: UIImage?
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = avatarName
        avatarImage.image = avaImage
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Navigate
extension QlueViewController{
    func presentAvatarPopUp(_ avatarName: String?, avaImage: UIImage?){
        if let ava = profileStoryboard.instantiateViewController(withIdentifier: "AvaPopUp") as? QlueAvatarPopUpController{
            ava.avatarName = avatarName
            ava.avaImage = avaImage
            
            semiModalWithViewController(ava)
        }
    }
}
