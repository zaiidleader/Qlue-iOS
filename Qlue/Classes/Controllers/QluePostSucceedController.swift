//
//  QluePostSucceedController.swift
//  Qlue
//
//  Created by Nurul on 6/15/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QluePostSucceedController: UIViewController {

    @IBOutlet weak var okButton: UIButton!{
        didSet{
            okButton.cornerRadius(5)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action
    
    @IBAction func okTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
