//
//  QlueTabBarController.swift
//  Qlue
//
//  Created by Nurul on 5/28/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

//        tabBar.backgroundImage = UIImage(named: "bgTabBar")
        tabBar.tintColor = kActiveTabBar
        
        for item in self.tabBar.items! as [UITabBarItem] {
            if let image = item.image {
                item.image = image.imageWithColor(kInactiveTabBar).withRenderingMode(.alwaysOriginal)
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
