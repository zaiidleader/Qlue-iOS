//
//  RegistrationPasswordController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class RegistrationPasswordController: RegistrationController {

    // MARK: - Outlets
    @IBOutlet weak var passwordTextfield: UITextField!
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Action
    override func nextTapped(_ sender: UIButton) {
        guard passwordTextfield.text != "" else{
            Util.showSimpleAlert("Please enter your password", vc: self, completion: nil)
            return
        }
        Engine.shared.createUser.password = passwordTextfield.text ?? ""
        performSegue(withIdentifier: "NextSegue", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
