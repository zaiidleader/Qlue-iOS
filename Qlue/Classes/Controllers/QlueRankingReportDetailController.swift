//
//  QlueRankingReportDetailController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/16/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

enum RankingReportDetailType{
     case dinas(dinasName: String)
     case swasta(swastaName: String)
     
     var color: UIColor{
          switch self {
          case .dinas(_):
               return kCompleteGreenColor
          case .swasta(_):
               return kProcessYellowColor
          }
     }
    
}
class QlueRankingReportDetailController: QlueViewController {

     var type:  RankingReportDetailType = .dinas(dinasName: "")
    @IBOutlet weak var tableView: UITableView!
    var feeds: [QlueFeed] = []
     
     
     var location: CLLocation?
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
     
     DejalActivityView.addActivityView(for: tableView)
     setCurrentLocation()
     
     tableView.estimatedRowHeight = 44
     tableView.rowHeight = UITableViewAutomaticDimension
     
     switch type {
     case .dinas(let dinasName):
          title = dinasName
     case .swasta(let swastaName):
          title = swastaName
     }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     
     // MARK: - Location
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               self.location = location
               
               downloadData()
               
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QlueRankingGovDetailController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }
    
    // MARK: - Data
    fileprivate func downloadData(){
     
     var req: DataRequest!
     
     switch type {
     case .dinas(let dinasName):
          req = Engine.shared.listRankingDinasDetail(dinasName: dinasName, lat: location!.coordinate.longitude, lng: location!.coordinate.latitude)
     case .swasta(let swastaName):
          req = Engine.shared.listRankingSwastaDetail(swastaName: swastaName, lat: location!.coordinate.longitude, lng: location!.coordinate.latitude)     
     }
     
     Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
          DejalActivityView.remove()
          }, completion: { (data, success, fail) in
               self.feeds = []
               
               if success == true{
                    for d in data.arrayValue{
                         self.feeds.append(QlueFeed.feedWithData(d))
                    }
                    
                    self.tableView.reloadData()
               }
               
     })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - TableView
extension QlueRankingReportDetailController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QlueRankingReportViewCell
        
        let feed = feeds[indexPath.row]
        
        cell.cellImage.image = nil
        if let labelUrl = feed.icon?.encodeURL(){
            cell.cellImage.sd_setImage(with: labelUrl as URL!)
        }
        
        cell.cellTitle.text = feed.qlueTitle
        
        let date = Date().getDateFromTimestamp(feed.timestamp)
        cell.cellTime.text = date?.timeAgoWithNumericDates(true)
     
        cell.cellProgress.textColor = type.color
        cell.cellProgress.text  = feed.progress?.lowercased()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let feed = feeds[indexPath.row]
        presentDetailFeed(withFeedId: feed.feedId, nav: self.navigationController)
    }
}

// MARK: - Navigate
extension QlueViewController{
     func pushRankingReportDetail(_ nav: UINavigationController?, type: RankingReportDetailType){
          if let detail = moreStoryboard.instantiateViewController(withIdentifier: "RankingReportDetail") as? QlueRankingReportDetailController{
               detail.type = type
               
               nav?.pushViewController(detail, animated: true)
          }
     }
}

