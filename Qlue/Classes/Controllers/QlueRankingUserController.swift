//
//  QlueRankingUserController.swift
//  Qlue
//
//  Created by Nurul on 8/14/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueRankingUserController: QlueViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var ranks: [QlueRanking] = []
    var start = 0
    
    
    var sortState: Bool = true // t = top , f: bottom
    @IBOutlet weak var fabView: UIView!{
        didSet{
            fabView.cornerRadius(25)
            fabView.shadowWithColor(UIColor.darkGray)
        }
    }
    @IBOutlet weak var fabButton: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DejalActivityView.addActivityView(for: view)
        
        configureTableView()
        downloadData()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    
    // MARK: - Configure
    fileprivate func configureTableView(){
        tableView.register(UINib.init(nibName: "QlueRankingCell2", bundle: nil), forCellReuseIdentifier: "RankingCell")
        tableView.rowHeight = 60
    }
    
    //MARK: - Data
    fileprivate func downloadData(){
        
        var type  = "top"
        if sortState == false { type = "bottom" }
        
        
        let req = Engine.shared.listRankingUser(withType: type)
        Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
            DejalActivityView.remove()
            }, completion:  { (data, success, fail) in
                
                if self.start == 0{
                    self.ranks = []
                }
                
                if success == true{
                    for d in data.arrayValue{
                        self.ranks.append(QlueRanking.init(fromJson: d))
                    }
                    
                    self.ranks.sort(by: { (r0, r1) -> Bool in
                        if r0.rank! < r1.rank! {
                            return true
                        }
                        
                        return false
                    })
                    self.tableView.reloadData()
                }
                
                
        })
    }
    
    // MARK: - Action
    @IBAction func sortTapped(_ sender: UIButton) {
        sortState = !sortState
        
        self.ranks = []
        tableView.reloadData()
        DejalActivityView.addActivityView(for: self.view)
        downloadData()
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension QlueRankingUserController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ranks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RankingCell", for: indexPath) as! QlueRankingCell2
        
        let rank = ranks[indexPath.row]
        
        cell.cellImage.isHidden = false
        cell.cellRankView.isHidden = true
        if rank.rank == 1{
            cell.cellImage.image = UIImage.init(named: "trophy_01")
        } else if rank.rank == 2{
            cell.cellImage.image = UIImage.init(named: "trophy_02")
        } else if rank.rank == 3{
            cell.cellImage.image = UIImage.init(named: "trophy_03")
        } else{
            
            
            cell.cellImage.isHidden = true
            cell.cellRankView.isHidden = false
            let numb = rank.rank ?? 0
            cell.cellRankLabel.text = numb.description
            
        }
        
        cell.cellLabel.text = rank.nama
        cell.cellKelurahanLabel.text = rank.provinsi
        cell.cellPoint.text = rank.total
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let userId = ranks[indexPath.row].code else { return}
        self.presentOtherProfile(userId)
    }
}
