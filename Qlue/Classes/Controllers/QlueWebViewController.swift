//
//  QlueWebViewController.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 10/17/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueWebViewController: QlueViewController {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!

    var urlString: String?
    var htmlFile: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_arrowback"), style: .plain, target: self, action: Selector("backTapped:"))
        navigationItem.leftBarButtonItem = backButton
        
        if let urlString = urlString {
            if let url = URL(string: urlString) {
                webView.loadRequest(URLRequest(url: url))
            }
        }
        else if let fileName = htmlFile {
            if let htmlFile = Bundle.main.path(forResource: fileName, ofType: "html") {
//                if let url = NSURL(string: htmlFile) {
                    do {
                        let htmlString = try String(contentsOfFile: htmlFile)
                        webView.loadHTMLString(htmlString, baseURL: nil)
                    }
                    catch let error as NSError {
                        print(error.localizedDescription)
                    }
//                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension QlueWebViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        webView.alpha = 0.2
        activityIndicatorView.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.alpha = 1.0
        activityIndicatorView.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webView.alpha = 1.0
        activityIndicatorView.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
}

extension UIViewController {
    
    func presentWebView(_ nav: UINavigationController?, urlString: String, title: String?) {
        
        let webViewController = QlueWebViewController(nibName: "QlueWebViewController", bundle: nil)
        webViewController.urlString = urlString
        webViewController.title = title
        
        if let nav = nav {
            nav.pushViewController(webViewController, animated: true)
        }
        else {
            let navigationController = UINavigationController(rootViewController: webViewController)
            present(navigationController, animated: true, completion: nil)
        }
    }
    
    func presentWebView(_ nav: UINavigationController?, htmlFile: String, title: String?) {
        
        let webViewController = QlueWebViewController(nibName: "QlueWebViewController", bundle: nil)
        webViewController.htmlFile = htmlFile
        webViewController.title = title
        
        if let nav = nav {
            nav.pushViewController(webViewController, animated: true)
        }
        else {
            let navigationController = UINavigationController(rootViewController: webViewController)
            present(navigationController, animated: true, completion: nil)
        }
    }
}
