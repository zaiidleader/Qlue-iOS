//
//  QlueTopicGovController.swift
//  Qlue
//
//  Created by Nurul on 6/19/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueTopicGovController: QlueViewController {
     
     @IBOutlet weak var collectionView: UICollectionView!
     var topics : [QlueParentTopic] = []
     var refreshControl = UIRefreshControl()
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          
          
          // Do any additional setup after loading the view.
          collectionView.register(UINib.init(nibName: "QlueSelectTopicViewCell", bundle: nil), forCellWithReuseIdentifier: "TopicCell")
          
          refreshControl.addTarget(self, action: #selector(QlueTopicGovController.refresh(_:)), for: .valueChanged)
          
          DejalActivityView.addActivityView(for: self.view)
          
          setCurrentLocation()
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     override func viewDidLayoutSubviews() {
          super.viewDidLayoutSubviews()
          setLayoutCollection()
     }
     
     // MARK: - Helpers
     func setLayoutCollection(){
          
          let layout = UICollectionViewFlowLayout()
          let size: CGFloat = ((collectionView.bounds.width - 45) / 4 )
          layout.itemSize  = CGSize(width: size, height: size + 25)
          layout.minimumLineSpacing = 5
          layout.minimumInteritemSpacing = 5
          layout.sectionInset = UIEdgeInsetsMake(15, 15, 0, 15)
          
          collectionView.setCollectionViewLayout(layout, animated: true)
          
          let alignedLayout = FSQCollectionViewAlignedLayout.init()
          
          collectionView.collectionViewLayout = alignedLayout
     }
     
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               let latitude: Double = location.coordinate.latitude
               let longitude: Double = location.coordinate.longitude
               
               downloadData(latitude, lng: longitude)
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QlueTopicGovController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
               
               refreshControl.endRefreshing()
               //            downloadService(nil, longitude: nil)
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }
     
     
     fileprivate func downloadData(_ lat: Double, lng: Double){
          
          
          topics = []
          
          let req =  Engine.shared.getTopicListGoverment(lat: lat, long: lng)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               self.refreshControl.endRefreshing()
               DejalActivityView.remove()
          },completion:  { (data, success, fail) in
               if success == true{
                    
                    if let group = data["group"].array{
                         for topics in group{
                              if topics["suggest_id"].string == "1"{
                                   self.topics.append(QlueParentTopic.parentTopicWithData(topics))
                                   break
                              }
                         }
                         
//                         for topics in group{
//                              if let topic = topics["group_topic"].array{
//                                   for t in topic{
//                                        self.topics.append(QlueTopic.topicWithData(t))
//                                   }
//                              }
//                              
//                         }
                    }
                    
                    self.collectionView.reloadData()
                    
               }
               
          })
          
          
     }
     
     // MARK: - Action
     func refresh(_ sender: UIRefreshControl){
          setCurrentLocation()
     }
     
     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
      }
      */
     
}

// MARK: - UICollectionViewDataSource
extension QlueTopicGovController: UICollectionViewDataSource{
     
     func numberOfSections(in collectionView: UICollectionView) -> Int {
          return topics.count
     }
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return topics[section].groupTopic.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopicCell", for: indexPath) as! QlueSelectTopicViewCell
          
          cell.layer.shouldRasterize = true
          cell.layer.rasterizationScale = UIScreen.main.scale
          
          let topic = topics[indexPath.section].groupTopic[indexPath.item]
          
          cell.cellTitle.text = topic.suggestName
          if let image = topic.suggestIcon?.encodeURL(){
               cell.cellImage.sd_setImage(with: image as URL!)
          }
          
          return cell
     }
}

// MARK: - UICollectionViewDelegate
extension QlueTopicGovController: UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          Engine.shared.currentReport?.parentTopic = topics[indexPath.section]
          Engine.shared.currentReport?.topic = topics[indexPath.section].groupTopic[indexPath.item]
          performSegue(withIdentifier: "Post", sender: self)
     }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueTopicGovController: FSQCollectionViewDelegateAlignedLayout{
     
     func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: FSQCollectionViewAlignedLayout!, sizeForItemAt indexPath: IndexPath!, remainingLineSpace: CGFloat) -> CGSize {
          let size: CGFloat = ((collectionView.bounds.width - 45) / 4 )
          var height = size + 16
          if let label = topics[indexPath.section].groupTopic[indexPath.item].suggestName{
               
               height += label.heightWithConstrainedWidth(size, font: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 12)!)
          }
          
          
          
          return CGSize(width: size, height: height)
          
     }
     func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: FSQCollectionViewAlignedLayout!, attributesForSectionAt sectionIndex: Int) -> FSQCollectionViewAlignedLayoutSectionAttributes! {
          return FSQCollectionViewAlignedLayoutSectionAttributes.topCenterAlignment()
     }
     //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
     //
     //
     //    }
     //
     //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
     //        return 5
     //    }
     //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
     //        return 5
     //    }
     //    
     //    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
     //        return UIEdgeInsetsMake(15, 15, 0, 15)
     //    }
     //    
     //
}

