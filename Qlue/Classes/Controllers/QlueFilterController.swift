//
//  QlueFilterController.swift
//  Qlue
//
//  Created by Nurul on 6/30/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

protocol QlueFilterControllerDelegate {
    func qlueFilterControllerGovermentButtonTapped(_ viewController: QlueFilterController)
    func qlueFilterControllerCompanyButtonTapped(_ viewController: QlueFilterController)
}

class QlueFilterController: QlueViewController {
    var delegate: QlueFilterControllerDelegate?
    
    @IBOutlet weak var wrapView: UIView!{
        didSet{
            wrapView.cornerRadiusWithShadow()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func govermentButtonTapped(_ sender: UIButton) {
        if let delegate = delegate {
            dismiss(animated: true, completion: { 
                delegate.qlueFilterControllerGovermentButtonTapped(self)
            })
        }
    }
    
    @IBAction func companyButtonTapped(_ sender: UIButton) {
        if let delegate = delegate {
            dismiss(animated: true, completion: { 
                delegate.qlueFilterControllerCompanyButtonTapped(self)
            })
        }
    }
}

extension QlueViewController {
     
    func presentFilterViewWithDelegate(_ delegate: QlueFilterControllerDelegate) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "QlueFilter") as? QlueFilterController {
            viewController.delegate = delegate
            semiModalWithViewController(viewController)
        }
    }
}
