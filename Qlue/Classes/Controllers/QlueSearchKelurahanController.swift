//
//  QlueSearchKelurahanController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/27/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreLocation

class QlueSearchKelurahanController: QlueViewController {
    
    // MARK: - Var
     var query = ""{
          didSet{
               self.oldQuery  = oldValue
          }
     }
     var oldQuery = ""
    @IBOutlet weak var tableView: UITableView!
    var page = 0
    var location: CLLocation?
    var kelurahan: [QlueKelurahan] = []
    var refreshControl : UIRefreshControl = UIRefreshControl.init()
    
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DejalActivityView.addActivityView(for: tableView)
        setCurrentLocation()
        configureView()
        
        
    }
     override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
          if query != oldQuery{
               search()
          }
     }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Configure
    func configureView(){
//        tableView.addInfiniteScrollWithHandler { (_) in
//            self.page += 1
//            self.downloadKelurahan()
//        }
        
        tableView.addSubview(refreshControl)
    }
    
    // MARK: - Location
    func setCurrentLocation() {
        
        Engine.shared.locationManager.startUpdatingLocation()
        
        if let location = Engine.shared.currentLocation {
            self.location = location
            self.downloadKelurahan()
            
        }else {
            
            NotificationCenter.default.addObserver(self, selector: #selector(QlueSearchKelurahanController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
            Engine.shared.locationManager.startUpdatingLocation()
        }
    }
    
    func removeObserverLoc() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
        setCurrentLocation()
    }
    
    // MARK: - Data
    func refresh(){
        self.page = 0
        downloadKelurahan()
    }
    
    func downloadKelurahan(){
        let start = page * 25
        let finish = (page + 1) * 25
        let req = Engine.shared.searchKelurahan(start, finish: finish, query: query, lat: location?.coordinate.latitude ?? 0, lng: location?.coordinate.longitude ?? 0)
        Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
            self.refreshControl.endRefreshing()
            self.tableView.finishInfiniteScroll()
            DejalActivityView.remove()
            }, completion: { (data, success, fail) in
                if success == true{
                    if start == 0{
                        self.kelurahan = []
                    }
                    for d in data.arrayValue{
                        self.kelurahan.append(QlueKelurahan.init(fromJson: d))
                    }
                    if (self.kelurahan.count == 0) && (self.query != ""){
                         let emptyBg = QlueSearchEmpty.fromNib()
                         emptyBg.labelButton.setTitle("No Search Result Found", for: .normal)
                         self.tableView.backgroundView = emptyBg
                    } else{
                         self.tableView.backgroundView = UIView.init()
                    }
                    self.tableView.reloadData()
                }
        })
        
    }
     
     // MARK: - Search
     func search() {
          page = 0
          kelurahan = []
          tableView.reloadData()
          
          self.tableView.backgroundView = UIView.init()
          DejalActivityView.addActivityView(for: tableView)
          downloadKelurahan()
     }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: - TableView
extension QlueSearchKelurahanController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return kelurahan.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QlueSearchKelCell
        
        let kel = kelurahan[indexPath.row]
        
        cell.cellLabel.text = kel.name
        
        return cell
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          let kel = kelurahan[indexPath.row]
          presentKelurahan(withId: kel.code, district: kel.district, nav: nil)
     }
}
