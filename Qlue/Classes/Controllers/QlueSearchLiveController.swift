//
//  QlueSearchLiveController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/28/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON

class QlueSearchLiveController: QlueFeedFusionController{

     var query = ""{
          didSet{
               self.oldQuery  = oldValue
          }
     }
     var oldQuery = ""
     
    override func viewDidLoad() {
        super.viewDidLoad()

     
     
     // location
     setCurrentLocation()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     
     override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
          if query != oldQuery{
               search()
          }
     }
     
     // MARK: - Location
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               currentLocation = location
               Engine.shared.checkProvinsi(currentLocation!.coordinate.latitude, lng: currentLocation!.coordinate.longitude, completion: { (result, error) in
                    self.downloadData()
               })
               NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QlueSearchLiveController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }

    
// MARK: - Data

     override func downloadData(){
          guard let loc = currentLocation?.coordinate, view != nil else { return }
          let start = page * 15
          let finish = (page + 1) * 15
          let req = Engine.shared.searchLive(query, start: start, finish: finish, lat: loc.latitude, lng: loc.longitude, filterId: 1, subfilterId: 0)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               self.refreshControl.endRefreshing()
               self.collectionView.finishInfiniteScroll()
               }, completion: { (data, success, fail) in
                    if success == true{
                         if self.page == 0{
                              self.feeds = []
                         }
                         
                         for feed in data.arrayValue{
                              if feed == nil{
                                   continue
                              }
                              self.feeds.append(QlueFeed.feedWithData(feed))
                         }
                         if (self.feeds.count == 0) && (self.query != ""){
                              let emptyBg = QlueSearchEmpty.fromNib()
                              emptyBg.labelButton.setTitle("No Search Result Found", for: .normal)
                              self.collectionView.backgroundView = emptyBg
                         } else{
                              self.collectionView.backgroundView = UIView.init()
                         }
                         
                         self.collectionView.reloadData()
                         
                    }
                    
          })
          
     }
     
     // MARK: - NewSearchDelegate
     func searchTapped(_ controller: QlueNewSearchController, query: String) {
          
          page = 0

          self.feeds = []
          collectionView.reloadData()
          
          DejalActivityView.addActivityView(for: self.collectionView)
          self.collectionView.backgroundView = UIView.init()
          self.query = query
          downloadData()
     }
     
     // MARK: - Search
     func search() {
          if view != nil{
          page = 0
          feeds = []
          collectionView.reloadData()
          
               
               self.collectionView.backgroundView = UIView.init()
          DejalActivityView.addActivityView(for: self.collectionView)
               downloadData()
          }
     }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
