//
//  QlueKProfilController.swift
//  Qlue
//
//  Created by Nurul on 6/8/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import UIScrollView_InfiniteScroll
import FLAnimatedImage
import Alamofire

let kKelurahanChanged = "kKelurahanChanged"
class QlueKProfilController: QlueViewController {
     
     var kelurahan: QlueKelurahan?
     var rating: Double = 0
     var members: [QlueMember]  = []
     var page = 0
     var paging = 25
     
     var kdKel: String?
     var district: String?
     
     var fromTab: Bool = false

     var weatherUrl: String = ""
     
    @IBOutlet var tableView: UITableView!
     override func viewDidLoad() {
          super.viewDidLoad()
          
          tableView.register(UINib.init(nibName: "QlueKHeaderNewCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
          tableView.register(UINib.init(nibName: "QlueKPRateViewCell", bundle: nil), forCellReuseIdentifier: "RateCell")
          tableView.register(UINib.init(nibName: "QlueMemberItemViewCell", bundle: nil), forCellReuseIdentifier: "MemberCell")
          
          
          
          tableView.addInfiniteScroll { (tableView) in
               self.loadMore()
          }
          tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 40, right: 0)
          
          
          DejalBezelActivityView.addActivityView(for: self.view)
          downloadData()
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
          
          if fromTab == false{
               tabBarController?.tabBar.isHidden = true
          }
     }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
     
     func initWithKodeKel(_ id: String?, district: String?){
          self.kdKel = id
          self.district = district
     }
     
     // MARK: - Data
     fileprivate func downloadData(){
          guard let codekel = kdKel, let district = district else {
               DejalActivityView.remove()
               return }
          
          let req = Engine.shared.profileKelurahan(codekel, district: district)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               }, completion:  { (data, success, fail) in
                    if success == true{
                         if let kel = data.array{
                              self.kelurahan = QlueKelurahan.init(fromJson: kel.first)
                              self.checkRating()
                              self.downloadMember()
                              self.tableView.reloadData()
                              self.getWeather()
                         }
                         
                    }
          })
     }
     
     fileprivate func getWeather(){
          guard let lat = kelurahan?.lat, let lng = kelurahan?.lng else { return  }
          let req = Engine.shared.getWeather(lat, lng: lng)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion: { (data, success, fail) in
                    if success == true{
                         self.weatherUrl = data["data"]["url"].stringValue
                         self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
                    }
          }) {
               
          }
     }
     
     fileprivate func checkRating(){
          guard let codeKel = kdKel else { return }
          
          let req = Engine.shared.checkRatingKelurahan(codeKel)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion:  { (data, success, fail) in
                    print(data)
                    if success == true{
                         self.rating = data["rate_kelurahan"].double ?? 0
                         self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                    }
          })
          
     }
     
     fileprivate func downloadMember(){
          guard let codeKel = kdKel, let district = district else { return }
          
          let req = Engine.shared.listMemberKelurahan(codeKel, district: district, start: (page * paging), finish: (page + 1) * paging)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               
               }, completion:  { (data, success, fail) in
                    if success == true{
                         
                         self.tableView.setShouldShowInfiniteScrollHandler { (tableView) -> Bool in
                              if (data.array?.count)! < 1{
                                   return false
                              }
                              
                              return true
                         }
                         
                         
                         if self.page == 0 {
                              self.members = []
                         }
                         
                         for d in data.arrayValue{
                              self.members.append(QlueMember.init(fromJson: d))
                         }
                         
                         self.tableView.reloadData()
                         
                    }
          })
     }
     
     
     
     // MARK: - Helpers
     func loadMore(){
          //          if (members.count) == (page+1)*paging{
          page += 1
          downloadMember()
          //          }
     }
     
     // MARK: - Navigation
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "Diskusi"{
               let destination = segue.destination as? QlueKDiskusiController
//               destination?.forumType = .Kelurahan
               destination?.initWithCodeKel(User.currentUser()?.codeKelurahan)
          } else if segue.identifier == "Laporan" {
               let destination = segue.destination as? QlueKLaporanController
               destination?.initWithKelurahan(kelurahan)
          }
     }
     
     // MARK: - Table view data source
     
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
          // #warning Incomplete implementation, return the number of sections
          return 2
     }
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          // #warning Incomplete implementation, return the number of rows
          if section == 0{
               return 2
          }else{
               return members.count
          }
     }
     
     
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
          
          switch (indexPath.section, indexPath.row) {
          case (0, 0): // Header
               let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! QlueKHeaderNewCell
     
               if let urlImage = Bundle.main.url(forResource: "", withExtension: "gif"), let dataImage = try? Data.init(contentsOf: urlImage){
                    
                    cell.cellGifImage.animatedImage = FLAnimatedImage.init(animatedGIFData: dataImage)
               }
               
               if weatherUrl != ""{
                    Alamofire.request(weatherUrl, method: .get).responseData(completionHandler: { (res) in
                         if let data = res.data {
                              cell.cellGifImage.animatedImage = FLAnimatedImage.init(animatedGIFData: data)
                         }
                    })
               }
               
               cell.cellKelurahanName.text = "\(kelurahan?.name ?? "") - "
               cell.cellLurah.text = kelurahan?.fullName
               cell.cellCodeKel.text = kelurahan?.code
               
               cell.cellReportLabel.text = kelurahan?.jumlahlaporan?.description
               cell.cellRankingLabel.text = kelurahan?.rank
               cell.cellMemberLabel.text = kelurahan?.memberCount?.description
               cell.cellDiscussionLabel.text = kelurahan?.jumlahdiskusi
               
               cell.cellRating.rate = kelurahan?.rating ?? 0
               
               cell.delegate = self

               
               return cell
          case (0, 1): // Rate
               let cell = tableView.dequeueReusableCell(withIdentifier: "RateCell", for: indexPath) as! QlueKPRateViewCell
               
               cell.cellCountReview.setTitle("\(kelurahan?.ratingCount ?? "0") Review", for: UIControlState())
               
               cell.cellStarRating.rate = kelurahan?.rating ?? 0
               cell.cellStarRating.isHidden = true
               if let desc = kelurahan?.desc, desc != ""{
                    cell.cellDesc.text = desc
               } else{
                    cell.cellDesc.text = "Belum diisi"
               }
               cell.cellEmail.text = kelurahan?.jumlahpenduduk
               cell.cellPhone.text = kelurahan?.noTelp
               cell.cellName.text = kelurahan?.name
               cell.cellRating.rate = rating
               cell.cellRating.delegate = self
               
               
               let camera = GMSCameraPosition.camera(withLatitude: kelurahan?.lat ?? 0, longitude: kelurahan?.lng ?? 0, zoom: 16)
               cell.cellMapsView.animate(to: camera)
               
               return cell
          case (1, _): // Members
               let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath) as! QlueMemberItemViewCell
               
               let member = members[indexPath.row]
               if let memberUrl = member.avatar?.encodeURL(){
                    cell.cellImage.sd_setImage(with: memberUrl as URL!)
               }
               
               cell.cellUsername.text = member.username
               
               if member.follow == true{
                    cell.cellFollowButton.backgroundColor = kRedColor
                    cell.cellFollowButton.setTitle("UNFOLLOW", for: UIControlState())
                    cell.cellFollowButton.setTitleColor(UIColor.white, for: UIControlState())
               }else{
                    cell.cellFollowButton.setTitle("+ FOLLOW", for: UIControlState())
                    cell.cellFollowButton.setTitleColor(kQlueBlue, for: UIControlState())
                    cell.cellFollowButton.backgroundColor = UIColor.white
               }
               
               cell.delegate = self
               return cell
          default:
               return UITableViewCell()
          }
     }
     
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
          let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 30))
          view.backgroundColor = kQlueBlue
          
          let label = UILabel.init(frame: CGRect.init(x: 15, y: 0, width: view.bounds.width, height: view.bounds.height))
          label.text = "Members"
          label.font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 13)
          label.textColor = UIColor.white
          view.addSubview(label)
          
          return view
     }
     
     // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
          switch (indexPath.section, indexPath.row) {
          case (0, 0): // Header
               let imageHeight = ( tableView.bounds.width / 2 )
               return imageHeight
          case (0, 1): // Rate
               let bioHeight = "Label".heightWithConstrainedWidth(tableView.bounds.width - 30, font: UIFont.init(name: QlueFont.HelveticaNeue, size: 14)!)
               
               return 153 + bioHeight + 245
          case (1, _): // Members
               return 60
          default:
               return 0
          }
     }
     
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
          if section == 1{
               return 30
          }
          
          return 0
     }
}

// MARK: - QlueRateViewDelegate
extension QlueKProfilController: QlueRateViewDelegate{
     func rating(_ cell: QlueRateView, didRatingSelected rating: Double) {
          guard let codeKel = User.currentUser()?.codeKelurahan, let prov = User.currentUser()?.provinsi else{ return }
          
          let req = Engine.shared.postRatingKelurahan(codeKel, rate: rating, district: prov)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion:  { (data, success, fail) in
                    print("data post rate: \(data)")
                    if success == true{
                         
                    }
          })
     }
}

// MARK: - QlueMemberItemViewCellDelegate
extension QlueKProfilController: QlueMemberItemViewCellDelegate{
     func followButtonTapped(_ cell: QlueMemberItemViewCell, button: UIButton) {
          guard let indexPath = tableView.indexPath(for: cell),  let fid = members[indexPath.row].userId else { return }
          
          
          
          let req = Engine.shared.followMember(withId: fid)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion:  { (data, success, fail) in
                    print("data")
                    if success == true{
                         
                         button.backgroundColor = kRedColor
                         button.setTitle("UNFOLLOW", for: .normal)
                         cell.cellFollowButton.setTitleColor(UIColor.white, for: .normal)
                         self.members[indexPath.row].follow = true
                    }
               }, falseCompletion: { Void in
                    
                    button.backgroundColor = UIColor.white
                    button.setTitle("+ FOLLOW", for: .normal)
                    cell.cellFollowButton.setTitleColor(kQlueBlue, for: .normal)
                    
                    self.members[indexPath.row].follow = false
          })
          
     }
}

// MARK: - QlueKelurahanFeedViewCellDelegate
extension QlueKProfilController: QlueKHeaderNewCellDelegate{
     
     func laporanTapped(_ cell: QlueKHeaderNewCell) {
          presentLaporanKelurahan(self.kelurahan, nav: self.navigationController)
     }
     func rankingTapped(_ cell: QlueKHeaderNewCell) {
          presentKTrophy(kelurahan, nav: self.navigationController)
     }
     func membersTapped(_ cell: QlueKHeaderNewCell) {
          presentKelMember(kelurahan, nav: self.navigationController)
     }
     func discussionTapped(_ cell: QlueKHeaderNewCell) {
          guard let codeKel = kelurahan?.code else{
               return
          }
          presentListForum(withType: .kelurahan(codeKel: codeKel), nav: self.navigationController)
     }
}


// MARK: - Navigate
extension QlueViewController{
     func presentKelurahan(withId kelId: String?, district: String?, nav: UINavigationController?){
          if let kel = kelurahanStoryboard.instantiateViewController(withIdentifier: "ProfileKelurahan") as? QlueKProfilController{
               
               kel.initWithKodeKel(kelId, district: district)
               
               if let nav = nav{
                    nav.pushViewController(kel, animated: true)
               } else{
                    present(kel, animated: true, completion: nil)
               }
               
          }
     }
}
