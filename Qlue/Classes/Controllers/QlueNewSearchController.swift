//
//  QlueNewSearchController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/27/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

enum TabSearch{
    case top
    case live
    case neighborhood
    case citizen
    
    var title: String{
        switch self {
        case .top:
            return "Top"
        case .live:
            return "Live"
        case .neighborhood:
            return "Neighborhood"
        case .citizen:
            return "Citizen"
        }
    }
    
    var textWidth: CGFloat{
        var titleAttr : [String: AnyObject]?
        if let font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 13){
            titleAttr = [NSFontAttributeName : font]
        }
        let nsTitle = NSString.init(string: self.title)
        let size = nsTitle.size(attributes: titleAttr)
        
        return size.width + 30
    }
    
}

class QlueNewSearchController: UIViewController {
     
    // MARK: Search Var
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextfield: UITextField!
     var query  = ""
    
    // MARK: Tab Var
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var tabCollectionView: UICollectionView!
    var tabs : [TabSearch] = [.top, .live, .citizen, .neighborhood]
    var currentTab: TabSearch = .top
    var diffWidth: CGFloat = 0
    
    // MARK: Container Var
    @IBOutlet weak var containerView: UIView!
     lazy var liveController: QlueSearchLiveController = {
          let controller = searchStoryboard.instantiateViewController(withIdentifier: "SearchLive") as! QlueSearchLiveController
          
          return controller
     }()
     lazy var popController: QlueSearchPopController = {
          let controller = searchStoryboard.instantiateViewController(withIdentifier: "SearchPop") as! QlueSearchPopController
          
          return controller
     }()
     lazy var citizenController: QlueSearchCitizenController = {
          let controller = searchStoryboard.instantiateViewController(withIdentifier: "SearchCitizen") as! QlueSearchCitizenController
          
          return controller
     }()
     lazy var kelurahanController: QlueSearchKelurahanController = {
          let controller = searchStoryboard.instantiateViewController(withIdentifier: "SearchKelurahan") as! QlueSearchKelurahanController
          
          return controller
     }()
     var currentController: UIViewController?
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        configureTabView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabCollectionView.selectItem(at: IndexPath.init(row: 0, section: 0), animated: false, scrollPosition: UICollectionViewScrollPosition())
    }
    
    // MARK: - Configure
    func configureView(){
        showPopular()
     searchTextfield.returnKeyType = .search
    }
    func configureTabView(){
        let screenWidth = UIScreen.main.bounds.width
        var tabWidth: CGFloat = 2
        for tab in tabs{
            tabWidth += tab.textWidth
            tabWidth += 8
        }
        
        if tabWidth < screenWidth{
            diffWidth = (screenWidth - tabWidth) / CGFloat(tabs.count)
        }
    }
     
     // MARK: - Tab
     func showLive(){
          if currentController == liveController{
               return
          }
          
          liveController.query = query
          replaceViewController(currentController, withViewController: liveController, inContainerView: containerView)
          currentController = liveController
     }
     func showPopular(){
          if currentController == popController{
               return
          }
          
          popController.query = query
          replaceViewController(currentController, withViewController: popController, inContainerView: containerView)
          currentController = popController
     }
     func showCitizen(){
          if currentController == citizenController{
               return
          }
          
          citizenController.query = query
          replaceViewController(currentController, withViewController: citizenController, inContainerView: containerView)
          currentController = citizenController
     }
     func showKelurahan(){
          if currentController == kelurahanController{
               return
          }
          
          kelurahanController.query = query
          replaceViewController(currentController, withViewController: kelurahanController, inContainerView: containerView)
          currentController = kelurahanController
     }
    
    // MARK: - Action
    @IBAction func searchTapped(_ sender: UIButton) {
        search(searchTextfield.text ?? "")
    }
    
    func search(_ query: String){
        self.query = query
        view.endEditing(true)
        
        switch currentTab {
        case .live:
            liveController.query = query
            currentController = nil
            showLive()
        case .top:
            popController.query = query
            currentController = nil
            showPopular()
        case .citizen:
            citizenController.query = query
          citizenController.search()
        case .neighborhood:
            kelurahanController.query = query
          kelurahanController.search()
        }

    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: - Tab CollectionView
extension QlueNewSearchController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TabCell", for: indexPath) as! SearchTabCell
        
        let tab = tabs[indexPath.item]
     cell.layoutIfNeeded()
        cell.activeView.cornerRadius(cell.activeView.bounds.height / 2)
        cell.label.text = tab.title
        
        if tab == currentTab {
            cell.isActive = true
        } else{
            cell.isActive = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let tab = tabs[indexPath.item]
        let width = tab.textWidth + diffWidth
        return CGSize(width: width, height: 25)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8.5, left: 5, bottom: 0, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.currentTab = tabs[indexPath.row]
     switch currentTab {
     case .live:
          showLive()
     case .top:
          showPopular()
     case .citizen:
          showCitizen()
     case .neighborhood:
          showKelurahan()
     }
        collectionView.reloadData()
    }

    
}

// MARK: - Search Textfield
extension QlueNewSearchController: UITextFieldDelegate{
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          search(textField.text ?? "")
          
          return true
     }
}
