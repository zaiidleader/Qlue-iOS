//
//  QlueProfileReportController.swift
//  Qlue
//
//  Created by Nurul on 7/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueProfileReportController: QlueProfileBaseController {
     
     // MARK: - Vars
     // MARK: Outlets
     @IBOutlet weak var collectionView: UICollectionView!
     
     // MARK: Data
     var feeds: [QlueFeed] = []
     var page = 0
     
     // MARK: - View Lifecycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          configureCollectionView()
          DejalActivityView.addActivityView(for: collectionView)
          collectionView.addInfiniteScroll { (_) in
               if self.feeds.count >= (self.page + 1) * 15{
                    self.loadMore()
               } else{
                    self.collectionView.finishInfiniteScroll()
               }
          }
          downloadData()
          
          if userId != User.currentUser()?.userId{
               setupFab()
          }
          
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Configure
     
     func configureCollectionView(){
          collectionView.register(UINib.init(nibName: "QlueKLDetailViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
     }
     // MARK: - Data
     func loadMore(){
          page += 1
          downloadData()
     }
     func downloadData(){
          
          guard let userId = userId else{
               DejalActivityView.remove()
               return
          }
          
          let start = page * 15
          let finish = (page + 1) * 15
          
          let req = Engine.shared.listReportUsert(userId: userId, start: start, finish: finish)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               self.collectionView.finishInfiniteScroll()
               DejalActivityView.remove()
               
               }, completion:  { (data, success, fail) in
                    if success == true{
                         
                         if self.page == 0{
                              self.feeds = []
                         }
                         
                         for feed in data.arrayValue{
                              self.feeds.append(QlueFeed.feedWithData(feed))
                         }
                         
                         self.collectionView.reloadData()
                         
                    }
          })
          
     }

     
}


// MARK: - UICollectionViewDataSource
extension QlueProfileReportController: UICollectionViewDataSource{
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return feeds.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! QlueKLDetailViewCell
          
          let feed = feeds[indexPath.item]
          
          if let state = feedState(rawValue: feed.progress ?? "process"){
               cell.cellImage.backgroundColor = state.colorGrid
               cell.cellImage.alpha = 0.8
          }
          
          cell.cellImageFeed.image = nil
          if let file = feed.fileUrl?.encodeURL(){
               cell.cellImageFeed.sd_setImage(with: file as URL!)
               
          }
          
          return cell
     }
}

// MARK: - UICollectionViewDelegate
extension QlueProfileReportController: UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          presentDetailFeed(withFeedId: feeds[indexPath.item].feedId, nav: self.navigationController)
     }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueProfileReportController: UICollectionViewDelegateFlowLayout{
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          let size: CGFloat = (collectionView.bounds.width / 3) - 1
          
          return CGSize(width: size, height: size)
          
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return 1
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 1
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsets.zero
     }
     
     
}
