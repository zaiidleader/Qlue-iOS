//
//  QluePreviewViewController.swift
//  Qlue
//
//  Created by Nurul on 7/14/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import AVFoundation

enum Media {
     case photo(image: UIImage)
     case video(url: URL)
     
     var description: String{
          switch self {
          case .photo( _): return "image"
          case .video( _): return "video"
          }
          
     }
}

class QluePreviewViewController: QlueViewController {
     @IBOutlet weak var imageView: UIImageView!
     @IBOutlet weak var buttonClose: UIButton!
     var media: Media!
     
     override func viewWillAppear(_ animated: Bool) {
          switch self.media! {
          case .photo(let image): self.imageView.image = image
          case .video(let url): self.playVideo(url)
          }
     }
     
     
     
     override func viewDidLoad() {
          super.viewDidLoad()
          self.view.backgroundColor = UIColor.black
     }
     
     
     
     @IBAction func closePreview(_ sender: AnyObject) {
          self.dismiss(animated: true, completion: nil)
     }
     
     fileprivate func playVideo(_ url: URL) {
          print("display video for url : \(url.absoluteString)")
          
          let player = AVPlayer.init(url: url)
          player.actionAtItemEnd = .none
          
          
          let playerLayer = AVPlayerLayer.init(player: player)
          
          NotificationCenter.default.addObserver(self,
                                                           selector: #selector(QluePreviewViewController.playerItemDidReachEnd(_:)),
                                                           name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                           object: player.currentItem)
          
          let screenRect = UIScreen.main.bounds
          playerLayer.frame = CGRect.init(x: 0, y: 0, width: screenRect.size.width, height: screenRect.size.height)
          
          view.layer.insertSublayer(playerLayer, at: 0)
          
          player.play()
     }
     
     // MARK: - Action
     func playerItemDidReachEnd(_ sender: Notification){
          
          let playerItem = sender.object
          (playerItem as AnyObject).seek(to: kCMTimeZero)
          
     }
     @IBAction func okTapped(_ sender: UIButton) {
          
          Engine.shared.currentReport?.format = media
          
          switch self.media! {
          case .photo(let image): Engine.shared.currentReport?.image = image
          case .video(let url):
               Engine.shared.currentReport?.image = Util.thumbnailImageFromVideoURL(url)
               Engine.shared.currentReport?.videoUrl = url
          }
          
          if Engine.shared.currentReport?.qlueType == .goverment{
               
               performSegue(withIdentifier: "TopicGoverment", sender: self)
          } else{
               performSegue(withIdentifier: "TopicCompany", sender: self)
          }
          
     }
}

extension QlueViewController{
     func presentPreviewImageWithMedia(_ media: Media, nav: UINavigationController?){
          if let preview = reportStoryboard.instantiateViewController(withIdentifier: "Preview") as? QluePreviewViewController{
               preview.media = media
               
               if let nav = nav{
                    nav.pushViewController(preview, animated: true)
                    
                    return
               }
               present(preview, animated: true, completion: nil)
          }
     }
}
