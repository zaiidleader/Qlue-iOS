//
//  QluePostForumController.swift
//  Qlue
//
//  Created by Nurul on 8/14/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import Photos
import SwiftyJSON

enum ForumPostType {
     case forum
     case comment(wid: String)
     case editForum(forum: QlueDiskusi?)
}

let kForumCreated = "kForumCreated"
let kForumEdited = "kForumEdited"

class QluePostForumController: QlueViewController {
     
     var forumType: ForumType = .general
     var type: ForumPostType = .forum
     
     @IBOutlet weak var collectionView: UICollectionView!
     let cachingImageManager = PHCachingImageManager()
     var assets: [PHAsset] = [] {
          willSet{
               cachingImageManager.stopCachingImagesForAllAssets()
          }
          
          didSet{
               cachingImageManager.startCachingImages(
                    for: self.assets,
                    targetSize: PHImageManagerMaximumSize,
                    contentMode: .aspectFit,
                    options: nil)
          }
     }
     var bgView = BgViewPostForum()
     var topic: QlueForumTopic?
     var location: CLLocation?
     
     var completionDone: () -> () = {}
     
     // MARK: - View Lifecycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          setCurrentLocation()
          configureCollectionView()
          fetchPhotoFromLibrary()
          getTopicId()
          
          
          // Do any additional setup after loading the view.
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     // MARK: - Location
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               self.location = location
               
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QluePostForumController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }

     
     // MARK: - Data
     fileprivate func getTopicId(){
          
          var isKel = false
          switch forumType {
          case .kelurahan(_):
               isKel = true
          default:
               isKel = false
          }
          
          let req = Engine.shared.getTopicForum(isKelurahan: isKel)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
          
               }, completion:  { (data, success, fail) in
                    if success == true{
                         self.topic = QlueForumTopic.init(fromJson: data.arrayValue.first)
                         self.configureTopic()
                    }
          })
     }
     
     // MARK: - Configure
     func configureCollectionView(){
          let layout = UICollectionViewFlowLayout()
          let size = (UIScreen.main.bounds.width / 3) + 0.3
          layout.itemSize = CGSize(width: size, height: size)
          layout.minimumLineSpacing = -2
          layout.minimumInteritemSpacing = -2
          collectionView.setCollectionViewLayout(layout, animated: false)
          
          collectionView.contentInset = UIEdgeInsetsMake(UIScreen.main.bounds.width , 0, 0, 0)
          
          bgView = BgViewPostForum.fromNib()
          bgView.delegate = self
          switch type {
               
          case .comment(_):
               bgView.titleHeight.constant = 0
               bgView.titleTextField.text = "."
               bgView.votingButton.isHidden = true
               bgView.title = "Comment"
          case .editForum(let forum):
               bgView.votingButton.isHidden = true
               bgView.cameraButton.isHidden = true
               
               bgView.titleTextField.text = forum?.title
               bgView.descText = forum?.desc
               
               collectionView.reloadData()
               
          default:
               break
               }
          
          collectionView.backgroundView = bgView
          
     }
     func configureTopic(){
          bgView.forumTopicName.text = topic?.title
     }
     
     // MARK: - List Photo from library
     func fetchPhotoFromLibrary(){
          let options = PHFetchOptions()
          options.sortDescriptors = [
               NSSortDescriptor(key: "creationDate", ascending: false)
          ]
          
          let results = PHAsset.fetchAssets(with: .image, options: options)
          results.enumerateObjects({ (object, index, stop) in
               if let asset = object as? PHAsset{
                    self.assets.append(asset)
               }
               if index > 100{
                    stop.pointee = true
               }
          })
          
          collectionView.reloadData()
     }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate
extension QluePostForumController: UICollectionViewDataSource, UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          switch type {
          case .editForum(_):
               return 0
          default:
               return assets.count + 1
          }
          
          
     }
     
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
          
          if indexPath.row == 0{
               if let imageView =  cell.viewWithTag(99) as? UIImageView{
                    imageView.image = nil
               }
               return cell
          }
          
          let manager = PHImageManager.default()
          if cell.tag != 0{
               manager.cancelImageRequest(PHImageRequestID(cell.tag))
          }
          
          let asset = assets[indexPath.item - 1]
          
          cell.tag = Int(manager.requestImage(for: asset,
               targetSize: CGSize.init(width: 200, height: 200),
               contentMode: .aspectFill,
               options: nil,
               resultHandler: { (image, _) in
                    if let imageView =  cell.viewWithTag(99) as? UIImageView {
                         let fixedImage = image?.fixImageOrientation()
                         imageView.image = fixedImage
                    }
          }))
          
          return cell
     }
     
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          if indexPath.row == 0{
               if UIImagePickerController.isSourceTypeAvailable(.camera){
                    let imagePicker = UIImagePickerController()
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .camera
                    imagePicker.delegate = self
                    
                    self.present(imagePicker, animated: true, completion: nil)
               }
          } else{
               let manager = PHImageManager.default()
               
               let option: PHImageRequestOptions = PHImageRequestOptions.init()
               option.isSynchronous = true
               manager.requestImage(for: assets[indexPath.item - 1],
                                            targetSize: CGSize.init(width: 300, height: 300),
                                            contentMode: .aspectFill,
                                            options: option,
                                            resultHandler: { (image, _) in
                                             if let image = image {
                                                  let fixedImage = image.fixImageOrientation()
                                                  self.bgView.addPhoto(withImage: fixedImage)
                                             }
                                             
               })
          }
     }
}

// MARK: - UIImagePickerControllerDelegate
extension QluePostForumController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
     func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
          dismiss(animated: true, completion: nil)
     }
     
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
          if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
               let fixedImage = image.fixImageOrientation()
               bgView.addPhoto(withImage: fixedImage)
          }
          
          dismiss(animated: true, completion: nil)
     }
     
}
 
// MARK: - BgViewPostForumDelegate
extension QluePostForumController: BgViewPostForumDelegate{
     func sendForumTapped(_ view: BgViewPostForum, vote: String) {
          
          DejalBezelActivityView.addActivityView(for: self.view)
          switch type {
          case .comment(let id):
               let req = Engine.shared.postCommentForum(withId: id, comment:view.contentTextView?.text ?? "", file: view.getPhoto())
               Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
                    DejalActivityView.remove()
                    },completion: { (data, success, fail) in
                         if success == true{
                              self.completionDone()
                              self.dismiss(animated: true, completion: nil)
                         }
               })
               
          case .forum:
          
          let req = Engine.shared.postForum(view.title, desc: view.contentTextView?.text ?? "", categoryId: topic?.categoryId ?? "", lat: location?.coordinate.latitude ?? 0, lng: location?.coordinate.longitude ?? 0, isVote: view.votingSelected, listVote: vote, file: bgView.getPhoto())
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               },completion: { (data, success, fail) in
                    if success == true{
                         
                         self.completionDone()
                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: kForumCreated), object: nil)
                         self.dismiss(animated: true, completion: nil)
                    }
          })
               
          case .editForum(let forum):
               
               DejalBezelActivityView.addActivityView(for: self.collectionView)
               let req = Engine.shared.editForum(withForumId: forum?.id ?? "", desc: view.contentTextView?.text ?? "")
               Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
                    DejalBezelActivityView.remove()
                    }, completion: { (data, success, fail) in
                         if success == true{
                              NotificationCenter.default.post(name: NSNotification.Name(rawValue: kForumEdited), object: nil)
                              self.dismiss(animated: true, completion: nil)
                         }
                    })
               
               break
               }
          
     }
     func closeForumTapped(_ view: BgViewPostForum) {
          let alert = UIAlertController.init(title: "Discard", message: "Are you sure want to discard this?", preferredStyle: .alert)
          let ok = UIAlertAction.init(title: "Yes", style: .default) { (action) in
               self.dismiss(animated: true, completion: nil)
          }
          
          alert.addAction(ok)
          let no = UIAlertAction.init(title: "No", style: .cancel, handler: nil)
          alert.addAction(no)
          
          present(alert, animated: true, completion: nil)
     }
}


// MARK: - Navigation
extension QlueViewController{
     func presentPostForum(withType type: ForumPostType, forumType: ForumType, completion: @escaping () -> () = {}){
          if let post = forumStoryboard.instantiateViewController(withIdentifier: "PostForum") as? QluePostForumController{
               post.type = type
               post.forumType = forumType
               post.completionDone = completion
               semiModalWithViewController(post)
          }
     }
}
