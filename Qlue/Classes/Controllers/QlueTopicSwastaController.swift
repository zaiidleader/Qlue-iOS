//
//  QlueTopicSwastaController.swift
//  Qlue
//
//  Created by Nurul on 7/14/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueTopicSwastaController: QlueViewController {
     
     @IBOutlet weak var collectionView: UICollectionView!
     var topics : [QlueParentTopic] = []
     var refreshControl = UIRefreshControl()
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          // Do any additional setup after loading the view.
          collectionView.register(UINib.init(nibName: "QlueSelectTopicViewCell", bundle: nil), forCellWithReuseIdentifier: "TopicCell")
          collectionView.register(UINib.init(nibName: "QlueCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCell")
          refreshControl.addTarget(self, action: #selector(QlueTopicGovController.refresh(_:)), for: .valueChanged)
          
          DejalActivityView.addActivityView(for: self.view)
          
          setCurrentLocation()
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     override func viewDidLayoutSubviews() {
          super.viewDidLayoutSubviews()
          setLayoutCollection()
     }
     
     // MARK: - Helpers
     func setLayoutCollection(){
          
          let layout: SectionBackgroundLayout = self.collectionView.collectionViewLayout as! SectionBackgroundLayout
          let size: CGFloat = ((collectionView.bounds.width - 45) / 4 )
          layout.itemSize  = CGSize(width: size, height: size + 16 + 36)
          layout.minimumLineSpacing = 5
          layout.minimumInteritemSpacing = 5
          layout.sectionInset = UIEdgeInsetsMake(0, 10, 15, 10)
          layout.decorationViewOfKinds = ["SectionBackgroundView1"]
          layout.alternateBackgrounds = true
          
     }
     
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               let latitude: Double = location.coordinate.latitude
               let longitude: Double = location.coordinate.longitude
               
               downloadData(latitude, lng: longitude)
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QlueTopicGovController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
               
               refreshControl.endRefreshing()
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }
     
     
     fileprivate func downloadData(_ lat: Double, lng: Double){
          
          
          topics = []
          
          let req =  Engine.shared.getTopicListSwasta(lat: lat, long: lng)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               self.refreshControl.endRefreshing()
               DejalActivityView.remove()
          },completion:  { (data, success, fail) in
               if success == true{
                    
                    if let group = data["group"].array{
                         for topic in group{
                              self.topics.append(QlueParentTopic.parentTopicWithData(topic))
                              
                         }
                    }
                    
                    self.collectionView.reloadData()
                    
               }
               
          })
          
          
     }
     
     // MARK: - Action
     func refresh(_ sender: UIRefreshControl){
          setCurrentLocation()
     }
     
     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
      }
      */
     
}

// MARK: - UICollectionViewDataSource
extension QlueTopicSwastaController: UICollectionViewDataSource{
     
     func numberOfSections(in collectionView: UICollectionView) -> Int {
          return topics.count
     }
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return topics[section].groupTopic.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopicCell", for: indexPath) as! QlueSelectTopicViewCell
          
          cell.layer.shouldRasterize = true
          cell.layer.rasterizationScale = UIScreen.main.scale
          
          
          let topic = topics[indexPath.section].groupTopic[indexPath.item]
          
          cell.cellTitle.text = topic.suggestName
          if let image = topic.suggestIcon?.encodeURL(){
               cell.cellImage.sd_setImage(with: image as URL!)
          }
          
          return cell
     }
     
     func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
          
          if kind == UICollectionElementKindSectionFooter{
               
               let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "FooterCell", for: indexPath)
               
               reusableView.backgroundColor = kGreyEEE
               
               return reusableView
          } else if kind == UICollectionElementKindSectionHeader{
               let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCell", for: indexPath)
               
               if let label = reusableView.viewWithTag(99) as? UILabel{
                    label.text = topics[indexPath.section].suggestName
               }
               
               return reusableView
          }
          
          return UICollectionReusableView()
     }
     
}



// MARK: - UICollectionViewDelegate
extension QlueTopicSwastaController: UICollectionViewDelegate{
     
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          Engine.shared.currentReport?.parentTopic = topics[indexPath.section]
          Engine.shared.currentReport?.topic = topics[indexPath.section].groupTopic[indexPath.item]
          performSegue(withIdentifier: "Post", sender: self)
     }
     
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueTopicSwastaController: UICollectionViewDelegateFlowLayout{
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
          return CGSize.init(width: collectionView.bounds.width, height: 20)
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
          return CGSize.init(width: collectionView.bounds.width, height: 50)
     }
}

