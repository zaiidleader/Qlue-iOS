//
//  QlueProfileViewController.swift
//  Qlue
//
//  Created by Nurul on 7/2/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueProfileViewController: QlueViewController {

    // MARK: - Outlets
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var followersButton: UIButton!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var joinDate: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var xpLabel: UILabel!
    @IBOutlet weak var xpProgress: UIProgressView!{
        didSet{
            xpProgress.cornerRadius(3)
        }
    }
    @IBOutlet weak var bio: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var kelurahanButton: UIButton!
    @IBOutlet weak var editProfileButton: UIButton!{
        didSet{
            editProfileButton.border(1, color: UIColor.lightGray)
            editProfileButton.cornerRadius(5)
        }
    }
    
    // MARK: - Private Vars
    fileprivate var user: User?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
     
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          
          user = User.currentUser()
          setupView()
     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private Func
    func setupView(){
        if let banner = user?.banner?.encodeURL(){
            bannerImage.sd_setImage(with: banner as URL!)
        }
        
        followersButton.setTitle("0", for: UIControlState())
        if let follower = user?.followerCount{
            followersButton.setTitle(String(describing: follower), for: UIControlState())
        }
        
        followingButton.setTitle("0", for: UIControlState())
        if let following = user?.followingCount{
            followingButton.setTitle(String(describing: following), for: UIControlState())
        }
        
        if let ava = user?.avatar?.encodeURL(){
            avatarImage.sd_setImage(with: ava as URL!)
        }
        
        fullnameLabel.text = user?.username
        usernameLabel.text = user?.fullname
        joinDate.text = user?.timeJoin?.formatShortStyle()
        bio.text = user?.bio
        email.text = user?.email
        phone.text = user?.phone
        kelurahanButton.setTitle(user?.kelurahan, for: UIControlState())
        
        xpLabel.text = "0 of 0 XP"
        if let currXp = user?.currXp, let nextXp = user?.nextXp{
            xpLabel.text = "\(currXp) of \(nextXp) XP"
            xpProgress.progress = currXp.floatValue / nextXp.floatValue
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
