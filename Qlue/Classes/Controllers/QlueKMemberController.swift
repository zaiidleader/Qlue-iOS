//
//  QlueKMemberController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueKMemberController: QlueViewController {
     
     @IBOutlet weak var tableView: UITableView!
     var members: [QlueMember]  = []
     var page = 0
     var paging = 25
     
     var kelurahan: QlueKelurahan?
     
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          tableView.register(UINib.init(nibName: "QlueMemberItemViewCell", bundle: nil), forCellReuseIdentifier: "MemberCell")
          tableView.addInfiniteScroll { (tableView) in
               self.loadMore()
          }
          tableView.rowHeight = 55
          DejalActivityView.addActivityView(for: tableView)
          self.downloadMember()
          // Do any additional setup after loading the view.
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Data
     fileprivate func downloadMember(){
          guard let codeKel = kelurahan?.code, let district = kelurahan?.district else { return }
          
          let req = Engine.shared.listMemberKelurahan(codeKel, district: district, start: (page * paging), finish: (page + 1) * paging)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               self.tableView.finishInfiniteScroll()
               DejalActivityView.remove()
               
               }, completion:  { (data, success, fail) in
                    if success == true{
                         
                         self.tableView.setShouldShowInfiniteScrollHandler { (tableView) -> Bool in
                              if data.array!.count < 1 {
                                   return false
                              }
                              
                              return true
                         }
                         
                         
                         if self.page == 0 {
                              self.members = []
                         }
                         
                         for d in data.arrayValue{
                              self.members.append(QlueMember.init(fromJson: d))
                         }
                         
                         self.tableView.reloadData()
                         
                    }
          })
     }
     
     // MARK: - Helpers
     func loadMore(){
          page += 1
          downloadMember()
     }
     
}

// MARK: - Table View
extension QlueKMemberController: UITableViewDataSource, UITableViewDelegate{
     
     // MARK: UITableViewDataSource
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return members.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath) as! QlueMemberItemViewCell
          
          let member = members[indexPath.row]
          if let memberUrl = member.avatar?.encodeURL(){
               cell.cellImage.sd_setImage(with: memberUrl as URL!)
          }
          
          cell.cellUsername.text = member.username
          
          if member.follow == true{
               cell.cellFollowButton.backgroundColor = kRedColor
               cell.cellFollowButton.setTitle("UNFOLLOW", for: UIControlState())
               cell.cellFollowButton.setTitleColor(UIColor.white, for: UIControlState())
          }else{
               cell.cellFollowButton.setTitle("+ FOLLOW", for: UIControlState())
               cell.cellFollowButton.setTitleColor(kQlueBlue, for: UIControlState())
               cell.cellFollowButton.backgroundColor = UIColor.white
          }
          
          cell.delegate = self
          return cell
     }
     // MARK: UITableViewDelegate
     
}

// MARK: - QlueMemberItemViewCellDelegate
extension QlueKMemberController: QlueMemberItemViewCellDelegate{
     func followButtonTapped(_ cell: QlueMemberItemViewCell, button: UIButton) {
          guard let indexPath = tableView.indexPath(for: cell),  let fid = members[indexPath.row].userId else { return }
          
          
          
          let req = Engine.shared.followMember(withId: fid)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion:  { (data, success, fail) in
                    print("data")
                    if success == true{
                         
                         button.backgroundColor = kRedColor
                         button.setTitle("UNFOLLOW", for: .normal)
                         cell.cellFollowButton.setTitleColor(UIColor.white, for: .normal)
                         self.members[indexPath.row].follow = true
                    }
               }, falseCompletion: { Void in
                    
                    button.backgroundColor = UIColor.white
                    button.setTitle("+ FOLLOW", for: .normal)
                    cell.cellFollowButton.setTitleColor(kQlueBlue, for: .normal)
                    
                    self.members[indexPath.row].follow = false
          })
          
     }
}

// MARK: - Navigation
extension QlueViewController{
     func presentKelMember(_ kelurahan: QlueKelurahan?, nav: UINavigationController?){
          if let member = kelurahanStoryboard.instantiateViewController(withIdentifier: "KMember") as? QlueKMemberController{
               
               member.kelurahan = kelurahan
               
               if let nav = nav{
                    nav.pushViewController(member, animated: true)
               } else{
                    present(member, animated: true, completion: nil)
               }
          }
     }
}
