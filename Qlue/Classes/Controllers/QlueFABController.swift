//
//  QlueFABController.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 12/11/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

enum FABType {
    case general
    case help
    case ask
    case report
    case review
    case market
    
    var icon: UIImage? {
        switch self {
        case .general:
            return UIImage(named: "fab_laporanbebas")
        case .help:
            return UIImage(named: "fab_mintatolong")
        case .ask:
            return UIImage(named: "fab_tanyatetangga")
        case .report:
            return UIImage(named: "fab_laporpermasalahan")
        case .review:
            return UIImage(named: "fab_reviewtempat")
        case .market:
            return UIImage(named: "fab_jualbelipinjam")
        }
    }
}

enum FABPostingType {
    case text
    case voting
    case photo
}

enum FABContentType {
    case photo
    case voting
    case location
    
    var icon: UIImage? {
        switch self {
        case .photo:
            return UIImage(named: "ic_camera")
        case .voting:
            return UIImage(named: "ic_voting")
        case .location:
            return nil
        }
    }
    
    var title: String {
        switch self {
        case .photo:
            return "Photo"
        case .voting:
            return "Create Voting"
        case .location:
            return ""
        }
    }
}

class QlueFABController: QlueViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var fabIconImageView: UIImageView!
    
    lazy var user: User? = {
        return User.currentUser()
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var fabContentTypes: [FABContentType] = [.photo, .voting]
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    
    var photos: [UIImage] = []
    @IBOutlet weak var fabPhotosView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var choices: [String] = []
    @IBOutlet weak var fabVotingView: UIView!
    @IBOutlet weak var votingTableView: UITableView!
    
    
    var fabType: FABType = .general
    
    var fabPostingType: FABPostingType = .text {
        didSet {
            if fabPostingType == .photo {
                textViewHeightConstraint.constant = 30
                fabPhotosView.isHidden = false
                fabVotingView.isHidden = true
            }
            else if fabPostingType == .voting {
                textViewHeightConstraint.constant = 30
                fabPhotosView.isHidden = true
                fabVotingView.isHidden = false
            }
            else {
                textViewHeightConstraint.constant = view.bounds.height
                fabPhotosView.isHidden = true
                fabVotingView.isHidden = true
            }
            
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            }) 
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupViews()
        
        fabPostingType = .text
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !viewDidAppearedOnce {
            viewDidAppearedOnce = true
            
            switch fabType {
            case .general:
                self.fabContentTypes = [.photo, .voting]
                self.textView.placeholder = "Post sesuatu ke lingkunganmu..."
                self.fabIconImageView.image = UIImage(named: "fab_laporanbebas")
                
            case .help:
                self.fabContentTypes = [.photo]
                self.textView.placeholder = "Tulis detil permintaaan tolong ke tetangga..."
                self.fabIconImageView.image = UIImage(named: "fab_mintatolong")
                
            case .ask:
                self.fabContentTypes = [.photo]
                self.textView.placeholder = "Mau tanya apa ke tetanggamu...?"
                self.fabIconImageView.image = UIImage(named: "fab_tanyatetangga")
                
            case .report:
                self.fabContentTypes = [.photo]
                self.fabIconImageView.image = UIImage(named: "fab_laporpermasalahan")
                
            case .review:
                self.fabContentTypes = [.photo]
                self.fabIconImageView.image = UIImage(named: "fab_reviewtempat")
                
            case .market:
                self.fabContentTypes = [.photo]
                self.textView.placeholder = "Detill barang / jasa yang ingin ditawarkan / dicari..."
                self.fabIconImageView.image = UIImage(named: "fab_jualbelipinjam")
            }
            self.tableViewHeightConstraint.constant = CGFloat(self.fabContentTypes.count * 52)
            UIView.animate(withDuration: 0.35, animations: {
                self.view.layoutIfNeeded()
            }) 
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Helpers
    
    func setupViews() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        let image = UIImage(named: "ic_back")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        backButton.setImage(image, for: UIControlState())
        
        if let avatar = user?.avatar?.encodeURL() {
            userImageView.sd_setImage(with: avatar as URL!)
        }
        let attText = NSMutableAttributedString()
        if let username = user?.username {
            attText.append(NSAttributedString(string: "\(username) ", attributes: [NSFontAttributeName: UIFont(name: QlueFont.ProximaNovaSoft, size: 15)!, NSForegroundColorAttributeName: UIColor.black]))
        }
        if let currLevel = user?.kelurahan {
            attText.append(NSAttributedString(string: "\n\(currLevel)", attributes: [NSFontAttributeName: UIFont(name: QlueFont.ProximaNovaRegular, size: 13)!, NSForegroundColorAttributeName: UIColor.darkGray]))
        }
        userLabel.attributedText = attText
        fabIconImageView.image = fabType.icon
        
        votingTableView.tableFooterView = UIView()
    }
    
    // MARK: - Antions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        if navigationController?.viewControllers.first == self {
            dismiss(animated: true, completion: nil)
        }
        else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func postButtonTapped(_ sender: UIButton) {
        
        if fabPostingType == .voting && choices.count == 0 {
            return
        }
        
        if fabPostingType == .photo && photos.count == 0 {
            return
        }
        
        if textView.text == nil || textView.text.characters.count == 0 {
            return
        }
        
        guard let coordinate = Engine.shared.currentLocation?.coordinate else {
            return
        }
        
        let title = textView.text
        
        var isVote = false
        var listVote = ""
        if fabPostingType == .voting {
            isVote = true
            listVote = choices.joined(separator: ",")
        }
        
        Engine.shared.postForum(title!, desc: "", categoryId: "2", lat: coordinate.latitude, lng: coordinate.longitude, isVote: isVote, listVote: listVote, file: photos)
        
        if navigationController?.viewControllers.first == self {
            dismiss(animated: true, completion: nil)
        }
        else {
            navigationController?.popViewController(animated: true)
        }
    }
}

// MARK: - UICollectionViewDataSource
extension QlueFABController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCellId", for: indexPath) as! QlueFABPhotoViewCell
        
        cell.imageView.image = photos[indexPath.item]
        
        cell.moreLabel.isHidden = true
        if indexPath.item == 3 && photos.count > 4 {
            cell.moreLabel.text = "+\(photos.count - 3)"
            cell.moreLabel.isHidden = false
        }
        
        return cell
    }
}


// MARK: - UICollectionViewDelegateFlowLayout
extension QlueFABController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if photos.count == 1 {
            return collectionView.frame.size
        }
        
        let width = (collectionView.frame.width - 8) / 2
        if photos.count == 3 && indexPath.item == 0 {
            return CGSize(width: width, height: collectionView.frame.height)
        }
        else {
            return CGSize(width: width, height: width)
        }
    }
}


// MARK: - UITableViewDataSource
extension QlueFABController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tableView {
            return fabContentTypes.count
        }
        else {
            return choices.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContentTypeCellId", for: indexPath)
            
            let fabContentType = fabContentTypes[indexPath.row]
            if let imageView = cell.viewWithTag(101) as? UIImageView {
                imageView.image = fabContentType.icon
            }
            if let label = cell.viewWithTag(102) as? UILabel {
                label.text = fabContentType.title
            }
            
            return cell
        }
        else {
            
            if indexPath.row == choices.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddMoreCellId", for: indexPath)
                
                if indexPath.row == 0 {
                    cell.textLabel?.text = "+ Add"
                }
                else {
                    cell.textLabel?.text = "+ Add More"
                }
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChoiceCellId", for: indexPath)
                cell.textLabel?.text = choices[indexPath.row]
                return cell
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension QlueFABController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView == self.tableView {
            let fabContentType = fabContentTypes[indexPath.row]
            switch fabContentType {
            case .photo:
                presentFABGallery(navigationController, completion: { (images) in
                    self.photos = images
                    if images.count > 0 {
                        self.fabPostingType = .photo
                        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                            if images.count == 3 {
                                layout.scrollDirection = .horizontal
                            }
                            else {
                                layout.scrollDirection = .vertical
                            }
                        }
                        self.collectionView.reloadData()
                        
                    }
                    else {
                        self.fabPostingType = .text
                    }
                })
                
                
            case .voting:
                self.fabPostingType = .voting
                
            default:
                break
            }
        }
        else {
            if indexPath.row == choices.count {
                
                let alertController = UIAlertController(title: "Vote", message: nil, preferredStyle: .alert)
                alertController.addTextField(configurationHandler: { (textField) in
                    
                })
                alertController.addAction(UIAlertAction(title: "Add", style: .default, handler: { (alertAction) in
                    if let text = alertController.textFields?.first?.text {
                        self.choices.append(text)
                        self.votingTableView.reloadData()
                    }
                }))
                alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if tableView == self.tableView {
            return false
        }
        else {
            return !(indexPath.row == choices.count)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if tableView == self.tableView {
            
        }
        else {
            if editingStyle == .delete {
                choices.remove(at: indexPath.row)
                votingTableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
    }
}

// MARK: - UIViewController
extension UIViewController {
    
    func presentFAB(_ nav: UINavigationController?, type: FABType) {
        
        if let fab = fabStoryboard.instantiateViewController(withIdentifier: "FAB") as? QlueFABController {
            fab.fabType = type
            
            if let nav = nav {
                nav.pushViewController(fab, animated: true)
                return
            }
            present(fab, animated: true, completion: nil)
        }
    }
}
