//
//  QlueOtherProfileController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CarbonKit

class QlueOtherProfileController: QlueViewController, CarbonTabSwipeNavigationDelegate {
    
    // MARK: Var
     let items: [String] = ["PROFILE", "QLUES", "FORUM", "AVATAR", "TROPHY"]
    var userId: String?
     var isBlock: Bool = false
     var user: QlueOtherUser?
    
    // MARK: - View Lifecycle
     override func viewDidLoad() {
          super.viewDidLoad()
          setupTabView()
          // Do any additional setup after loading the view.
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Private Func
          fileprivate func setupTabView(){
          let carbonTabSwipeNavigation = CarbonTabSwipeNavigation.init(items: items, delegate: self)
          
          carbonTabSwipeNavigation.insert(intoRootViewController: self)
          carbonTabSwipeNavigation.toolbarHeight.constant = 40.0
          carbonTabSwipeNavigation.setIndicatorColor(kCreamColor)
          
          carbonTabSwipeNavigation.toolbar.setBackgroundImage(UIImage(named: "bgNavbar"), forToolbarPosition: UIBarPosition.any, barMetrics: UIBarMetrics.default)
          carbonTabSwipeNavigation.toolbar.shadowWithColor(UIColor.clear)
          
          
          for i in 0..<items.count{
               carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width / 5, forSegmentAt: i)
          }
          
          if let barFont = UIFont(name: QlueFont.ProximaNovaSoft, size: 12.0) {
               carbonTabSwipeNavigation.carbonSegmentedControl?.setTitleTextAttributes([NSForegroundColorAttributeName: kCreamColor
                    ,NSFontAttributeName:barFont], for: UIControlState())
               carbonTabSwipeNavigation.carbonSegmentedControl?.setTitleTextAttributes([NSForegroundColorAttributeName: kCreamColor
                    ,NSFontAttributeName:barFont], for: .selected)
               
          }
     }
     
     // MARK: - Data
     func blockUser(){
          guard let userId = userId else {
               return
          }
          
          let req = Engine.shared.blockUser(userId: userId)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion: { (data, success, fail) in
                    if success == true{
                         if data["response"].string == "success"{
                              self.isBlock = true
                         } else{
                              self.isBlock = false
                         }
                    }
               })     }
    
    // MARK: - Action
     
    @IBAction func moreTapped(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController.init(title: "More", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let laporkan = UIAlertAction.init(title: "Laporkan", style: .default) { (action) in
            self.presentReportUserModal(userId: self.userId)
        }
        alert.addAction(laporkan)
     
     
     let blockTitle = isBlock == true ? "Unblock" : "Block"
     let block = UIAlertAction.init(title: blockTitle, style: .default) { (action) in
            self.blockUser()
        }
        alert.addAction(block)
        
        
        let cancel = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
     // MARK: - CarbonTabSwipeNavigationDelegate
     func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
          
          switch index {
          case 0:
               if let userD: QlueOPDetailController = profileStoryboard.instantiateViewController(withIdentifier: "OPDetail") as? QlueOPDetailController{
                    
                    userD.delegate = self
                    userD.userId = userId
                    
                    return userD
               }
          case 1:
               if let report = profileStoryboard.instantiateViewController(withIdentifier: "ProfileReport") as? QlueProfileReportController{
                    
                    report.userId = userId
                    report.user = user
                    return report
               }
          case 2:
               if let forum = profileStoryboard.instantiateViewController(withIdentifier: "ProfileForum") as? QlueProfileForumController{
                    forum.userId = userId
                    forum.user = user
                    return forum
               }
               
          case 3:
               if let ava = profileStoryboard.instantiateViewController(withIdentifier: "ProfileAvatar") as? QlueProfileAvatarController{
                    ava.userId = userId
                    ava.user = user
                    return ava
               }
               
          default:
               if let thropy = profileStoryboard.instantiateViewController(withIdentifier: "ProfileThropy") as? QlueProfileThropyController{
                    thropy.userId = userId
                    thropy.user = user
                    return thropy
               }
          }
          
          return UIViewController()
          
     }
     
     
     
     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
      }
      */
     
}

// MARK: - OPDetail
extension QlueOtherProfileController: OPDetailDelegate{
     
     func downloadDetailProfileFinished(_ isBlock: Bool?, user: QlueOtherUser?) {
          self.isBlock = isBlock ?? false
          self.user = user
     }
}

// MARK: - Navigate
extension UIViewController {
     func presentOtherProfile(_ userId: String?){
          if let nav = profileStoryboard.instantiateViewController(withIdentifier: "OP") as? QlueOtherProfileController{
               
               nav.userId = userId
               
               let navController = UINavigationController(rootViewController: nav)
               
               // 2. Present the navigation controller
               
               self.present(navController, animated: true, completion: nil)
               
//               presentViewController(nav, animated: true, completion: nil)
          }
     }
}


