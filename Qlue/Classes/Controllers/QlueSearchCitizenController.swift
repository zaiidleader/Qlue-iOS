//
//  QlueSearchCitizenController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/27/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueSearchCitizenController: QlueViewController {

     // MARK: - Var
     var query = ""{
          didSet{
               self.oldQuery  = oldValue
          }
     }
     var oldQuery = ""
     @IBOutlet weak var tableView: UITableView!
     var page = 0
     var people: [QlueMember] = []
     var refreshControl : UIRefreshControl = UIRefreshControl.init()
     
     
     // MARK: - View Lifecycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          DejalActivityView.addActivityView(for: tableView)
          configureView()
          downloadPeople()
          
          
     }
     
     override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
          
          if oldQuery != query{
               search()
          }
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Configure
     func configureView(){
                  tableView.addInfiniteScroll { (_) in
                      self.page += 1
                      self.downloadPeople()
                  }
          
          tableView.addSubview(refreshControl)
     }
     
     // MARK: - Data
     func refresh(){
          self.page = 0
          downloadPeople()
     }
     
     func downloadPeople(){
          let start = page * 25
          let finish = (page + 1) * 25
          let req = Engine.shared.searchUsername(query, start: start, finish: finish)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               self.refreshControl.endRefreshing()
               self.tableView.finishInfiniteScroll()
               DejalActivityView.remove()
               }, completion: { (data, success, fail) in
                    if success == true{
                         if start == 0{
                              self.people = []
                         }
                         for d in data.arrayValue{
                              self.people.append(QlueMember.init(fromJson: d))
                         }
                         if (self.people.count == 0) && (self.query != ""){
                              let emptyBg = QlueSearchEmpty.fromNib()
                              emptyBg.labelButton.setTitle("No Search Result Found", for: .normal)
                              self.tableView.backgroundView = emptyBg
                         } else{
                              self.tableView.backgroundView = UIView.init()
                         }
                         
                         self.tableView.reloadData()
                    }
          })
     }
     
     // MARK: - Search
     func search() {
          page = 0
          people = []
          tableView.reloadData()
          
          self.tableView.backgroundView = UIView.init()
          DejalActivityView.addActivityView(for: tableView)
          downloadPeople()
     }


     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
      }
      */
     
}

// MARK: - TableView
extension QlueSearchCitizenController: UITableViewDataSource, UITableViewDelegate{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return people.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QlueSearchCitizenCell
          
          let ppl = people[indexPath.row]
          
          cell.cellLabel.text = ppl.username
          cell.cellImage.image = nil
          if let imgUrl = ppl.avatar?.encodeURL(){
               cell.cellImage.sd_setImage(with: imgUrl as URL, placeholderImage: placeholderImage)
          }
          
          return cell
     }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          let ppl = people[indexPath.row]
          presentOtherProfile(ppl.userId)
     }
}


