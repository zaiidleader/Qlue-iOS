//
//  RegistrationGenderController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class RegistrationGenderController: RegistrationController {

    // MARK: Outlets
    @IBOutlet weak var femaleLabel: UILabel!
    @IBOutlet weak var femaleButton: RadioButton!
    @IBOutlet weak var maleLabel: UILabel!
    @IBOutlet weak var maleButton: RadioButton!
    var genderButtons: [RadioButton] = []
    var hasSelect = false
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureButton()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action
    @IBAction func maleTapped(_ sender: RadioButton) {
        Engine.shared.createUser.gender = Gender.Male
        hasSelect = true
    }
    
    @IBAction func femaleTapped(_ sender: RadioButton) {
        Engine.shared.createUser.gender = Gender.Female
        hasSelect = true
    }
    
    override func nextTapped(_ sender: UIButton) {
        guard hasSelect == true else{
            Util.showSimpleAlert("Please select your gender", vc: self, completion: nil)
            return
        }
        
        performSegue(withIdentifier: "NextSegue", sender: self)
        
    }
    // MARK: - Configure
    func configureButton(){
        maleButton.cornerRadius(5)
        maleButton.buttonLabel = maleLabel
        maleButton.buttonLabelColorNormal = kGrey708896Color
        femaleButton.cornerRadius(5)
        femaleButton.buttonLabel = femaleLabel
        femaleButton.buttonLabelColorNormal = kGrey708896Color
        
        genderButtons = [maleButton, femaleButton]
        for button in genderButtons{
            button.downStateImage = nil
            button.myAlternateButton = genderButtons.filter{$0 != button}
            button.backgroundColorNormal = kGreyEEE
            button.backgroundColorSelected = kBlue0988EEColor
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
