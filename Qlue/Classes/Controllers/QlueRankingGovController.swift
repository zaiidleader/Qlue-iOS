//
//  QlueRankingGovController.swift
//  Qlue
//
//  Created by Nurul on 8/15/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

class QlueRankingGovController: QlueViewController {
     
     enum RankingType{
          case kelurahan
          case kecamatan
          case kotamadya
          case dinas
          case staff
          
          var description: String{
               switch self{
               case .kelurahan:
                    return "Kelurahan"
               case .kecamatan:
                    return "Kecamatan"
               case .kotamadya:
                    return "Kotamadya"
               case .dinas:
                    return "Dinas"
               case .staff:
                    return "Staff"
               }
          }
     }
     
     @IBOutlet weak var staffButton: UIButton!
     @IBOutlet weak var dinasButton: UIButton!
     @IBOutlet weak var kotamadyaButton: UIButton!
     @IBOutlet weak var kecamatanButton: UIButton!
     @IBOutlet weak var kelurahanButton: UIButton!
     var rangkingType: RankingType = .kelurahan
     @IBOutlet weak var sortCategoryView: UIView!
     var categoryVisibleState: Bool = false
     @IBOutlet weak var sortView: UIView!{
          didSet{
               sortView.layer.shadowColor = UIColor.darkGray.cgColor
               sortView.layer.shadowOpacity = 0.5
               sortView.layer.shadowRadius = 2
               sortView.layer.shadowOffset = CGSize(width: 1, height: 1)
          }
     }
     
     @IBOutlet weak var sortButton: UIButton!
     var sortState: Bool = true // t = top , f: bottom
     
     @IBOutlet weak var fabView: UIView!{
          didSet{
               fabView.cornerRadius(25)
               fabView.shadowWithColor(UIColor.darkGray)
          }
     }
     @IBOutlet weak var fabButton: UIButton!
     
     
     @IBOutlet weak var tableView: UITableView!
     var ranks: [QlueRanking] = []
     var start = 0
     
     
     var location: CLLocation?
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          DejalActivityView.addActivityView(for: view)
          
          sortButton.setTitle(rangkingType.description, for: UIControlState())
          configureTableView()
          setCurrentLocation()
          // Do any additional setup after loading the view.
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
          
          
     }
     
     // MARK: - Configure
     fileprivate func configureTableView(){
          tableView.register(UINib.init(nibName: "QlueRankingCell", bundle: nil), forCellReuseIdentifier: "RankingCell")
          tableView.rowHeight = 60
     }
     
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               self.location = location
               
               downloadData()
               
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QlueKSearchViewController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }
     
     //MARK: - Data
     fileprivate func downloadData(){
          
          var req: DataRequest!
          
          var type  = "top"
          if sortState == false { type = "bottom" }
          
          switch rangkingType {
          case .kelurahan:
               req = Engine.shared.listRankingKelurahan(withType: type,lat: location!.coordinate.latitude, lng: location!.coordinate.longitude,  limit: 25)
          case .kecamatan:
               req = Engine.shared.listRankingKecamatan(withType: type,lat: location!.coordinate.latitude, lng: location!.coordinate.longitude,  limit: 25)
          case .kotamadya:
               req = Engine.shared.listRankingKotamadya(withType: type,lat: location!.coordinate.latitude, lng: location!.coordinate.longitude,  limit: 25)
          case .dinas:
               req = Engine.shared.listRankingDinas(withType: type,lat: location!.coordinate.latitude, lng: location!.coordinate.longitude,  limit: 25)
          case .staff:
               req = Engine.shared.listRankingStaff(withType: type,lat: location!.coordinate.latitude, lng: location!.coordinate.longitude,  limit: 25)
               
          }
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               }, completion:  { (data, success, fail) in
                    
                    if self.start == 0{
                         self.ranks = []
                    }
                    
                    if success == true{
                         for d in data.arrayValue{
                              self.ranks.append(QlueRanking.init(fromJson: d))
                         }
                         
                         
                         self.tableView.reloadData()
                    }
                    
                    
          })
     }
     
     // MARK: - Action
     @IBAction func sortTapped(_ sender: UIButton) {
          sortState = !sortState
          
          self.ranks = []
          tableView.reloadData()
          DejalActivityView.addActivityView(for: self.view)
          downloadData()
     }
     @IBAction func categoryTapped(_ sender: UIButton) {
          categoryVisibleState = !categoryVisibleState
          
          if categoryVisibleState == true{
               
               sortButton.setTitle("Ranking ...", for: UIControlState())
               
               sortCategoryView.isHidden = false
               sortCategoryView.transform = CGAffineTransform(translationX: 0, y: -(UIScreen.main.bounds.height))
               sortCategoryView.alpha = 0
               
               UIView.animate(withDuration: 0.5, animations: {
                    self.sortCategoryView.transform = CGAffineTransform.identity
                    self.sortCategoryView.alpha = 1
               })
          } else{
               sender.setTitle(rangkingType.description, for: UIControlState())
               UIView.animate(withDuration: 0.5, animations: {
                    self.sortCategoryView.transform = CGAffineTransform(translationX: 0, y: -(UIScreen.main.bounds.height))
                    self.sortCategoryView.alpha = 0
                    }, completion: { (finished) in
                         self.sortCategoryView.isHidden = true
               })
          }
          
     }
     
     @IBAction func rankingTypeTapped(_ sender: UIButton) {
          switch sender {
          case kecamatanButton:
               rangkingType = .kecamatan
          case kotamadyaButton:
               rangkingType = .kotamadya
          case dinasButton:
               rangkingType = .dinas
          case staffButton:
               rangkingType = .staff
          default:
               rangkingType = .kelurahan
          }
          
          self.categoryVisibleState = false
          
          sortButton.setTitle(rangkingType.description, for: UIControlState())
          UIView.animate(withDuration: 0.5, animations: {
               self.sortCategoryView.transform = CGAffineTransform(translationX: 0, y: -(UIScreen.main.bounds.height))
               self.sortCategoryView.alpha = 0
               }, completion: { (finished) in
                    self.sortCategoryView.isHidden = true
          })
          self.ranks = []
          tableView.reloadData()
          DejalActivityView.addActivityView(for: self.view)
          downloadData()
          
     }
     
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension QlueRankingGovController: UITableViewDataSource, UITableViewDelegate{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return ranks.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
          if rangkingType == .staff{
               
               let cell = tableView.dequeueReusableCell(withIdentifier: "StaffCell", for: indexPath) as! QlueRankingStaffViewCell
               
               let rank = ranks[indexPath.row]
               
               if let ava = rank.avatar?.encodeURL(){
                    cell.cellImage.sd_setImage(with: ava as URL!)
               }
               
               
               if sortState == true{
                    cell.cellTrophy.isHidden = false
                    cell.cellRankView.isHidden = true
                    if rank.rank == 1{
                         cell.cellTrophy.image = UIImage.init(named: "trophy_01")
                    } else if rank.rank == 2{
                         cell.cellTrophy.image = UIImage.init(named: "trophy_02")
                    } else if rank.rank == 3{
                         cell.cellTrophy.image = UIImage.init(named: "trophy_03")
                    }
                    else{
                         
                         
                         cell.cellTrophy.isHidden = true
                         cell.cellRankView.isHidden = false
                         
                         
                         let numb = rank.rank ?? 0
                         cell.cellRankLabel.text = numb.description
                         //                    var str : String = ""
                         //                    for c in String(numb).characters{
                         //                         str.append(c)
                         //                         str += " "
                         //                    }
                         //
                         //                    if numb < 100{
                         //                    cell.cellTrophy.setImageWithString("1 0 0", color: kQlueBlue, circular: true, textAttributes: [
                         //                         NSFontAttributeName: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 16)!,
                         //                         NSForegroundColorAttributeName : UIColor.whiteColor()
                         //                         ])
                         //
                         //                    } else{
                         //                         cell.cellTrophy.setImageWithString(str, color: kQlueBlue, circular: true, textAttributes: [
                         //                              NSFontAttributeName: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 12)!,
                         //                              NSForegroundColorAttributeName : UIColor.whiteColor()
                         //                              ])
                         //
                         //                    }
                    }
               } else{
                    
                    cell.cellTrophy.isHidden = true
                    cell.cellRankView.isHidden = false
                    
                    let numb = rank.rank ?? 0
                    cell.cellRankLabel.text = numb.description
                    //                    var str : String = ""
                    //                    for c in String(numb).characters{
                    //                         str.append(c)
                    //                         str += " "
                    //                    }
                    //                    if numb < 100{
                    //                         cell.cellTrophy.setImageWithString(str, color: kQlueBlue, circular: true, textAttributes: [
                    //                              NSFontAttributeName: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 16)!,
                    //                              NSForegroundColorAttributeName : UIColor.whiteColor()
                    //                              ])
                    //
                    //                    } else{
                    //                         cell.cellTrophy.setImageWithString("1 0 0", color: kQlueBlue, circular: true, textAttributes: [
                    //                              NSFontAttributeName: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 12)!,
                    //                              NSForegroundColorAttributeName : UIColor.whiteColor()
                    //                              ])
                    //
                    //                    }
               }
               cell.cellLabel.text = rank.username
               
               let bagian = rank.bagian ?? "-"
               let detil = rank.detil ?? "-"
               cell.cellDescription.text = detil + " - " + bagian
               cell.cellPoint.text = rank.report
               
               return cell
               
          }else{
               let cell = tableView.dequeueReusableCell(withIdentifier: "RankingCell", for: indexPath) as! QlueRankingCell
               
               let rank = ranks[indexPath.row]
               
               cell.cellImage.isHidden = false
               cell.cellRankView.isHidden = true
               if sortState == true{
                    if rank.rank == 1{
                         cell.cellImage.image = UIImage.init(named: "trophy_01")
                    } else if rank.rank == 2{
                         cell.cellImage.image = UIImage.init(named: "trophy_02")
                    } else if rank.rank == 3{
                         cell.cellImage.image = UIImage.init(named: "trophy_03")
                    } else{
                         
                         cell.cellImage.isHidden = true
                         cell.cellRankView.isHidden = false
                         let numb = rank.rank ?? 0
                         cell.cellRankLabel.text = numb.description
                         
                    }
                    
               } else{
                    cell.cellImage.isHidden = true
                    cell.cellRankView.isHidden = false
                    let numb = rank.rank ?? 0
                    cell.cellRankLabel.text = numb.description
               }
               
               cell.cellLabel.text = rank.nama
               cell.cellRedCount.text = rank.wait
               cell.cellYellowCount.text = rank.process
               cell.cellGreenCount.text = rank.complete
               cell.cellPoint.text = rank.total
               
               return cell
          }
     }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          let rank = ranks[indexPath.row]
          
          switch rangkingType {
          case .kelurahan:
               presentKelurahan(withId: rank.nama, district: ranks[indexPath.row].code, nav: self.navigationController)
          case .kecamatan:
               pushRankingGovDetail(self.navigationController, type: rankingGovType.kelurahan(kec: rank.nama ?? ""))
          case .kotamadya:
               pushRankingGovDetail(self.navigationController, type: rankingGovType.kecamatan(kota: rank.nama ?? ""))
          case .dinas:
               pushRankingReportDetail(self.navigationController, type: RankingReportDetailType.dinas(dinasName: rank.nama ?? ""))
          case .staff:
               presentOtherProfile(rank.code)
          }

     }
}
