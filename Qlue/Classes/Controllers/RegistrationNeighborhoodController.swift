//
//  RegistrationNeighborhoodController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class RegistrationNeighborhoodController: RegistrationController {

    @IBOutlet weak var kelurahanButton: UIButton!
    @IBOutlet weak var cityButton: UIButton!
    @IBOutlet weak var countryButton: UIButton!
     
     var countries: [JSON] = []
     var cdNegara: String = ""
     var countryIndex = 0
     
     var cities: [JSON] = []
     var cdKota: String = ""
     var cityIndex = 0
     
     var kelurahans: [JSON] = []
     var cdKelurahan: String = ""
     var kelurahanIndex = 0

    
    // MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
     downloadCountry()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     // MARK: - Data
     func downloadCountry(){
          let req = Engine.shared.getCountryList()
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               }, completion: { (data, success, fail) in
                    if success == true{
                         self.countries = data.arrayValue
                    }
          })
     }
     
     func downloadCity(){
          guard cdNegara != "" else{ return }
          let req = Engine.shared.getCityList(cdNegara)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               }, completion: { (data, success, fail) in
                    if success == true{
                         self.cities = data.arrayValue
                    }
          })
     }
     
     func downloadKelurahan(){
          guard cdKota != "" else{ return }
          let req = Engine.shared.getKelurahanListReg(cdKota)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               }, completion: { (data, success, fail) in
                    if success == true{
                         self.kelurahans = data.arrayValue
                    }
          })
     }

     
    
    // MARK: - Action
    @IBAction func countryTapped(_ sender: UIButton) {
        
        let actionSheet = ActionSheetStringPicker(title: "Country", rows: countries.map( { $0["negara"].stringValue } ), initialSelection: countryIndex, doneBlock: { (picker, index, value) in
            
            print("index = \(self.countries[index]["cd_negara"].stringValue)")
            self.countryIndex = index
            self.cdNegara = self.countries[index]["cd_negara"].stringValue
            self.countryButton.setTitleColor(kGrey708896Color, for: .normal)
            self.countryButton.setTitle(value as? String, for: .normal)
            
            self.downloadCity()
            
        }, cancel: { (picker) in
            return
        }, origin: self.view)
        
        let doneButton = UIBarButtonItem.init()
        doneButton.title = "Done"
        doneButton.setTitleTextAttributes(barButtonStyle, for: UIControlState())
        
        actionSheet?.setDoneButton(doneButton)
        
        let cancelButton = UIBarButtonItem.init()
        cancelButton.title = "Cancel"
        cancelButton.setTitleTextAttributes(barButtonStyle, for: UIControlState())
        
        actionSheet?.setCancelButton(cancelButton)
        
        actionSheet?.show()
    }
    
    @IBAction func cityTapped(_ sender: UIButton) {
     let actionSheet = ActionSheetStringPicker.init(title: "City", rows: cities.map({$0["kota"].stringValue}), initialSelection: cityIndex, doneBlock:  {
          picker, index, value in
          
          print("index = \(self.cities[index]["cd_kota"].stringValue)")
          self.cityIndex = index
          self.cdKota = self.cities[index]["cd_kota"].stringValue
          self.cityButton.setTitleColor(kGrey708896Color, for: .normal)
          self.cityButton.setTitle(value as? String, for: .normal)
          
          self.downloadKelurahan()
          
          return
          }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
     
     let doneButton = UIBarButtonItem.init()
     doneButton.title = "Done"
     doneButton.setTitleTextAttributes(
          barButtonStyle,
          for: UIControlState())
     
     actionSheet?.setDoneButton(doneButton)
     
     let cancelButton = UIBarButtonItem.init()
     cancelButton.title = "Cancel"
     cancelButton.setTitleTextAttributes(
          barButtonStyle,
          for: UIControlState())
     
     actionSheet?.setCancelButton(cancelButton)
     
     actionSheet?.show()

    }
     @IBAction func kelurahanTapped(_ sender: UIButton) {
          let actionSheet = ActionSheetStringPicker.init(title: "Kelurahan", rows: kelurahans.map({$0["kelurahan"].stringValue}), initialSelection: kelurahanIndex, doneBlock:  {
          picker, index, value in
          
          print("index = \(self.kelurahans[index]["cd_kelurahan"].stringValue)")
               self.kelurahanIndex = index
          self.cdKelurahan = self.kelurahans[index]["cd_kelurahan"].stringValue
          self.kelurahanButton.setTitleColor(kGrey708896Color, for: .normal)
               self.kelurahanButton.setTitle(value as? String, for: .normal)
          
          
          return
          }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
          
          let doneButton = UIBarButtonItem.init()
          doneButton.title = "Done"
          doneButton.setTitleTextAttributes(
               barButtonStyle,
               for: UIControlState())
          
          actionSheet?.setDoneButton(doneButton)
          
          let cancelButton = UIBarButtonItem.init()
          cancelButton.title = "Cancel"
          cancelButton.setTitleTextAttributes(
               barButtonStyle,
               for: UIControlState())
          
          actionSheet?.setCancelButton(cancelButton)
          
          actionSheet?.show()
    }
     
    override func nextTapped(_ sender: UIButton) {
        guard cdKelurahan != "" else{
            Util.showSimpleAlert("Please choose your kelurahan", vc: self, completion: nil)
            return
        }
        Engine.shared.createUser.kdKel = cdKelurahan
        performSegue(withIdentifier: "NextSegue", sender: self)
    }
}

// MARK: - Navigation
extension UIViewController{
    func presentChooseNeighborhood(){
        let neighborhood = mainStoryboard.instantiateViewController(withIdentifier: "NeighborhoodNav")
     present(neighborhood, animated: true, completion: nil)
    }
}
