//
//  QlueKLaporanController.swift
//  Qlue
//
//  Created by Nurul on 6/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueKLaporanController: QlueViewController {
     
     @IBOutlet weak var collectionView: UICollectionView!
     var indexSelected = 0
     var kLaporan : [QlueKelurahanLaporan] = []
     var kelurahan: QlueKelurahan?
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          collectionView?.register(UINib.init(nibName: "QlueKLViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
          
          DejalActivityView.addActivityView(for: self.view)
          downloadData()
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
     
     func initWithKelurahan(_ kel: QlueKelurahan?){
          self.kelurahan = kel
     }
     
     // MARK: - Data
     fileprivate func downloadData(){
          guard let kelId = kelurahan?.codeKel, let district = kelurahan?.district else{
               return
          }
          
          let req = Engine.shared.listLaporanKelurahan(withCodeKel: kelId, district: district)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               }, completion:  { (data, success, fail) in
                    print(data)
                    
                    self.kLaporan = []
                    if success == true{
                         for d in data.arrayValue{
                              self.kLaporan.append(QlueKelurahanLaporan.init(fromJson: d))
                         }
                         self.collectionView.reloadData()
                    }
          })
     }
     
     // MARK: - Navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "DetailSegue"{
               let destination = segue.destination as! QlueKLDetailController
               
               let data = kLaporan[indexSelected]
               
               destination.initWithCode(kelurahan?.name, district: kelurahan?.district, category: data.data.first?.icon?.getLabelTopic())
               destination.title = data.label?.capitalized
               
          }
     }
     
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
extension QlueKLaporanController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
     func numberOfSections(in collectionView: UICollectionView) -> Int {
          // #warning Incomplete implementation, return the number of sections
          return 1
     }
     
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          // #warning Incomplete implementation, return the number of items
          return kLaporan.count
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! QlueKLViewCell
          cell.cornerRadiusWithShadow()
          
          cell.delegate = self
          let data = kLaporan[indexPath.row]
          
          cell.cellNotif.isHidden = false
          
          if let lastGov = UserDefaults.standard.object(forKey: kLastReportGov) as? String, lastGov == kelurahan?.laporanterakhirGov && indexPath.item == 0{
               cell.cellNotif.isHidden = true
          }
          
          if let lastBuss = UserDefaults.standard.object(forKey: kLastReportBuss) as? String, lastBuss == kelurahan?.laporanterakhirBuss && indexPath.item == 1{
               cell.cellNotif.isHidden = true
          }
          
          cell.cellTitle.text = data.label?.capitalized
          cell.laporan  = data.data
          cell.cellCollectionView.reloadData()
          
          return cell
     }
     
     // MARK: UICollectionViewDelegate
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          let collectionPhotoHeight: CGFloat = (collectionView.bounds.width - 26 - 30) / 3
          let height: CGFloat = collectionPhotoHeight + 24 + 40
          let width: CGFloat = collectionView.bounds.width - 30
          return CGSize(width: width, height: height)
          
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return 20
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 20
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsetsMake(12, 15, 12, 15)
     }
}

// MARK: - QlueKLViewCellDelegate
extension QlueKLaporanController: QlueKLViewCellDelegate{
     func moreTapped(_ view: QlueKLViewCell) {
          if let indexPath = collectionView?.indexPath(for: view){
               self.indexSelected = indexPath.item
               if indexPath.item == 0{
                    UserDefaults.standard.set(kelurahan?.laporanterakhirGov, forKey: kLastReportGov)
                    UserDefaults.standard.synchronize()
                    
                    view.cellNotif.isHidden = true
               }else{
                    UserDefaults.standard.set(kelurahan?.laporanterakhirBuss, forKey: kLastReportGov)
                    UserDefaults.standard.synchronize()
                    view.cellNotif.isHidden = true
               }
          }
          performSegue(withIdentifier: "DetailSegue", sender: self)
     }
}

// MARK: - Navigation
extension QlueViewController{
     func presentLaporanKelurahan(_ kel: QlueKelurahan?, nav: UINavigationController?){
          if let lap = kelurahanStoryboard.instantiateViewController(withIdentifier: "KLaporan") as? QlueKLaporanController{
               
               lap.kelurahan = kel
               
               if let nav = nav{
                    nav.pushViewController(lap, animated: true)
               } else{
                    present(lap, animated: true, completion: nil)
               }
          }
     }
}
