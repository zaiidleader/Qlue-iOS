//
//  QlueChatDetailController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/11/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON
import AssetsLibrary
import Photos

enum ChatType{
     case group
     case personal
}

class QlueChatDetailController: QlueViewController {
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
     @IBOutlet weak var chatTextView: UITextView!
     
     @IBOutlet weak var chatTextViewHeight: NSLayoutConstraint!
     var currentHeight: CGFloat = 50
     var heightBefore: CGFloat = 50

     var members: [QlueMember] = []
     var chats: [QlueChatDetail] = []
     var groupId: String = ""
     var isGroup: Bool = true
    
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var imagePreviewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
     DejalActivityView.addActivityView(for: tableView)
     downloadData(withGroupId: groupId)
     configureTableView()
     configureTitle()
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initWithGroupId(_ groupId: String, isGroup: Bool, members: [QlueMember]){
        self.groupId = groupId
        self.isGroup = isGroup
        self.members = members
    }
     // MARK: - Configure
    
     func configureTitle(){
          var title = ""
          for member in members{
               title += member.username ?? ""
               title += ", "
          }
          titleView.text = title.trimmingCharacters(in: CharacterSet.init(charactersIn: ", "))
          
     }
     
    // MARK: - Action
    @IBAction func deleteImageTapped(_ sender: UIButton) {
        imagePreview.image = nil
        imagePreviewHeight.constant = 1
    }
    @IBAction func moreTapped(_ sender: UIButton) {
     let alert = UIAlertController.init(title: "More", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
     
          let member = UIAlertAction.init(title: "Member", style: .default) { (action) in
          self.presentListMember(withMembers: self.members)
     }
     alert.addAction(member)
     
     let cancel = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
     alert.addAction(cancel)
     
     present(alert, animated: true, completion: nil)
    }
    @IBAction func dismissTapped(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendTapped(_ sender: UIButton) {
        
        let data: [String : Any] = [
            "message" : chatTextView.text,
            "timestamp" : Date().getTimestamp() ?? "",
            "user_id" : User.currentUser()?.userId ?? "",
            "username" : User.currentUser()?.username ?? ""
        ]
        
        let json = JSON.init(data)
        let newChat = QlueChatDetail.init(fromJson: json, isGroup: isGroup)
        newChat.imageSend = imagePreview.image
        newChat.isSend = false
        if newChat.imageSend != nil{
            if isGroup == true{
                newChat.type = .groupRightImage
            } else{
                newChat.type = .rightImage
            }
        }
        
        let image = imagePreview.image
        imagePreview.image =  nil
        self.imagePreviewHeight.constant = 0
        chats.append(newChat)
        
        
        tableView.insertRows(at: [IndexPath.init(row: (chats.count - 1), section: 0)], with: .automatic)
        tableView.scrollToRow(at: IndexPath.init(row: (chats.count - 1), section: 0), at: .bottom, animated: true)
        let req = Engine.shared.postChat(withGroupId: groupId, rid: User.currentUser()?.userId ?? "", message: chatTextView.text, image: image)
        Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
            
        }, completion: { (data, success, fail) in
            if success == true{
                self.chats.last?.isSend = true
                self.chats.last?.timestamp = Date().getTimestamp()
                self.tableView.reloadRows(at: [IndexPath(row: (self.chats.count - 1), section: 0)], with: .none)
                
                print("send")
            }
        }) {
            
        }
        chatTextView.text = ""
        
    }
    
    @IBAction func cameraTapped(_ sender: UIButton) {
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let camera = UIAlertAction.init(title: "Take Photo", style: .default) { (action) in
          let imagePicker = UIImagePickerController()
          imagePicker.allowsEditing = false
          imagePicker.sourceType = .camera
          imagePicker.delegate = self
          
          self.present(imagePicker, animated: true, completion: nil)
        }
        alert.addAction(camera)
     
        let gallery = UIAlertAction.init(title: "Choose Photo from Gallery", style: .default) { (action) in
          if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
               let imagePicker = UIImagePickerController()
               imagePicker.allowsEditing = false
               imagePicker.sourceType = .photoLibrary
               imagePicker.delegate = self
               
               self.present(imagePicker, animated: true, completion: nil)
          }
        }
        alert.addAction(gallery)
        
        let cancel = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
     // MARK: - Data
     fileprivate func downloadData(withGroupId id: String){
          let req = Engine.shared.getChatDetail(withGroupId: id)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               },completion:  { (data, success, fail) in
                    if success == true{
                         self.chats = []
                         for d in data.arrayValue{
                              self.chats.append(QlueChatDetail.init(fromJson: d, isGroup: self.isGroup))
                         }
                         
                         self.chats = self.chats.sorted(by: { (chat0, chat1) -> Bool in
                              if chat0.timestamp! > chat1.timestamp! {
                                   return false
                              }
                              return true
                         })
                         self.tableView.reloadData()
                         if self.tableView.numberOfRows(inSection: 0) > 0{
                            self.tableView.scrollToRow(at: IndexPath(row: self.chats.count - 1, section: 0), at: .bottom, animated: false)
                         }
                    }
          })
     }
}

// MARK: - TableView
extension QlueChatDetailController: UITableViewDelegate, UITableViewDataSource{
    func configureTableView(){
        tableView.register(UINib.init(nibName: "QlueChatTextRightCell", bundle: nil), forCellReuseIdentifier: "RightCell")
     tableView.register(UINib.init(nibName: "QlueChatImageRightCell", bundle: nil), forCellReuseIdentifier: "ImageRightCell")
     tableView.register(UINib.init(nibName: "QlueChatTextLeftCell", bundle: nil), forCellReuseIdentifier: "LeftCell")
     tableView.register(UINib.init(nibName: "QlueChatImageLeftCell", bundle: nil), forCellReuseIdentifier: "ImageLeftCell")
     
     tableView.register(UINib.init(nibName: "QlueChatGroupTextRightCell", bundle: nil), forCellReuseIdentifier: "GroupRightCell")
     tableView.register(UINib.init(nibName: "QlueChatGroupImageRightCell", bundle: nil), forCellReuseIdentifier: "GroupImageRightCell")
     tableView.register(UINib.init(nibName: "QlueChatGroupTextLeftCell", bundle: nil), forCellReuseIdentifier: "GroupLeftCell")
     tableView.register(UINib.init(nibName: "QlueChatGroupImageLeftCell", bundle: nil), forCellReuseIdentifier: "GroupImageLeftCell")
    }
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
     let chat = chats[indexPath.row]
     let message = Util.convertEmojiFromUnicode(chat.message)
     
     switch chat.type {
     case .rightText:
          let cell = tableView.dequeueReusableCell(withIdentifier: "RightCell", for: indexPath) as! QlueChatTextRightCell
          
          cell.cellTextLabel.text = message
          
          if chat.isSend == false{
               cell.cellTime.text = "Sending..."
          } else{
               cell.cellTime.text = Date().getDateFromTimestamp(chat.timestamp)?.timeAgoWithNumericDates(true)
          }
          
          return cell
     case .rightImage:
          let cell = tableView.dequeueReusableCell(withIdentifier: "ImageRightCell", for: indexPath) as! QlueChatImageRightCell
          
          if let imageSend = chat.imageSend{
          cell.cellImage.image = imageSend
          }else if let url = chat.file?.encodeURL(){
               cell.cellImage.sd_setImage(with: url as URL!)
          }
          
          if chat.isSend == false{
               
               cell.cellTime.text = "Sending..."
          } else{
               cell.cellTime.text = Date().getDateFromTimestamp(chat.timestamp)?.timeAgoWithNumericDates(true)
          }
          
          return cell
     case .leftText:
          let cell = tableView.dequeueReusableCell(withIdentifier: "LeftCell", for: indexPath) as! QlueChatTextLeftCell
          
          cell.cellTextLabel.text = message
          cell.cellTime.text = Date().getDateFromTimestamp(chat.timestamp)?.timeAgoWithNumericDates(true)
          if let avatarUrl = chat.avatar?.encodeURL(){
               cell.cellAvatar.sd_setImage(with: avatarUrl as URL!)
          }
          
          return cell
     case .leftImage:
          let cell = tableView.dequeueReusableCell(withIdentifier: "ImageLeftCell", for: indexPath) as! QlueChatImageLeftCell
          
          if let url = chat.file?.encodeURL(){
               cell.cellImage.sd_setImage(with: url as URL!)
          }
          cell.cellTime.text = Date().getDateFromTimestamp(chat.timestamp)?.timeAgoWithNumericDates(true)
          if let avatarUrl = chat.avatar?.encodeURL(){
               cell.cellAvatar.sd_setImage(with: avatarUrl as URL!)
          }
          
          return cell
     case .groupRightText:
          let cell = tableView.dequeueReusableCell(withIdentifier: "GroupRightCell", for: indexPath) as! QlueChatGroupTextRightCell
          
          cell.cellTextLabel.text = message
          cell.cellTime.text = Date().getDateFromTimestamp(chat.timestamp)?.timeAgoWithNumericDates(true)
          cell.cellUsername.text = chat.username
          
          return cell

     case .groupRightImage:
          let cell = tableView.dequeueReusableCell(withIdentifier: "GroupImageRightCell", for: indexPath) as! QlueChatGroupImageRightCell
          
          if let url = chat.file?.encodeURL(){
               cell.cellImage.sd_setImage(with: url as URL!)
          }
          cell.cellTime.text = Date().getDateFromTimestamp(chat.timestamp)?.timeAgoWithNumericDates(true)
          cell.cellUsername.text = chat.username
          
          return cell
     case .groupLeftText:
          let cell = tableView.dequeueReusableCell(withIdentifier: "GroupLeftCell", for: indexPath) as! QlueChatGroupTextLeftCell
          
          cell.cellTextLabel.text = message
          cell.cellTime.text = Date().getDateFromTimestamp(chat.timestamp)?.timeAgoWithNumericDates(true)
          if let avatarUrl = chat.avatar?.encodeURL(){
               cell.cellAvatar.sd_setImage(with: avatarUrl as URL!)
          }
          cell.cellUsername.text = chat.username
          
          return cell
     case .groupLeftImage:
          let cell = tableView.dequeueReusableCell(withIdentifier: "GroupImageLeftCell", for: indexPath) as! QlueChatGroupImageLeftCell
          
          if let url = chat.file?.encodeURL(){
               cell.cellImage.sd_setImage(with: url as URL!)
          }
          cell.cellTime.text = Date().getDateFromTimestamp(chat.timestamp)?.timeAgoWithNumericDates(true)
          if let avatarUrl = chat.avatar?.encodeURL(){
               cell.cellAvatar.sd_setImage(with: avatarUrl as URL!)
          }
          cell.cellUsername.text = chat.username
          
          return cell
     }
     
    }
     
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          
          let chat = chats[indexPath.row]
          
          let textFont = UIFont.init(name: QlueFont.HelveticaNeue, size: 14)!
          let username: CGFloat = 12 + 10
          
          let textHeight = chat.message?.heightWithConstrainedWidth(184, font: textFont) ?? 0
          
          let heightImage: CGFloat  = 100
          let height: CGFloat = 8 + 16 + 8 + 12 + 8
          
          switch chat.type {
          case .rightText, .leftText:
               return height + textHeight
          case .rightImage, .leftImage:
               return height + heightImage
          case .groupRightText, .groupLeftText:
               return height + username + textHeight
               
          case .groupRightImage, .groupLeftImage:
               return height + username + heightImage
          }

          
     }
}

// MARK: - UITextViewDelegate
extension QlueChatDetailController: UITextViewDelegate{
     func textViewDidChange(_ textView: UITextView) {
          let height = textView.contentSize.height
          
          if height == currentHeight || height < 15{
               return
          }
          
          if height > 120{
               heightBefore = currentHeight
               UIView.animate(withDuration: 0.5, animations: {
                    self.chatTextViewHeight.constant = 120
               })
               currentHeight = 120
               return
          }
          
          if height > currentHeight{
               heightBefore = currentHeight
               UIView.animate(withDuration: 0.5, animations: {
                    self.chatTextViewHeight.constant = height
               })
               currentHeight = height
               return
          }
          
          if height < currentHeight{
               UIView.animate(withDuration: 0.5, animations: {
                    self.chatTextViewHeight.constant = self.heightBefore
               })
               
               currentHeight = height
          }
          
}
}

// MARK: - UIIMagePicker
extension QlueChatDetailController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
          
          if let imageUrl = info[UIImagePickerControllerReferenceURL] as? URL{
               
               ALAssetsLibrary().asset(for: imageUrl, resultBlock: { (asset) in
          
               
               let imageRep = asset?.defaultRepresentation()
               let metadata = asset?.defaultRepresentation().metadata()
                    let imageRef = imageRep?.fullScreenImage()
                    let image = UIImage(cgImage: (imageRef?.takeUnretainedValue())!)
                    let fixedImage = image.fixImageOrientation()
                    self.imagePreview.image = fixedImage
                    
                    self.imagePreviewHeight.constant = 100
                    
               }, failureBlock: { (error) in
                    
           })
          
          
         
          
          
          }
            dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
//        if let image = editingInfo?[UIImagePickerControllerOriginalImage] as? UIImage{
//            chatTextView.text = ""
//            let data = [
//                "message" : "",
//                "timestamp" : NSDate().getTimestamp(),
//                "user_id" : User.currentUser()?.userId ?? "",
//                "username" : User.currentUser()?.username
//            ]
//            let json = JSON.init(data)
//            let newChat = QlueChatDetail.init(fromJson: json, isGroup: isGroup)
//            chats.insert(newChat, atIndex: 0)
            
//            tableView.insertRowsAtIndexPaths([NSIndexPath.init(forRow: 0, inSection: 0)], withRowAnimation: .Automatic)
        
        //        }
//        imagePreview.image = image
//        imagePreviewHeight.constant = 100
//        dismissViewControllerAnimated(true, completion: nil)
    }
}

// MARK: - Navigation
extension UIViewController {
    func presentChatDetail(withGroupId groupId: String, isGroup: Bool, members: [QlueMember]){
        if let detail = chatStoryboard.instantiateViewController(withIdentifier: "ChatDetail") as? QlueChatDetailController{
            detail.initWithGroupId(groupId, isGroup: isGroup, members: members)
            present(detail, animated: true, completion: nil)
        }
    }
}
