//
//  QlueKDiskusiController.swift
//  Qlue
//
//  Created by Nurul on 6/10/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import Alamofire
import UIScrollView_InfiniteScroll
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum ForumType{
    case kelurahan(codeKel: String)
     case general
}

enum ForumGroup {
     case favorite
     case other
     
     var description: String {
          switch self {
          case .favorite:
               return "Favorite Communities"
          case .other:
               return "Other Communities"
          }
     }
}


class QlueKDiskusiController: QlueViewController {
     
     
     var forumType: ForumType = .general
     
     // MARK: - Outlets
    @IBOutlet weak var barHeight: NSLayoutConstraint!
     @IBOutlet weak var collectionView: UICollectionView!
     fileprivate var codeKel: String!
     var diskusi: [QlueDiskusi] = []
     var favDiscussion: [QlueDiskusi] = []
     var communityDiscussion: [QlueDiskusi] = []
     var start = 0
     
     var forums : [ForumGroup: [QlueDiskusi]] = [
          .favorite: [],
          .other: []
     ]
    var selectedForum: QlueDiskusi?
     
     // MARK: - View Lifecycle
     override func viewDidLoad() {
          super.viewDidLoad()
          configureView()
          configureCollectionView()
          observeNotification()
          
          DejalActivityView.addActivityView(for: collectionView)
          
     }
    
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          
          downloadData()
          self.navigationController?.isNavigationBarHidden = true
     }
     
     override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
//          self.navigationController?.navigationBarHidden = false
     }
    
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     
     func initWithCodeKel(_ codeKel: String?){
          self.codeKel = codeKel ?? "0"
     }
     
     // MARK: - Configure
     func observeNotification(){
          NotificationCenter.default.addObserver(self, selector: #selector(QlueKDiskusiController.downloadData), name: NSNotification.Name(rawValue: kForumCreated), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(QlueKDiskusiController.downloadData), name: NSNotification.Name(rawValue: kForumEdited), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(QlueKDiskusiController.forumDeleted(_:)), name: NSNotification.Name(rawValue: kForumDeleted), object: nil)
     }
     
     func configureView(){
          switch forumType {
          case .kelurahan(_):
               barHeight.constant = 64
          case .general:
               barHeight.constant = 0
          }
          
     }
     
     func configureCollectionView(){
          
          collectionView.register(UINib.init(nibName: "QlueForumViewCell", bundle: nil), forCellWithReuseIdentifier: "ForumCell")
          collectionView.register(UINib.init(nibName: "QlueForumHeaderReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCell")
          collectionView.contentInset = UIEdgeInsets(top: -8, left: 0, bottom: 0, right: 0)
          
          collectionView.addInfiniteScroll { (collectionView) in
               self.loadMore()
          }
     }
     
     // MARK: - Helper
     func forumDeleted(_ sender: Notification){
          if let id = sender.userInfo?["id"] as? String{
               if let i = diskusi.index(where: {$0.id == id}) {
                    self.diskusi.remove(at: i)
                    self.collectionView.reloadData()
               }
          }
     }
     
     func downloadData(){
          
          var req : DataRequest!
        switch forumType {
        case .kelurahan(let codeKel):
            req = Engine.shared.listDiskusiKelurahan(codeKel, start: start)
        case .general:
            req = Engine.shared.listForumGeneral(start)
        }
        
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               self.collectionView.finishInfiniteScroll()
               },completion:  { (data, success, fail) in
                    
                    if self.start == 0{ self.diskusi = [] }
                    
                    if success == true{
                         for d in data.arrayValue{
                              self.diskusi.append(QlueDiskusi.init(fromJson: d))
                         }
                    }
                    
                    self.filterDiskusi()
          })
          
     }
     
     fileprivate func filterDiskusi() {
          
          
          forums[.favorite] = diskusi.filter { (diskusi) -> Bool in
               if diskusi.isFav == true{
                    return true
               }
               return false
          }.sorted(by: { (diskusi0, diskusi1) -> Bool in
               if diskusi0.isSticky == true{
                    return true
               }
               return false
          })
          
          
          
          forums[.other] = diskusi.filter({ (diskusi) -> Bool in
               if diskusi.isFav != true{
                    return true
               }
               return false
          }).sorted(by: { (diskusi0, diskusi1) -> Bool in
               if diskusi0.isSticky == true{
                    return true
               }
               return false
          })
          
          collectionView.reloadData()
     }
     
     func loadMore(){
          start = diskusi.count
          downloadData()
     }
    
    // MARK: - Action
    @IBAction func createForumTapped(_ sender: UIButton) {
     switch forumType {
     case .general:
          if User.currentUser()?.currLevel == "Newbie"{
               Util.showSimpleAlert("Posting pada Forum Umum hanya untuk level citizen ke atas", vc: self, completion: nil)
               
               return
          }

     default:
          break
     }
    
        presentPostForum(withType: .forum, forumType: forumType)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Detail"{
            let destination = segue.destination as? QlueForumDetailController
            destination?.initWithForumId(selectedForum?.id ?? "", forumType:  forumType)
          
        }
    }
     
}

// MARK: - UICollectionViewDataSource
extension QlueKDiskusiController: UICollectionViewDataSource{
     func numberOfSections(in collectionView: UICollectionView) -> Int {
          return forums.count
     }
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          let key = Array(forums.keys)[section]
          if let forumCount = forums[key]?.count{
               return forumCount
          }
          return 0
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForumCell", for: indexPath) as! QlueForumViewCell
          
          cell.cornerRadius(5)
          cell.border(1, color: kGreyEEE)
          
          let key = Array(forums.keys)[indexPath.section]
          guard let data =  forums[key]?[indexPath.row] else {
               return cell
          }
          if data.isFav != true{
               cell.cellStar.setImage(UIImage.init(named: "ic_staroff"), for: UIControlState())
          }else{
               cell.cellStar.setImage(UIImage.init(named: "ic_staron"), for: UIControlState())
          }
          
//          if data.favable != true{
//               cell.cellStarWidth.constant = 0
//          } else{
//               cell.cellStarWidth.constant = 32
//          }
          
          if (data.isVote == true) && (data.isFav == false){
               cell.indicatorView.backgroundColor = kDarkColor
          } else if data.isFav == true{
                cell.indicatorView.backgroundColor = kCommunityForumIndicatorColor
          } else{
               cell.indicatorView.backgroundColor = kQlueBlue
          }

          
          cell.cellTitle.text = data.title
          if let imgUrl = data.file?.encodeURL(){
               cell.cellImage.sd_setImage(with: imgUrl as URL!)
          }
          let timestamp = Date().getDateFromTimestamp(data.timestamp)
          cell.cellDate.text = timestamp?.formatDate(withStyle: DateFormatter.Style.full)
          cell.cellTime.text = timestamp?.formatTime(withStyle: DateFormatter.Style.short)
          cell.cellUsername.setTitle(data.username, for: UIControlState())
          cell.cellSeen.setTitle(data.viewCount, for: UIControlState())
          cell.cellComment.setTitle(data.commentCount, for: UIControlState())
          cell.delegate = self
          
          return cell
     }
     func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
          if kind == UICollectionElementKindSectionHeader{
               let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCell", for: indexPath) as! QlueForumHeaderReusableView
               
               cell.cellTitle.text = Array(forums.keys)[indexPath.section].description
               
               return cell
          }
          
          
          return UICollectionReusableView()
     }
}

// MARK: - UICollectionViewDelegate
extension QlueKDiskusiController: UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let key = Array(forums.keys)[indexPath.section]
        selectedForum =  forums[key]?[indexPath.row]
          performSegue(withIdentifier: "Detail", sender: self)
     }
     
     
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueKDiskusiController: UICollectionViewDelegateFlowLayout{
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          return CGSize(width: collectionView.bounds.width - 24, height: 120)
          
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return 8
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 8
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsetsMake(0, 12, 0, 12)
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
          
          let key = Array(forums.keys)[section]
          if forums[key]?.count > 0{
               return CGSize(width: collectionView.bounds.width - 40, height: 45)
          }
          
          return CGSize.zero
          
     }
     
//     func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
//          let lastElement = diskusi.count - 1
//          if indexPath.item == lastElement{
//               loadMore()
//          }
//     }
     
}

// MARK: - QlueForumViewCellDelegate
extension QlueKDiskusiController: QlueForumViewCellDelegate{
     func starDidTapped(_ cell: UICollectionViewCell, button: UIButton) {
          guard let indexPath = collectionView.indexPath(for: cell) ,
               let id = forums[Array(forums.keys)[indexPath.section]]?[indexPath.row].id
               else { return }
          
          
          let req = Engine.shared.favoriteDiskusi(withId: id)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion: { (data, success, fail) in
                    if success == true{
                         
                         self.forums[Array(self.forums.keys)[indexPath.section]]?[indexPath.row].isFav = true
                         
                         button.setImage(UIImage.init(named: "ic_staron"), for: .normal)
                    }
          }) {
               
               self.forums[Array(self.forums.keys)[indexPath.section]]?[indexPath.row].isFav = false
               button.setImage(UIImage.init(named: "ic_staroff"), for: .normal)
          }
     }
     
     func seenDidTapped(_ cell: UICollectionViewCell, button: UIButton) {
          guard let indexPath = collectionView.indexPath(for: cell) ,
               let forum = forums[Array(forums.keys)[indexPath.section]]?[indexPath.row]
               else { return }
          presentSeenBy(withId: forum.id ?? "", countSeen: forum.viewCount ?? "0")
     }
}

// MARK: - Navigate
extension UIViewController {
     func presentListForum(withType type: ForumType, nav: UINavigationController?){
          if let forum = forumStoryboard.instantiateViewController(withIdentifier: "KDiskusi") as? QlueKDiskusiController{
               
               forum.forumType = type
               
               if let nav = nav{
                    nav.pushViewController(forum, animated: true)
               } else{
                    present(forum, animated: true, completion: nil)
               }
          }
     }
}
