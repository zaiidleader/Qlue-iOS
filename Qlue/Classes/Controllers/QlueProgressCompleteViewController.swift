//
//  QlueProgressCompleteViewController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 8/28/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueProgressCompleteViewController: QlueViewController {

     var feedId: Int = 0
     
    @IBOutlet weak var userRate: QlueRateView!
    @IBOutlet weak var wrapView: UIView!
    @IBOutlet weak var workDuration: UILabel!
    @IBOutlet weak var timeago: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var kelurahanName: UILabel!
    @IBOutlet weak var ratingView: QlueRateView!
    @IBOutlet weak var totalReview: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    
     var progress: QlueFeedProgress?
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
     

        downloadData()
     DejalBezelActivityView.addActivityView(for: wrapView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
     
        
    }
    
    // MARK: - Configure
    fileprivate func configureView(){
     if let imageUrl = progress?.file?.encodeURL(){
          mainImage.sd_setImage(with: imageUrl as URL!)
     }
     
     let reviewCount = progress?.reviewCount ?? "0"
     totalReview.text = "(" + reviewCount + " Review)"
     
     let review = progress?.review ?? "0"
     ratingView.rate = Double.init(review) ?? 0
     
     kelurahanName.text = progress?.name
     
     userName.text = progress?.username
     
     descLabel.text = progress?.descriptionField
     
     timeago.text = Date().getDateFromTimestamp(progress?.timestamp)?.timeAgoWithNumericDates(true)
     
     userRate.delegate = self
     
     let period = progress?.period ?? "0"
     workDuration.text = "(work duration " + period + ")"
    }
    
    // MARK: - Data
     fileprivate func geCurrentRanking(_ progressId: String){
          let req = Engine.shared.getFeedProgressRating(feedId, progressId: progressId)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               },completion:  { (data, success, fail) in
                    if success == true{
                         if let review = data["review_progress"].string, let rate = Double(review) {
                            self.userRate.rate = rate
                        }
                    }
          })
     }
    fileprivate func downloadData(){
    
          let req = Engine.shared.getFeedProgressDetail(feedId, type: "complete")
     Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
          DejalBezelActivityView.remove()
          },completion:  { (data, success, fail) in
               if success == true{
                    self.progress = QlueFeedProgress.init(fromJson: data)
                    self.configureView()
                    self.geCurrentRanking(self.progress?.id ?? "")
               }
     })
        
    }
}

// MARK: - QlueRateViewDelegate
extension QlueProgressCompleteViewController: QlueRateViewDelegate{
     func rating(_ cell: QlueRateView, didRatingSelected rating: Double) {
          guard let progressId = progress?.id else{ return }

          let req = Engine.shared.postFeedProgressRating(self.feedId, progressId: progressId, rate: rating)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion:  { (data, success, fail) in
                    print("data post rate: \(data)")
                    if data["response"].string == "success"{
                         
                         self.progress?.review = data["review"].string
                         self.progress?.reviewCount = data["count_review"].string
                         self.configureView()
                         
                    }
          })
     }
}


// MARK: - Navigate
extension QlueViewController{
     func presentProgressComplete(withFeedId id: Int){
          let progressVc = QlueProgressCompleteViewController()
          progressVc.feedId = id
          semiModalWithViewController(progressVc)
     }
}
