//
//  QlueKSearchViewController.swift
//  Qlue
//
//  Created by Nurul on 7/23/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON
import Alamofire

class QlueKSearchViewController: QlueViewController {
     
     // MARK: - Outlets
     @IBOutlet weak var searchTextfield: UITextField!{
          didSet{
               Util.textfieldPlaceholderWithColor(kQlueBlue, font: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 12), textfield: searchTextfield)
          }
     }
     @IBOutlet weak var searchTextView: UIView!{
          didSet{
               searchTextView.cornerRadius(5)
          }
     }
     @IBOutlet weak var tableView: UITableView!
     @IBOutlet weak var locationPlaceName: UILabel!
     
     // MARK: - Private var
     var location: CLLocation?
     var header: [Character] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
     var kelurahan: [QlueKelurahan] = []
     
     var groupedKelurahan : [Character: [QlueKelurahan]] = [:]
     var searchReq: DataRequest?
     
     // MARK: - View Life cycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          // Do any additional setup after loading the view.
          
          DejalActivityView.addActivityView(for: self.view)
          setCurrentLocation()
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          navigationController?.isNavigationBarHidden = true
     }
     
     override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          navigationController?.isNavigationBarHidden = false
     }
     
     // MARK: - Helper
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               self.location = location
               geocoderFromLocation(location)
               self.searchKelurahanWithQuery("")
               
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QlueKSearchViewController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }
     
     fileprivate func groupAlphabetically(){
          
          for alphabet in header{
               groupedKelurahan[alphabet]  = kelurahan.filter { (kel) -> Bool in
                    if kel.name?.characters.first == alphabet{
                         return true
                    }
                    return false
               }
          }
          
          tableView.reloadData()
          
     }
     
     // MARK: - Data
     fileprivate func geocoderFromLocation(_ location: CLLocation){
          
          let geocoder = CLGeocoder.init()
          geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
               if let error = error{
                    print("Geocode failed with error: \(error.localizedDescription)")
               }
               if let locality = placemarks?.first?.locality, let subLocality = placemarks?.first?.subLocality{
                    self.locationPlaceName.text = "Your location: " + locality + ", " + subLocality
               }
               
          }
          
     }
     
     fileprivate func searchKelurahanWithQuery(_ query: String){
        guard let location = location else { return }
        
          searchReq = Engine.shared.searchKelurahan(query: query, lat: location.coordinate.latitude, lng: location.coordinate.longitude)
          Engine.shared.parseJsonWithReq(searchReq!, controller: self, loadingStop: {
               DejalActivityView.remove()
          },completion:  { (data, success, fail) in
               if success == true{
                    print("data \(data)")
                    self.kelurahan = []
                    if let kels = data.array{
                         for kel in kels{
                              self.kelurahan.append(
                                   
                                   QlueKelurahan.init(fromJson: kel))
                         }
                    }
                    
                    if self.kelurahan.count > 0{
                         self.tableView.backgroundView = UIView()
                    } else{
                         self.tableView.layoutIfNeeded()
                         let view = Bundle.main.loadNibNamed("QlueCantConnectView", owner: nil, options: nil)?.first as! UIView
                         view.frame = self.tableView.frame
                         
                         self.tableView.backgroundView = view
                         
                    }
                    
                    self.groupAlphabetically()
               }
          })
     }
    
    fileprivate func joinKelurahanWithKel(_ kel: QlueKelurahan){
        guard let codeKel = kel.codeKel, let district = kel.district else{
            return
        }
     DejalBezelActivityView.addActivityView(for: self.tableView)
     let req = Engine.shared.joinKelurahan(codeKel, district: district)
     Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
          DejalActivityView.remove()
          }, completion: { (data, success, fail) in
               print(data)
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               
               let user = User.currentUser()
               if success == true{
                    user?.codeKelurahan = codeKel
               }
               appDelegate.saveContext()
               
               self.dismiss(animated: true, completion: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kKelurahanChanged), object: nil)
               })
               
     }) {
          
          
     }
     

    }
    
    // MARK: - Action
    @IBAction func searchDidChange(_ sender: UITextField) {
        
        if let _ = location, let text = sender.text{
          searchReq?.cancel()
            searchKelurahanWithQuery(text)
        }
        
    }
    
     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
      }
      */
     
}

// MARK: - UITableView
extension QlueKSearchViewController: UITableViewDataSource, UITableViewDelegate{
     func numberOfSections(in tableView: UITableView) -> Int {
          return groupedKelurahan.count
     }
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return groupedKelurahan[header[section]]!.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
          
          cell.textLabel?.text = groupedKelurahan[header[indexPath.section]]![indexPath.row].name
          
          return cell
     }
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
          let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 45))
          view.backgroundColor = UIColor.white
          
          let label = UILabel.init(frame: CGRect.init(x: 15, y: 0, width: tableView.bounds.width - 16, height: view.bounds.height))
          label.textAlignment = .left
          label.text = String(header[section])
          label.font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 13)
          label.textColor = UIColor.lightGray
          view.addSubview(label)
          
          let borderBottom = UIView.init(frame: CGRect.init(x: 0, y: view.bounds.height - 1, width: view.bounds.width, height: 1))
          borderBottom.backgroundColor = kGreyEEE
          view.addSubview(borderBottom)
          
          return view
     }
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
          if groupedKelurahan[header[section]]!.count > 0{
               return 45
               
          }
          
          return 0
          
     }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 50
     }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        joinKelurahanWithKel(groupedKelurahan[header[indexPath.section]]![indexPath.row])
    }
}
