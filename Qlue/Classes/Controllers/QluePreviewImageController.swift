//
//  QluePreviewImageController.swift
//  Qlue
//
//  Created by Nurul on 8/7/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QluePreviewImageController: QlueViewController, UIScrollViewDelegate {
          
          @IBOutlet weak var scrollView: UIScrollView!
          @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
          
          fileprivate var image: UIImage?
          var imgUrl: String?
          
          override func viewDidLoad() {
               super.viewDidLoad()
               
               guard let imgUrl: String = imgUrl else{
                    return
               }
               
               view.layoutIfNeeded()
               
               activityIndicator.startAnimating()
               
               let imageView: UIImageView = UIImageView()
               imageView.sd_setImage(with: URL(string: imgUrl)) { (image, error, cache, url) in
                    
                    self.activityIndicator.stopAnimating()
                    
                    if let image: UIImage = image{
                         imageView.frame = CGRect(origin: CGPoint.zero, size: image.size)
                         self.image = image
                         imageView.image = image
                         imageView.tag = 99
                         
                         self.scrollView.addSubview(imageView)
                         self.scrollView.contentSize = image.size
                         
                         self.scrollView.delegate = self
                         
                         let minScale: CGFloat = min(UIScreen.main.bounds.width / image.size.width, UIScreen.main.bounds.height / image.size.height)
                         self.scrollView.minimumZoomScale = minScale
                         self.scrollView.maximumZoomScale = 2.0
                         self.scrollView.zoomScale = minScale
                         self.setCenterContent(self.scrollView)
                         
                         // Double tap gesture
                         let doubleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleDoubleTap:"))
                         doubleTap.numberOfTapsRequired = 2
                         doubleTap.numberOfTouchesRequired = 1
                         self.scrollView.addGestureRecognizer(doubleTap)
                    }
               }
          }
          
          override func viewWillLayoutSubviews() {
               super.viewWillLayoutSubviews()
               guard let image: UIImage = image else{
                    return
               }
               
               
               let minScale: CGFloat = min(UIScreen.main.bounds.width / image.size.width, UIScreen.main.bounds.height / image.size.height)
               scrollView.minimumZoomScale = minScale
               scrollView.zoomScale = max(minScale, scrollView.zoomScale)
          }
          
          // MARK: - Helpers
          
          fileprivate func setCenterContent(_ scrollView: UIScrollView){
               if let view: UIView = scrollView.viewWithTag(99) {
                    let offsetX: CGFloat = max((scrollView.bounds.width - scrollView.contentSize.width) * 0.5, 0.0)
                    let offsetY: CGFloat = max((scrollView.bounds.height - scrollView.contentSize.height) * 0.5, 0.0)
                    
                    view.center = CGPoint(x: scrollView.contentSize.width * 0.5 + offsetX, y: scrollView.contentSize.height * 0.5 + offsetY)
               }
          }
          
          fileprivate func handleDoubleTap(_ sender: UITapGestureRecognizer) {
               
               if sender.state == UIGestureRecognizerState.ended {
                    
                    if scrollView.zoomScale == scrollView.minimumZoomScale {
                         scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
                    }
                    else {
                         scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
                    }
               }
          }
          
          // MARK: - UIScrollViewDelegate
          
          func viewForZooming(in scrollView: UIScrollView) -> UIView? {
               return scrollView.viewWithTag(99)
          }
          
          func scrollViewDidZoom(_ scrollView: UIScrollView) {
               setCenterContent(scrollView)
          }
          
          // MARK: - Action
          
          @IBAction func closeTapped(_ sender: UIButton) {
               self.dismiss(animated: true, completion: nil)
          }
}

extension QlueViewController{
    func presentPreviewImage(withUrlString url: String){
        if let preview = kelurahanStoryboard.instantiateViewController(withIdentifier: "PreviewImage") as? QluePreviewImageController{
            preview.imgUrl = url
          semiModalWithViewController(preview)
        }
    }
}

