//
//  QluePlaceSwastaController.swift
//  Qlue
//
//  Created by Nurul on 6/25/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QluePlaceSwastaController: QlueViewController, UITextFieldDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: UIView!{
        didSet{
            searchView.cornerRadius(5)
        }
    }
    @IBOutlet weak var searchTextfield: UITextField!{
        didSet{
            searchTextfield.cornerRadius(5)
        }
    }
    
    // MARK: - Vars
    fileprivate var places : [QluePoi] = []
    fileprivate var oriPlaces : [QluePoi] = []
    fileprivate var didBackTapped: (() -> Void)?
    fileprivate var selected = 0
     
     fileprivate var newState = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextfield.delegate = self
        
        tableView.register(UINib.init(nibName: "QlueSwastaSearchViewCell", bundle: nil), forCellReuseIdentifier: "SearchCell")
        
        DejalActivityView.init(for: tableView)
        setCurrentLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Helpers
    func setCurrentLocation() {
        
        Engine.shared.locationManager.startUpdatingLocation()
        
        if let location = Engine.shared.currentLocation {
            let latitude: Double = location.coordinate.latitude
            let longitude: Double = location.coordinate.longitude
            
            downloadData(latitude, lng: longitude)
        }else {
            
            NotificationCenter.default.addObserver(self, selector: #selector(QlueTopicGovController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
            Engine.shared.locationManager.startUpdatingLocation()
            
            //            downloadService(nil, longitude: nil)
        }
    }
    
    func removeObserverLoc() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
        setCurrentLocation()
    }
    fileprivate func downloadData(_ lat: Double, lng: Double){
        
        
        places = []
        
        let req =  Engine.shared.getPlaceListSwasta(1, lat: lat, long: lng)
        
        Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
            
            DejalActivityView.remove()
            
        },completion:  { (data, success, fail) in
            if success == true{
                
                if let poi = data.array{
                    for p in poi{
                        self.places.append(QluePoi.poiWithData(p))
                        self.oriPlaces.append(QluePoi.poiWithData(p))
                    }
                }
                
                self.tableView.reloadData()
            }
            
        })
        
    }
    
    
    // MARK: - Action
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        
        guard let text = sender.text, text.characters.count > 0 else{ return}
        
        self.places = placesFilterWithText(text)
        tableView.reloadData()
        
    }
    override func backTapped(_ sender: AnyObject) {
        super.backTapped(sender)
        if let didBackTapped = didBackTapped{
            didBackTapped()
        }
    }
    
    func placesFilterWithText(_ text: String) -> [QluePoi]{
    
        let fileredPlaces = oriPlaces.filter { (place) -> Bool in
            if place.name?.lowercased().range(of: text.lowercased()) != nil{
                return true
            }
            
            return false
        }
        
        if fileredPlaces.count == 0{
          newState = true
            let poi = QluePoi()
            poi.name = text
            poi.note = "Tambah lokasi baru: \" \(text) \" "
            return [poi]
        } else{
          newState = false
     }
        
        return fileredPlaces
    }

    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "CategorySegue"{
               let destination = segue.destination as? QluePlaceCategoryController
               destination?.initWithPlaceName(places[0].name)
          }
     
     }
     
    
}

// MARK: - UITableViewCellDataSource
extension QluePlaceSwastaController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! QlueSwastaSearchViewCell
        
        let place = places[indexPath.row]
        
        cell.cellDesc.text = place.note
        cell.cellLabel.text = place.name
     if let imageStr = place.icon, let image = PlaceMark(rawValue: imageStr){
          
            cell.cellImage.image = image.image
        }
        
        return cell
    }
}

// MARK: - UITableViewCellDelegate
extension QluePlaceSwastaController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        selected = indexPath.row
     
     if newState == true{
          
          self.presentPlaceCategory(places[0].name, completion: {
               
               self.performSegue(withIdentifier: "ChoosePhoto", sender: self)
          })
          return
     }
        Engine.shared.currentReport?.poi = places[indexPath.item]
        
        self.performSegue(withIdentifier: "ChoosePhoto", sender: self)
     
    }
    
}

// MARK: - Navigation
extension QlueViewController{
    func presentReportSwastaSearchPlace(_ nav: UINavigationController?, completion: (()-> Void)? = nil){
        if let place = reportStoryboard.instantiateViewController(withIdentifier: "SearchPlace") as? QluePlaceSwastaController{
            
            place.didBackTapped = completion
            
            if let nav = nav{
                nav.pushViewController(place, animated: true)
                return
            }
            present(place, animated: true, completion: nil)
            
        }
    }
}
