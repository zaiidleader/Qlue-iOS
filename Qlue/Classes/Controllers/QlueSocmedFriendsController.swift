//
//  QlueSocmedFriendsController.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 12/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import TwitterKit


class QlueSocmedFriendsController: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var socmed: Socmed = .facebook
    var data: [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(true, animated: false)
        let image = UIImage(named: "ic_back")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        backButton.setImage(image, for: UIControlState())
        backButton.setTitle(socmed.backButtonTitle, for: UIControlState())
        
        tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - Actions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITableViewDataSource
extension QlueSocmedFriendsController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCellId", for: indexPath) as! QlueSocmedFriendViewCell
        
        cell.friendNameLabel.text = nil
        cell.friendImageView.image = nil
        
        let d = data[indexPath.row]
        if socmed == .facebook {
            cell.friendNameLabel.text = d["name"].string
            
            if let userId = d["id"].string {
                let urlString = "https://graph.facebook.com/\(userId)/picture?width=78&height=78"
                if let url = URL(string: urlString) {
                    cell.friendImageView.sd_setImage(with: url)
                }
            }
        }
        else if socmed == .twitter {
            cell.friendNameLabel.text = d["name"].string
            
            if let urlString = d["profile_image_url"].string {
                if let url = URL(string: urlString) {
                    cell.friendImageView.sd_setImage(with: url)
                }
            }
        }
        
        cell.delegate = self
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension QlueSocmedFriendsController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let d = data[indexPath.row]
        if socmed == .facebook {
            
        }
    }
}


// MARK: - QlueSocmedFriendViewCellDelegate
extension QlueSocmedFriendsController: QlueSocmedFriendViewCellDelegate {
    
    func socmedFriendViewCellInviteButtonTapped(_ cell: QlueSocmedFriendViewCell) {
        
        if let indexPath = tableView.indexPath(for: cell) {
            let d = data[indexPath.row]
            
            if socmed == .facebook {
                
                let shareString = "Download Qlue"
                let objectsToShare = [shareString]
                
                let vc = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                vc.completionWithItemsHandler = { (activityType, completed, returnedItems, activityError) -> Void in
                }
                if let popoverPresentationController = vc.popoverPresentationController {
                    popoverPresentationController.sourceView = cell
                    popoverPresentationController.sourceRect = cell.bounds
                }
                present(vc, animated: true, completion: nil)
            }
            else if socmed == .twitter {
                
                let client = TWTRAPIClient.withCurrentUser()
                let request = client.urlRequest(withMethod: "POST", url: "https://api.twitter.com/1.1/direct_messages/new.json", parameters: ["text": "Download Qlue", "user_id": d["id_str"].stringValue], error: nil)
                
                DejalBezelActivityView.addActivityView(for: self.view)
                client.sendTwitterRequest(request, completion: { (response, data, error) in
                    DejalBezelActivityView.remove(animated: true)
                    
                    if let _ = data {
                        self.data.remove(at: indexPath.row)
                        self.tableView.deleteRows(at: [indexPath], with: .automatic)
                    }
                    else if let error = error {
                        print(error.localizedDescription)
                    }
                })
            }
        }
    }
}
