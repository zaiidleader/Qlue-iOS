//
//  QlueFeedViewController.swift
//  Qlue
//
//  Created by Nurul on 6/4/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage
import SwiftyJSON
import ActionSheetPicker_3_0
//import AssetsLibrary
import FLAnimatedImage
import Alamofire
import PBJVideoPlayer
import Photos

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class QlueFeedViewController: QlueScrollViewController {
     
     // MARK: Outlets
     @IBOutlet weak var collectionView: UICollectionView!
     @IBOutlet weak var goUpButton: UIButton!
     
     // MARK: Private Var
     fileprivate var feeds : [QlueFeed] = []
     fileprivate var page = 0
     
     // User
     fileprivate var user: User?
     
     // Mark: Refresh
     let refreshControl = UIRefreshControl()
     
     fileprivate var currentLocation: CLLocation?
     
     var selectedItem = 0
     
     // MARK: asset library var
//     var library: ALAssetsLibrary = ALAssetsLibrary()
     
     var kelurahan: QlueKelurahan?
     var weatherUrl: String = ""
     
     
     
     // MARK: - View Life Cycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          //collectionView
          registerNib()
          
          //refresh
          collectionView.addSubview(refreshControl)
          refreshControl.addTarget(self, action: #selector(QlueFeedViewController.refresh(_:)), for: .valueChanged)
          DejalActivityView.addActivityView(for: self.collectionView)
          //        refreshControl.beginRefreshing()
          
          
          // location
          setCurrentLocation()
          
          
          // loadmore
          collectionView.addInfiniteScroll { (collectionView) in
               self.loadMore()
          }
     }
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          // getProfileData
//          navigationController?.navigationBarHidden = true
          loadUserData()
          downloadData()
          
     }
     override func viewWillDisappear(_ animated: Bool) {
          super.viewDidDisappear(animated)
//          navigationController?.navigationBarHidden = false
     }
     
     
     // MARK: - Private Func
     fileprivate func registerNib(){
          collectionView.register(UINib.init(nibName: "QlueForumFeedViewCell", bundle: nil), forCellWithReuseIdentifier: "ForumCell")
          collectionView.register(UINib.init(nibName: "QlueFeedViewCell2", bundle: nil), forCellWithReuseIdentifier: "FeedCell")
          collectionView.register(UINib.init(nibName: "QlueKelurahanFeedViewCell", bundle: nil), forCellWithReuseIdentifier: "KelurahanCell")
          collectionView.register(UINib.init(nibName: "QlueKelurahanFeedViewCell2", bundle: nil), forCellWithReuseIdentifier: "KelurahanCell")
          collectionView.register(UINib.init(nibName: "QlueFeedNoContentViewCell", bundle: nil), forCellWithReuseIdentifier: "NoContentCell")
     }
     fileprivate func loadMore(){
          self.page += 1
          downloadData()
     }
     
     // MARK: - Location
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               currentLocation = location
               Engine.shared.checkProvinsi(currentLocation!.coordinate.latitude, lng: currentLocation!.coordinate.longitude, completion: { (result, error) in
                    
                    self.getWeather()
                    self.downloadData()
               })
               
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QlueFeedViewController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }
     
     // MARK: - Data
     fileprivate func loadUserData(){
          user = User.currentUser()
          downloadKelurahanData()
          
     }
     
     fileprivate func downloadKelurahanData(){
          guard let codekel = user?.codeKelurahan, let district = user?.provinsi else { return }
          
          let req = Engine.shared.profileKelurahan(codekel, district: district)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               }, completion:  { (data, success, fail) in
                    if success == true{
                         if let kel = data.array{
                              self.kelurahan = QlueKelurahan.init(fromJson: kel.first)
                              self.collectionView.reloadSections(NSIndexSet.init(index: 0) as IndexSet)
                         }
                         
                    }
          })
     }
     
     fileprivate func getWeather(){
          guard let location = currentLocation?.coordinate else { return  }
          let req = Engine.shared.getWeather(location.latitude, lng: location.longitude)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion: { (data, success, fail) in
                    if success == true{
                         self.weatherUrl = data["data"]["url"].stringValue
                         self.collectionView.reloadSections(NSIndexSet.init(index: 0) as IndexSet)
                    }
          }) {
               
          }
     }
     
     fileprivate func downloadData(){
          
          let req = Engine.shared.getFeed(User.currentUser()?.kelurahan, page: page, lat: currentLocation?.coordinate.latitude, lng: currentLocation?.coordinate.longitude)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               self.refreshControl.endRefreshing()
               self.collectionView.finishInfiniteScroll()
               }, completion: { (data, success, fail) in
                    if success == true{
                         if self.page == 0{
                              self.feeds = []
                         }
                         
                         for feed in data.arrayValue{
                              if feed == JSON.null {
                                   continue
                              }
                              self.feeds.append(QlueFeed.feedWithData(feed))
                         }
                         
                         self.collectionView.reloadData()
                         
                    }
          })
          
     }
     
     
     
     // MARK: - Action
     
     @IBAction func goUpTapped(_ sender: UIButton) {
          collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
          
          refreshControl.beginRefreshing()
          refreshControl.sendActions(for: UIControlEvents.valueChanged)
          
          
          //        goUpButton.hidden = true
          
     }
     func refresh(_ sender: UIRefreshControl){
          page = 0
          downloadData()
     }
     
     // MARK: - Helper
     func saveToImageAlbum(withImage image: UIImage){
          // FIXME: - to Swift 3
          PHPhotoLibrary.shared().performChanges({
               PHAssetChangeRequest.creationRequestForAsset(from: image)
          }) { (success, error) in
               self.saveDone(nil, error: error)
          }
     }
     
     func saveDone(_ assetURL: URL!, error: Error!){
          Util.showSimpleAlert("Save image done", vc: self, completion: nil)
          // FIXME: - to Swift 3
//          library.asset(for: assetURL, resultBlock: self.saveToAlbum, failureBlock: nil)
          
     }
     
     // FIXME: - to Swift 3
//     func saveToAlbum(_ asset:ALAsset!){
//          
//          library.enumerateGroupsWithTypes(ALAssetsGroupAlbum, usingBlock: { group, stop in
//               stop?.pointee = false
//               if(group != nil){
//                    let str = group?.value(forProperty: ALAssetsGroupPropertyName) as! String
//                    if(str == "MY_ALBUM_NAME"){
//                         group!.add(asset!)
//                         let assetRep:ALAssetRepresentation = asset.defaultRepresentation()
//                         let iref = assetRep.fullResolutionImage().takeUnretainedValue()
//                         let image:UIImage =  UIImage(cgImage:iref)
//                         
//                    }
//                    
//               }
//               
//               },
//                                           failureBlock: { error in
//                                             print("NOOOOOOO")
//          })
//          
//     }
     
     // MARK: - Navigation
     
     @IBAction func unwindToFeed(_ segue: UIStoryboardSegue) {
          if segue.source.isKind(of: QluePostGovController.self) || segue.source.isKind(of: QluePostSwastaController.self){
               
               downloadData()
          }
     }
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "FeedDetailSegue" {
               let destination = segue.destination as? QlueFeedDetailController
               destination?.initWithFeedId(feeds[selectedItem].feedId)
               
          } else if segue.identifier == "CommentSegue"{
               let destination = segue.destination as? QlueCommentController
               if let id = feeds[selectedItem].feedId{
                    destination?.initWithFeedId(String(id))
               }
          }
     }
     
     
}

//// MARK: - CLLocationManagerDelegate
//extension QlueFeedViewController: CLLocationManagerDelegate{
//     func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
////          currentLocation = newLocation
////          
////          getWeather()
////          downloadData()
////          locationManager.stopUpdatingLocation()
//     }
//}

// MARK: - UICollectionViewDataSource
extension QlueFeedViewController: UICollectionViewDataSource{
     func numberOfSections(in collectionView: UICollectionView) -> Int {
          return 2
     }
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          
          if section == 0 {
               return 1
          }
          if feeds.count == 0 {
               return 1
          }
          return feeds.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
          
          if indexPath.section == 0 {
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KelurahanCell", for: indexPath) as! QlueKelurahanFeedViewCell2
               
               
               if let urlImage = Bundle.main.url(forResource: "", withExtension: "gif"), let dataImage = try? Data.init(contentsOf: urlImage){
                    
                    cell.cellGifImage.animatedImage = FLAnimatedImage.init(animatedGIFData: dataImage)
               }
               
               if weatherUrl != ""{
                    Alamofire.request(weatherUrl, method: .get).responseData(completionHandler: { (res) in
                         if let data = res.data {
                              cell.cellGifImage.animatedImage = FLAnimatedImage.init(animatedGIFData: data)
                         }
                    })
               }
               
               
               let kelName =  kelurahan?.name ?? ""
//               let kelLurah = kelurahan?.fullName ?? ""
//               cell.cellKelurahanName.text = kelName + " - " + kelLurah
//               cell.cellReportLabel.text = kelurahan?.jumlahlaporan?.description
//               cell.cellDiscussionLabel.text = kelurahan?.jumlahdiskusi
//               cell.cellRankingLabel.text = kelurahan?.rank
//               cell.cellMemberLabel.text = kelurahan?.memberCount?.description
               
               cell.cellKelurahanName.text = kelName
               cell.cellRating.rate = kelurahan?.rating ?? 0
               
               cell.delegate = self
               
               return cell
          }
          
          if feeds.count == 0 {
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoContentCell", for: indexPath) as! QlueFeedNoContentViewCell
               cell.delegate = self
               return cell
          }
          
          
          let data = feeds[indexPath.item]
          
          if data.postType == "forum" {
          
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForumCell", for: indexPath) as! QlueForumViewCell
               
               cell.cornerRadius(0)
               cell.border(1, color: kGreyEEE)


               
               if (data.forumIsVote == true) && (data.forumIsFav == false){
                    cell.indicatorView.backgroundColor = kDarkColor
               } else if data.forumIsFav == true{
                    cell.indicatorView.backgroundColor = kCommunityForumIndicatorColor
               } else{
                    cell.indicatorView.backgroundColor = kQlueBlue
               }
               
               
               cell.cellTitle.text = data.qlueTitle
               if let imgUrl = data.fileUrl?.encodeURL(){
                    cell.cellImage.sd_setImage(with: imgUrl as URL!)
               }
               let timestamp = Date().getDateFromTimestamp(data.timestamp)
               cell.cellDate.text = timestamp?.formatDate(withStyle: DateFormatter.Style.full)
               cell.cellTime.text = timestamp?.formatTime(withStyle: DateFormatter.Style.short)
               cell.cellUsername.setTitle(data.username, for: UIControlState())
               cell.cellSeen.setTitle(data.forumViewCount, for: UIControlState())
               cell.cellComment.setTitle(data.forumCommentCount, for: UIControlState())
               cell.delegate = self
               
               return cell
          }
          
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCell", for: indexPath) as! QlueFeedViewCell2
          
          
          
          cell.cellAvatar.image = nil
          if let avaUrl = data.avatar?.encodeURL(){
               cell.cellAvatar.sd_setImage(with: avaUrl as URL!)
          }
          
          cell.cellName.text = data.username
          
          let date = Date().getDateFromTimestamp(data.timestamp)
          cell.cellTime.text = date?.timeAgoWithNumericDates(true)
          
//          cell.cellTitle.text     = data.title
          cell.cellDesc.text      = data.desc
          cell.cellDesc.handleHashtagTap { (hashtag) in
               self.presentFeedByCategory(hashtag, categoryName: hashtag, isCategory: false)
          }
          
          
          if (data.reportType == .ReviewNegatif ) || (data.reportType == .ReviewPositif ){
               cell.cellPlaceName.text = data.poi?.title
          } else{
               let kel = data.kelurahan ?? ""
               let kec = data.kecamatan ?? ""
               cell.cellPlaceName.text = kel + ", " + kec
          }
     
          
          cell.cellMainImage.image = nil
          cell.cellVideoButton.isHidden = true
          
          cell.videoPlayer.stop()
          cell.videoPlayer.view.isHidden = true
          cell.cellMainImage.image = nil
          if data.format == "image"{
               if let picUrl = data.fileUrl?.encodeURL(){
                    cell.cellMainImage.sd_setImage(with: picUrl as URL!)
               }

          }else{
               cell.cellVideoButton.isHidden = false
               cell.videoPlayer.videoPath = data.fileUrl
                cell.videoPlayer.videoFillMode = "AVLayerVideoGravityResizeAspectFill"
               if let picUrl = data.thumbUrl?.encodeURL(){
                    cell.cellMainImage.sd_setImage(with: picUrl as URL!)
               }

          }
          
          
//          cell.cellPoiTitle.text = "0 km"
//          if let distance = data.distance{
//               cell.cellPoiTitle.text = "\(round(Double(distance))) km"
//          }
          
          cell.cellCategoryImage.image = nil
          if let labelUrl = data.labelUrl?.encodeURL(){
               cell.cellCategoryImage.sd_setImage(with: labelUrl as URL!)
          }
          
          if data.like == true{
               cell.cellLikeButton.setImage(data.reportType?.likeImageActive2, for: UIControlState())
               //            cell.cellLikeButton.enabled = false
          }else{cell.cellLikeButton.setImage(data.reportType?.likeImageDeactive2, for: UIControlState())
               //            cell.cellLikeButton.enabled = true
          }
          
//          cell.cellLikeTitle.text = "0"
//          if let like = data.countLike {
//               cell.cellLikeTitle.text = String(like)
//               
//          }
          
//          cell.cellCommentTitle.text = "0"
//          if let comment = data.countComment {
//               cell.cellCommentTitle.text = String(comment)
//          }
          
          cell.cellLikeCommentTitle.text = nil
          if let like = data.countLike, like > 0 {
               cell.cellLikeCommentTitle.text = "\(like) Likes"
          }
          
          if let comment = data.countComment, comment > 0 {
               if let text = cell.cellLikeCommentTitle.text {
                    cell.cellLikeCommentTitle.text = "\(text), \(comment) Comments"
               }
               else {
                    cell.cellLikeCommentTitle.text = "\(comment) Comments"
               }
          }
          
          var commentHeight: CGFloat = 0
          for comment in data.comments{
               
               commentHeight += Util.getHeightCommentInFeedList(Util.getCommentAttributedString(comment.username, comment: comment.comment))
          }
          
          cell.cellCommentHeightConstraint.constant = commentHeight
          
          cell.comments = data.comments
          
//          cell.cellSeenButton.setTitle("0", forState: .Normal)
//          if let countView = data.countView{
//               cell.cellSeenButton.setTitle(String(countView), forState: .Normal)
//          }
          
//          if let state = data.progressState where (data.reportType != .ReviewNegatif ) && (data.reportType != .ReviewPositif ) {
//               cell.progressStateView.hidden = false
//               cell.cellGreenButton.setImage(state.image, forState: .Normal)
//               cell.stateDetail.text = state.labelDesc
//               
//          } else{
//               cell.progressStateView.hidden = true
//          }
          
          cell.delegate = self
          cell.cellTableView.reloadData()
          
          return cell
     }
     
}

// MARK: - UICollectionViewDelegate
extension QlueFeedViewController: UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          if indexPath.section == 0{
               presentKelurahan(withId: kelurahan?.code, district: kelurahan?.district, nav: self.navigationController)
          } else{
               
               if feeds.count == 0 {
                    return
               }
               
               let data = feeds[indexPath.item]
               
               if data.postType == "forum" {
                    presentForumDetail(forumId: data.feedId?.description ?? "", forumType: ForumType.general)
                    return
               }
               
               selectedItem = indexPath.item
               performSegue(withIdentifier: "FeedDetailSegue", sender: self)
          }
     }
     
     func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
          if indexPath.item > 3{
               goUpButton.isHidden = false
          } else{
               goUpButton.isHidden = true
          }
     }
     
     func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
          if let cell = cell as? QlueFeedViewCell2 {
               cell.videoPlayer.removeFromParentViewController()
          }
     }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueFeedViewController: UICollectionViewDelegateFlowLayout{
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          if indexPath.section == 0 {
               return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.width / (533 / 296))
          }
          
          if feeds.count == 0 {
               return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height - collectionView.bounds.width / (533 / 296))
          }
          
          let width = collectionView.bounds.width
          let data = feeds[indexPath.item]
          
          if data.postType == "forum" {
               return CGSize(width: collectionView.bounds.width, height: 120)
          }
          
          if data.postType == "ads" { return CGSize.zero }
          
          let imageHeight: CGFloat = width / 1.61803
          
          var height = imageHeight + 85 + 29 + 32 + 1 + 51
//          if let title = data.title {
//               height += title.heightWithConstrainedWidth(width - 35, font: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 17)!)
//          }
          
          if let desc = data.desc {
               height += desc.heightWithConstrainedWidth(width - 40 , font: UIFont.init(name: QlueFont.HelveticaNeue, size: 13)!)
          }
          
          if data.countComment > 0 {
               
               height += 69
               
               for comment in data.comments{
                    height += Util.getHeightCommentInFeedList(Util.getCommentAttributedString(comment.username, comment: comment.comment))
               }
               
          }
          
          return CGSize(width: collectionView.bounds.width, height: height)
     }
}

// MARK: - QlueFeedViewCell2Delegate

extension QlueFeedViewController: QlueFeedViewCell2Delegate {
     
     
     func videoTapped(_ cell: QlueFeedViewCell2, videoPlayer: PBJVideoPlayerController) {
          
          
          cell.videoPlayer.view.isHidden = false
          self.addChildViewController(videoPlayer)
          cell.cellMainImage.addSubview(videoPlayer.view)
          videoPlayer.didMove(toParentViewController: self)
          videoPlayer.playFromBeginning()
          
          
          
     }
     func avatarFeedTapped(_ cell: QlueFeedViewCell2) {
          guard let indexPath = collectionView.indexPath(for: cell) else{
               return
          }
          
          let feed = feeds[indexPath.item]
                    presentOtherProfile(feed.userId)

     }
     func avatarCommentTapped(_ feedCell: QlueFeedViewCell2, indexComment: Int) {
//          guard let indexPath = collectionView.indexPathForCell(feedCell) else{
//               return
//          }
//          
//          
//          let feed = feeds[indexPath.item]
//          presentOtherProfile(userId: feed.comments[indexComment].userId)
//
     }
     
     func categoryTapped(_ cell: QlueFeedViewCell2) {
          
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          
          let feed = feeds[indexPath.item]
          presentFeedByCategory(feed.categoryIcon ?? "", categoryName: feed.categoryName ?? "")
          
          
     }
     
     func countForCommentTableView(_ cell: QlueFeedViewCell2) -> Int {
          
          guard let indexPath = collectionView.indexPath(for: cell) else{
               return 0
          }
          
          if let count = feeds[indexPath.item].countPreviewComment{
               return count
          }
          return 0
          
          
     }
     
     func seeAllCommentTapped(_ cell: QlueFeedViewCell2) {
          if let indexPath = collectionView.indexPath(for: cell)
          {
               selectedItem = indexPath.item
               
               if let id = feeds[indexPath.item].feedId{
                    presentComment(withId: String(id), nav: self.navigationController)
               }
          }
     }
     
     func commentData(_ cell: QlueFeedViewCell2) -> [QlueComment] {
          if let indexPath = collectionView.indexPath(for: cell) {
               return feeds[indexPath.item].comments
          }
          
          return []
     }
     
     func poiTapped(_ cell: QlueFeedViewCell2) {
//          if let indexPath = collectionView.indexPathForCell(cell) {
//               let feed = feeds[indexPath.item]
//               if let lat = feed.lat, let lng = feed.lng {
//                    let location = CLLocation.init(latitude: CLLocationDegrees.init(lat), longitude: CLLocationDegrees.init(lng))
//                    presentMaps(nav: self.navigationController)
//               }
//          }
          kelurahanTapped(cell)
     }
     
     func likeTapped(_ cell: QlueFeedViewCell2, button: UIButton, label: UILabel) {
          if let indexPath = collectionView.indexPath(for: cell) {
               guard let postId = feeds[indexPath.item].feedId  else { return }
               let req = Engine.shared.postLike(withFeedId: String(postId))
               
               Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
                    }, completion: { (data, success, fail) in
                         if self.feeds[indexPath.item].like == false {
                              button.setImage(self.feeds[indexPath.item].reportType?.likeImageActive2, for: .normal)
                              self.feeds[indexPath.item].like = true
                              
                              if let countLike = self.feeds[indexPath.item].countLike{
                                   self.feeds[indexPath.item].countLike = countLike + 1
//                                   label.text = String(countLike + 1)
                              }
                         } else{
                              button.setImage(self.feeds[indexPath.item].reportType?.likeImageDeactive2, for: .normal)
                              self.feeds[indexPath.item].like = false
                              
                              if let countLike = self.feeds[indexPath.item].countLike{
                                   self.feeds[indexPath.item].countLike = countLike - 1
//                                   label.text = String(countLike - 1)
                              }
                         }
                         
                         cell.cellLikeCommentTitle.text = nil
                         if let like = self.feeds[indexPath.item].countLike, like > 0 {
                              cell.cellLikeCommentTitle.text = "\(like) Likes"
                         }
                         
                         if let comment = self.feeds[indexPath.item].countComment, comment > 0 {
                              if let text = cell.cellLikeCommentTitle.text {
                                   cell.cellLikeCommentTitle.text = "\(text), \(comment) Comments"
                              }
                              else {
                                   cell.cellLikeCommentTitle.text = "\(comment) Comments"
                              }
                         }
                    }, falseCompletion: {
                         

               })
          }
     }
     
     func kelurahanTapped(_ cell: QlueFeedViewCell2) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          
          let feed = feeds[indexPath.item]
          
          if (feed.reportType == .ReviewNegatif ) || (feed.reportType == .ReviewPositif ){
               Util.showSimpleAlert("Detail tidak tersedia", vc: self, completion: nil)
               return
          }
          
          if (feed.kdKel == nil)/* || (feed.reportType)*/{
               Util.showSimpleAlert("Detail tidak tersedia", vc: self, completion: nil)
               
               return
          }
          presentKelurahan(withId: feed.kdKel, district: feed.district, nav: self.navigationController)
     }
     
     func seenByTapped(_ cell: QlueFeedViewCell2) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          let feed = feeds[indexPath.item]
          presentSeenBy(withId: feed.feedId?.description ?? "", countSeen: feed.countView?.description ?? "", type:  0)
     }
     
     func moreTapped(_ cell: QlueFeedViewCell2) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
          let feed = feeds[indexPath.item]
          
          let alert = UIAlertController.init(title: "More", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
          
          let nomorKel = UIAlertAction.init(title: "Nomor Laporan", style: .default) { (action) in
               self.presentNomorLaporan(withNoLap: feed.feedId?.description)
          }
          alert.addAction(nomorKel)
          
          let simpanGambar = UIAlertAction.init(title: "Simpan Gambar", style: .default) { (action) in
               if let image = cell.cellMainImage.image{
                    self.saveToImageAlbum(withImage: image)
               }
          }
          alert.addAction(simpanGambar)
          
//          let laporkan = UIAlertAction.init(title: "Laporkan", style: .Default) { (action) in               
//          }
//          alert.addAction(laporkan)
        
//          let streetView = UIAlertAction.init(title: "Lihat Street View", style: .Default) { (action) in               
//          }
//          alert.addAction(streetView)
        
//          let editKel = UIAlertAction.init(title: "Edit Kelurahan", style: .Default) { (action) in
//          }
//          alert.addAction(editKel)
        
//          let tindakLanjut = UIAlertAction.init(title: "Tindak Lanjut", style: .Default) { (action) in
//          }
//          alert.addAction(tindakLanjut)
        
          let cancel = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
          alert.addAction(cancel)
          
          present(alert, animated: true, completion: nil)
     }
     
     func progressTapped(_ cell: QlueFeedViewCell2, button: UIButton) {
          
          guard let indexPath = collectionView.indexPath(for: cell) else{
               return
          }
          
          let feed = feeds[indexPath.item]
          
          if feed.progressState == .Complete{
               presentProgressComplete(withFeedId: feed.feedId ?? 0)
          } else if feed.progressState == .Process{
               presentProgressProcess(withFeedId: feed.feedId ?? 0)
          }
          
     }
}

// MARK: - QlueKelurahanFeedViewCell2Delegate
extension QlueFeedViewController: QlueKelurahanFeedViewCell2Delegate {
     func leaveTapped(_ cell: QlueKelurahanFeedViewCell2) {
//          guard let codeKel = User.currentUser()?.kelurahan, let district = User.currentUser()?.provinsi else{
//               return
//          }
//          DejalBezelActivityView.addActivityViewForView(collectionView)
//          let req = Engine.shared.joinKelurahan(codeKel, district: district)
//          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
//               DejalActivityView.removeView()
//               }, completion: { (data, success, fail) in
//                    print(data)
//                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//                    
//                    let user = User.currentUser()
//                    user?.codeKelurahan = ""
//                    appDelegate.saveContext()
//                    
//                    NSNotificationCenter.defaultCenter().postNotificationName(kKelurahanChanged, object: nil)
//                    
//          }) {
//               
//               
//          }
          presentChooseNeighborhood()
     }
//     func diskusiTapped(cell: QlueKelurahanFeedViewCell) {
//          guard let codeKel = kelurahan?.code else{
//               return
//          }
//          presentListForum(withType: .Kelurahan(codeKel: codeKel), nav: self.navigationController)
//     }
//     func laporanTapped(cell: QlueKelurahanFeedViewCell) {
//          presentLaporanKelurahan(self.kelurahan, nav: self.navigationController)
//     }
//     func rankingTapped(cell: QlueKelurahanFeedViewCell) {
//          presentKTrophy(kelurahan, nav: self.navigationController)
//     }
//     func membersTapped(cell: QlueKelurahanFeedViewCell) {
//          presentKelMember(kelurahan, nav: self.navigationController)
//     }
}

// MARK: - QlueForumViewCellDelegate
extension QlueFeedViewController: QlueForumViewCellDelegate {
     func starDidTapped(_ cell: UICollectionViewCell, button: UIButton) {
          
     }
     
     func seenDidTapped(_ cell: UICollectionViewCell, button: UIButton) {
          guard let indexPath = collectionView.indexPath(for: cell) else { return }
               let forum = feeds[indexPath.item]
          
          presentSeenBy(withId: forum.feedId?.description ?? "", countSeen: forum.forumViewCount ?? "0")
     }
}

// MARK: - 
extension QlueFeedViewController: QlueFeedNoContentViewCellDelegate {
     func qlueFeedNoContentViewCellPostButtonTapped(_ cell: QlueFeedNoContentViewCell) {
          
     }
}



