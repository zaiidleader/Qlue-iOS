//
//  QlueRankingInfoController.swift
//  Qlue
//
//  Created by Nurul on 8/16/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

enum RankingInfoType{
    case user
    case goverment
    case business
    
    
    var title: String{
        switch self{
        case .user:
            return "Info Ranking"
        case .goverment:
            return "Neighbourhood Point Info"
        case .business:
            return "Info Ranking"
        }
    }
    
    var description: String{
        switch self{
        case .user:
            return "Qlue user rankings, will be calculated each month. \n \nEach post complaints will get 1 point"
        case .goverment:
            return "The faster the complaint deal with, the higher the points that come by. Weights of complaint is different from one another \n \n \n* Info Ranking will be reset every 3 months."
        case .business:
            return "Ranking swasta dinilai berdasarkan performa perusahaan yang paling banyak dilaporkan"
        }
    }
}

class QlueRankingInfoController: QlueViewController {
    
    @IBOutlet weak var bgInfo: UIView!{
        didSet{
            bgInfo.cornerRadius(10)
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    var infoType: RankingInfoType = .user
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        titleLabel.text = infoType.title
        contentLabel.text = infoType.description
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Action
    
    override func tap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }

}

extension QlueViewController{
    func presentRankingInfo(withType type: RankingInfoType){
        if let info = moreStoryboard.instantiateViewController(withIdentifier: "RankingInfo") as? QlueRankingInfoController{
            
            info.infoType = type
            semiModalWithViewController(info)
            
        }
    }
}
