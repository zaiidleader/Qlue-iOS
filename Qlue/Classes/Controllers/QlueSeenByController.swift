//
//  QlueSeenByController.swift
//  Qlue
//
//  Created by Nurul on 7/30/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class QlueSeenByController: QlueViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
     var id: String = ""
     var members: [QlueMember] = []
     var countSeen: String  = "0"
     var start = 0
     var type = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
          tableView.register(UINib.init(nibName: "QlueMemberItemViewCell", bundle: nil), forCellReuseIdentifier: "MemberCell")
     tableView.rowHeight = 60
     
      titleLabel.text = "Dilihat oleh " + countSeen + " orang"
     
     DejalActivityView.addActivityView(for: self.view)
     tableView.addInfiniteScroll { (tableView) in
          self.loadMore()
     }
     downloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

     // MARK: - Data
     fileprivate func downloadData(){
          let req = Engine.shared.getListSeen(withId: id, start: start, type: type)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               }, completion: { (data, success, fail) in
                    print(data)
                    
                    if success == true{
                         
                         
                         if self.start == 0 {
                              self.members = []
                         }
                         
                         if data.array?.count < 1{
                              self.tableView.setShouldShowInfiniteScrollHandler({ (tableView) -> Bool in
                                   return false
                              })
                         }
                         
                         for d in data.arrayValue{
                              self.members.append(QlueMember.init(fromJson: d))
                         }
                         
                         self.tableView.reloadData()
                    }
               })
     }
     
     // MARK: - Helper
     func loadMore(){
          start = members.count
          downloadData()
     }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
 // MARK: - UITableViewDataSource, UITableViewDelegate
extension QlueSeenByController: UITableViewDataSource, UITableViewDelegate{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return members.count
     }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath) as! QlueMemberItemViewCell
          
          cell.backgroundColor = UIColor.white
     
          let member = members[indexPath.row]
          if let memberUrl = member.avatar?.encodeURL(){
               cell.cellImage.sd_setImage(with: memberUrl as URL!)
          }
          
          cell.cellUsername.text = member.username
          cell.cellFollowButton.isHidden = true
          
          return cell
     }
     
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
          let lastElement = members.count - 1
          if members.count < Int(countSeen){
               if indexPath.row == lastElement{
                    loadMore()
               }
          }
     }
     
     
}

// MARK: - PresentQlueSeenController
extension QlueViewController{
     func presentSeenBy(withId id: String, countSeen: String, type: Int = 1){
          if let seenBy = kelurahanStoryboard.instantiateViewController(withIdentifier: "SeenBy") as? QlueSeenByController{
               seenBy.type = type
               seenBy.id = id
               seenBy.countSeen = countSeen
               semiModalWithViewController(seenBy)
          }
     }
}
