//
//  QlueSearchController.swift
//  Qlue
//
//  Created by Nurul on 6/15/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


enum SearchType: String{
    case People
    case Kelurahan
    case Qlue
    case Forum
    
    
    var headerTitle: String{
        switch self {
        case .People:
            return "People"
        case .Kelurahan:
            return "Kelurahan"
        case .Qlue:
            return "Qlue"
        case .Forum:
            return "Forum"
        }
    }
    
    
}

class QlueSearchController: QlueViewController {
    
     fileprivate let sectionArr : [SearchType] = [
          .People, .Kelurahan, .Qlue, .Forum
     ]
     
     @IBOutlet weak var searchTextfield: UITextField!{
          didSet{
               Util.textfieldPlaceholderWithColor(kQlueBlue, font: nil, textfield: searchTextfield)
          }
     }
     @IBOutlet weak var searchTextfieldWrapper: UIView!{
          didSet{
               searchTextfieldWrapper.cornerRadius(5)
          }
     }
     @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var recentTableView: UITableView!
     
     // MARK: Data Var
     var req: DataRequest?
     var people: [QlueMember] = []
     var peopleTotal: Int = 0
     var kelurahan: [QlueKelurahan] = []
     var kelurahanTotal: Int = 0
     var qlue: [QlueFeed] = []
     var qlueTotal: Int = 0
     var forum: [QlueDiskusi] = []
     var forumTotal: Int = 0
     var searchKey: [SearchKey] = []
     
     // MARK: - View Life Cycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
//          let tap = UITapGestureRecognizer.init(target: self, action: #selector(QlueSearchController.tapHandle(_:)))
//          view.addGestureRecognizer(tap)
          
          tableView.register(UINib.init(nibName: "QlueSearchViewCell", bundle: nil), forCellReuseIdentifier: "SearchCell")
          tableView.rowHeight = 50
          tableView.cornerRadius(5)
          recentTableView.register(UINib.init(nibName: "QlueSearchViewCell", bundle: nil), forCellReuseIdentifier: "SearchCell")
          recentTableView.rowHeight = 50
          recentTableView.cornerRadius(5)
          
          searchTextfield.becomeFirstResponder()
          
          loadLocal()
          // Do any additional setup after loading the view.
     }
     override func viewWillAppear(_ animated: Bool) {
          
          let visualEffect = UIVisualEffectView.init(effect: UIBlurEffect.init(style: .dark))
          visualEffect.frame = view.bounds
          view.insertSubview(visualEffect, at: 0)
          
     }
     // MARK: - Data
     func loadLocal(){
          let appDelegate = UIApplication.shared.delegate as! AppDelegate
          let moc = appDelegate.managedObjectContext
          
          searchKey = SearchKey.getKeys(inManagedObjectContext: moc)
          if searchKey.count > 0{
               recentTableView.isHidden = false
               recentTableView.reloadData()
          }else{
               recentTableView.isHidden = true
          }
     }
     
     // MARK: - Action
     func tapHandle(_ sender: UITapGestureRecognizer){
          dismiss(animated: true, completion: nil)     }
     
    @IBAction func closeTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
     @IBAction func moreTapped(_ sender: UIButton) {
          let alert = UIAlertController.init(title: "More", message:"" , preferredStyle: UIAlertControllerStyle.actionSheet)
          
          let clear = UIAlertAction.init(title: "Clear", style: .default) { (action) in
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               let moc = appDelegate.managedObjectContext
               
               let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "SearchKey")
               do{
                    let items = try moc.fetch(fetchRequest)as! [NSManagedObject]
                    for item in items{
                         moc.delete(item)
                    }
                    
                    appDelegate.saveContext()
               }catch let error as NSError{
                    print("error: \(error.localizedDescription)")
               }
               self.loadLocal()
          }
          alert.addAction(clear)
          
          let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
          alert.addAction(cancel)
          
          present(alert, animated: true, completion: nil)
          
     }
}

// MARK: - Table View
extension QlueSearchController:  UITableViewDataSource, UITableViewDelegate{
     // MARK: - UITableViewDataSource
     func numberOfSections(in tableView: UITableView) -> Int {
          if tableView == recentTableView{
               return 1
          }
          return sectionArr.count
     }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
          if tableView == recentTableView{
               return searchKey.count
          }
          
          let type = sectionArr[section]
          
          switch type {
          case .People:
               return people.count
          case .Kelurahan:
               return kelurahan.count
          case .Qlue:
               return qlue.count
          case .Forum:
               return forum.count
          }
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! QlueSearchViewCell
          
          cell.cellDesc.text = ""
          
          if tableView == recentTableView{
               let key = searchKey[indexPath.row]
               cell.cellName.text = key.searchKey
               return cell
          }
          
          
          let type = sectionArr[indexPath.section]

          switch type {
          case .People:
               let data = people[indexPath.row]
               cell.cellName.text = data.username
            cell.cellDesc.text = data.currLevel
               cell.cellImage.contentMode = UIViewContentMode.scaleAspectFit
               cell.cellImage.cornerRadius(cell.cellImage.bounds.width / 2)
               if let avatar = data.avatar?.encodeURL(){
                    cell.cellImage.sd_setImage(with: avatar as URL!)
               }
          case .Kelurahan:
               let data = kelurahan[indexPath.row]
               cell.cellName.text = data.name
               // TODO: Image kelurahan icon
          case .Qlue:
               let data = qlue[indexPath.row]
               cell.cellName.text = data.qlueTitle
               if let fileUrl = data.fileUrl?.encodeURL(){
                    cell.cellImage.sd_setImage(with: fileUrl as URL!)
               }
          case .Forum:
               let data = forum[indexPath.row]
               cell.cellName.text = data.title
               if let fileUrl = data.file?.encodeURL(){
                    cell.cellImage.sd_setImage(with: fileUrl as URL!)
               }
               
          }
          
          
          return cell
     }
     
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
          
          if tableView == recentTableView{
               return 0
          }
          
          if section == sectionArr.count - 1{
               return 0
          } else {
               return 30
          }
     }
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
          
          return 40
     }
     
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
          let headerView = QlueSearchHeaderView.fromNib()
          
          headerView.seeAllButton.tag = section
          headerView.delegate = self
          headerView.seeAllButton.isHidden = true
          
          if tableView == recentTableView{
               headerView.title.text = "Terbaru"
               return headerView
          }
          
          let type = sectionArr[section]
          headerView.title.text = type.headerTitle
          
          switch type {
          case .People:
               if peopleTotal > 5{
                    headerView.seeAllButton.isHidden = false
               }
          case .Kelurahan:
               if kelurahanTotal > 5{
                    headerView.seeAllButton.isHidden = false
               }
          case .Qlue:
               if qlueTotal > 5{
                    headerView.seeAllButton.isHidden = false
               }
          case .Forum:
               if forumTotal > 5{
                    headerView.seeAllButton.isHidden = false
               }
          }
          
          return headerView
     }
     
     func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
          let view = QlueSearchFooterView.fromNib()
          
          return view
     }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == recentTableView{
            searchTextfield.text = searchKey[indexPath.row].searchKey
            self.tableView.isHidden = false
            recentTableView.isHidden = true
            
            downloadData(searchTextfield.text)
            return
        }
        
          let appDelegate = UIApplication.shared.delegate as! AppDelegate
          let moc = appDelegate.managedObjectContext
          
          SearchKey.searchKeyWithData(searchTextfield.text ?? "", inManagedObjectContext: moc)
          appDelegate.saveContext()
        
        let type = sectionArr[indexPath.section]
        switch type {
        case .People:
          let data = people[indexPath.row]
          presentOtherProfile(data.userId)
            return
        case .Kelurahan:
            let kel = kelurahan[indexPath.row]
            print(kel)
            presentKelurahan(withId: kel.code, district: kel.provinsi, nav: self.navigationController)
        case .Qlue:
            let data = qlue[indexPath.row]
            presentDetailFeed(withFeedId: data.feedId, nav: self.navigationController)
        case .Forum:
            let data = forum[indexPath.row]
            presentForumDetail(forumId: data.id ?? "", forumType: .general)
        }
        
     }
}


// MARK: - Search Textfield
extension QlueSearchController: UITextFieldDelegate{
     
     func downloadData(_ query: String?){
          req = Engine.shared.searchAll(withQuery: query ?? "")
          Engine.shared.parseJsonWithReq(req!, controller: self, loadingStop: {
               DejalActivityView.remove()
               }, completion: { (data, success, fail) in
                    if success == true{
                         
                         var totalRes = data["total_result"]
                         self.peopleTotal = totalRes["people"].intValue
                         self.people = []
                         for data in data["people"].arrayValue{
                              self.people.append(QlueMember.init(fromJson: data))
                         }
                         
                         self.kelurahanTotal = totalRes["kelurahan"].intValue
                         self.kelurahan = []
                         for data in data["kelurahan"].arrayValue{
                              self.kelurahan.append(QlueKelurahan.init(fromJson: data))
                         }
                         
                         self.qlueTotal = totalRes["qlue"].intValue
                         self.qlue = []
                         for data in data["qlue"].arrayValue{
                              self.qlue.append(QlueFeed.feedWithData(data))
                         }
                         
                         self.forumTotal = totalRes["forum"].intValue
                         self.forum = []
                         for data in data["forum"].arrayValue{
                              self.forum.append(QlueDiskusi.init(fromJson: data))
                         }
                         
                         self.tableView.reloadData()
                         
                    }
          })
     }
     
     @IBAction func searchDidChange(_ sender: UITextField) {
          
          req?.cancel()
          DejalActivityView.addActivityView(for: tableView)
          // if text > 0, show tableView
          if sender.text?.characters.count  > 0{
               tableView.isHidden = false
            recentTableView.isHidden = true
          } else {
               tableView.isHidden = true
            if searchKey.count > 0{
               loadLocal()
            }
               return
          }
          
          downloadData(sender.text)
          
     }
     
}

// MARK: - QlueSearchHeaderViewDelegate
extension QlueSearchController: QlueSearchHeaderViewDelegate{
     func searchAllTapped(_ cell: QlueSearchHeaderView) {
          let section = cell.seeAllButton.tag
          presentSearchDetail(sectionArr[section], searchKey: self.searchTextfield.text!)
     }
}
