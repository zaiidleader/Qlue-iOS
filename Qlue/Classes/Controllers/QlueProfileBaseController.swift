//
//  QlueProfileBaseController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/10/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON

class QlueProfileBaseController: QlueViewController{

     var userId: String?
     var user : QlueOtherUser?
     
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     // MARK: - Setup
     func setupFab(){
          let screenBound = UIScreen.main.bounds
          let button = UIButton.init(frame: CGRect(x: screenBound.width - 60, y: screenBound.height - 165, width: 50, height: 50))
          button.backgroundColor = kQlueBlue
          button.setImage(UIImage.init(named: "pencil-edit-button"), for: UIControlState())
          button.clipsToBounds = true
          button.layer.cornerRadius = 25
          button.shadowWithColor(UIColor.darkGray)
          
          button.addTarget(self, action: #selector(QlueProfileBaseController.startChat(_:)), for: .touchUpInside)
          
          view.addSubview(button)
          
     }

     // MARK: - Action
     func startChat(_ sender: UIButton){
          guard let userId = userId, let currentUserId = User.currentUser()?.userId else{
               return
          }
          
          
          let memberDict = [
               "avatar" : user?.avatar ?? "",
               "follow" : user?.follow ?? false,
               "user_id" : userId,
               "username" : user?.username ?? "",
               "isBlocked": user?.isBlocked ?? false,
               "curr_level": user?.currLevel ?? ""
          ] as [String : Any]
          let member = QlueMember.init(fromJson: JSON(memberDict))
          
          presentChatDetail(withGroupId: userId + "," + currentUserId, isGroup: false, members: [member])
     }
     
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
