//
//  QlueAvatarController.swift
//  Qlue
//
//  Created by Nurul on 7/1/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class QlueAvatarController: QlueViewController {

     var isPublic: Bool = true
     
     var publics: [QlueAvatar] = []
     var staffs: [QlueAvatar] = []
    var publicSelected: Int?
    var staffSelected: Int?
     
     var userAvatar: UIImage?
    
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
     @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bgAvatar: UIView!{
        didSet{
            bgAvatar.cornerRadiusWithShadow()
        }
    }
    
    // MARK: - View Lifecycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          // Do any additional setup after loading the view.
        
        if let avaUrl = User.currentUser()?.avatar?.encodeURL(){
            avatarImage.sd_setImage(with: avaUrl as URL!, completed: { (image, _, _, _) in
               self.userAvatar = image
            })
          
        }
        
        if isPublic == true{
            titleLabel.text = "My Avatar"
        } else{
            titleLabel.text = "Staff"
        }
        
          DejalActivityView.addActivityView(for: collectionView)
          downloadData()
          collectionView.register(UINib.init(nibName: "QlueAvatarViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Configure
    
    // MARK: - Action
    
    @IBAction func simpanTapped(_ sender: UIButton) {
        var data: QlueAvatar?
        if let index = publicSelected, isPublic == true{
            data = publics[index]
        } else if let index = staffSelected, isPublic == false{
            data = staffs[index]
        }
     
     if let avatar = data?.avatar{
          DejalBezelActivityView.addActivityView(for: self.collectionView)
          let req = Engine.shared.changeAvatar(avatar)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               }, completion: { (data, success, fail) in
                    if success == true{
                         User.currentUser()?.avatar = avatar
                         let appDelegate = UIApplication.shared.delegate as! AppDelegate
                         appDelegate.saveContext()
                         self.navigationController?.popViewController(animated: true)
                    }
          })
     }
        
        
    }
     
     // MARK: - Data
     func downloadData(){
          let req = Engine.shared.listAvatarCurrentUser(avatar: User.currentUser()?.avatar ?? "")
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               DejalActivityView.remove()
               
               }, completion:  { (data, success, fail) in
                    if success == true{
                         var avaArr: [QlueAvatar] = []
                         for d in data.arrayValue{
                              avaArr.append(QlueAvatar.avatarWithData(d))
                         }
                         
                         self.publics = avaArr.filter{$0.isGov == false}
                         self.staffs = avaArr.filter{$0.isGov == true}
                         
                         self.collectionView.reloadData()
                         
                    }
          })
     }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - CollectionViewController
extension QlueAvatarController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isPublic == true{
            return publics.count
        } else{
            return staffs.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! QlueAvatarViewCell
        
        var data: QlueAvatar!
        if isPublic == true{
            data = publics[indexPath.item]
        } else{
            data = staffs[indexPath.item]
        }
     
     
     if Int(User.currentUser()?.level ?? "0") < Int(data.level ?? "0"){
          cell.cellBgLock.isHidden = false
          let grade = data.grade ?? ""
          cell.cellLockLabel.text = "Unlocked at " + grade
     } else{
          cell.cellBgLock.isHidden = true
     }
     
     if let url = data.avatar?.encodeURL(){
          cell.cellImage.sd_setImage(with: url as URL!)
     }
     
     cell.cellLabel.text = data.type
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var data: QlueAvatar!
        if isPublic == true{
            data = publics[indexPath.item]
        } else{
            data = staffs[indexPath.item]
        }

        let cell = collectionView.cellForItem(at: indexPath) as? QlueAvatarViewCell
        
        if Int(User.currentUser()?.level ?? "0") < Int(data.level ?? "0"){
            presentAvatarPopUp(data.type, avaImage: cell?.cellImage.image)
          avatarImage.image = userAvatar
            if isPublic == true{
                publicSelected = nil
            } else{
                staffSelected = nil
            }
        } else{
          cell?.checklistImage.isHidden = false
            avatarImage.image = cell?.cellImage.image
            if isPublic == true{
                publicSelected = indexPath.item
            } else{
                staffSelected = indexPath.item
            }
        }
    }
     
     func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
          var data: QlueAvatar!
          if isPublic == true{
               data = publics[indexPath.item]
          } else{
               data = staffs[indexPath.item]
          }
          
          let cell = collectionView.cellForItem(at: indexPath) as? QlueAvatarViewCell
          
          if Int(User.currentUser()?.level ?? "0") >= Int(data.level ?? "0"){
              cell?.checklistImage.isHidden = true
          }
     }
    
    // MARK: UICollectionViewDelegateFlowLayout
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          let size: CGFloat = (UIScreen.main.bounds.width / 3) + 0.3
          
          return CGSize(width: size, height: size)
          
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return -2
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 0
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsets.zero
     }
}
