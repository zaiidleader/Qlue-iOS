//
//  QlueCommentController.swift
//  Qlue
//
//  Created by Nurul on 6/24/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueCommentController: QlueViewController {
    
    @IBOutlet weak var commentTextView: UITextView!
     var feedId: String = ""
     var page  = 0
     var comments: [QlueComment] = []
     @IBOutlet weak var tableView: UITableView!
     
    @IBOutlet weak var commentTextViewHeight: NSLayoutConstraint!
     var currentHeight: CGFloat = 50
     var heightBefore: CGFloat = 50
     
    @IBOutlet weak var mentionView: UIView!
     var mentionTableView: MentionTableView!
     var rangeMention : UITextRange?
     var indexMention : String.CharacterView.Index?
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          tableView.estimatedRowHeight = 50
          tableView.rowHeight = UITableViewAutomaticDimension
          tableView.register(UINib.init(nibName: "CommentDetailViewCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
          
          
          DejalActivityView.addActivityView(for: self.view)
          downloadData()
        configureTextView()
          setupMentionView()
          
          
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     func initWithFeedId(_ id: String){
          self.feedId = id
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Configure
    func configureTextView(){
     
     commentTextView.delegate = self
        commentTextView.textContainerInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
    }
     func setupMentionView(){
          tap.delegate = self
          
          mentionTableView = Bundle.main.loadNibNamed("MentionTableView", owner: nil, options: nil)?.first as! MentionTableView
        
          mentionView.layoutIfNeeded()
          mentionTableView.frame = mentionView.bounds
          mentionTableView.mentionDelegate = self
          
          mentionView.addSubview(mentionTableView)
          
          mentionView.isHidden = true
     }
     
     // MARK: - Helper
     func downloadData(){
          
          let req = Engine.shared.getCommentList(feedId, page: 0)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
          }, completion:  { (data, success, fail) in
               
               if self.page == 0{
                    self.comments = []
               }
               
               if success == true{
                    if let comments = data.array{
                         for comment in comments{
                              self.comments.append(QlueComment.commentWithData(comment))
                         }
                    }
               }
               
               self.tableView.reloadData()
          })
          
     }
    
    // MARK: - Action
    
    @IBAction func kirimTapped(_ sender: UIButton) {
        
        let req = Engine.shared.postComment(feedId, comment: commentTextView.text)
        Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
            
            },completion:  { (data, success, fail) in
                if success == true{
                    self.downloadData()
                    self.commentTextView.text = ""
                }
        })
        
    }
     
}

// MARK: - UITableViewCellDataSource
extension QlueCommentController: UITableViewDataSource{
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return comments.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentDetailViewCell
          let comment = comments[indexPath.row]
          
          if let image = comment.avatar?.encodeURL(){
               cell.cellImage.sd_setImage(with: image as URL!)
          }
          cell.cellUsername.text  = comment.username
          
          cell.cellTime.text = comment.timestamp?.timeAgoWithNumericDates(true)
          0
          
          let  mentionText = Util.replaceIdWithUsername(comment)
          cell.cellComment.text = Util.convertEmojiFromUnicode(mentionText)
          
          cell.cellComment.handleMentionTap { (username) in
               let atSelected = comment.at.filter({$0.username == username}).first
               self.presentOtherProfile(atSelected?.idno)
          }
          
          return cell
     }
}

// MARK: - UITableViewCellDelegate
extension QlueCommentController: UITableViewDelegate{
     
//     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//          return 55
//     }
     
}


// MARK: - UITextViewDelegate
extension QlueCommentController: UITextViewDelegate{
     func textViewDidChange(_ textView: UITextView) {
          let height = textView.contentSize.height
          
          if height == currentHeight || height < 15{
               return
          }
        
        if height > 120{
            heightBefore = currentHeight
            UIView.animate(withDuration: 0.5, animations: {
                self.commentTextViewHeight.constant = 120
            })
            currentHeight = 120
            return
        }
        
          if height > currentHeight{
               heightBefore = currentHeight
               UIView.animate(withDuration: 0.5, animations: {
                    self.commentTextViewHeight.constant = height
               })
               currentHeight = height
               return
          }
          
          if height < currentHeight{
               UIView.animate(withDuration: 0.5, animations: {
                    self.commentTextViewHeight.constant = self.heightBefore
               })
               
               currentHeight = height
          }
          
     }
     
     func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
          
          if text == "@"{
               mentionView.isHidden = false
               indexMention = textView.text.endIndex
//               rangeMention = textView.textRangeFromPosition(<#T##fromPosition: UITextPosition##UITextPosition#>, toPosition: UITextPosition)
               mentionTableView.reloadTableWithMention("", isStart: true)
               
          }
          
          if text == " "{
               indexMention = nil
               mentionView.isHidden = true
          }
          if let index = indexMention{
               let word = (textView.text + text ).substring(from: index)
               mentionTableView.reloadTableWithMention(word)
               print(word)
               
          }
          return true
     }


}

// MARK: - MentionTableViewDelegate
extension QlueCommentController: MentionTableViewDelegate{
     func mentionDidSelect(_ view: MentionTableView, text: String) {
          guard let index = indexMention else { return }
          
          let removeText = commentTextView.text.substring(from: index)
          print("uu" + removeText)
          
          let dropStr = String(commentTextView.text.characters.dropLast(removeText.characters.count))
          let newStr = dropStr + "@" + text
          
          commentTextView.text = newStr
          mentionView.isHidden = true
          indexMention = nil
//          commentTextView.replaceRange(range, withText: text)
//          commentTextView.text.string
     }
}
 // MARK: - UIGestureRecognizerDelegate
extension QlueCommentController: UIGestureRecognizerDelegate{
          func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
               if touch.view?.isDescendant(of: mentionTableView) == true{
                    return false
               }
               
               return true
          }
}

// MARK: - Navigation
extension UIViewController {
     
     func presentComment(withId id: String, nav: UINavigationController?){
          if let comment = mainStoryboard.instantiateViewController(withIdentifier: "Comment") as? QlueCommentController{
               comment.feedId = id
               
               if let nav = nav{
                    nav.pushViewController(comment, animated: true)
                    
                    return
               }
               present(comment, animated: true, completion: nil)
          }
     }
}
