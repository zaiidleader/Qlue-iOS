//
//  RegistrationEmailController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/20/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class RegistrationEmailController: RegistrationController {

    @IBOutlet weak var emailTextfield: UITextField!
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NextSegue"{
            Engine.shared.createUser = QlueRegistration.init()
            Engine.shared.createUser.email = emailTextfield.text
        }
    }
     
     override func nextTapped(_ sender: UIButton) {
        guard let email = emailTextfield.text, email != "" else{
            Util.showSimpleAlert("Please enter your email address.", vc: self, completion: nil)
            return
        }
          if Util.validateEmail(emailTextfield.text) == false{
               Util.showSimpleAlert("Email address is not valid", vc: self, completion: { (action) in
                    self.emailTextfield.becomeFirstResponder()
               })
               return
               
          } else{
               performSegue(withIdentifier: "NextSegue", sender: self)
          }
     }
    

}
