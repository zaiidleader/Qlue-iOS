//
//  QlueOPDetailController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol OPDetailDelegate{
     func downloadDetailProfileFinished(_ isBlock: Bool?, user: QlueOtherUser?)
}

class QlueOPDetailController: QlueProfileBaseController {
     
     var delegate: OPDetailDelegate?
     
     // MARK: - Outlets
     @IBOutlet weak var bannerImage: UIImageView!
     @IBOutlet weak var avatarImage: UIImageView!{
          didSet{
               avatarImage.cornerRadiusWithShadow()
          }
     }
    @IBOutlet weak var followersButton: UIButton!
     @IBOutlet weak var joinDate: UILabel!
     @IBOutlet weak var fullnameLabel: UILabel!
     @IBOutlet weak var levelLabel: UILabel!
    
     @IBOutlet weak var followButton: UIButton!{
          didSet{
               followButton.cornerRadiusWithShadow()
          }
     }
     
     @IBOutlet weak var nameIcLabel: UILabel!
     @IBOutlet weak var kelurahanButton: UIButton!
     // MARK: - Private Vars
     
     // MARK: - View Life Cycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          DejalBezelActivityView.addActivityView(for: self.view)
        downloadData()
          setupFab()
          // Do any additional setup after loading the view.
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Private Func
    
     fileprivate func setupView(){
          if let banner = user?.banner?.encodeURL(){
               bannerImage.sd_setImage(with: banner as URL!)
          }
          followersButton.setTitle("0", for: UIControlState())
          if let follower = user?.followerCount{
               followersButton.setTitle(String(follower), for: UIControlState())
          }
     
          if let ava = user?.avatar?.encodeURL(){
               avatarImage.sd_setImage(with: ava as URL!)
          }
          
          fullnameLabel.text = user?.fullName
          levelLabel.text = user?.currLevel
          joinDate.text = user?.timeJoin?.formatShortStyle()

          kelurahanButton.setTitle(user?.kdKel, for: UIControlState())
        nameIcLabel.text = user?.username
          
          setupFollowButton(user?.follow)

          
     }
     
     fileprivate func setupFollowButton(_ isFollow: Bool?){
     
          let follow = isFollow ?? false
          
          if follow == true{
               followButton.setTitle("Unfollow", for: UIControlState())
               followButton.setTitleColor(UIColor.white, for: UIControlState())
               followButton.backgroundColor = kRedColor
          } else{
               followButton.setTitle("+Follow", for: UIControlState())
               followButton.setTitleColor(kQlueBlue, for: UIControlState())
               followButton.backgroundColor = UIColor.white
          }
          
     }
     
     // MARK: - Data
     func downloadData(){
          
          
          guard let userId = userId else {
               DejalActivityView.remove()
               return
          }
          
          let req = Engine.shared.getDetailProfile(userId: userId)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               },completion:  { (data, success, fail) in
                    if success == true{
                         self.user = QlueOtherUser.init(fromJson: data.arrayValue.first)
                         
                         if let delegate = self.delegate{
                              delegate.downloadDetailProfileFinished(self.user?.isBlocked, user: self.user)
                         }
                         
                         self.setupView()
                    }
          })
     }
    
    // MARK: - Action
    @IBAction func followTapped(_ sender: AnyObject) {
     guard let userId = userId else { return }
     let req = Engine.shared.followMember(withId: userId)
     Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
          
          }, completion:  { (data, success, fail) in
               if success == true{
                    
                   self.setupFollowButton(true)
               }
          }, falseCompletion: { Void in
               
               self.setupFollowButton(false)
     })
    }
     
    @IBAction func followersTapped(_ sender: UIButton) {
     
        presentListFollow(self.userId)
        
    }
    @IBAction func kelurahanTapped(_ sender: UIButton) {
        
        presentKelurahan(withId: user?.kdKel, district: user?.district, nav: self.navigationController)
    }
}
