//
//  QlueLogInEmailViewController.swift
//  Qlue
//
//  Created by Nurul on 5/26/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON

class QlueLogInEmailViewController: QlueScrollViewController, QlueSegueHandlerType {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!{
        didSet{
            loginBtn.layer.cornerRadius = 5
            loginBtn.layer.masksToBounds = false
            
            loginBtn.layer.shadowColor = UIColor.darkGray.cgColor
            loginBtn.layer.shadowOpacity = 0.3
            loginBtn.layer.shadowRadius = 2
            loginBtn.layer.shadowOffset = CGSize(width: 1, height: 2)
        }
    }
    
    @IBOutlet weak var loadingLogin: UIActivityIndicatorView!
    var visibleState: Bool = false
    
    enum SegueIdentifier: String {
        case HomeSegue
        case TipsSegue
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //textfield
        let textFields = [usernameTextField, passwordTextField]
        for tf in textFields{
            setupTextfield(tf!)
        }
        
    }
     
     override func viewWillAppear(_ animated: Bool) {
          
          // Custom Navigation Bar
         self.navigationController?.navigationBar.barTintColor = kBgNavBar
          self.navigationController?.navigationBar.tintColor = kCreamColor
          self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "bgNavbar"), for: .default)
          self.navigationController?.navigationBar.isTranslucent = false
          
          
          self.navigationController?.navigationBar.shadowImage = UIImage()
          self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:kCreamColor, NSFontAttributeName:UIFont.init(name: QlueFont.ProximaNovaSoft, size: 16)!]

     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func resetPasswordTapped(_ sender: UIButton) {
        if let forgotPassword = storyboard?.instantiateViewController(withIdentifier: "QlueForgotPassword") as? QlueForgotPasswordViewController{
            semiModalWithViewController(forgotPassword)
        }
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        login()
    }
    @IBAction func visiblePasswordTapped(_ sender: UIButton) {
        
        passwordTextField.isSecureTextEntry = visibleState
        
        visibleState = !visibleState
        
    }
    // MARK: - Helpers
    fileprivate func setupTextfield(_ tf: UITextField){
        tf.textColor = UIColor.white
        tf.font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 16)
        
        // placeholder
        if let pc = tf.placeholder, let font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 16){
            tf.attributedPlaceholder = NSAttributedString(string: pc, attributes: [NSFontAttributeName : font,
                NSForegroundColorAttributeName: kDarkBlue])
        }
        
    }
    
    fileprivate func login(){
        guard let uid = usernameTextField.text, let pwd = passwordTextField.text?.md5 else { return  }
        
        // (uid, pwd: pwd)
        loginBtn.isEnabled = false
        loginBtn.setTitle("", for: UIControlState())
        loadingLogin.startAnimating()
        Engine.shared.loginWithEmail(uid, pwd: pwd){ (result, error) in
            
            
            self.loadingLogin.stopAnimating()
            self.loginBtn.isEnabled = true
            self.loginBtn.setTitle("LOGIN", for: UIControlState())
            
            if let res = result as? String{
                
                let data = JSON.parse(res)
                
                if data["success"].string == "false"{
                    Util.showSimpleAlert(data["message"].string, vc: self, completion: nil)
                } else{
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.registerForRemoteNotifications()
                    
                    self.performSegue(withIdentifier: "TipsSegue", sender: self)
                    
                }
                
            } else if let error = error{
                print(error.localizedDescription)
            } else{}

        }
        
    }
    
    
}
