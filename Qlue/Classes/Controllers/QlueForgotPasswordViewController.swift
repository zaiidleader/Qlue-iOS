//
//  QlueForgotPasswordViewController.swift
//  Qlue
//
//  Created by Nurul on 5/25/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON

class QlueForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    // MARK: - Outlets
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cancelButton.cornerRadius(10.0)
        okButton.cornerRadius(10.0)
        // Do any additional setup after loading the view.
        bgView.layer.cornerRadius = 20.0
        bgView.layer.masksToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func bgTapped(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okTapped(_ sender: UIButton) {
        guard let email = emailTextField.text, email != "" else{ return }
        
        Engine.shared.forgotPassword(email) { (result, error) in
            if let res = result as? String{
                
                let json = JSON.parse(res)
                if json["success"].string == "fail"{
                    Util.showSimpleAlert(json["message"].string, vc: self, completion: nil)
                } else{
                    
                    Util.showSimpleAlert(json["message"].string, vc: self, completion: {(action) in
                        
                        self.dismiss(animated: true, completion: nil)
                    })
                    
                }
                
                
            } else if let error = error{
                print(error.localizedDescription)
            }else{
                
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
