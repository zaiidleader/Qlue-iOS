//
//  QluePlaceCategoryController.swift
//  Qlue
//
//  Created by Nurul on 7/20/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreLocation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class QluePlaceCategoryController: QlueViewController {
     
     fileprivate var placeName: String?
     var address: String?
     @IBOutlet weak var label: UILabel!
     @IBOutlet weak var collectionView: UICollectionView!
     var categories : [QlueTopic] = []
     
     var saveCompletion: () -> () = {}
     
     var location: CLLocation?
     
     // MARK: - View Lifecycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          collectionView.register(UINib.init(nibName: "QlueSelectTopicViewCell", bundle: nil), forCellWithReuseIdentifier: "TopicCell")
          setLayoutCollection()
          DejalActivityView.addActivityView(for: self.view)
          downloadData()
          if let name = placeName{
               label.text = "Help us know type of \" \(name) \" location is"
          }
          
          setCurrentLocation()
          
     }
     
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Location
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               self.location = location
               CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                    if placemarks?.count > 0{
                         
                         let pl = placemarks![0]
                         var addressComb = pl.thoroughfare ?? ", "
                         addressComb += pl.subLocality ?? ", "
                         addressComb += pl.locality ?? ", "
                         
                         self.address = addressComb
                    }
               })
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QluePlaceCategoryController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
               
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }
     
     // MARK: - Helpers
     func initWithPlaceName(_ place: String?){
          placeName = place
     }
     func setLayoutCollection(){
          
          collectionView.layoutIfNeeded()
          view.setNeedsLayout()
          
          let layout = UICollectionViewFlowLayout()
          let size: CGFloat = ((collectionView.bounds.width - 45) / 3 )
          layout.itemSize  = CGSize(width: 80, height: size + 16 + 20)
          layout.minimumLineSpacing = 10
          layout.minimumInteritemSpacing = 10
          layout.sectionInset = UIEdgeInsetsMake(15, 15, 15, 15)
          
          collectionView.setCollectionViewLayout(layout, animated: true)
          
     }
     
     fileprivate func downloadData(){
          
          
          categories = []
          
          let req =  Engine.shared.getCategoryPlace()
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
          },completion:  { (data, success, fail) in
               if success == true{
                    
                    if let group = data["group"].array{
                         for topic in group{
                              self.categories.append(QlueTopic.topicWithData(topic))
                              
                         }
                    }
                    
                    self.collectionView.reloadData()
                    
               }
               
          })
          
          
     }
     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
      }
      */
     
}

// MARK: - UICollectionViewDataSource
extension QluePlaceCategoryController: UICollectionViewDataSource{
     func numberOfSections(in collectionView: UICollectionView) -> Int {
          return 1
     }
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return categories.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopicCell", for: indexPath) as! QlueSelectTopicViewCell
          
          cell.layer.shouldRasterize = true
          cell.layer.rasterizationScale = UIScreen.main.scale
          
          
          let topic = categories[indexPath.item]
          
          cell.cellTitle.text = topic.suggestName
          
          if let imageStr = topic.suggestIcon, let image = PlaceMark(rawValue: imageStr){
               cell.cellImage.image = image.image
          }
          
          
          return cell
     }
     
}



// MARK: - UICollectionViewDelegate
extension QluePlaceCategoryController: UICollectionViewDelegate{
     
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          guard let loc = location else{ return }
          
          let topic = categories[indexPath.item]
          DejalBezelActivityView.addActivityView(for: collectionView)
                   let req = Engine.shared.postPlaceSwasta(loc.coordinate.latitude, lng: loc.coordinate.longitude, title: placeName ?? "", category: topic.suggestIcon ?? "", address: address ?? "")
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalBezelActivityView.remove()
               },completion:  { (data, success, fail) in
                    if success == true{
                         let poi = QluePoi.poiWithData(data)
                         poi.name = self.placeName
                         Engine.shared.currentReport?.poi = poi
                         self.dismiss(animated: true, completion: {
                              
                              self.saveCompletion()
                         })
                    }
          })
          
          //          performSegueWithIdentifier("Post", sender: self)
     }
     
}

// MARK: - Navigation
extension UIViewController{
     func presentPlaceCategory(_ placeName: String?, completion: @escaping () -> ()){
          if let cat = reportStoryboard.instantiateViewController(withIdentifier: "CategoryPlace") as? QluePlaceCategoryController{
               cat.saveCompletion  = completion
               cat.initWithPlaceName(placeName)
               
               present(cat, animated: true, completion: nil)
          }
     }
}


