//
//  QlueProfileForumController.swift
//  Qlue
//
//  Created by Nurul on 7/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueProfileForumController: QlueProfileBaseController {
     
     // MARK: - Outlets
     @IBOutlet weak var collectionView: UICollectionView!
     var diskusi: [QlueDiskusi] = []
     var page = 0
     var selectedForum: QlueDiskusi?
     
     override func viewDidLoad() {
          super.viewDidLoad()
          configureCollectionView()
          observeNotification()
          
          DejalActivityView.addActivityView(for: collectionView)
          downloadData()
          if userId != User.currentUser()?.userId{
          
               setupFab()
          }
          
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     // MARK: - Configure
     func observeNotification(){
          NotificationCenter.default.addObserver(self, selector: #selector(QlueProfileForumController.downloadData), name: NSNotification.Name(rawValue: kForumCreated), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(QlueProfileForumController.downloadData), name: NSNotification.Name(rawValue: kForumEdited), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(QlueProfileForumController.forumDeleted(_:)), name: NSNotification.Name(rawValue: kForumDeleted), object: nil)
     }
     
     func configureCollectionView(){
          
          collectionView.register(UINib.init(nibName: "QlueForumViewCell", bundle: nil), forCellWithReuseIdentifier: "ForumCell")
     
          collectionView.addInfiniteScroll { (collectionView) in
               if self.diskusi.count >= (self.page + 1) * 25{
                    self.loadMore()
               } else{
                    self.collectionView.finishInfiniteScroll()
               }
          }
     }
     
     // MARK: - Helper
     func forumDeleted(_ sender: Notification){
          if let id = sender.userInfo?["id"] as? String{
               if let i = diskusi.index(where: {$0.id == id}) {
                    self.diskusi.remove(at: i)
                    self.collectionView.reloadData()
               }
          }
     }
     
     func downloadData(){
          
          guard let userId = userId else { return }
          
          let start = page * 25
          let finish = (page + 1) * 25
          
          let req = Engine.shared.listForumUser(userId: userId, start: start, finish: finish)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               self.collectionView.finishInfiniteScroll()
               },completion:  { (data, success, fail) in
                    
                    if self.page == 0{ self.diskusi = [] }
                    
                    if success == true{
                         for d in data.arrayValue{
                              self.diskusi.append(QlueDiskusi.init(fromJson: d))
                         }
                    }
                    
                    self.collectionView.reloadData()
          })
          
     }
     
     func loadMore(){
          page += 1
          downloadData()
     }

     
     
     
}

// MARK: - Collection View
extension QlueProfileForumController: UICollectionViewDataSource{
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return diskusi.count
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForumCell", for: indexPath) as! QlueForumViewCell
          
          cell.cornerRadius(5)
          cell.border(1, color: kGreyEEE)
          
          let data =  diskusi[indexPath.row]
          if data.isFav != true{
               cell.cellStar.setImage(UIImage.init(named: "ic_staroff"), for: UIControlState())
          }else{
               cell.cellStar.setImage(UIImage.init(named: "ic_staron"), for: UIControlState())
          }
          
          if data.favable != true{
               cell.cellStarWidth.constant = 0
          } else{
               cell.cellStarWidth.constant = 32
          }
          
          if (data.isVote == true) && (data.isFav == false){
               cell.indicatorView.backgroundColor = kDarkColor
          } else if data.isFav == true{
               cell.indicatorView.backgroundColor = kCommunityForumIndicatorColor
          } else{
               cell.indicatorView.backgroundColor = kQlueBlue
          }
          
          
          cell.cellTitle.text = data.title
          if let imgUrl = data.file?.encodeURL(){
               cell.cellImage.sd_setImage(with: imgUrl as URL!)
          }
          let timestamp = Date().getDateFromTimestamp(data.timestamp)
          cell.cellDate.text = timestamp?.formatDate(withStyle: DateFormatter.Style.full)
          cell.cellTime.text = timestamp?.formatTime(withStyle: DateFormatter.Style.short)
          cell.cellUsername.setTitle(data.username, for: UIControlState())
          cell.cellSeen.setTitle(data.viewCount, for: UIControlState())
          cell.cellComment.setTitle(data.commentCount, for: UIControlState())
          cell.delegate = self
          
          return cell
     }
}

// MARK: - UICollectionViewDelegate
extension QlueProfileForumController: UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          selectedForum =  diskusi[indexPath.row]
          presentForumDetail(forumId: selectedForum?.id ?? "", forumType: ForumType.general)
     }
     
     
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueProfileForumController: UICollectionViewDelegateFlowLayout{
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          return CGSize(width: collectionView.bounds.width - 24, height: 120)
          
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return 8
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 8
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsetsMake(20, 12, 0, 12)
     }
    
     
}

// MARK: - QlueForumViewCellDelegate
extension QlueProfileForumController: QlueForumViewCellDelegate{
     func starDidTapped(_ cell: UICollectionViewCell, button: UIButton) {
          guard let indexPath = collectionView.indexPath(for: cell)
               else { return }
          
          let id = diskusi[indexPath.row].id
          let req = Engine.shared.favoriteDiskusi(withId: id)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion: { (data, success, fail) in
                    if success == true{
                         
                         self.diskusi[indexPath.row].isFav = true
                         
                         button.setImage(UIImage.init(named: "ic_staron"), for: .normal)
                    }
          }) {
               
               self.diskusi[indexPath.row].isFav = false
               button.setImage(UIImage.init(named: "ic_staroff"), for: .normal)
          }
     }
     
     func seenDidTapped(_ cell: UICollectionViewCell, button: UIButton) {
          guard let indexPath = collectionView.indexPath(for: cell) 
               else { return }
          let forum = diskusi[indexPath.row]
          presentSeenBy(withId: forum.id ?? "", countSeen: forum.viewCount ?? "0")
     }
}
