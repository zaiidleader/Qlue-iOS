//
//  QlueSearchDetailController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreLocation

class QlueSearchDetailController: QlueViewController {
     
     @IBOutlet weak var searchKeyLabel: UILabel!
     @IBOutlet weak var tableView: UITableView!
     @IBOutlet weak var titleLabel: UILabel!
     
     var type: SearchType = .Kelurahan
     var searchKey: String = ""
     
     var start = 0
     var finish = 15
     
     var location: CLLocation?
     
     // MARK: Data
     var people: [QlueMember] = []
     var kelurahan: [QlueKelurahan] = []
     var qlue: [QlueFeed] = []
     var forum: [QlueDiskusi] = []
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          // Do any additional setup after loading the view.
          configureTableView()
          configureView()
          downloadData()
          
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Configure
     func configureTableView(){
          tableView.rowHeight = 55
          tableView.addInfiniteScroll { (tableView) in
               self.loadMore()
          }
     }
     func configureView(){
          titleLabel.text = type.headerTitle
          searchKeyLabel.text = "Search \"\(searchKey)\""
     }
     
     // MARK: - Location
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               self.location = location
               
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QlueSearchDetailController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
          }
     }
     
     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }
     
     // MARK: - Data
     func loadMore(){
          start = finish
          finish += 15
          downloadData()
     }
     func downloadData(){
          if start == 0{
               DejalActivityView.addActivityView(for: self.tableView)
          }
          
          switch type {
          case .Forum:
               downloadForum()
          case .Kelurahan:
               downloadKelurahan()
          case .Qlue:
               downloadQlue()
          case .People:
               downloadPeople()
          }
     }
     
     func downloadForum(){
          let req = Engine.shared.searchForum(searchKey, start: start, finish: finish)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               self.tableView.finishInfiniteScroll()
               DejalActivityView.remove()
               }, completion: { (data, success, fail) in
                    if success == true{
                         if self.start == 0{
                              self.forum = []
                         }
                         for f in data.arrayValue{
                              self.forum.append(QlueDiskusi.init(fromJson: f))
                         }
                         self.tableView.reloadData()
                    }
          })
     }
     
     func downloadKelurahan(){
          let req = Engine.shared.searchKelurahan(start, finish: finish, query: searchKey, lat: location?.coordinate.latitude ?? 0, lng: location?.coordinate.longitude ?? 0)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               self.tableView.finishInfiniteScroll()
               DejalActivityView.remove()
               }, completion: { (data, success, fail) in
                    if success == true{
                         if self.start == 0{
                              self.kelurahan = []
                         }
                         for d in data.arrayValue{
                              self.kelurahan.append(QlueKelurahan.init(fromJson: d))
                         }
                         self.tableView.reloadData()
                    }
          })
          
     }
     
     func downloadQlue(){
          let req = Engine.shared.searchQlue(searchKey, start: start, finish: finish)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               self.tableView.finishInfiniteScroll()
               DejalActivityView.remove()
               }, completion: { (data, success, fail) in
                    if success == true{
                         if self.start == 0{
                              self.qlue = []
                         }
                         for d in data.arrayValue{
                              self.qlue.append(QlueFeed.feedWithData(d))
                         }
                         self.tableView.reloadData()
                    }
          })
     }
     
     func downloadPeople(){
          let req = Engine.shared.searchUsername(searchKey, start: start, finish: finish)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               self.tableView.finishInfiniteScroll()
               DejalActivityView.remove()
               }, completion: { (data, success, fail) in
                    if success == true{
                         if self.start == 0{
                              self.people = []
                         }
                         for d in data.arrayValue{
                              self.people.append(QlueMember.init(fromJson: d))
                         }
                         self.tableView.reloadData()
                    }
          })
     }
     
}

// MARK: - TableView
extension QlueSearchDetailController: UITableViewDelegate, UITableViewDataSource{
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          switch type {
          case .Forum:
               return forum.count
          case .Kelurahan:
               return kelurahan.count
          case .People:
               return people.count
          case .Qlue:
               return qlue.count
               
          }
     }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! QlueSearchDetailCell
          
          switch type {
          case .People:
               let data = people[indexPath.row]
               cell.cellName.text = data.username
               cell.cellImage.contentMode = UIViewContentMode.scaleAspectFit
               cell.cellImage.cornerRadius(cell.cellImage.bounds.width / 2)
               if let avatar = data.avatar?.encodeURL(){
                    cell.cellImage.sd_setImage(with: avatar as URL!)
               }
          case .Kelurahan:
               let data = kelurahan[indexPath.row]
               cell.cellName.text = data.name
          // TODO: Image kelurahan icon
          case .Qlue:
               let data = qlue[indexPath.row]
               cell.cellName.text = data.title
               if let fileUrl = data.fileUrl?.encodeURL(){
                    cell.cellImage.sd_setImage(with: fileUrl as URL!)
               }
          case .Forum:
               let data = forum[indexPath.row]
               cell.cellName.text = data.title
               if let fileUrl = data.file?.encodeURL(){
                    cell.cellImage.sd_setImage(with: fileUrl as URL!)
               }
               
          }

          
          return cell
     }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          switch type {
          case .People:
               let data = people[indexPath.row]
               presentOtherProfile(data.userId)
               return
          case .Kelurahan:
               let kel = kelurahan[indexPath.row]
               presentKelurahan(withId: kel.code, district: kel.district, nav: self.navigationController)
          case .Qlue:
               let data = qlue[indexPath.row]
               presentDetailFeed(withFeedId: data.feedId, nav: self.navigationController)
          case .Forum:
               let data = forum[indexPath.row]
               presentForumDetail(forumId: data.id ?? "", forumType: .general)
          }
     }
}


// MARK: - Navigation

extension QlueViewController{
     func presentSearchDetail(_ type: SearchType, searchKey: String){
     if let searchDetail = menuStoryboard.instantiateViewController(withIdentifier: "SearchDetail") as? QlueSearchDetailController{
          
          searchDetail.type  = type
          searchDetail.searchKey = searchKey
          
          present(searchDetail, animated: true, completion: nil)
          
     }
     }
}
