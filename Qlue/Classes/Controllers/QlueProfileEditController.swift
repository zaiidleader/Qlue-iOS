//
//  QlueProfileEditController.swift
//  Qlue
//
//  Created by Nurul on 7/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class QlueProfileEditController: QlueScrollViewController {

    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var fullnameTextfield: UITextField!
    @IBOutlet weak var dobButton: UIButton!
    @IBOutlet weak var genderButton: UIButton!
    var user: User?
    
    // MARK: - Private Var
    fileprivate let barButtonStyle : [String:AnyObject]? = [
        NSForegroundColorAttributeName: kBgNavBar,
        NSFontAttributeName : UIFont(name: QlueFont.HelveticaNeue, size: 13)!
    ]
    fileprivate var textfields : [UITextField] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        user = User.currentUser()
     setupView()
        
        // Do any additional setup after loading the view.
        textfields = [emailTextfield, phoneTextfield, usernameTextfield, fullnameTextfield]
        for tf in textfields{
            setupTextfield(tf)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helpers
    fileprivate func setupView(){
        fullnameTextfield.text = user?.fullname
        usernameTextfield.text = user?.username
        phoneTextfield.text = user?.phone
        emailTextfield.text = user?.email
        bioTextView.text = user?.bio
        if let banner = user?.banner?.encodeURL(){
            bannerImage.sd_setImage(with: banner as URL!)
        }
        
        
        
    }
    
    fileprivate func setupTextfield(_ tf: UITextField){
        tf.textColor = UIColor.darkGray
        tf.font = UIFont.init(name: QlueFont.HelveticaNeue, size: 15)
        
        //bottom line
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: tf.frame.height - 1, width: tf.frame.width, height: 1)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        tf.borderStyle = .none
        tf.layer.addSublayer(bottomLine)
    }
    
    // MARK: - Action
    @IBAction func dobTapped(_ sender: UIButton) {
     
     var dateSelected = Date()
//     if let dob = user.dob{
//          dateSelected = dob
//     }
        let datePicker = ActionSheetDatePicker(title: "Date:", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            if let date = value as? Date{
                
                sender.setTitle("\(date.formatDateReadable())", for: UIControlState())
            }
            
            return
            }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        datePicker?.maximumDate = Date()
        
        let doneButton = UIBarButtonItem.init()
        doneButton.title = "Done"
        doneButton.setTitleTextAttributes(
            barButtonStyle,
            for: UIControlState())
        
        datePicker?.setDoneButton(doneButton)
        
        let cancelButton = UIBarButtonItem.init()
        cancelButton.title = "Cancel"
        cancelButton.setTitleTextAttributes(
            barButtonStyle,
            for: UIControlState())
        
        datePicker?.setCancelButton(cancelButton)
        
        datePicker?.show()
        
    }
    
    @IBAction func genderTapped(_ sender: UIButton) {
        
        let actionSheet = ActionSheetStringPicker.init(title: "Gender", rows: ["Male", "Female"], initialSelection: 1, doneBlock:  {
            picker, index, value in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            
            sender.setTitle("\(value)", for: UIControlState())
            
            return
            }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        
        let doneButton = UIBarButtonItem.init()
        doneButton.title = "Done"
        doneButton.setTitleTextAttributes(
            barButtonStyle,
            for: UIControlState())
        
        actionSheet?.setDoneButton(doneButton)
        
        let cancelButton = UIBarButtonItem.init()
        cancelButton.title = "Cancel"
        cancelButton.setTitleTextAttributes(
            barButtonStyle,
            for: UIControlState())
        
        actionSheet?.setCancelButton(cancelButton)
        
        actionSheet?.show()
        
    }

}
