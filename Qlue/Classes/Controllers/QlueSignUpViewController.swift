//
//  QlueSignUpViewController.swift
//  Qlue
//
//  Created by Nurul on 5/26/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

class QlueSignUpViewController: QlueScrollViewController {

    // MARK: - Outlets
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var fullnameTextfield: UITextField!
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var bgView: FXBlurView!
    @IBOutlet weak var dobButton: UIButton!
    @IBOutlet weak var genderButton: UIButton!
    
    // Var from social media
    var username: String?
    var phone: Int?
    var email: String?
    var gender: Gender?
    var fullname: String?
    var regis: Int = 0
    var dob: Date?
    var idSocmed: String?
    var socialData: [String: AnyObject]?
    
    // MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        createButton.cornerRadius(10.0)
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
        bgView.blurRadius = 10
        
        let textFields : [UITextField] = [fullnameTextfield, usernameTextfield, passwordTextField, repeatPasswordTextField, emailTextField]
        for tf in textFields{
            setupTextfield(tf)
        }
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    

    // MARK: - Action
    @IBAction func dobTapped(_ sender: UIButton) {
        var dateSelected = Date()
        if let dob = dob{
            dateSelected = dob
        }
        
        let datePicker = ActionSheetDatePicker(title: "Date:", datePickerMode: UIDatePickerMode.date, selectedDate: dateSelected, doneBlock: {
            picker, value, index in
            
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            if let date = value as? Date{
                
                sender.setTitle("\(date.formatDateReadable())", for: UIControlState())
                self.dob = date
            }
            
            return
            }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        datePicker?.maximumDate = Date()
        
        let doneButton = UIBarButtonItem.init()
        doneButton.title = "Done"
        doneButton.setTitleTextAttributes(
            barButtonStyle,
            for: UIControlState())
        
        datePicker?.setDoneButton(doneButton)
        
        let cancelButton = UIBarButtonItem.init()
        cancelButton.title = "Cancel"
        cancelButton.setTitleTextAttributes(
            barButtonStyle,
            for: UIControlState())
        
        datePicker?.setCancelButton(cancelButton)
        
        datePicker?.show()
        
    }

    @IBAction func genderTapped(_ sender: UIButton) {
        
        var selection = 0
        if let gender = gender{
            switch gender {
            case .Male :
                selection = 0
            default:
                selection = 1
            }
        }
        
        let actionSheet = ActionSheetStringPicker.init(title: "Gender", rows: ["Male", "Female"], initialSelection: selection, doneBlock:  {
            picker, index, value in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            
            
            self.gender = Gender(rawValue: (value as! String).lowercased())
            sender.setTitle("\(value)", for: UIControlState())
            
            return
            }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        
        let doneButton = UIBarButtonItem.init()
        doneButton.title = "Done"
        doneButton.setTitleTextAttributes(
            barButtonStyle,
            for: UIControlState())
        
        actionSheet?.setDoneButton(doneButton)
        
        let cancelButton = UIBarButtonItem.init()
        cancelButton.title = "Cancel"
        cancelButton.setTitleTextAttributes(
            barButtonStyle,
            for: UIControlState())
        
        actionSheet?.setCancelButton(cancelButton)
        
        actionSheet?.show()
    }
    
    @IBAction func createTapped(_ sender: UIButton) {
     
     if Util.validateEmail(emailTextField.text) == false{
          Util.showSimpleAlert("Email address is not valid", vc: self, completion: { (action) in
               self.emailTextField.text = ""
               self.emailTextField.becomeFirstResponder()
          })
          return

     }
     
     
        sender.setTitle("", for: UIControlState())
        loadingIndicator.startAnimating()
        sender.isEnabled = false
        
        defer{
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
                sender.setTitle("Create", for: UIControlState())
                self.loadingIndicator.stopAnimating()
                sender.isEnabled = true
            })
            
            
        }
        
        if repeatPasswordTextField.text != passwordTextField.text{
            Util.showSimpleAlert("Password doesn't match", vc: self, completion: nil)
            return
        }
        
        guard let uid = usernameTextfield.text, let pwd = passwordTextField.text, let email = emailTextField.text, let gender = gender, let fullname = fullnameTextfield.text, let dob = dob else{
            Util.showSimpleAlert("Please complete all field", vc: self, completion: nil)
            return
            
        }
        
     Engine.shared.signUp(uid, phone: phone ?? 0 , pwd: pwd, email: email, gender: gender, fullname: fullname, regis: regis, dob: dob, idSocmed: idSocmed, socialData: socialData, source: .qlue).responseString { (res) in
            print("status:\(res.response?.statusCode)")
            if res.result.isSuccess{
                if let result = res.result.value{
                    let data = JSON.parse(result)
                      Util.showSimpleAlert(data["message"].string, vc: self, completion: { (action) in
                        self.dismiss(animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)
                      })
                   
                }
            } else if let error = res.result.error{
                print("error: \(error.localizedDescription)")
            } else{
                
            }
            
        }
        
    }
    // MARK: - Helpers
    func setupView(){
        
        fullnameTextfield.text = fullname
        usernameTextfield.text = username
        emailTextField.text = email
        if let dob = dob{
            dobButton.setTitle(dob.formatDateReadable(), for: UIControlState())
        }
        if let gender = gender{
            
            genderButton.setTitle(gender.description, for: UIControlState())
        }
    }
    
    func setupTextfield(_ tf: UITextField){
        tf.textColor = UIColor.white
        tf.font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 13)
        
        // placeholder
        if let pc = tf.placeholder, let font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 13){
            tf.attributedPlaceholder = NSAttributedString(string: pc, attributes: [NSFontAttributeName : font,
                NSForegroundColorAttributeName: UIColor.white])
        }
        
        
        //bottom line
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: tf.frame.height - 1, width: tf.frame.width, height: 1)
        bottomLine.backgroundColor = UIColor.white.cgColor
        tf.borderStyle = .none
        tf.layer.addSublayer(bottomLine)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension QlueViewController{
    func pushSignUpWithSocmed(_ username: String?, phone: Int?, email: String?, gender: Gender?, fullname: String?, dob: Date?, idSocmed: String?, socialData: [String: AnyObject]?){
        
        if let signUp = storyboard?.instantiateViewController(withIdentifier: "SignUp") as? QlueSignUpViewController{
            
            signUp.username = username
            signUp.phone = phone
            signUp.email = email
            signUp.gender = gender
            signUp.fullname = fullname
            signUp.regis = 1
            signUp.dob = dob
            signUp.idSocmed = idSocmed
            signUp.socialData = socialData
            
            present(signUp, animated: true, completion: nil)
            
        }
        
    }
}
