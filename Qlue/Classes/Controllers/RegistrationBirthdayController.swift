//
//  RegistrationBirthdayController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class RegistrationBirthdayController: RegistrationController {

    @IBOutlet weak var yearPicker: UIPickerView!
    @IBOutlet weak var monthPicker: UIPickerView!
    @IBOutlet weak var datePicker: UIPickerView!
     var days: [Int] {
          var date: [Int] = []
          for i in 1...31{
               date.append(i)
          }
          return date
     }
     
     var months: [String] {
          return ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
     }
     
     var years: [Int] {
          let calendar = Calendar.autoupdatingCurrent
          let components = (calendar as NSCalendar).components([ .year], from: Date())
          let currYear = components.year
          
          var year: [Int] = []
          if let currYear = currYear {
               for i in 1900...currYear {
                    year.append(i)
               }
          }
          return year
     }
     
     
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - PickerView
extension RegistrationBirthdayController: UIPickerViewDelegate, UIPickerViewDataSource{
     // MARK: UIPickerViewDataSource
     func numberOfComponents(in pickerView: UIPickerView) -> Int {
          return 1
     }
     
     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
          switch pickerView {
          case yearPicker:
               return years.count
          case monthPicker:
               return months.count
          default:
               return days.count
          }
     }
     
     // MARK: UIPickerViewDelegate
     
     func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
          let label = UILabel.init(frame: CGRect(x: 0, y: 0, width: pickerView.frame.size.width, height: 35))
          label.textColor = kBlue0988EEColor
          label.textAlignment = .center

          if let font = UIFont.init(name: QlueFont.ProximaNovaRegular, size: 15){
               label.font = font
          }
          
          switch pickerView {
          case yearPicker:
               label.text = years[row].description
          case monthPicker:
               label.text = months[row]
          default:
               label.text = days[row].description
          }
          
          return label
     }
     func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
          return 35
     }
     
}
