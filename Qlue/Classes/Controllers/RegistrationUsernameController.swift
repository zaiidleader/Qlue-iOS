//
//  RegistrationUsernameController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 11/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class RegistrationUsernameController: RegistrationController {

    @IBOutlet weak var usernameTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

     override func nextTapped(_ sender: UIButton) {
          guard let text = usernameTextfield.text, text != "" else{
               Util.showSimpleAlert("Please complete your username", vc: self, completion: nil)
               return
          }
     
          Engine.shared.createUser.username = text
          performSegue(withIdentifier: "NextSegue", sender: self)
     
     }
     
}
