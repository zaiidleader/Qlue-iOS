//
//  QlueFeedContainerController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueFeedContainerController: QlueViewController {

     
     @IBOutlet weak var chatButton: UIButton!
     
    // MARK: Controller
    @IBOutlet weak var containerView: UIView!
     lazy var addKelurahan: QlueKelurahanStartController = {
          let vc = kelurahanStoryboard.instantiateViewController(withIdentifier: "AddKelurahan") as! QlueKelurahanStartController
          return vc
     }()
     
     lazy var feedController: QlueFeedViewController = {
          let controller = menuStoryboard.instantiateViewController(withIdentifier: "FeedList") as! QlueFeedViewController
          
          return controller
     }()
     
     // MARK: - View Lifecycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          setupController()
          configureChatButton(chatButton)
          
          NotificationCenter.default.addObserver(self, selector: #selector(QlueFeedContainerController.setupController), name: NSNotification.Name(rawValue: kKelurahanChanged), object: nil)
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Helpers
     func setupController(){
          if let codeKel = User.currentUser()?.codeKelurahan, codeKel != ""{
               displayViewController(viewController: feedController, inView: self.containerView)
          } else{
               displayViewController(viewController: addKelurahan, inView: self.containerView)
          }
     }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
