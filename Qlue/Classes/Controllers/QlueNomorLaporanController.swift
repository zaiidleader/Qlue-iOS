//
//  QlueNomorLaporanController.swift
//  Qlue
//
//  Created by Nurul on 8/8/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueNomorLaporanController: QlueViewController {
     
     var noLap: String? = ""
     @IBOutlet weak var nomorLaporan: UILabel!
     override func viewDidLoad() {
          super.viewDidLoad()
          
          let tap = UITapGestureRecognizer.init(target: self, action: #selector(QlueNomorLaporanController.tapHandle(_:)))
          view.addGestureRecognizer(tap)
          
          
          nomorLaporan.text = noLap
          // Do any additional setup after loading the view.
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: -Action
     func tapHandle(_ sender: UITapGestureRecognizer){
          dismiss(animated: true, completion: nil)
     }
     
     
     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
      }
      */
     
}

extension QlueViewController{
     func presentNomorLaporan(withNoLap no: String?){
          if let noLap = mainStoryboard.instantiateViewController(withIdentifier: "NomorLaporan") as? QlueNomorLaporanController{
               noLap.noLap = no
               semiModalWithViewController(noLap)
          }
     }
}
