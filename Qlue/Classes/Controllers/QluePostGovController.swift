//
//  QluePostGovController.swift
//  Qlue
//
//  Created by Nurul on 7/17/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON

class QluePostGovController: QlueViewController, UITextViewDelegate {
     
     @IBOutlet weak var titleTextfield: UITextField!{
          didSet{
               Util.textfieldPlaceholderWithColor(UIColor.white, font: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 17), textfield: titleTextfield)
          }
     }
    @IBOutlet weak var contentTextview: UITextView!{
        didSet{
            contentTextview.placeholder = "Tell your story.."
            contentTextview.placeholderColor = UIColor.white
        }
    }

     @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    var currentHeight: CGFloat = 35
     var heightBefore: CGFloat = 35
    var locationState = false
    
    // MARK: - View Lifecyle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          locationState = false
          setCurrentLocation()
          // Do any additional setup after loading the view.
          imageView.image = Engine.shared.currentReport?.image
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
    
    
    //MARK: - UITextViewDelegate
     func textViewDidChange(_ textView: UITextView) {
          let height = textView.contentSize.height
          
          if height == currentHeight || height < 15{
               return
          }
          
          if height > currentHeight{
               heightBefore = currentHeight
               UIView.animate(withDuration: 0.5, animations: {
                    self.textViewHeight.constant = height
               })
               currentHeight = height
               return
          }
          
          if height < currentHeight{
               UIView.animate(withDuration: 0.5, animations: {
                    self.textViewHeight.constant = self.heightBefore
               })
               
               currentHeight = height
          }
          
     }
    
    //MARK: - Action
    
     func setCurrentLocation() {
          
          Engine.shared.locationManager.startUpdatingLocation()
          
          if let location = Engine.shared.currentLocation {
               locationState = true
               Engine.shared.currentReport?.currentLocation = location
          }else {
               
               NotificationCenter.default.addObserver(self, selector: #selector(QlueTopicGovController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
               Engine.shared.locationManager.startUpdatingLocation()
               
          }
     }

     func removeObserverLoc() {
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
          setCurrentLocation()
     }

     
    @IBAction func sendTapped(_ sender: UIBarButtonItem) {
        DejalBezelActivityView.addActivityView(for: self.view)
        
        Engine.shared.currentReport?.title = titleTextfield.text
     Engine.shared.currentReport?.desc = contentTextview.text
     
     Engine.shared.postReport(Engine.shared.currentReport!) { (result, error) in
          DejalBezelActivityView.remove(animated: true)
          if let result = result as? Bool, result == true{
                    Util.showSimpleAlert("Qlue berhasil dikirim", vc: self, completion: { (action) in
                         
                         self.navigationController?.popToRootViewController(animated: true)
                    })
          } else if let result = result as? String{
               Util.showSimpleAlert(result, vc: self, completion: nil)
          } else {
               Util.showSimpleAlert(error?.localizedDescription, vc: self, completion: nil)
          }
     }
     
    }
     
     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
      }
      */
     
}
