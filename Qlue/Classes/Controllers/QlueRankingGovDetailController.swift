//
//  QlueRankingGovDetailController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/16/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

enum rankingGovType{
    case kecamatan(kota: String)
    case kelurahan(kec: String)
    
    var title: String {
        switch self {
        case .kecamatan(_):
            return "Kecamatan"
        case .kelurahan(_):
            return "Kelurahan"
        }
    }
}
class QlueRankingGovDetailController: QlueViewController {
    
    var type: rankingGovType = .kecamatan(kota: "")
    @IBOutlet weak var tableView: UITableView!
    var ranks: [QlueRanking] = []
    var start = 0
    
    var location: CLLocation?
    
    
    var sortState: Bool = true // t = top , f: bottom
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DejalActivityView.addActivityView(for: tableView)
        configureTableView()
        setCurrentLocation()
        
        self.title = type.title
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Configure
    fileprivate func configureTableView(){
        tableView.register(UINib.init(nibName: "QlueRankingCell2", bundle: nil), forCellReuseIdentifier: "RankingCell")
        tableView.rowHeight = 60
    }
    
    func setCurrentLocation() {
        
        Engine.shared.locationManager.startUpdatingLocation()
        
        if let location = Engine.shared.currentLocation {
            self.location = location
            
            downloadData()
            
        }else {
            
            NotificationCenter.default.addObserver(self, selector: #selector(QlueRankingGovDetailController.removeObserverLoc), name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
            Engine.shared.locationManager.startUpdatingLocation()
        }
    }
    
    func removeObserverLoc() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDidUpdateLocationNotification), object: nil)
        setCurrentLocation()
    }
    
    //MARK: - Data
    fileprivate func downloadData(){
        var req: DataRequest!
        
        
        var typeSort  = "top"
        if sortState == false { typeSort = "bottom" }
        
        switch type {
        case .kecamatan(let kota):
            req = Engine.shared.listRankingKecamatanByKota(kota, type: typeSort, lat: location!.coordinate.longitude, lng: location!.coordinate.latitude)
        case .kelurahan(let kecamatan):
            req = Engine.shared.listRankingKelurahanByKecamatan(kecamatan, type: typeSort, lat: location!.coordinate.longitude, lng: location!.coordinate.latitude)
        }
        
        Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
            DejalActivityView.remove()
            }, completion: { (data, success, fail) in
                self.ranks = []
                
                if success == true{
                    for d in data.arrayValue{
                        self.ranks.append(QlueRanking.init(fromJson: d))
                    }
                    
                    self.tableView.reloadData()
                }
                
        })
    }
    
    // MARK: - Action
    @IBAction func sortTapped(_ sender: UIButton) {
        sortState = !sortState
        
        self.ranks = []
        tableView.reloadData()
        DejalActivityView.addActivityView(for: self.view)
        downloadData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension QlueRankingGovDetailController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ranks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RankingCell", for: indexPath) as! QlueRankingCell2
        let rank = ranks[indexPath.row]
        
        
        cell.cellImage.isHidden = false
        cell.cellRankView.isHidden = true
        if sortState == true{
            if rank.rank == 1{
                cell.cellImage.image = UIImage.init(named: "trophy_01")
            } else if rank.rank == 2{
                cell.cellImage.image = UIImage.init(named: "trophy_02")
            } else if rank.rank == 3{
                cell.cellImage.image = UIImage.init(named: "trophy_03")
            } else{
                
                
                cell.cellImage.isHidden = true
                cell.cellRankView.isHidden = false
                let numb = rank.rank ?? 0
                cell.cellRankLabel.text = numb.description
                
            }
            
        } else{
            
            let numb = rank.rank ?? 0
            cell.cellRankLabel.text = numb.description
        }
        
        cell.cellLabel.text = rank.nama
        cell.cellKelurahanLabel.text = nil
        cell.cellPoint.text = rank.total
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rank = ranks[indexPath.row]
        
        switch type{
        case .kecamatan(_):
            self.pushRankingGovDetail(self.navigationController, type: rankingGovType.kelurahan(kec: rank.nama ?? ""))
        case .kelurahan(_):
            self.presentKelurahan(withId: rank.nama ?? "", district: rank.code ?? "", nav: self.navigationController)
        }
    }
    
    
}

// MARK: - Navigate
extension QlueViewController{
    func pushRankingGovDetail(_ nav: UINavigationController?, type: rankingGovType){
        if let detail = moreStoryboard.instantiateViewController(withIdentifier: "RankingGovDetail") as? QlueRankingGovDetailController{
            detail.type = type
            
            nav?.pushViewController(detail, animated: true)
        }
    }
}
