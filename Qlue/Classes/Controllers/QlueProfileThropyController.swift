//
//  QlueProfileThropyController.swift
//  Qlue
//
//  Created by Nurul on 7/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueProfileThropyController: QlueProfileBaseController {
     // MARK: - Vars
     // MARK: Outlet
     @IBOutlet weak var collectionView: UICollectionView!
     
     lazy var labelView: UILabel = {
          let label = UILabel.init(frame: CGRect(x: 15, y: 150, width: UIScreen.main.bounds.width - 30, height: 100))
          label.font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 17)
          label.textColor = UIColor.darkGray
          label.text = "Anda belum memiliki piala"
          label.textAlignment = NSTextAlignment.center
          
          return label
     }()
     
     
     // MARK: Data
     var trophyArr : [QlueTrophy] = []
     var kelurahan: QlueKelurahan?

     
     // MARK: - View Lifecycle
     override func viewDidLoad() {
          super.viewDidLoad()
          
          
          // Do any additional setup after loading the view.
          collectionView.register(UINib.init(nibName: "QlueKTrophyViewCell", bundle: nil), forCellWithReuseIdentifier: "TrophyCell")
          DejalActivityView.addActivityView(for: collectionView)
          downloadData()
          if userId != User.currentUser()?.userId{
                    setupFab()
          }
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Data
     func downloadData(){
          
          guard let userId = userId else{
               DejalActivityView.remove()
               return
          }
          
          let req = Engine.shared.listTrophyUser(userId: userId)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               
               }, completion: { (data, success, fail) in
                    self.trophyArr = []
                    if success == true{
                         for d in data.arrayValue{
                              self.trophyArr.append(QlueTrophy.init(fromJson: d))
                         }
                    }
                    
                    if self.trophyArr.count > 0{
                         self.labelView.removeFromSuperview()
                    } else{
                         self.view.addSubview(self.labelView)
                    }
                    
                    self.collectionView.reloadData()
          })
          
     }

     
}

// MARK: - UICollectionViewDataSource
extension QlueProfileThropyController: UICollectionViewDataSource{
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return trophyArr.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrophyCell", for: indexPath) as! QlueKTrophyViewCell
          cell.cornerRadius(5)
          
          let data = trophyArr[indexPath.item]
          
          cell.cellImage.image = nil
          if let imageUrl = data.icon?.encodeURL(){
               cell.cellImage.sd_setImage(with: imageUrl as URL!)
          }
          cell.cellPoint.text = "\(data.total ?? "0") Points"
          cell.cellTime.text = data.date

          
          return cell
     }
}

// MARK: - UICollectionViewDelegate
extension QlueProfileThropyController: UICollectionViewDelegate{
     
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueProfileThropyController: UICollectionViewDelegateFlowLayout{
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
          let size: CGFloat = ((collectionView.bounds.width - 24) / 2 )
          
          return CGSize(width: size, height: size)
          
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return 8
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return 8
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsetsMake(8, 8, 0, 8)
     }
     
     
}

