//
//  QlueFABGalleryController.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 12/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import Photos

typealias GalleryCompletionHandler = (_ images: [UIImage]) -> Void

class QlueFABGalleryController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var imageManager: PHCachingImageManager = PHCachingImageManager()
    var fetchResult: PHFetchResult<PHAsset>?
    var selectedIndexPaths: [IndexPath] = []
    
    var completion: GalleryCompletionHandler = { (images) -> Void in }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(true, animated: false)
        let image = UIImage(named: "ic_back")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        backButton.setImage(image, for: UIControlState())
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if PHPhotoLibrary.authorizationStatus() == .denied || PHPhotoLibrary.authorizationStatus() == .notDetermined {
            PHPhotoLibrary.requestAuthorization({ (authorizationStatus) in
                if authorizationStatus == .authorized {
                    DispatchQueue.main.async(execute: { 
                        self.loadImages()
                    })
                }
            })
        }
        else {
            loadImages()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Helpers
    
    func loadImages() {
        
        //fetch all assets, then sub fetch only the range we need
        fetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
        collectionView.reloadData()
    }
    
    // MARK: - Actions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        if navigationController?.viewControllers.first == self {
            dismiss(animated: true, completion: nil)
        }
        else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func doneButtonTapped(_ sender: UIButton) {
        
        if selectedIndexPaths.count == 0 {
            if navigationController?.viewControllers.first == self {
                dismiss(animated: true, completion: nil)
            }
            else {
                navigationController?.popViewController(animated: true)
            }
        }
        else {
            
            var images: [UIImage] = []
            for indexPath in selectedIndexPaths {
                
                if let asset = fetchResult?.object(at: indexPath.item) {
                    let targetSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
                    imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: PHImageContentMode.default, options: nil, resultHandler: { (image, info) in
                        if let image = image {
                            images.append(image)
                        }
                    })
                }
            }
            
            completion(images)
            
            if navigationController?.viewControllers.first == self {
                dismiss(animated: true, completion: nil)
            }
            else {
                navigationController?.popViewController(animated: true)
            }
        }
    }
}

// MARK: - UICollectionViewDataSource
extension QlueFABGalleryController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let fetchResult = fetchResult {
            return fetchResult.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCellId", for: indexPath) as! QlueFABGalleryViewCell
        
        if let index = selectedIndexPaths.index(of: indexPath) {
            cell.numberLabel.isHidden = false
            cell.numberLabel.text = "\(index + 1)"
        }
        else {
            cell.numberLabel.isHidden = true
        }
        
        cell.imageView.image = nil
        if let asset = fetchResult?.object(at: indexPath.item) {
            let width = (collectionView.frame.width - 8 * 4) / 3
            let targetSize = CGSize(width: width, height: width)
            
            imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFill, options: nil, resultHandler: { (image, info) in
                cell.imageView.image = image
            })
        }
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueFABGalleryController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.width - 8 * 4) / 3
        return CGSize(width: width, height: width)
    }

}

// MARK: - UICollectionViewDelegate
extension QlueFABGalleryController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let index = selectedIndexPaths.index(of: indexPath) {
            selectedIndexPaths.remove(at: index)
            collectionView.reloadItems(at: [indexPath])
        }
        else {
            selectedIndexPaths.append(indexPath)
        }
        collectionView.reloadItems(at: selectedIndexPaths)
    }
}

// MARK: - UIViewController
extension UIViewController {
    
    func presentFABGallery(_ nav: UINavigationController?, completion: @escaping GalleryCompletionHandler) {
        
        if let fab = fabStoryboard.instantiateViewController(withIdentifier: "FABGallery") as? QlueFABGalleryController {
            fab.completion = completion
            
            if let nav = nav {
                nav.pushViewController(fab, animated: true)
                return
            }
            present(fab, animated: true, completion: nil)
        }
    }
}
