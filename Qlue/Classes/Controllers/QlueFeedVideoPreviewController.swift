//
//  QlueFeedVideoPreviewController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/4/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

import AVFoundation


class QlueFeedVideoPreviewController: QlueViewController {
     
     @IBOutlet weak var previewImage: UIImageView!
     var thumbImage: UIImage?
     var videoUrlStr: String = ""
     
     var player: AVPlayer?
     var playerLayer: AVPlayerLayer?
     override func viewDidLoad() {
          super.viewDidLoad()
          
          previewImage.image = thumbImage
          playVideo()
          // Do any additional setup after loading the view.
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Helpers
     fileprivate func playVideo(){
          if let url = videoUrlStr.encodeURL(){
               player = AVPlayer.init(url: url as URL)
               player?.actionAtItemEnd = .none
               
               playerLayer = AVPlayerLayer.init(player: player)
               playerLayer?.videoGravity = AVLayerVideoGravityResizeAspect
               
               NotificationCenter.default.addObserver(self,
                                                                selector: #selector(QlueFeedVideoPreviewController.playerItemDidReachEnd(_:)),
                                                                name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                                object: player?.currentItem)
               
               let screenRect = UIScreen.main.bounds
               playerLayer?.frame = CGRect.init(x: 0, y: 0, width: screenRect.size.width, height: screenRect.size.height)
               
               if let playerLayer = playerLayer{
               previewImage.layer.insertSublayer(playerLayer, at: 0)
               }
               player?.play()
          }
          
     }
     func playerItemDidReachEnd(_ sender: Notification){
          
          let playerItem = sender.object
          (playerItem as AnyObject).seek(to: kCMTimeZero)
          
     }
     override func backTapped(_ sender: AnyObject) {
          super.backTapped(sender)
          player?.pause()
          playerLayer?.removeFromSuperlayer()
     }
    
    // MARK: - Action
    
    
     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
      }
      */
     
}
extension QlueViewController{
   func presentPreviewVideoFeed(withUrl url: String, thumbImage: UIImage?){
    
    let preview = QlueFeedVideoPreviewController()
    preview.thumbImage = thumbImage
    preview.videoUrlStr = url
    
    present(preview, animated: true, completion: nil)
    
    }
}
