//
//  QlueInviteController.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 12/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON
import Alamofire
import TwitterKit

enum Socmed {
    case google
    case twitter
    case facebook
    
    var icon: UIImage? {
        switch self {
        case .google:
            return UIImage(named: "btn_sosmed_google")
        case .twitter:
            return UIImage(named: "btn_sosmed_twitter")
        case .facebook:
            return UIImage(named: "btn_sosmed_facebook")
        }
    }
    
    var title: String {
        switch self {
        case .google:
            return "Find Google+ Friends"
        case .twitter:
            return "Find Twitter Friends"
        case .facebook:
            return "Find Facebook Friends"
        }
    }
    
    var backButtonTitle: String {
        switch self {
        case .google:
            return "Google+ Friends"
        case .twitter:
            return "Twitter Friends"
        case .facebook:
            return "Facebook Friends"
        }
    }
}

class QlueInviteController: QlueViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    let socmeds: [Socmed] = [.google, .twitter, .facebook]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(true, animated: false)
        let image = UIImage(named: "ic_back")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        backButton.setImage(image, for: UIControlState())
        configureChatButton(chatButton)
        
        tableView.tableFooterView = UIView()
        
        // Sign In With Google delegate setup
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().scopes = ["https://www.googleapis.com/auth/plus.login", "https://www.googleapis.com/auth/plus.me"]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - Actions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - UITableViewDataSource
extension QlueInviteController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return socmeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InviteCellId", for: indexPath)
        
        let socmed = socmeds[indexPath.row]
        if let imageView = cell.viewWithTag(101) as? UIImageView {
            imageView.image = socmed.icon
        }
        if let label = cell.viewWithTag(102) as? UILabel {
            label.text = socmed.title
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension QlueInviteController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let socmed = socmeds[indexPath.row]
        if socmed == .facebook {
            
            let login: FBSDKLoginManager = FBSDKLoginManager()
            login.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) in
                if let error = error {
                    print(error.localizedDescription)
                }
                else if (result?.isCancelled)! {
                    
                }
                else {
                    
                    FBSDKGraphRequest(graphPath: "me/friends", parameters: nil).start(completionHandler: { (connection, result, error) in
                        
                        if let error = error {
                            print(error.localizedDescription)
                        }
                        else {
                            
                            let json = JSON(result)
                            let data = json["friends"]["data"].arrayValue
                            
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SocmedFriends") as! QlueSocmedFriendsController
                            viewController.socmed = socmed
                            viewController.data = data
                            
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    })
                }
            }
        }
        else if socmed == .google {
            
            GIDSignIn.sharedInstance().signOut()
            GIDSignIn.sharedInstance().signIn()
        }
        else if socmed == .twitter {
            
            Twitter.sharedInstance().logIn(withMethods: [.webBased]) { (session, error) in
                if let _ = session {
                    
                    let client = TWTRAPIClient.withCurrentUser()
                    let request = client.urlRequest(withMethod: "GET", url: "https://api.twitter.com/1.1/friends/list.json", parameters: nil, error: nil)
                    
                    DejalBezelActivityView.addActivityView(for: self.view)
                    client.sendTwitterRequest(request, completion: { (response, data, error) in
                    DejalBezelActivityView.remove(animated: true)
                        
                        if let data = data {
                            let json = JSON(data: data)
                            let users = json["users"].arrayValue
                            
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SocmedFriends") as! QlueSocmedFriendsController
                            viewController.socmed = socmed
                            viewController.data = users
                            
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                        else if let error = error {
                            print(error.localizedDescription)
                        }
                    })
                }
                else if let error = error {
                    NSLog(error.localizedDescription);
                }
            }
        }
    }
}

// MARK: - GIDSignInDelegate
extension QlueInviteController: GIDSignInDelegate {
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error == nil {
            
            let url = "https://www.googleapis.com/plus/v1/people/me/people/visible?access_token=\(user.authentication.accessToken)"
            
            DejalBezelActivityView.addActivityView(for: view)
            Alamofire.request(url, method: .get).response(completionHandler: { (defaultDataResponse) in
                DejalBezelActivityView.remove(animated: true)
                
                if let result = defaultDataResponse.data {
                    let json = JSON(data: result)
                    let code = json["code"].intValue
                    
                    if code != 200 {
                        let errorMessage = json["error"]["message"].string
                        let alertController = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else {
                        
                    }
                }
                else if let error = defaultDataResponse.error {
                    print(error.localizedDescription)
                }
            })
        }
        else {
            print(error.localizedDescription)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
}

// MARK: - GIDSignInUIDelegate
extension QlueInviteController: GIDSignInUIDelegate {
    
}
