//
//  QlueMenuViewController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueMenuViewController: QlueViewController {
//    
//    // MARK: - Navbar
//    @IBOutlet weak var searchViewBar: UIView!{
//        didSet{
//            searchViewBar.cornerRadius(5)
//        }
//    }
    
    // MARK: Chat
    
    @IBOutlet weak var chatButton: UIButton!
    
    // MARK: Fab
//    var fabView: QlueFabView!
    
    // MARK: Controller
    @IBOutlet weak var containerView: UIView!
    var currentViewController: UIViewController?
    lazy var feedController: QlueFeedContainerController = {
        let controller = menuStoryboard.instantiateViewController(withIdentifier: "FeedContainer") as! QlueFeedContainerController
        
        return controller
    }()
    lazy var forumController: QlueNewSearchController = {
        let controller = searchStoryboard.instantiateViewController(withIdentifier: "NewSearch") as! QlueNewSearchController
        
        return controller
    }()
    lazy var notifController: QlueNotifController = {
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "Notif") as! QlueNotifController
        
        return controller
    }()
    lazy var moreController: QlueMoreController = {
        let controller = mainStoryboard.instantiateViewController(withIdentifier: "More") as! QlueMoreController
        
        return controller
    }()
    
    // MARK: Tab
    @IBOutlet weak var feedImage: UIImageView!
    @IBOutlet weak var feedButton: UIButton!
    
//    @IBOutlet weak var fabImage: UIImageView!
    @IBOutlet weak var fabButton: UIButton!
    
    @IBOutlet weak var forumImage: UIImageView!
    @IBOutlet weak var forumButton: UIButton!
    
    @IBOutlet weak var notifImage: UIImageView!
    @IBOutlet weak var notifButton: UIButton!
    
    @IBOutlet weak var moreImage: UIImageView!
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var tabView: UIView!
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        
//        tabView.layer.shadowColor = kGreyEEE.CGColor
//        tabView.layer.shadowOpacity = 1
//        tabView.layer.shadowRadius = 0.5
//        tabView.layer.shadowOffset = CGSize(width: 0, height: -1)
        
        
        
        // Do any additional setup after loading the view.
        configureFirstView()
        configureFabView()
        configureChatButton()
        Util.setStatusBarBackgroundColor(kBlue0988EEColor)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        
//        fabView.hideWrapView(true)
//        fabView.setNeedsLayout()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Configure
    fileprivate func configureFirstView(){
        enableAllButton(except: feedButton)
        showFeed()
    }
    
    fileprivate func configureFabView(){
//          fabView = showFabView(self)
    }

    fileprivate func configureChatButton(){
        if self.revealViewController() != nil {
            chatButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
        }
    }
    
    
    // MARK: - Helpers
    func enableAllButton(except button: UIButton){
        
        button.layoutIfNeeded()
        
        let tabs : [UIButton : UIImageView] = [
            feedButton : feedImage,
            forumButton : forumImage,
//            fabButton : fabImage,
            notifButton : notifImage,
            moreButton : moreImage
        ]
        for (tab, image) in tabs{
            
            if tab == button{
                tab.isEnabled = false
                image.isHighlighted = true
                
                continue
            }
            
            tab.isEnabled = true
            image.isHighlighted = false
            
        }
    }
    
    // MARK: - Action
    
    @IBAction func tabTapped(_ sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = true
        
        if sender == fabButton {
            showFAB()
            return
        }
        
        enableAllButton(except: sender)
        
        switch sender {
        case feedButton:
            showFeed()
        case forumButton:
            showForum()
        case notifButton:
            showNotif()
        case moreButton:
            showMore()
        default:
            return
        }
        
    }
    
    // MARK: - Tab
    func showFeed(){
        
        if currentViewController == feedController{
            return
        }
        
        replaceViewController(currentViewController, withViewController: feedController, inContainerView: containerView)
        currentViewController = feedController
        
    }
    
    func showForum(){
        if currentViewController == forumController{
            return
        }
        
        replaceViewController(currentViewController, withViewController: forumController, inContainerView: containerView)
        currentViewController = forumController
    }
    
    func showNotif(){
        if currentViewController == notifController{
            return
        }
        
        replaceViewController(currentViewController, withViewController: notifController, inContainerView: containerView)
        currentViewController = notifController
    }
    
    func showMore(){
        if currentViewController == moreController{
            return
        }
        
        replaceViewController(currentViewController, withViewController: moreController, inContainerView: containerView)
        currentViewController = moreController
    }
    
    func showFAB() {
        presentFABMenu(navigationController, delegate: self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func unwindToMenu(_ segue: UIStoryboardSegue) {
        if segue.source.isKind(of: QluePostGovController.self) || segue.source.isKind(of: QluePostSwastaController.self){
            
        }
    }
    
}

extension QlueMenuViewController: QlueFABMenuControllerDelegate {
    
    func qlueFABMenuControllerFABGeneralButtonTapped(_ viewController: QlueFABMenuController) {
        
        navigationController?.popViewController(animated: true)
        presentFAB(self.navigationController, type: .general)
    }
    
    func qlueFABMenuControllerFABHelpButtonTapped(_ viewController: QlueFABMenuController) {
        
        navigationController?.popViewController(animated: true)
        presentFAB(self.navigationController, type: .help)
    }
    
    func qlueFABMenuControllerFABAskButtonTapped(_ viewController: QlueFABMenuController) {
        
        navigationController?.popViewController(animated: true)
        presentFAB(self.navigationController, type: .ask)
    }
    
    func qlueFABMenuControllerFABReportButtonTapped(_ viewController: QlueFABMenuController) {
        
        navigationController?.popViewController(animated: true)
        presentCameraView(navigationController) {
            
        }
//        presentFAB(self.navigationController, type: .Report)
    }
    
    func qlueFABMenuControllerFABReviewButtonTapped(_ viewController: QlueFABMenuController) {
        
        navigationController?.popViewController(animated: true)
        presentReportSwastaSearchPlace(navigationController) { 
            
        }
//        presentFAB(self.navigationController, type: .Review)
    }
    
    func qlueFABMenuControllerFABMarketButtonTapped(_ viewController: QlueFABMenuController) {
        
        navigationController?.popViewController(animated: true)
        presentFAB(self.navigationController, type: .market)
    }
}
