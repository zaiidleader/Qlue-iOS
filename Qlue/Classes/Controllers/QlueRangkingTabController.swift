//
//  QlueRangkingTabController.swift
//  Qlue
//
//  Created by Nurul on 6/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CarbonKit

enum TabRanking {
    case user
    case kelurahan
    case business
    
    var title: String{
        switch self {
        case .user:
            return "User"
        case .kelurahan:
            return "Kelurahan"
        case .business:
            return "Business"
        }
    }
    
    var textWidth: CGFloat{
        var titleAttr : [String: AnyObject]?
        if let font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 13){
            titleAttr = [NSFontAttributeName : font]
        }
        let nsTitle = NSString.init(string: self.title)
        let size = nsTitle.size(attributes: titleAttr)
        
        return size.width + 30
    }
    
}


class QlueRangkingTabController: QlueViewController, CarbonTabSwipeNavigationDelegate {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    
    var type: RankingInfoType = .user
    let items: [String] = ["User", "Pemerintah", "Swasta"]
    
    // MARK: Tab Var
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var tabCollectionView: UICollectionView!
    var tabs : [TabRanking] = [.user, .kelurahan, .business]
    var currentTab: TabRanking = .user
    var diffWidth: CGFloat = 0
    
    // MARK: Container Var
    @IBOutlet weak var containerView: UIView!
    lazy var userController: QlueRankingUserController = {
        let controller = moreStoryboard.instantiateViewController(withIdentifier: "RankingUser") as! QlueRankingUserController
        return controller
    }()
    lazy var govController: QlueRankingGovController = {
        let controller = moreStoryboard.instantiateViewController(withIdentifier: "RankingGov") as! QlueRankingGovController
        return controller
    }()
    lazy var swastaController: QlueRankingSwastaController = {
        let controller = moreStoryboard.instantiateViewController(withIdentifier: "RankingSwasta") as! QlueRankingSwastaController
        return controller
    }()
    var currentController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(true, animated: false)
        let image = UIImage(named: "ic_back")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        backButton.setImage(image, for: UIControlState())
        configureChatButton(chatButton)
        
//        setupTabView()
        configureTabView()
        showUser()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private Func
    fileprivate func setupTabView(){
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation.init(items: items, delegate: self)
        
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.toolbarHeight.constant = 40.0
        carbonTabSwipeNavigation.setIndicatorColor(kCreamColor)
        
     carbonTabSwipeNavigation.toolbar.setBackgroundImage(UIImage(named: "bgNavbar"), forToolbarPosition: UIBarPosition.any, barMetrics: UIBarMetrics.default)
     carbonTabSwipeNavigation.toolbar.shadowWithColor(UIColor.clear)
     
        
        for i in 0..<items.count{
            carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width / 3, forSegmentAt: i)
        }
        
        if let barFont = UIFont(name: QlueFont.ProximaNovaSoft, size: 12.0) {
            carbonTabSwipeNavigation.carbonSegmentedControl?.setTitleTextAttributes([NSForegroundColorAttributeName: kCreamColor
                ,NSFontAttributeName:barFont], for: UIControlState())
            carbonTabSwipeNavigation.carbonSegmentedControl?.setTitleTextAttributes([NSForegroundColorAttributeName: kCreamColor
                ,NSFontAttributeName:barFont], for: .selected)
            
        }
    }
    
    func configureTabView() {
        
        let screenWidth = UIScreen.main.bounds.width
        var tabWidth: CGFloat = 2
        for tab in tabs {
            tabWidth += tab.textWidth
            tabWidth += 8
        }
        
        if tabWidth < screenWidth{
            diffWidth = (screenWidth - tabWidth) / CGFloat(tabs.count)
        }
    }
    
    // MARK: - Tab
    
    func showUser() {
        if currentController == userController {
            return
        }
        
        type = .user
        replaceViewController(currentController, withViewController: userController, inContainerView: containerView)
        currentController = userController
    }
    
    func showGoverment() {
        if currentController == govController {
            return
        }
        
        type = .goverment
        replaceViewController(currentController, withViewController: govController, inContainerView: containerView)
        currentController = govController
    }
    
    func showSwasta() {
        if currentController == swastaController {
            return
        }
        
        type = .business
        replaceViewController(currentController, withViewController: swastaController, inContainerView: containerView)
        currentController = swastaController
    }
    
    // MARK: - Action
    @IBAction func infoTapped(_ sender: UIBarButtonItem) {
        presentRankingInfo(withType: type)
    }
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - CarbonTabSwipeNavigationDelegate
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        switch index {
        case 0:
            if let user: QlueRankingUserController = moreStoryboard.instantiateViewController(withIdentifier: "RankingUser") as? QlueRankingUserController{
                type = .user
                return user
            }
        case 1:
          if let gov: QlueRankingGovController = moreStoryboard.instantiateViewController(withIdentifier: "RankingGov") as? QlueRankingGovController{
            type = .goverment
               return gov
          }
        case 2:
            
          if let swasta: QlueRankingSwastaController = moreStoryboard.instantiateViewController(withIdentifier: "RankingSwasta") as? QlueRankingSwastaController{
            type = .business
               return swasta
          }
        
        default:
            break
        }
     
        return UIViewController()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

// MARK: - Tab CollectionView
extension QlueRangkingTabController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TabCell", for: indexPath) as! SearchTabCell
        
        let tab = tabs[indexPath.item]
        cell.layoutIfNeeded()
        cell.activeView.cornerRadius(cell.activeView.bounds.height / 2)
        cell.label.text = tab.title
        
        if tab == currentTab {
            cell.isActive = true
        } else{
            cell.isActive = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let tab = tabs[indexPath.item]
        let width = tab.textWidth + diffWidth
        return CGSize(width: width, height: 25)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8.5, left: 5, bottom: 0, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.currentTab = tabs[indexPath.row]
        switch currentTab {
        case .user:
            showUser()
        case .kelurahan:
            showGoverment()
        case .business:
            showSwasta()
        }
        collectionView.reloadData()
    }
    
    
}
