//
//  QlueForumDetailController.swift
//  Qlue
//
//  Created by Nurul on 8/4/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


let kForumDeleted = "kForumDeleted"

class QlueForumDetailController: QlueViewController, UITableViewDataSource, UITableViewDelegate {
     
     @IBOutlet weak var tableView: UITableView!
     var voteTableView: UITableView = UITableView.init()
     var id: String = ""
     var forum: QlueDiskusi?
     
    @IBOutlet weak var commentFab: UIButton!{
        didSet{
            commentFab.shadowWithColor(UIColor.black)
        }
    }
     var commentSort = "old" // old / new
     var comments: [QlueComment] = []
     
     var arrowRight: UIButton!
     var arrowLeft: UIButton!
     
     var allReviewIndex = 0
     var commentExpanded = false
     
     var commentPage = 0
     
     var forumType : ForumType = .general
    
     @IBOutlet weak var moreButton: UIButton!{
          didSet{
               moreButton.isHidden = true
          }
     }
    // MARK: - View Lifecycle
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          configureTableView()
          DejalBezelActivityView.addActivityView(for: self.view)
          downloadData()
          downloadComment()
          observeNotification()
          // Do any additional setup after loading the view.
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     func initWithForumId(_ id: String, forumType: ForumType){
          self.id = id
          self.forumType = forumType
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
     
     }
     override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
          
          tableView.contentOffset = CGPoint(x: 0, y: 0)
          tableView.contentInset = UIEdgeInsets.zero
     }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
     
     // MARK: - Configure
    func configureView(){
     
        if forum?.userId == User.currentUser()?.userId{
            moreButton.isHidden = false
        } else{
            moreButton.isHidden = true
        }
    }
     
     func observeNotification(){
          NotificationCenter.default.addObserver(self, selector: #selector(QlueForumDetailController.downloadData), name: NSNotification.Name(rawValue: kForumEdited), object: nil)
     }
     
     func configureTableView(){
          tableView.register(UINib.init(nibName: "QlueForumDetailViewCell", bundle: nil), forCellReuseIdentifier: "DetailCell")
          tableView.register(UINib.init(nibName: "QlueFeedReviewViewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
          
          
          tableView.register(UINib.init(nibName: "CommentHeaderViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "CommentHeaderCell")
          
          tableView.register(UINib.init(nibName: "CommentFooterViewCell", bundle: nil), forCellReuseIdentifier: "CommentFooterCell")
          tableView.register(UINib.init(nibName: "CommentListCell", bundle: nil), forCellReuseIdentifier: "CommentListCell")
          tableView.register(UINib.init(nibName: "CreateCommentViewCell", bundle: nil), forCellReuseIdentifier: "CreateCommentCell")
          
          
          
          tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
          
          
     }
     
     
     // MARK: - Data
     func deleteForum(){
          guard let catId = forum?.categoryId else { return }
          
          Util.showSimpleCancelAlert("Are you sure want to delete this?", vc: self) { (action) in
               
               DejalBezelActivityView.addActivityView(for: self.tableView)
               
               let req = Engine.shared.deleteForum(withForumId: self.id, categoryId: catId)
               Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
                    DejalBezelActivityView.remove()
                    }, completion: { (data, success, fail) in
                         if success == true{
                              let objectNotif = [
                                   "id" : self.id
                              ]
                              NotificationCenter.default.post(name: NSNotification.Name(rawValue: kForumDeleted), object: objectNotif)
                              self.dismiss(animated: true, completion: nil)
                              self.navigationController?.popViewController(animated: true)
                         }
                    })
          }
     
     }
     
     func downloadData(){
          let req = Engine.shared.getDetailForum(withId: id)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               }, completion:  { (data, success, fail) in
                    if success == true{
                         self.forum = QlueDiskusi.init(fromJson: data)
                         
                         self.configureView()
                         self.tableView.reloadData()
                    }
          })
     }
     
     func loadMoreComment(){
          commentPage += 1
          downloadComment()
     }
     
     func downloadComment(_ sortButton: UIButton? = nil, sortIndicator: UIActivityIndicatorView? = nil){
          let req = Engine.shared.getCommentDiskusiList(id, sortBy: commentSort, page: (commentPage * 25))
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               sortIndicator?.stopAnimating()
               sortButton?.isHidden = false
               
               self.tableView.finishInfiniteScroll()
               
               }, completion:  { (data, success, fail) in
                    if success == true{
                         if self.commentPage == 0{
                            
                            self.commentFab.isHidden = true
                              self.comments = []
                         }
                         
                         if data.arrayValue.count == 0{
                              self.tableView.removeInfiniteScroll()
                         }
                         
                         for c in data.arrayValue{
                              self.comments.append(QlueComment.commentWithData(c))
                         }
                         
                         
                         self.tableView.reloadSections(NSIndexSet.init(index: 1) as IndexSet, with: .automatic)
                    }
          })
     }
     
     // MARK: UITableViewDataSource
     
     func numberOfSections(in tableView: UITableView) -> Int {
//          
          if tableView == voteTableView{
               return 1
          }
//
          return 2
     }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
          if let voteCount = forum?.votes.count, tableView == voteTableView{
               return voteCount
          }
          
          switch section {
          case 1:
               if comments.count < 1{
                    return 1
               } else if comments.count < 6{
                    allReviewIndex = comments.count + 1
                    return comments.count + 2
               } else if commentExpanded == false{
                    allReviewIndex = 6
                    return 5 + 2
               } else{
                    return comments.count + 1
               }
          default:
               return 1
          }
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
          if tableView == voteTableView{
               let cell = tableView.dequeueReusableCell(withIdentifier: "VoteCell", for: indexPath) as! QlueVoteItemCell
               
               let vote = forum?.votes[indexPath.row]
               
               if let voteId = forum?.myVoteId, voteId != ""{
                    
                    let voteCount = vote?.count ?? 0
                    let totalVote = forum?.totalVote ?? 0
                    let percent: CGFloat = CGFloat(voteCount) / CGFloat(totalVote)
                    cell.percent.text = "\(Int(percent * 100))%"
                    
                    let voteWidth = tableView.bounds.width - 16
                    cell.trailingProgress.constant = (1 - percent) * voteWidth
               }else{
                    cell.trailingProgress.constant = 0
                    cell.percent.text = ""
               }
               
               cell.voteLabel.text = vote?.name
               
               cell.delegate = self
               
               return cell
          }
          
          let lastRow = tableView.numberOfRows(inSection: 1) - 1
          
          switch (indexPath.section, indexPath.row) {
          case (0,0):
               let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! QlueForumDetailViewCell
               
               if (forum?.isVote == true) && (forum?.votes.count > 0){
                    cell.cellVoteHeight.constant = 40  + (CGFloat(forum!.votes.count) * 40)
                    
                    let totalVotes = forum?.totalVote ?? 0
                    cell.cellVoteTotal.text = "\(totalVotes) Total Votes"
                    self.voteTableView = cell.cellVoteTableView
                    voteTableView.register(UINib.init(nibName: "QlueVoteItemCell", bundle: nil), forCellReuseIdentifier: "VoteCell")
                    voteTableView.dataSource = self
                    voteTableView.delegate = self
                    voteTableView.reloadData()
               } else{
                    cell.cellVoteHeight.constant = 0
               }
               
               if let avatar = forum?.avatar?.encodeURL(){
                    cell.cellAvatar.sd_setImage(with: avatar)
               }
               cell.cellName.text = forum?.username
               
               let date = Date().getDateFromTimestamp(forum?.timestamp)
               cell.cellTime.text = date?.timeAgoWithNumericDates(true)
               cell.cellTitle.text = forum?.title
               cell.cellDesc.text = forum?.desc
               cell.cellSeenBy.setTitle(forum?.viewCount, for: UIControlState())
               
               arrowLeft = cell.cellArrowLeft
               arrowRight = cell.cellArrowRight
               
               cell.cellArrowLeft.isHidden = true
               cell.delegate = self
               
               if forum?.files.count <= 1 {
                    cell.cellArrowRight.isHidden = true
                    cell.cellArrowLeft.isHidden = true
               }
               if forum?.files.count > 0{
                    cell.cellHeightCollectionView.constant = (tableView.bounds.width / 4) * 3
               
               } else{
                    cell.cellHeightCollectionView.constant = 30
               }
               
               
               
               cell.cellCollectionView.register(UINib.init(nibName: "QlueForumPhotoViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCell")
               cell.cellCollectionView.delegate = self
               cell.cellCollectionView.dataSource = self
               cell.cellCollectionView.reloadData()
               let layout = UICollectionViewFlowLayout()
               layout.scrollDirection = .horizontal
               let size = (tableView.bounds.width / 4) * 3
               layout.itemSize = CGSize.init(width: tableView.bounds.width, height: size)
               layout.minimumInteritemSpacing = 0
               layout.minimumLineSpacing = 0
               cell.cellCollectionView.setCollectionViewLayout(layout, animated: false)
               
               return cell
          case (1,0):
               let cell = tableView.dequeueReusableCell(withIdentifier: "CreateCommentCell", for: indexPath) as! CreateCommentViewCell
               
               cell.delegate = self
               
               return cell
          case (1, lastRow):
               
               if commentExpanded == true{
                    fallthrough
               } else{
               
               let footer = tableView.dequeueReusableCell(withIdentifier: "CommentFooterCell", for: indexPath) as! CommentFooterViewCell
               
               footer.delegate = self
               
                    return footer
               }
               
          default:
               let cell = tableView.dequeueReusableCell(withIdentifier: "CommentListCell", for: indexPath) as! CommentListCell
               
               let comment = comments[indexPath.row - 1]
               
               if let image = comment.avatar?.encodeURL(){
                    cell.cellImageView.sd_setImage(with: image as URL!)
               }
               
               cell.cellName.text = comment.username
               cell.cellTime.text = comment.timestamp?.timeAgoWithNumericDates(false)
               
               let  mentionText = Util.replaceIdWithUsername(comment)
               cell.cellComment.text = Util.convertEmojiFromUnicode(mentionText)
               
               cell.cellComment.handleMentionTap { (username) in
                    let atSelected = comment.at.filter({$0.username == username}).first
                    self.presentOtherProfile(atSelected?.idno)
               }
               
               if let image = comment.image?.encodeURL(), image.absoluteString != "" {
                    cell.cellImageHeight.constant = 110
                    cell.cellImage.sd_setImage(with: image as URL!)
               }
               
               cell.delegate = self
               
               return cell
          }
          
          
          //          if indexPath.row == 0{
          //
          //          } else{
          //               let cell = tableView.dequeueReusableCellWithIdentifier("ReviewCell", forIndexPath: indexPath) as! QlueFeedReviewViewCell
          //
          //
          ////               cell.delegate = self
          //
          //               let commentCount = forum?.commentCount ?? "0"
          //               if let count = Int(commentCount)
          //               {
          //                    cell.cellTableViewHeight.constant = CGFloat.init(count) * 80
          //               } else{
          //                    cell.cellTableViewHeight.constant = 0
          //               }
          //
          //               cell.cellTitle.text = "Reviews (" + commentCount + ")"
          //
          //               cell.comments = comments
          //               cell.cellTableView.reloadData()
          //               return cell
          //          }
     }
     
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//          
          if tableView == voteTableView{
               return nil
          }
          
          switch section {
          case 1:
               let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentHeaderCell") as! CommentHeaderViewCell
               
               let commentCount = forum?.commentCount ?? "0"
               header.cellTitle.text = "Reviews (" + commentCount + ")"
               header.delegate = self
               
               if commentSort == "old"{
                    header.sortButton.setImage(UIImage.init(named: "ic_sortby_old"), for: UIControlState())
               }else {
                    header.sortButton.setImage(UIImage.init(named: "ic_sortby_new"), for: UIControlState())
               }
               
               return header
          default:
               return nil
          }
          
     }
     
     func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
          
          if tableView == voteTableView{
               return nil
          }
          
          switch section {
          case 1:
               let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentFooterCell") as! CommentFooterViewCell
               
               footer.delegate = self
               
               return footer
          default:
               return nil
          }
          
     }
     
     
     
     // MARK: - UITableViewDelegate
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//          if tableView == voteTableView{
//               let voteCount = forum?.votes[indexPath.row].count ?? 0
//               forum?.votes[indexPath.row].count = voteCount + 1
//               let totalVote = forum?.totalVote ?? 0
//               forum?.totalVote = totalVote + 1
//               
//               let selectedVote = forum?.votes[indexPath.row]
//               forum?.myVoteId = selectedVote?.id
//               forum?.myVoteName = selectedVote?.name
//               
//               voteTableView.reloadData()
//               
//          }
     }
     
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
          
          if tableView == voteTableView{
               return 0
          }
          
          switch section {
          case 1:
               return 60
          default:
               return 0
          }
     }
     
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
          return 0
     }
     

     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//          
          if tableView == voteTableView{
               return 40
          }
          
          switch (indexPath.section, indexPath.row) {
          case (0,0):
               var height: CGFloat = 8 + 45 + 8
               
               if forum?.files.count > 0{
                    height += (tableView.bounds.width / 4) * 3
               } else{
                    height += 30
               }
               if let title = forum?.title{
                    height += title.heightWithConstrainedWidth(tableView.bounds.width - 30, font: UIFont.init(name: QlueFont.ProximaNovaSoft, size: 17)!)
                    height += 8
               }
               
               if (forum?.isVote == true) && (forum?.votes.count > 0){
                    height += 40  + (CGFloat(forum!.votes.count) * 40)
               } else{
                    height += 0
               }
               
               if let desc = forum?.desc{
                    height += desc.heightWithConstrainedWidth(tableView.bounds.width - 30, font: UIFont.init(name: QlueFont.HelveticaNeue, size: 12)!)
               }
               
               height += 15 + 15
               
               
               return height
          case (1, 0), (1, allReviewIndex):
               return 55
          default:
//               if indexPath.row == tableView.numberOfRowsInSection(1) - 1{
//                    return 60
//               }
               
               let comment = comments[indexPath.row - 1]
               
               var height: CGFloat = 70
               
               let commentText = Util.convertEmojiFromUnicode(comment.comment)
               height += commentText.heightWithConstrainedWidth(tableView.bounds.width - 69, font: UIFont.init(name: QlueFont.HelveticaNeue, size: 12)!)
               
               if let image = comment.image?.encodeURL(), image.absoluteString != "" {
                    height += 110
               }
               
               return height
          }
          
     }
     
     // MARK: - Action
     @IBAction func addCommentTapped(_ sender: UIButton) {
          createComment()
     }
     
    @IBAction func moreTapped(_ sender: UIButton) {
        let more = UIAlertController.init(title: "More", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let editAction = UIAlertAction.init(title: "Edit", style: .default) { (action) in
            self.presentPostForum(withType: ForumPostType.editForum(forum: self.forum), forumType: self.forumType)
        }
        more.addAction(editAction)
        
        let deleteAction = UIAlertAction.init(title: "Delete", style: .destructive) { (action) in
            self.deleteForum()
        }
        more.addAction(deleteAction)
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        more.addAction(cancelAction)
        
        present(more, animated: true, completion: nil)
    }
     
     func createComment(){
          presentPostForum(withType: .comment(wid: forum?.id ?? ""), forumType: forumType, completion: {
               self.commentSort = "new"
               self.downloadComment()
          })
     }
     
     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
      }
      */
     
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate

extension QlueForumDetailController: UICollectionViewDataSource, UICollectionViewDelegate{
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return forum?.files.count ?? 0
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! QlueForumPhotoViewCell
          
          if let imgStr = forum?.files[indexPath.item].encodeURL(){
               cell.cellImage.sd_setImage(with: imgStr as URL!)
          }
     
          return cell
     }
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          
          if let url = forum?.files[indexPath.item]{
               presentPreviewImage(withUrlString: url)
          }
     }
     
}

// MARK: - QlueForumDetailViewCellDelegate
extension QlueForumDetailController: QlueForumDetailViewCellDelegate{
     func leftArrowTapped(_ cell: QlueForumDetailViewCell, button: UIButton) {
          guard let indexPath = cell.cellCollectionView.indexPathsForVisibleItems.first else{
               return
          }
          
          if indexPath.item - 1 <= 0{
               arrowLeft.isHidden = true
          } else{
               arrowLeft.isHidden = false
          }
          
          arrowRight.isHidden = false
          cell.cellCollectionView.scrollToItem(at: IndexPath.init(item: indexPath.item - 1, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
          
     }
     
     func rightArrowTapped(_ cell: QlueForumDetailViewCell, button: UIButton) {
          guard let indexPath = cell.cellCollectionView.indexPathsForVisibleItems.first else{
               return
          }
          if indexPath.item + 2 >= forum?.files.count{
               arrowRight.isHidden =  true
          } else{
               arrowRight.isHidden = false
          }
          
          arrowLeft.isHidden = false
          
          cell.cellCollectionView.scrollToItem(at: IndexPath.init(item: indexPath.item + 1, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
          
     }
     func seenByTapped(_ cell: QlueForumDetailViewCell) {
          
          presentSeenBy(withId: forum?.id ?? "", countSeen: forum?.viewCount ?? "0")
     }
}

// MARK: - Comment Delegate
extension QlueForumDetailController: CommentHeaderViewCellDelegate, CommentFooterViewCellDelegate, CreateCommentViewCellDelegate{
     func sortTapped(_ cell: CommentHeaderViewCell, button: UIButton) {
          commentPage = 0
          commentExpanded = false
          
          button.isHidden = true
          cell.sortIndicator.startAnimating()
          
          if commentSort == "old"{
               button.setImage(UIImage.init(named: "ic_sortby_new"), for: UIControlState())
               commentSort = "new"}
          else {
               button.setImage(UIImage.init(named: "ic_sortby_old"), for: UIControlState())
               commentSort = "old"
          }
          
          downloadComment(button, sortIndicator: cell.sortIndicator)
     }
     func readAllReview(_ cell: CommentFooterViewCell) {
          
          if comments.count > 5{
               
               commentFab.isHidden = false
               commentExpanded = true
               tableView.reloadSections(IndexSet.init(integer: 1), with: .none)
               tableView.addInfiniteScroll { (tableView) in
                    self.loadMoreComment()
               }
          }
     }
     func createTapped(_ cell: CreateCommentViewCell) {
          createComment()
     }
}

// MARK:  - QlueVoteItemCellDelegate
extension QlueForumDetailController: QlueVoteItemCellDelegate{
     func voteTapped(_ cell: QlueVoteItemCell) {
          guard let indexPath = voteTableView.indexPath(for: cell), forum?.myVoteId == ""  else { return }
          
          let voteCount = forum?.votes[indexPath.row].count ?? 0
          forum?.votes[indexPath.row].count = voteCount + 1
          let totalVote = forum?.totalVote ?? 0
          forum?.totalVote = totalVote + 1
          
          let selectedVote = forum?.votes[indexPath.row]
          forum?.myVoteId = selectedVote?.id ?? ""
          forum?.myVoteName = selectedVote?.name
          
          voteTableView.reloadData()
//          tableView.reloadRowsAtIndexPaths([NSIndexPath.init(forRow: 0, inSection: 0)], withRowAnimation: .None)
          
          let req = Engine.shared.postVoteForum(id, voteId: selectedVote?.id ?? "")
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               }, completion: { (data, success, fail) in
                    if success == true{
                         print("vote submitted")
                    }
               })
     }
}

// MARK: - CommentListCellDelegate
extension QlueForumDetailController: CommentListCellDelegate{
     func commentLongTapped(_ cell: CommentListCell) {
          guard let indexPath = tableView.indexPath(for: cell) else { return }
          
          let comment = comments[indexPath.row - 1]
          
          let alert = UIAlertController.init(title: "", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
          
          let copyAction = UIAlertAction.init(title: "Copy", style: .default) { (action) in
               UIPasteboard.general.string = comment.comment ?? ""
          }
          alert.addAction(copyAction)
          
          
          
          let deleteAction = UIAlertAction.init(title: "Delete", style: .default) { (action) in
               self.deleteComment(withIndex: indexPath.row - 1, section: indexPath.section)
          }
          if comment.userId == User.currentUser()?.userId{
               alert.addAction(deleteAction)
          }
          
          let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
          alert.addAction(cancelAction)
          
          present(alert, animated: true, completion: nil)
          
     }
     
     func deleteComment(withIndex index: Int, section: Int){
          
          let comment = comments[index]
          
          let alert = UIAlertController.init(title: "Delete", message: "Are you sure want to delete this?", preferredStyle: .alert)
          let ok = UIAlertAction.init(title: "Yes", style: .default) { (action) in
               
               self.comments.remove(at: index)
               self.tableView.reloadData()
               
               let req = Engine.shared.deleteComment(withCommentId: comment.commentId ?? "", type: "forum")
               
               
               Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
                    
                    }, completion: { (data, success, fail) in
                         
               })
          }
          
          alert.addAction(ok)
          let no = UIAlertAction.init(title: "No", style: .cancel, handler: nil)
          alert.addAction(no)
          
          present(alert, animated: true, completion: nil)
          
          
     }
     
     func commentImageTapped(_ cell: CommentListCell) {
          guard let indexPath = tableView.indexPath(for: cell), let imageUrl = comments[indexPath.row - 1].image else { return }
          presentPreviewImage(withUrlString: imageUrl)
     }
}
// MARK: - Navigation
extension UIViewController {
     func presentForumDetail(forumId id: String, forumType: ForumType){
          if let detail = forumStoryboard.instantiateViewController(withIdentifier: "ForumDetail") as? QlueForumDetailController{
               detail.id = id
               detail.forumType = forumType
               present(detail, animated: true, completion: nil)
          }
    }
}
