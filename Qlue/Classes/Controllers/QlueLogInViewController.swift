//
//  QlueLogInViewController.swift
//  Qlue
//
//  Created by Nurul on 5/25/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit
import SwiftyJSON
import Alamofire

class QlueLogInViewController: QlueViewController, GIDSignInUIDelegate, GIDSignInDelegate {
     
    @IBOutlet weak var loginWithEmail: UIButton!
     @IBOutlet weak var signUpEmailButton: UIButton!
    @IBOutlet weak var loginWithFacebook: UIButton!
    @IBOutlet weak var loginWithGoogle: UIButton!
    
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          view.setNeedsLayout()
          view.layoutIfNeeded()
          
          setupView()
          
          // Sign In With Google delegate setup
          GIDSignIn.sharedInstance().uiDelegate = self
          GIDSignIn.sharedInstance().delegate = self
          
          //notif
          NotificationCenter.default.addObserver(self, selector: #selector(QlueLogInViewController.completeRegistration(_:)), name: NSNotification.Name(rawValue: kCompleteRegistrationDone), object: nil)
          
     }
     
     override func viewWillAppear(_ animated: Bool) {
          self.navigationController?.isNavigationBarHidden = true
     }
     
     override func viewWillDisappear(_ animated: Bool) {
          self.navigationController?.isNavigationBarHidden = false
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Helpers
     fileprivate func setupView(){
          
        loginWithEmail.cornerRadius(withRadius: 25, andBorderWithColor: UIColor.white, borderWidth: 2)
        loginWithGoogle.cornerRadius(withRadius: 25, andBorderWithColor: UIColor.white, borderWidth: 2)
        loginWithFacebook.cornerRadius(withRadius: 25, andBorderWithColor: UIColor.white, borderWidth: 2)
     }
     
     // MARK: - GIDSignInDelegate
     
     func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
          if (error == nil) {
               
               let url = "https://www.googleapis.com/oauth2/v3/userinfo?access_token=\(user.authentication.accessToken)"
               Alamofire.request(url, method: .get).response(completionHandler: { (defaultDataResponse) in
                    if let result = defaultDataResponse.data {
                         let json = JSON.init(data: result)
                         
                         print(json)
                         
                         var gender: Gender = .Male
                         if let genderStr = json["gender"].string, let uGender = Gender(rawValue: genderStr){
                              gender = uGender
                         }
                         
                         self.signInWithSocialMedia(QlueRegistration.init(
                              username: json["email"].stringValue,
                              phone: 0,
                              email: json["email"].stringValue,
                              gender: gender,
                              fullname: json["name"].stringValue,
                              dob: nil,
                              idSocmed: user.userID,
                              socialData: json.dictionaryObject,
                              type: .google))
                         
                    } else if let error = defaultDataResponse.error {
                         print(error.localizedDescription)
                    }
               })
               
          } else {
               print("\(error.localizedDescription)")
          }
     }
     
     func signIn(_ signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                 withError error: NSError!) {
          // Perform any operations when the user disconnects from app here.
          // ...
     }
     
     
     // MARK: - Action
     func completeRegistration(_ sender: Notification){
          self.performSegue(withIdentifier: "TipsSegue", sender: self)
     }
     @IBAction func loginWithFacebook(_ sender: UIButton) {
          
          
          let login: FBSDKLoginManager = FBSDKLoginManager.init()
          login.logIn(
               withReadPermissions: ["public_profile", "email", "user_friends"],
               from: self) { (result, error) in
                    if let error = error {
                         print(error.localizedDescription)
                    } else if (result?.isCancelled)! {
                         print("Cancelled")
                    } else {
                         print(result?.token.tokenString)
                         
                         FBSDKGraphRequest.init(graphPath: "me?fields=id,email,gender,name", parameters: nil).start(completionHandler: { (connection, result, error) in
                              if let error = error{
                                   print(error.localizedDescription)
                              } else{
                                   print(result)
                                   let json = JSON(result)
                                   
                                   var gender: Gender = .Male
                                   if let genderStr = json["gender"].string, let uGender = Gender(rawValue: genderStr){
                                        gender = uGender
                                   }
                           
                                   let index = json["id"].stringValue.index(json["id"].stringValue.startIndex, offsetBy: 4)
                                   var username = "QlueUser" + json["id"].stringValue.substring(to: index)
                                   if let name = json["name"].string {
                                        username = name.replacingOccurrences(of: " ", with: " ")
                                   }
                                   
                                   
                                   self.signInWithSocialMedia(
                                        QlueRegistration.init(
                                             username: json["email"].stringValue,
                                             phone: 0,
                                             email: json["email"].stringValue,
                                             gender: gender,
                                             fullname: json["name"].stringValue,
                                             dob: nil,
                                             idSocmed: json["id"].stringValue,
                                             socialData: json.dictionaryObject as [String : AnyObject]?,
                                             type: .facebook))
                                   
                              }
                         })
                         
                    }
          }
          
     }
     
     @IBAction func loginWithGoogle(_ sender: UIButton) {
          GIDSignIn.sharedInstance().signOut()
          GIDSignIn.sharedInstance().signIn()
          
     }
     
     @IBAction func loginWithTwitter(_ sender: UIButton) {
          Twitter.sharedInstance().logIn(withMethods: [.webBased]) { (session, error) in
               if let unwrappedSession = session {
                    
                    let client = TWTRAPIClient.withCurrentUser()
                    let request = client.urlRequest(withMethod: "GET", url: "https://api.twitter.com/1.1/account/verify_credentials.json", parameters: ["include_email": "true", "skip_status": "true"], error: nil)
                    
                    client.sendTwitterRequest(request, completion: { (response, data, error) in
                         if let data = data{
                              let json = JSON.init(data: data)
                              
                              print(json)
                              
                              self.signInWithSocialMedia(QlueRegistration.init(
                                   username: json["screen_name"].stringValue,
                                   phone: 0,
                                   email: "-",
                                   gender: Gender.Male,
                                   fullname: json["name"].stringValue,
                                   dob: nil,
                                   idSocmed: unwrappedSession.userID,
                                   socialData: json.dictionaryObject as [String : AnyObject]?,
                                   type: .twitter))
                              
                              
                         }
                    })
               } else {
                    NSLog("Login error: %@", error!.localizedDescription);
               }
               
          }
          
     }
     
     // MARK: - Data
     func signInWithSocialMedia(_ reg: QlueRegistration){
     
          DejalBezelActivityView.addActivityView(for: self.view)
          
          let idSocmed = reg.idSocmed ?? ""
          let password = "password" + idSocmed
          let pwdMd5 = password.md5
        
          let req = Engine.shared.registration(reg, pwd: pwdMd5)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               DejalActivityView.remove()
               
               }, completion: { (data, success, fail) in
                    print(data)
                    if success == true{
                         if let username = data["response"].string{
                              self.signIn(withUsername: username, password: pwdMd5)
                         }
                    } else if fail == true{
                         if data["code"].stringValue == "100"{
                              reg.email = ""
                         } else if data["code"].stringValue == "200"{
                              reg.username = ""
                         } else if data["code"].stringValue == "300"{
                              reg.email = ""
                              reg.username = ""
                         }
                         self.presentCompleteRegistration(withRegistration: reg)
                    }
          }) {
        }
          
     }
     
     fileprivate func signIn(withUsername username: String, password: String){
         DejalBezelActivityView.addActivityView(for: self.view)
          
          Engine.shared.loginWithEmail(username, pwd: password){ (result, error) in
               
               DejalActivityView.remove()
               
               if let res = result as? String{
                    
                    let data = JSON.parse(res)
                    
                    if data["success"].string == "false"{
                         Util.showSimpleAlert(data["message"].string, vc: self, completion: nil)
                    } else {
                         
                         let appDelegate = UIApplication.shared.delegate as! AppDelegate
                         appDelegate.registerForRemoteNotifications()
                         
                         self.performSegue(withIdentifier: "TipsSegue", sender: self)
                         
                    }
                    
               } else if let error = error{
                    print(error.localizedDescription)
               } else{}
               
          }
          
     }
     
     // MARK: - Navigation
     @IBAction func unwindToLogin(_ segue: UIStoryboardSegue) {
          
     }

     
}
