//
//  QlueCompleteRegistrationController.swift
//  Qlue
//
//  Created by Nurul on 8/17/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON

let kCompleteRegistrationDone = "kCompleteRegistrationDone"

class QlueCompleteRegistrationController: QlueViewController {
    
    @IBOutlet weak var emailBorder: UIView!
    @IBOutlet weak var emailHeight: NSLayoutConstraint!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var usernameBorder: UIView!
    @IBOutlet weak var usernameHeight: NSLayoutConstraint!
    @IBOutlet weak var usernameTextfield: UITextField!
     
     // Var from social media
     var registration: QlueRegistration!

     override func viewDidLoad() {
          super.viewDidLoad()
          
          emailTextfield.text = registration.email
          usernameTextfield.text = registration.username
        
        if registration.email != ""{
            emailHeight.constant = 0
            emailBorder.isHidden = true
        } else{
            emailHeight.constant = 30
            emailBorder.isHidden = false
        }
        
//        if registration.username != ""{
////            usernameHeight.constant = 0
////            usernameBorder.hidden = true
//        } else{
            usernameHeight.constant = 30
            usernameBorder.isHidden = false
//        }
        
        
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Action
     @IBAction func cancelTapped(_ sender: UIButton) {
          dismiss(animated: true, completion: nil)
    }
    @IBAction func okTapped(_ sender: UIButton) {
     registration.email = emailTextfield.text ?? ""
     registration.username = usernameTextfield.text ?? ""
     signInWithSocialMedia(registration)
    }
    
     // MARK: - Data
     func signInWithSocialMedia(_ reg: QlueRegistration){
          DejalBezelActivityView.addActivityView(for: self.view)
          
          let idSocmed = reg.idSocmed ?? ""
          let password = "password" + idSocmed
          let pwdMd5 = password.md5
          
          let req = Engine.shared.registration(reg, pwd: pwdMd5)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               
               DejalActivityView.remove()
               
               }, completion: { (data, success, fail) in
                    print(data)
                    if success == true{
                         self.signIn(withUsername: reg.username, password: pwdMd5)
                    } else if fail == true{
                         if data["code"].stringValue == "100"{
                              self.emailTextfield.text = ""
                         } else if data["code"].stringValue == "200"{
                              self.usernameTextfield.text = ""
                         } else if data["code"].stringValue == "300"{
                              self.emailTextfield.text = ""
                              self.usernameTextfield.text = ""
                         }
                         
                         Util.showSimpleAlert(data["message"].string, vc: self, completion: nil)
                    }
          }) {
               //
          }
          
     }
     
     fileprivate func signIn(withUsername username: String, password: String){
          DejalBezelActivityView.addActivityView(for: self.view)
          
          Engine.shared.loginWithEmail(username, pwd: password){ (result, error) in
               
               DejalActivityView.remove()
               
               if let res = result as? String{
                    
                    let data = JSON.parse(res)
                    
                    if data["success"].string == "false"{
                         Util.showSimpleAlert(data["message"].string, vc: self, completion: nil)
                    } else{
                    
                         self.dismiss(animated: true, completion: {
                              NotificationCenter.default.post(name: Notification.Name(rawValue: kCompleteRegistrationDone), object: nil)
                         })
                         
                    }
                    
               } else if let error = error{
                    print(error.localizedDescription)
               } else{}
               
          }
          
     }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension QlueViewController{
     func presentCompleteRegistration(withRegistration reg: QlueRegistration){
        if let complete = mainStoryboard.instantiateViewController(withIdentifier: "CompleteRegistration") as? QlueCompleteRegistrationController{
            complete.registration = reg
            
            semiModalWithViewController(complete)
            
            
        }
    }
}
