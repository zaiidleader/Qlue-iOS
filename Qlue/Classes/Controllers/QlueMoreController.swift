//
//  QlueMoreController.swift
//  Qlue
//
//  Created by Nurul on 6/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit


class QlueMoreController: QlueViewController {

    @IBOutlet weak var chatButton: UIButton!
    
    fileprivate struct MoreCell {
        let image: UIImage?
        let title: String?
        let timer: Bool!
        var imageLink: String?
        var selectHandler: (_ viewController: QlueMoreController) -> Void = { (viewController: QlueMoreController) -> Void in }
        
        init(imageName: String = "", title: String?, timer: Bool = false) {
            self.image = UIImage(named: imageName)
            self.title = title
            self.timer = timer
        }
        init(imageName: String = "", title: String?, selectHandler: @escaping (_ viewController: QlueMoreController) -> Void) {
            self.image = UIImage(named: imageName)
            self.title = title
            self.timer = false
            self.selectHandler = selectHandler
        }
        init(imageLink: String?, title: String?, selectHandler: @escaping (_ viewController: QlueMoreController) -> Void) {
            self.image = UIImage(named: "")
            self.imageLink = imageLink
            self.title = title
            self.timer = false
            self.selectHandler = selectHandler
        }
    }
    
    fileprivate let pointCell : [MoreCell] = [
        MoreCell(imageName: "btn_shop", title: "Shop", selectHandler: { (viewController) -> Void in
            viewController.performSegue(withIdentifier: "ShopSegue", sender: nil)
        }),
        MoreCell(imageName: "btn_discusstion", title: "Discussion", selectHandler: { (viewController) -> Void in
            
        }),
        MoreCell(imageName: "btn_invite", title: "Invite", selectHandler: { (viewController) -> Void in
            viewController.performSegue(withIdentifier: "InviteSegue", sender: nil)
        }),
        MoreCell(imageName: "btn_maps", title: "Map", selectHandler: { (viewController) -> Void in
            viewController.performSegue(withIdentifier: "MapsSegue", sender: nil)
        }),
        MoreCell(imageName: "btn_trophy", title: "Ranking", selectHandler: { (viewController) -> Void in
            viewController.performSegue(withIdentifier: "RangkingSegue", sender: nil)
        }),
        MoreCell(imageName: "btn_favorite", title: "Favorite", selectHandler: { (viewController) -> Void in
            viewController.presentListFavorite(viewController.navigationController)
        }),
        MoreCell(imageName: "btn_help", title: "Help", selectHandler: { (viewController) -> Void in
            
        }),
        MoreCell(imageName: "btn_setting", title: "Settings", selectHandler: { (viewController) -> Void in
            viewController.presentSettings(viewController.navigationController, settingItem: nil)
        }),
//        MoreCell(imageName: "btn_promocode", title: "Promo Code", timer: true),
//        MoreCell(imageName: "btn_setting", title: "Logout")
    ]
    
    fileprivate let moreAppsCell : [MoreCell] = [
        MoreCell(
            imageLink: "http://is1.mzstatic.com/image/thumb/Purple71/v4/0d/3c/db/0d3cdb33-3f98-5d4a-d06e-40391f385dd9/source/100x100bb.jpg",
            title: "Qlue Play : Minipolis",
            selectHandler: { (viewController) in
                if let url = URL(string: "https://itunes.apple.com/id/app/qlue-minipolis/id1149210716?mt=8") {
                    UIApplication.shared.openURL(url)
                }
        })
//        MoreCell(title: "Qlue Safe"),
//        MoreCell(title: "Qlue Transit (Jakarta)")
    ]
    
    
//    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureChatButton(chatButton)
        
        user = User.currentUser()
        
        // Do any additional setup after loading the view.
//        tableView.registerNib(UINib.init(nibName: "QlueMHeaderViewCell", bundle: nil), forCellReuseIdentifier: "HeaderCell")
//        tableView.registerNib(UINib.init(nibName: "QlueMoreViewCell", bundle: nil), forCellReuseIdentifier: "MoreCell")
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(QlueMoreController.userDidUpdate(_:)), name: NSNotification.Name(rawValue: kUserDidUpdateNotification), object: nil)
    }
     
     override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//          tableView.reloadSections(NSIndexSet.init(index: 0), withRowAnimation: .None)
        collectionView.reloadItems(at: [IndexPath(item: 0, section: 0)])
     }
     
     
     override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
          
//          tableView.contentOffset = CGPoint(x: 0, y: 0)
//          tableView.contentInset = UIEdgeInsetsZero
     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func userDidUpdate(_ sender: Notification) {
//        tableView.reloadData()
        collectionView.reloadItems(at: [IndexPath(item: 0, section: 0)])
    }

}

// MARK: - UICollectionViewDataSource
extension QlueMoreController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return pointCell.count + 1
        }
        else {
            return moreAppsCell.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreCellId", for: indexPath)
            
            guard let imageView = cell.viewWithTag(101) as? UIImageView, let label = cell.viewWithTag(102) as? UILabel else {
                return cell
            }
            
            if indexPath.item == 0 {
                if let avatar = user?.avatar?.encodeURL(){
                    imageView.sd_setImage(with: avatar as URL!)
                }
                label.text = "Profile"
            }
            else {
                let data = pointCell[indexPath.item - 1]
                imageView.image = data.image
                label.text = data.title
            }
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AppCellId", for: indexPath)
            
            guard let imageView = cell.viewWithTag(101) as? UIImageView, let label = cell.viewWithTag(102) as? UILabel else {
                return cell
            }
            
            let data = moreAppsCell[indexPath.item]
            label.text = data.title
            imageView.image = data.image
            if let urlString = data.imageLink {
                if let url = URL(string: urlString) {
                    imageView.sd_setImage(with: url)
                }
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderViewId", for: indexPath)
        return view
    }
}

// MARK: - UICollectionViewDelegate
extension QlueMoreController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        if indexPath.section == 0 {
            if indexPath.item == 0 {
                self.presentProfileDetail(self.navigationController)
            }
            else {
                
                let data = pointCell[indexPath.item - 1]
                data.selectHandler(self)
            }
        }
        else {
            let data = moreAppsCell[indexPath.item]
            data.selectHandler(self)
        }
    }
}

// MARK: - UICollectionViewDataSource
extension QlueMoreController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        if indexPath.section == 0 {
            return CGSize(width: floor(width / 3.0), height: floor(width / 3))
        }
        else {
            return CGSize(width: width, height: 65)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if section == 0 {
            return CGSize.zero
        }
        else {
            return CGSize(width: 40, height: 40)
        }
    }
}

// MARK: - UITableViewDataSource
extension QlueMoreController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: // Profile User
            return 1
        case 1: // Point
            return pointCell.count
        default: // More Apps
            return moreAppsCell.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.section == 0 { // Profile User
//            let cell = tableView.dequeueReusableCellWithIdentifier("HeaderCell", forIndexPath: indexPath) as! QlueMHeaderViewCell
//            
//            cell.cellGemButton.setTitle(user?.diamond, forState: .Normal)
//            
//            if let banner = user?.banner?.encodeURL(){
//                cell.cellBanner.sd_setImageWithURL(banner)
//            }
//            
//            if let ava = user?.avatar?.encodeURL(){
//                cell.cellImage.sd_setImageWithURL(ava)
//            }
//
//            cell.cellName.text = user?.username
//            cell.cellLevel.text = user?.currLevel
//            
//            if let follower = user?.followerCount{
//                cell.cellFollowersButton.setTitle("\(follower) Followers", forState: .Normal)
//            }
//            
//            if let following = user?.followingCount{
//                cell.cellFollowingButton.setTitle("\(following) Following", forState: .Normal)
//            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell", for: indexPath) as! QlueMoreViewCell
            
            if let ava = user?.avatar?.encodeURL(){
                cell.cellImage.sd_setImage(with: ava as URL!)
            }
            let attText = NSMutableAttributedString()
            if let username = user?.username {
                attText.append(NSAttributedString(string: "\(username) ", attributes: [NSFontAttributeName: UIFont(name: QlueFont.ProximaNovaSoft, size: 15)!, NSForegroundColorAttributeName: UIColor.black]))
            }
            if let currLevel = user?.currLevel {
                attText.append(NSAttributedString(string: "\n\(currLevel)", attributes: [NSFontAttributeName: UIFont(name: QlueFont.ProximaNovaRegular, size: 13)!, NSForegroundColorAttributeName: UIColor.darkGray]))
            }
            cell.cellTitle.attributedText = attText
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell", for: indexPath) as! QlueMoreViewCell
            
            var data: MoreCell!
            if indexPath.section == 1 { // Point Cell
                data = pointCell[indexPath.row]
            } else{ // More Apps
                data = moreAppsCell[indexPath.row]
            }
            
            cell.cellImage.image = data.image
            cell.cellTitle.text = data.title
            if data.timer == true{
                cell.cellTimer.text = "03:45:05"
            } else{
                cell.cellTimer.text = ""
            }
            
            return cell
        }
    }
}

// MARK: - UITableViewDelegate

extension QlueMoreController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
          self.presentProfileDetail(self.navigationController)
        case (1, 1):
            self.performSegue(withIdentifier: "MapsSegue", sender: self)
        case (1, 2):
            self.presentListFavorite(self.navigationController)
        case (1, 3):
            self.performSegue(withIdentifier: "InviteSegue", sender: self)
        case (1, 4):
            self.performSegue(withIdentifier: "RangkingSegue", sender: self)
        case (1, 5):
          self.performSegue(withIdentifier: "ShopSegue", sender: self)
        case (1, 6):
          self.presentSettings(self.navigationController, settingItem: nil)
        case (1, 7):
          Engine.shared.logout()
          
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            return UIView()
            
        case 1: // Point Cell
//            let view = NSBundle.mainBundle().loadNibNamed("QlueMPointHeaderView", owner: nil, options: nil).first as! QlueMPointHeaderView
//            
//            view.viewPointButton.setTitle(user?.point, forState: .Normal)
//            view.viewGemButton.setTitle(user?.diamond, forState: .Normal)
//            
//            if let wait = user?.reportWait{
//                view.waitingButton.setTitle(String(wait), forState: .Normal)
//            }
//            
//            if let process = user?.reportProcess{
//                view.progressButton.setTitle(String(process), forState: .Normal)
//            }
//            
//            if let complete = user?.reportComplete{
//                view.completedButton.setTitle(String(complete), forState: .Normal)
//            }
            let view = UIView()
            view.backgroundColor = UIColor.clear
            
            return view
            
        case 2:// More Apps
            let view = Bundle.main.loadNibNamed("QlueMAppsHeaderView", owner: nil, options: nil)?.first as! QlueMAppsHeaderView
            return view
            
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0.00001
            
        case 1: // Point Cell
//            return 85
            return 10
            
        case 2: // More Apps
            return 10
            
        default:
            return 0.00001
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0: // Profile User
//            return (tableView.bounds.width / 3) * 2
            return 60
        case 1: // Point Cell
          if indexPath.row == 0 {
               return 0
          }
          fallthrough
        default: // Point Cell, More Apps
            return 50
        }
    }
}
