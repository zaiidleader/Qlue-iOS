//
//  QlueAvatarTabController.swift
//  Qlue
//
//  Created by Nurul on 7/1/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import CarbonKit

class QlueAvatarTabController: QlueViewController, CarbonTabSwipeNavigationDelegate {
     
     let items: [String] = ["PUBLIC" /*, "STAFF"*/]
     
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          setupTabView()
          
          // Do any additional setup after loading the view.
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
         
     // MARK: - Private Func
     fileprivate func setupTabView(){
          let carbonTabSwipeNavigation = CarbonTabSwipeNavigation.init(items: items, delegate: self)
          
          carbonTabSwipeNavigation.insert(intoRootViewController: self)
          carbonTabSwipeNavigation.toolbarHeight.constant = 40.0
          carbonTabSwipeNavigation.setIndicatorColor(kCreamColor)
          
          carbonTabSwipeNavigation.toolbar.setBackgroundImage(UIImage(named: "bgNavbar"), forToolbarPosition: UIBarPosition.any, barMetrics: UIBarMetrics.default)
          carbonTabSwipeNavigation.toolbar.shadowWithColor(UIColor.clear)
          
          
          for i in 0..<items.count{
               carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(UIScreen.main.bounds.width , forSegmentAt: i)
          }
          
          if let barFont = UIFont(name: QlueFont.ProximaNovaSoft, size: 12.0) {
               carbonTabSwipeNavigation.carbonSegmentedControl?.setTitleTextAttributes([NSForegroundColorAttributeName: kCreamColor
                    ,NSFontAttributeName:barFont], for: UIControlState())
               carbonTabSwipeNavigation.carbonSegmentedControl?.setTitleTextAttributes([NSForegroundColorAttributeName: kCreamColor
                    ,NSFontAttributeName:barFont], for: .selected)
               
          }
     }
     
     // MARK: - CarbonTabSwipeNavigationDelegate
     func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
          
          if let ava = profileStoryboard.instantiateViewController(withIdentifier: "AvatarUser") as? QlueAvatarController{
               
               switch index {
               case 0:
                    ava.isPublic = true
               default:
                    ava.isPublic = false
                    
               }
               
               return ava
          }
          return UIViewController()
          
     }
}
