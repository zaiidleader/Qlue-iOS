//
//  QlueChatController.swift
//  Qlue
//
//  Created by Nurul on 6/13/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueChatController: QlueViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
     var chats: [QlueChat] = []
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

     configureTableView()
     
        // Do any additional setup after loading the view.
     
     DejalActivityView.addActivityView(for: self.tableView)
     self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          
          downloadData()
          tableView.reloadData()
     }
    
     // MARK: - Configure
     func configureTableView(){
          
          tableView.rowHeight = 50
          
          tableView.register(UINib.init(nibName: "QlueChatViewCell", bundle: nil), forCellReuseIdentifier: "ChatCell")
          tableView.register(UINib.init(nibName: "QlueChatGroupCell", bundle: nil), forCellReuseIdentifier: "GroupCell")
     }

     // MARK: - Data
     func downloadData(){
          let req = Engine.shared.getListChat()
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               },completion: { (data, success, fail) in
                    if success == true{
                         self.chats = []
                         for d in data.arrayValue{
                              self.chats.append(QlueChat.init(fromJson: d))
                         }
                         self.tableView.reloadData()
                    }
               })
          
          
     }
     
     func readWithGroupid(_ groupId : String){
          let readReq = Engine.shared.chatRead(withGroupId: groupId)
          Engine.shared.parseJsonWithReq(readReq, controller: self, loadingStop: {
               
               }, completion: { (data, success, fail) in
                    print("read")
               }) { 
                    
          }
     }
    
    // MARK: - Action
    @IBAction func createTapped(_ sender: UIButton) {
        presentChatSearch()
    }
    
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        
        let chat = chats[indexPath.row]
     
     if chat.isGroup == true{
          let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCell", for: indexPath) as! QlueChatGroupCell
          
          if chat.status == "read"{
               cell.backgroundColor = UIColor.white
          } else{
               cell.backgroundColor = kBlueE0F5FDColor

          }
          
          var i = 0
          var username = ""
          for member in chat.members.prefix(4){
               i += 1
               if let url = member.avatar?.encodeURL(){
                    if i == 1{
                         cell.cellImage1.sd_setImage(with: url as URL!)
                    } else if i == 2{
                         cell.cellImage2.sd_setImage(with: url as URL!)
                    } else if i == 3{
                         cell.cellImage3.sd_setImage(with: url as URL!)
                    } else if i == 4{
                         cell.cellImage4.sd_setImage(with: url as URL!)
                    }
               }
               
               let name = member.username ?? ""
               username += name + ", "
          }
          
          cell.cellUsername.text = username.trimmingCharacters(in: CharacterSet(charactersIn: ", "))
          cell.cellTime.text = Date().getDateFromTimestamp(chat.timestamp)?.formatChatTime()
          
          return cell
     
     } else{
     let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! QlueChatViewCell
          
          if chat.status == "read"{
               cell.backgroundColor = UIColor.white
          } else{
               cell.backgroundColor = kBlueE0F5FDColor
          }
          
          if let member = chat.members.first{
               if let url = member.avatar?.encodeURL(){
                    cell.cellImage.sd_setImage(with: url as URL!)
               }
               cell.cellUsername.text = member.username
          }
          cell.cellTime.text = Date().getDateFromTimestamp(chat.timestamp)?.formatChatTime()
          
          return cell
     }
     
     
     
    }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          let chat = chats[indexPath.row]
          readWithGroupid(chat.groupId ?? "")
          chats[indexPath.row].status = "read"
        presentChatDetail(withGroupId: chat.groupId ?? "", isGroup: chat.isGroup ?? false, members: chat.members)
     }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
