//
//  QlueFeedDetailController.swift
//  Qlue
//
//  Created by Nurul on 6/6/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps
import SDWebImage
//import AVFoundation
//import AssetsLibrary
import PBJVideoPlayer
import Photos

class QlueFeedDetailController: QlueViewController {
     
     // MARK: - Outlets
     @IBOutlet var tableView: UITableView!
     
     // MARK: - Vars
     var feedId: Int?
     var user: User?
     var feed: QlueFeed?
     
     var commentSort = "old" // old / new
     var comments: [QlueComment] = []
     
     // Mark: - Refresh
     let refreshControl = UIRefreshControl()
     
     // MARK: - Video
     var header: QlueFeedHeaderView?
     //     var player: AVPlayer?
     //     var playerLayer: AVPlayerLayer?
     let videoPlayer: PBJVideoPlayerController = PBJVideoPlayerController.init()
     
     
     // MARK: asset library var
//     var library: ALAssetsLibrary = ALAssetsLibrary()
     
     // MARK: - LifeCycle
     override func viewDidLoad() {
          super.viewDidLoad()
          user = User.currentUser()
          registerNib()
          
          
          //refresh
          tableView.addSubview(refreshControl)
          refreshControl.addTarget(self, action: #selector(QlueFeedDetailController.refresh(_:)), for: .valueChanged)
          refreshControl.beginRefreshing()
          
          downloadData()
          downloadComment()
          
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
     }
     
     func initWithFeedId(_ id: Int?){
          self.feedId = id
     }
     
     //     deinit{
     //          if let player = player{
     //
     //               player.removeObserver(self, forKeyPath: "status")
     //          }
     //     }
     
     override func viewWillAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
          self.navigationController?.isNavigationBarHidden = true
          self.downloadData()
          self.downloadComment()
     }
     
     override func viewWillDisappear(_ animated: Bool) {
          super.viewDidDisappear(animated)
          self.navigationController?.isNavigationBarHidden = false
          videoPlayer.stop()
     }
     // MARK: - Action
     @IBAction func moreTapped(_ sender: UIButton) {
          let alert = UIAlertController.init(title: "More", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
          
          let nomorKel = UIAlertAction.init(title: "Nomor Kelurahan", style: .default) { (action) in
               self.presentNomorLaporan(withNoLap: self.feed?.feedId?.description)
          }
          alert.addAction(nomorKel)
          
          let simpanGambar = UIAlertAction.init(title: "Simpan Gambar", style: .default) { (action) in
               if let image = self.header?.cellImage.image{
                    self.saveToImageAlbum(withImage: image)
               }
          }
          alert.addAction(simpanGambar)
          
          let laporkan = UIAlertAction.init(title: "Laporkan", style: .default) { (action) in
               
          }
          //          alert.addAction(laporkan)
          
          let streetView = UIAlertAction.init(title: "Lihat Street View", style: .default) { (action) in
               
          }
          //          alert.addAction(streetView)
          
          let editKel = UIAlertAction.init(title: "Edit Kelurahan", style: .default) { (action) in
               
          }
          //          alert.addAction(editKel)
          
          let tindakLanjut = UIAlertAction.init(title: "Tindak Lanjut", style: .default) { (action) in
               
          }
          //          alert.addAction(tindakLanjut)
          
          let cancel = UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
          alert.addAction(cancel)
          
          present(alert, animated: true, completion: nil)
          
     }
     func refresh(_ sender: UIRefreshControl){
          downloadData()
          downloadComment()
     }
     // MARK: - Private Func
     fileprivate func registerNib(){
          
          tableView.register(UINib.init(nibName: "QlueFeedPostViewCell", bundle: nil), forCellReuseIdentifier: "PostCell")
          tableView.register(UINib.init(nibName: "QlueFeedReportStateViewCell", bundle: nil), forCellReuseIdentifier: "ReportStatusCell")
          tableView.register(UINib.init(nibName: "QlueSponsoredViewCell", bundle: nil), forCellReuseIdentifier: "SponsoredCell")
          tableView.register(UINib.init(nibName: "QlueFeedReviewViewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
          
          tableView.register(UINib.init(nibName: "CreateCommentViewCell", bundle: nil), forCellReuseIdentifier: "CreateCommentCell")
          
     }
     
     func setFavButton(){
          if feed?.history == false{
               header?.cellStarButton.backgroundColor = kPinkEA1E63Color
               header?.cellStarButton.setImage(UIImage.init(named: "ic_star_fav")?.imageWithColor(UIColor.white), for: UIControlState())
          } else{
               header?.cellStarButton.backgroundColor = kDarkColor
               header?.cellStarButton.setImage(UIImage.init(named: "ic_star_fav")?.imageWithColor(kCreamColor), for: UIControlState())
          }
     }
     
     // MARK: - Save to Album
     // MARK: - Helper
     func saveToImageAlbum(withImage image: UIImage){
          // FIXME: - to Swift 3
          PHPhotoLibrary.shared().performChanges({
               PHAssetChangeRequest.creationRequestForAsset(from: image)
          }) { (success, error) in
               self.saveDone(nil, error: error)
          }
     }
     
     func saveDone(_ assetURL: URL!, error: Error!){
          Util.showSimpleAlert("Save image done", vc: self, completion: nil)
          // FIXME: - to Swift 3
//          library.asset(for: assetURL, resultBlock: self.saveToAlbum, failureBlock: nil)
          
     }
     
     // FIXME: - to Swift 3
//     func saveToAlbum(_ asset:ALAsset!){
//          
//          library.enumerateGroupsWithTypes(ALAssetsGroupAlbum, usingBlock: { group, stop in
//               stop?.pointee = false
//               if(group != nil){
//                    let str = group?.value(forProperty: ALAssetsGroupPropertyName) as! String
//                    if(str == "MY_ALBUM_NAME"){
//                         group!.add(asset!)
//                         let assetRep:ALAssetRepresentation = asset.defaultRepresentation()
//                         let iref = assetRep.fullResolutionImage().takeUnretainedValue()
//                         
//                         
//                    }
//                    
//               }
//               
//               },
//                                           failureBlock: { error in
//                                             print("NOOOOOOO")
//          })
//          
//     }
     
     
     // MARK: - Data
     fileprivate func downloadData(){
          guard let id = feedId else{
               self.refreshControl.endRefreshing()
               return
          }
          
          
          let req  =  Engine.shared.getFeedDetail(feedId: id)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: { () -> Void in
               self.refreshControl.endRefreshing()
               }, completion:  { (data, success, fail) -> Void in
                    if success == true{
                         if let d = data.array?.first{
                              self.feed = QlueFeed.feedWithData(d)
                              self.tableView.reloadData()
                         }
                    }
          })
          
     }
     
     
     fileprivate func downloadComment(){
          
          guard let feedId = feedId else{
               return
          }
          
          //          let feedId = 731151
          let req = Engine.shared.getCommentList(String(feedId), sortBy: commentSort, page: 0)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               DejalActivityView.remove()
               },completion:  { (data, success, fail) in
                    self.comments = []
                    if success == true{
                         if let comments = data.array{
                              for comment in comments{
                                   self.comments.append(QlueComment.commentWithData(comment))
                              }
                         }
                    }
                    
                    self.tableView.reloadData()
          })
          
     }
     
     
}

// MARK: - UITableViewDataSource
extension QlueFeedDetailController: UITableViewDataSource{
     
     func numberOfSections(in tableView: UITableView) -> Int {
          if feed == nil{
               return 0
          }
          
          return 1
     }
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return 4
          
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          switch indexPath.row {
          case 0: // Post
               let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! QlueFeedPostViewCell
               
               if let avaUrl = feed?.avatar?.encodeURL(){
                    cell.cellImage.sd_setImage(with: avaUrl as URL!)
               }
               cell.cellName.text = feed?.username
               let timestamp = Date().getDateFromTimestamp(feed?.timestamp)
               cell.cellTime.text = timestamp?.formatDateReadable()
               
               cell.cellPost.text = feed?.desc
               if let lat = feed?.lat, let lng = feed?.lng{
                    cell.cellStreetView.moveNearCoordinate(CLLocationCoordinate2D(latitude: lat, longitude: lng))
               }
               //            cell.lat = feed?.lat
               //            cell.lng = feed?.long
               
               cell.cellPost.handleHashtagTap({ (hashtag) in
                    self.presentFeedByCategory(hashtag, categoryName: hashtag, isCategory: false)
               })
               
               return cell
               
          case 1: //Report Status
               let cell = tableView.dequeueReusableCell(withIdentifier: "ReportStatusCell", for: indexPath) as! QlueFeedReportStateViewCell
               
               cell.delegate = self
               cell.cellWaitingButton.isHidden = true
               cell.cellProgressButton.isHidden = true
               cell.cellDoneButton.isHidden = true
               
               if let progress = feed?.progressState{
                    switch progress {
                    case .Wait:
                         cell.cellWaitingButton.isHidden = false
                    case .Process:
                         cell.cellWaitingButton.isHidden = false
                         cell.cellProgressButton.isHidden = false
                    case .Complete:
                         cell.cellWaitingButton.isHidden = false
                         cell.cellProgressButton.isHidden = false
                         cell.cellDoneButton.isHidden = false
                    }
               }
               
               return cell
               
          case 2: //Sponsored
               let cell = tableView.dequeueReusableCell(withIdentifier: "SponsoredCell", for: indexPath) as! QlueSponsoredViewCell
               
               return cell
               
          case 3: // Review
               let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! QlueFeedReviewViewCell
               
               cell.delegate = self
               
               let commentCount = feed?.forumCommentCount ?? "0"
               if let count = Int(commentCount) {
                    if count < 10{
                         cell.cellTableViewHeight.constant = CGFloat.init(count) * 80
                    } else{
                         cell.cellTableViewHeight.constant = 10 * 80
                    }
               } else{
                    cell.cellTableViewHeight.constant = 0
               }
               
               cell.cellTitle.text = "Reviews (" + commentCount + ")"
               
               cell.comments = comments
               cell.cellTableView.reloadData()
               
               return cell
               
          default:
               return UITableViewCell()
          }
     }
     
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          switch indexPath.row {
          case 0: // Post
               var height: CGFloat = 86 + 117 + 8
               if let desc = feed?.desc{
                    height += desc.heightWithConstrainedWidth(tableView.bounds.width - 16, font: UIFont.init(name: QlueFont.HelveticaNeue, size: 12)!)
               }
               
               return height
               
          case 1: //Report Status
               if let _ = feed?.progressState {
                    return 120
                    
               }
               
               return 0
               
          case 2: //Sponsored
               return 0
               
          case 3: // Review
               var height: CGFloat = 100 + 16 + 53
               
               if let commentStr = feed?.forumCommentCount, let commentCount = Int(commentStr){
                    height += 15
                    
                    if commentCount < 10{
                         height += CGFloat.init(commentCount) * 80
                    } else{
                         height += 10 * 80
                    }
               }
               return height
               
               
          default:
               return 0
          }
          
     }
     
}


// MARK: - UITableViewDelegate
extension QlueFeedDetailController: UITableViewDelegate{
     
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
          header = Bundle.main.loadNibNamed("QlueFeedHeaderView", owner: nil, options: nil)?.first as? QlueFeedHeaderView
          
          var imgStr: String?
          if feed?.format == "video"{
               imgStr = feed?.thumbnail
          } else{
               imgStr = feed?.fileUrl
          }
          if let imgUrl = imgStr?.encodeURL(){
               header?.cellImage.sd_setImage(with: imgUrl as URL!)
          }
          if let catUrl = feed?.label2?.encodeURL(){
               SDWebImageManager.shared().downloadImage(with: catUrl as URL!, options: SDWebImageOptions.avoidAutoSetImage, progress: { (start, from) in
                    
                    }, completed: { (image, error, cache, finished, url) in
                         if image != nil{
                              self.header?.cellCategoryImage.image = image?.drawOutlineWithColor(UIColor.white)
                         }
               })
               
          }
          
          if feed?.format == "video"{
               videoPlayer.delegate = self
               videoPlayer.view.frame = header?.cellImage.bounds ?? CGRect.zero
               if let url = feed?.fileUrl{
                    
                    videoPlayer.videoPath = url
                    videoPlayer.playbackLoops = true
                    videoPlayer.videoFillMode = "AVLayerVideoGravityResizeAspectFill"
                    
                    // present
                    self.addChildViewController(videoPlayer)
                    
                    header?.cellImage.addSubview(videoPlayer.view)
               }
               header?.videoButton.isHidden = false
          } else{
               header?.videoButton.isHidden = true
          }
          
          header?.cellTitle.text = feed?.qlueTitle
          if (feed?.reportType == .ReviewNegatif ) || (feed?.reportType == .ReviewPositif ){
               header?.cellKelurahan.text = feed?.poi?.title
          } else{
               let kel = feed?.kelurahan ?? ""
               let kec = feed?.kecamatan ?? ""
               header?.cellKelurahan.text = kec + ", " + kel
          }
          
          
          
          
          header?.cellSeenBy.setTitle(feed?.forumViewCount, for: UIControlState())
          header?.delegate = self
          
          var desc = " Support"
          if feed?.reportType == QlueReportType.ReviewPositif{
               header?.isLike = true
               desc = " Likes"
               header?.cellLikeLabel.text = "Like"
          } else{
               header?.isLike = false
               header?.cellLikeLabel.text = "Support"
          }
          
          if let like = feed?.countLike, Int(like) > 0{
               header?.cellLikeLabel.text = like.description + desc
          }
          
          if feed?.like == true{
               header?.buttonLiked = true
          }
          else{
               
               header?.buttonLiked = false
          }
          
          setFavButton()
          
          return header
     }
     
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
          let imageHeight = (tableView.bounds.width / 8) * 7
          var height = imageHeight
          
          let kel = feed?.kelurahan ?? ""
          let kec = feed?.kecamatan ?? ""
          let kelurahan = kec + ", " + kel
          height += kelurahan.heightWithConstrainedWidth(tableView.bounds.width - 77 - 84 , font: UIFont.init(name: QlueFont.HelveticaNeue, size: 12)!)
          height += 58 + 8
          
          height += 88
          
          return height
     }
     
     
}
// MARK: - QlueFeedReviewViewCellDelegate{
extension QlueFeedDetailController: QlueFeedReviewViewCellDelegate{
     func sortTapped(_ cell: QlueFeedReviewViewCell, button: UIButton) {
          if commentSort == "old"{
               button.setImage(UIImage.init(named: "ic_sortby_old"), for: UIControlState())
               commentSort = "new"}
          else {
               button.setImage(UIImage.init(named: "ic_sortby_new"), for: UIControlState())
               commentSort = "old"
          }
          
          downloadComment()
     }
     func readAllReview(_ cell: QlueFeedReviewViewCell) {
          guard let feedId = feed?.feedId else { return }
          presentComment(withId: feedId.description, nav: self.navigationController)
          
     }
     
}


// MARK: - QlueFeedReviewViewCellDelegate{
extension QlueFeedDetailController: QlueFeedHeaderViewDelegate{
     func lokasiTapped(_ view: QlueFeedHeaderView) {
          if let lat = feed?.lat,
               let lng = feed?.lng {
               
               let location = CLLocation.init(latitude: CLLocationDegrees.init(lat), longitude: CLLocationDegrees.init(lng))
               //               pushMapsWithLocation(location)
          }
     }
     func commentTapped(_ view: QlueFeedHeaderView) {
          guard let feedId = feed?.feedId else { return }
          presentComment(withId: feedId.description, nav: self.navigationController)
     }
     func kelurahanTapped(_ view: QlueFeedHeaderView) {
          
          if (feed?.reportType == .ReviewNegatif ) || (feed?.reportType == .ReviewPositif ){
               Util.showSimpleAlert("Detail tidak tersedia", vc: self, completion: nil)
               return
          }
          
          if (feed?.kdKel == nil)/* || (feed.reportType)*/{
               Util.showSimpleAlert("Detail tidak tersedia", vc: self, completion: nil)
               
               return
          }
          presentKelurahan(withId: feed?.kdKel, district: feed?.district, nav: self.navigationController)
     }
     func favoriteTapped(_ view: QlueFeedHeaderView) {
          guard let postId = feed?.feedId, feed?.like == false else { return }
          
          if feed?.history == true{
               header?.cellStarButton.backgroundColor = kPinkEA1E63Color
               header?.cellStarButton.setImage(UIImage.init(named: "ic_star_fav")?.imageWithColor(UIColor.white), for: UIControlState())
          } else{
               header?.cellStarButton.backgroundColor = kDarkColor
               header?.cellStarButton.setImage(UIImage.init(named: "ic_star_fav")?.imageWithColor(kCreamColor), for: UIControlState())
          }
          
          let req = Engine.shared.postFavorite(withFeedId: postId.description)
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               }, completion: { (data, success, fail) in
                    if data["success"].boolValue == true{
                         self.feed?.history = true
                         Util.showSimpleAlert("Qlue telah masuk di favorite", vc: self, completion: nil)
                    } else{
                         self.feed?.history = false
                         Util.showSimpleAlert("Qlue telah dihapus dari favorite", vc: self, completion: nil)
                    }
          }){
               
          }
          
          
     }
     func likeTapped(_ view: QlueFeedHeaderView) {
          guard let postId = feed?.feedId else { return }
          
          
          let req = Engine.shared.postLike(withFeedId: postId.description)
          
          
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
               }, completion: { (data, success, fail) in
                    if success == true{
                         
                         //                         self.header?.cellLikeButton.enabled = false
                         
                         var desc = " Support"
                         if self.feed?.reportType == .ReviewPositif{
                              desc = " Likes"
                         }
                         
                         self.header?.cellLikeLabel.text = data["like_count"].stringValue + desc
                         if let like_count = data["like_count"].string{
                              self.feed?.countLike = Int(like_count)
                         } else{
                              self.feed?.countLike = data["like_count"].int
                         }
                         self.header?.buttonLiked = data["success"].boolValue
                         
                         
                    } else{
                         var desc = " Support"
                         if self.feed?.reportType == .ReviewPositif{
                              desc = " Likes"
                         }
                         
                         
                         if let intLike = self.feed?.countLike{
                              let totalLike = intLike - 1
                              self.header?.cellLikeLabel.text = totalLike.description + desc
                              self.feed?.countLike =  totalLike
                         }
                         //                         self.header?.cellLikeButton.enabled = true
                    }
               }, falseCompletion:  { void in})
     }
     func seenByTapped(_ view: QlueFeedHeaderView) {
          guard let id = feedId, let countSeen = feed?.forumViewCount else { return }
          presentSeenBy(withId: String(id), countSeen: countSeen, type: 0)
     }
     
     func videoPlayTapped(_ view: QlueFeedHeaderView, button: UIButton) {
          header?.videoButton.alpha = 0.3
          header?.videoButton.isEnabled = false
          
          videoPlayer.playFromBeginning()
          //               [self addChildViewController:videoPlayerController];
          //               [self.view addSubview:videoPlayerController.view];
          //               [videoPlayerController didMoveToParentViewController:self];
          //               player = AVPlayer.init(URL: url)
          //               player?.actionAtItemEnd = .None
          //
          //               playerLayer = AVPlayerLayer.init(player: player)
          //               playerLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
          //
          //               NSNotificationCenter.defaultCenter().addObserver(self,
          //                                                                selector: #selector(QlueFeedDetailController.playerItemDidReachEnd(_:)),
          //                                                                name: AVPlayerItemDidPlayToEndTimeNotification,
          //                                                                object: player?.currentItem)
          //
          //               let screenRect = UIScreen.mainScreen().bounds
          //               playerLayer?.frame = CGRect.init(x: 0, y: 0, width: screenRect.size.width, height: screenRect.size.width)
          //
          //               if let playerLayer = playerLayer{
          //                    view.cellImage.layer.insertSublayer(playerLayer, atIndex: 0)
          //               }
          //
          //               player?.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.Initial, context: nil)
          //          }
     }
     //
     override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
          //
          //          if let keyPath = keyPath, let object = object as? AVPlayer where ( object == player ) && ( keyPath == "status" ){
          //               if player?.status == .ReadyToPlay{
          //                    player?.play()
          //                    header?.fullscreenVideoButton.hidden = false
          //                    header?.videoButton.alpha = 0
          //
          //               } else if player?.status == .Failed{
          //
          //                    playerLayer?.removeFromSuperlayer()
          //
          //                    header?.videoButton.alpha = 1
          //                    header?.videoButton.enabled = true
          //
          //               }
          //          }
          
          
     }
     
     // MARK: - Action
     override func backTapped(_ sender: AnyObject) {
          super.backTapped(sender)
          removePlayerLayer()
     }
     func removePlayerLayer(){
          //          player?.pause()
          //          playerLayer?.removeFromSuperlayer()
          //          header?.videoButton.enabled = true
          //          header?.videoButton.alpha = 1.0
          //
          //          header?.fullscreenVideoButton.hidden = true
          //
          //     }
          //     func playerItemDidReachEnd(sender: NSNotification){
          //
          //          let playerItem = sender.object
          //          playerItem?.seekToTime(kCMTimeZero)
          //
     }
     
     func fullscreenVideoTapped(_ view: QlueFeedHeaderView) {
          //          removePlayerLayer()
          //          player?.removeObserver(self, forKeyPath: "status")
          //          presentPreviewVideoFeed(withUrl: feed?.file ?? "", thumbImage: header?.cellImage.image)
     }
     
     func mainImageTapped(_ view: QlueFeedHeaderView) {
          
          if feed?.format == "video"{
               //               removePlayerLayer()
          } else{
               //               presentPreviewImage(withUrlString: feed?.file ?? "")
          }
     }
     
     
}

// MARK: - QlueFeedReportStateViewCellDelegate
extension QlueFeedDetailController: QlueFeedReportStateViewCellDelegate{
     func processTapped(_ cell: QlueFeedReportStateViewCell) {
          guard let id = feed?.feedId else { return }
          presentProgressProcess(withFeedId: id)
     }
     
     func doneTapped(_ cell: QlueFeedReportStateViewCell) {
          guard let id = feed?.feedId else { return }
          presentProgressComplete(withFeedId: id)
     }
}

extension QlueFeedDetailController: PBJVideoPlayerControllerDelegate{
     
}

// MARK: - NAVIGATE
extension UIViewController {
     func presentDetailFeed(withFeedId id: Int?, nav: UINavigationController?){
          if let detail = menuStoryboard.instantiateViewController(withIdentifier: "FeedDetail") as? QlueFeedDetailController{
               detail.feedId = id
               
               if let nav = nav {
                    nav.pushViewController(detail, animated: true)
               } else{
                    present(detail, animated: true, completion: nil)
               }
          }
     }
}
