//
//  QlueViewController.swift
//  Qlue
//
//  Created by Nurul on 5/26/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueViewController: UIViewController {

    var viewDidAppearedOnce: Bool = false
     
    //keyboard
    var firstShow = true
    var keyBefore: CGFloat?
    
    lazy var loadingView: UIActivityIndicatorView = {
        
        let loadingView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        loadingView.hidesWhenStopped = true
        loadingView.tag = 100
        
        return loadingView
    }()
     
     lazy var loadMoreView: UIView = {
          
          let window = UIApplication.shared.keyWindow
          
          let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: window!.frame.width, height: 44.0))
          view.backgroundColor = UIColor.clear
          
          let loadingView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
          loadingView.hidesWhenStopped = true
          loadingView.tag = 100
          
          loadingView.center = view.center
          view.addSubview(loadingView)
          
          return view
     }()

     var tap: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //keyboard
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(QlueViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(QlueViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        // nav
        

        UITableViewCell().selectionStyle = .none
        
        // tap gesture recognizer
        tap = UITapGestureRecognizer(target: self, action: #selector(QlueViewController.tap(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Helpers
    func semiModalWithViewController(_ viewController: UIViewController, backgroundShade: UIColor = UIColor.black, backgroundaAlpha: CGFloat = 0.7){
        
        let semiModal = LGSemiModalNavViewController(rootViewController: viewController)
        semiModal.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height + 64)
        
        semiModal.isNavigationBarHidden = true
        semiModal.backgroundShadeColor = backgroundShade
        semiModal.animationSpeed = 0.35
        semiModal.isTapDismissEnabled = true
        semiModal.backgroundShadeAlpha = backgroundaAlpha
        semiModal.scaleTransform = CGAffineTransform(translationX: 0, y: 100)
        semiModal.scaleTransform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        present(semiModal, animated: true, completion: nil)
        
    }
     
     func displayViewController(viewController view: UIViewController, inView containerView: UIView){
          addChildViewController(view)
          view.view.frame = containerView.bounds
          view.view.autoresizingMask = [.flexibleWidth , .flexibleHeight]
          containerView.addSubview(view.view)
          view.didMove(toParentViewController: self)
     }
     
     func configureChatButton(_ chatButton: UIButton){
          if self.revealViewController() != nil {
               chatButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
          }
     }
    
    // MARK: -Keyboard Handle
    
    func keyboardWillShow(_ sender: Notification) {
        var info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            if self.firstShow == false{
                self.view.frame.size.height += self.keyBefore!
                self.keyBefore = keyboardFrame.size.height
            }
            self.view.frame.size.height -= keyboardFrame.size.height
            self.firstShow = false
            self.keyBefore = keyboardFrame.size.height
        })
        
    }
    func keyboardWillHide(_ sender: Notification) {
        var info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.view.frame.size.height += keyboardFrame.size.height
            self.firstShow = true
        })
    }
    
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        
        if sender.state == .ended {
            
            view.endEditing(true)
        }
    }
    
    // MARK: - Action
    
    @IBAction func backTapped(_ sender: AnyObject) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



