//
//  QlueScrollViewController.swift
//  Qlue
//
//  Created by Nurul on 5/26/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueScrollViewController: QlueViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Actions
    
    func viewTapped(_ sender: UIGestureRecognizer) {
        
        if sender.state == .changed || sender.state == .ended {
            view.endEditing(true)
        }
    }

    // MARK: - Keyboard
    
    override func keyboardWillShow(_ sender: Notification) {
        
        if scrollView == nil{
            return
        }
        
        if let userInfo = sender.userInfo as? [String: AnyObject] {
            
            var keyboardFrameValue = userInfo["UIKeyboardBoundsUserInfoKey"] as? NSValue
            if keyboardFrameValue == nil {
                keyboardFrameValue = userInfo["UIKeyboardFrameEndUserInfoKey"] as? NSValue
            }
            
            
            
            if let uKeyboardFrameValue = keyboardFrameValue {
                
                var animationDuration = 0.25
                if let duration = userInfo["UIKeyboardAnimationDurationUserInfoKey"] as? Double {
                    animationDuration = duration
                }
                
                let screenHeight = UIScreen.main.bounds.height
                let keyboardHeight = uKeyboardFrameValue.cgRectValue.height
                let height = self.scrollView.frame.height - screenHeight + keyboardHeight
                
                UIView.animate(withDuration: animationDuration, animations: { () -> Void in
                    
                    self.scrollView.contentInset = UIEdgeInsets(top: self.scrollView.contentInset.top, left: self.scrollView.contentInset.left, bottom: height, right: self.scrollView.contentInset.right)
                    
                    self.scrollView.scrollIndicatorInsets = UIEdgeInsets(top: self.scrollView.scrollIndicatorInsets.top, left: self.scrollView.scrollIndicatorInsets.left, bottom: height, right: self.scrollView.scrollIndicatorInsets.right)
                    
                    }, completion: { (finished) -> Void in
                        
                        if let frView = self.scrollView.firstResponder() {
                            let y = frView.frame.origin.y + frView.frame.size.height
                            if y >= self.scrollView.frame.height - (keyboardHeight + 44.0) {
                                self.scrollView.setContentOffset(CGPoint(x: 0.0, y: y - (self.scrollView.frame.height - keyboardHeight) + 44.0), animated: true)
                            }
                           
                        }
                })
            }
        }
    }
    
    override func keyboardWillHide(_ sender: Notification) {
        if scrollView == nil{
            return
        }
        if let userInfo = sender.userInfo as? [String: AnyObject] {
            
            var keyboardFrameValue = userInfo["UIKeyboardBoundsUserInfoKey"] as? NSValue
            if keyboardFrameValue == nil {
                keyboardFrameValue = userInfo["UIKeyboardFrameEndUserInfoKey"] as? NSValue
            }
            
            if let _ = keyboardFrameValue {
                
                var animationDuration = 0.25
                if let duration = userInfo["UIKeyboardAnimationDurationUserInfoKey"] as? Double {
                    animationDuration = duration
                }
                
                UIView.animate(withDuration: animationDuration, animations: { () -> Void in
                    
                    self.scrollView.contentInset = UIEdgeInsets(top: self.scrollView.contentInset.top, left: self.scrollView.contentInset.left, bottom: 0, right: self.scrollView.contentInset.right)
                    
                    self.scrollView.scrollIndicatorInsets = UIEdgeInsets(top: self.scrollView.scrollIndicatorInsets.top, left: self.scrollView.scrollIndicatorInsets.left, bottom: 0, right: self.scrollView.scrollIndicatorInsets.right)
                    
                    }, completion: { (finished) -> Void in
                        self.scrollView.setContentOffset(CGPoint.zero, animated: true)
                })
            }
        }
    }


}
