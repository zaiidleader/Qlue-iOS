//
//  QlueKTrophyController.swift
//  Qlue
//
//  Created by Nurul on 6/10/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueKTrophyController: QlueViewController {

     // MARK: - Vars
     // MARK: Outlet
    @IBOutlet weak var collectionView: UICollectionView!
     
     lazy var labelView: UILabel = {
          let label = UILabel.init(frame: CGRect(x: 15, y: 150, width: UIScreen.main.bounds.width - 30, height: 100))
          label.font = UIFont.init(name: QlueFont.ProximaNovaSoft, size: 17)
          label.textColor = UIColor.darkGray
          label.text = "Anda belum memiliki piala"
          label.textAlignment = NSTextAlignment.center
          
          return label
     }()
     
     
     // MARK: Data
     var trophyArr : [QlueTrophy] = []
     var kelurahan: QlueKelurahan?
     
     // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        collectionView.register(UINib.init(nibName: "QlueKTrophyViewCell", bundle: nil), forCellWithReuseIdentifier: "TrophyCell")
     DejalActivityView.addActivityView(for: collectionView)
     downloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

     // MARK: - Data
     func downloadData(){
     
          guard let codeKel = kelurahan?.codeKel, let district = kelurahan?.district else { return }
          
          let req = Engine.shared.getTrophy(codeKel, district: district)
          Engine.shared.parseJsonWithReq(req, controller: self, loadingStop: {
                    DejalActivityView.remove()
               
               }, completion: { (data, success, fail) in
                    self.trophyArr = []
                    if success == true{
                         for d in data.arrayValue{
                              self.trophyArr.append(QlueTrophy.init(fromJson: d))
                         }
                    }
                    
                    if self.trophyArr.count > 0{
                         self.labelView.removeFromSuperview()
                    } else{
                         self.view.addSubview(self.labelView)
                    }
                    
                    self.collectionView.reloadData()
               })
          
     }

}

// MARK: - UICollectionViewDataSource
extension QlueKTrophyController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trophyArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrophyCell", for: indexPath) as! QlueKTrophyViewCell
        cell.cornerRadius(5)
     
     let data = trophyArr[indexPath.item]
     
     cell.cellImage.image = nil
     if let imageUrl = data.icon?.encodeURL(){
          cell.cellImage.sd_setImage(with: imageUrl as URL!)
     }
     cell.cellPoint.text = "\(data.total ?? "0") Points"
     cell.cellTime.text = data.date
        
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension QlueKTrophyController: UICollectionViewDelegate{
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension QlueKTrophyController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size: CGFloat = ((collectionView.bounds.width - 24) / 2 )
        
        return CGSize(width: size, height: size)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, 8, 0, 8)
    }
    
    
}

// MARK: - Navigation
extension QlueViewController{
     func presentKTrophy(_ kelurahan: QlueKelurahan?, nav: UINavigationController?){
          if let lap = kelurahanStoryboard.instantiateViewController(withIdentifier: "KTrophy") as? QlueKTrophyController{
               
               lap.kelurahan = kelurahan
               
               if let nav = nav{
                    nav.pushViewController(lap, animated: true)
               } else{
                    present(lap, animated: true, completion: nil)
               }
          }
     }
}

