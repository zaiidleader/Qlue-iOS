//
//  QlueOtherProfileNavController.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/10/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit

class QlueOtherProfileNavController: UINavigationController {

     var userId: String?
     
    override func viewDidLoad() {
        super.viewDidLoad()

     
     if let op = self.viewControllers.first as? QlueOtherProfileController{
          op.userId = self.userId
     }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
