//
//  Engine.swift
//  Qlue
//
//  Created by Nurul on 6/1/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation
import CoreData
import Firebase

typealias CompletionHandler = (_ result: Any?, _ error: Error?) -> ()

class Engine: NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var currentReport: QlueReport?
    var createUser: QlueRegistration = QlueRegistration()
    var currentProv: String?
    
    class var shared: Engine {
        struct Static {
            static let instance: Engine = Engine()
        }
        return Static.instance
    }
    
    override init() {
        super.init()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        let newLocation = locations.last!
        
        if currentLocation == nil {
            currentLocation = newLocation
            NotificationCenter.default.post(name: Notification.Name(rawValue: kDidUpdateLocationNotification), object: nil, userInfo: ["location": currentLocation!])
        } else if newLocation.distance(from: currentLocation!) > 100 {
            currentLocation = newLocation
            NotificationCenter.default.post(name: Notification.Name(rawValue: kDidUpdateLocationNotification), object: nil, userInfo: ["location": currentLocation!])
        } else {
            currentLocation = newLocation
        }
    }
    
    var defaultParam: [String:AnyObject]{
        let user = User.currentUser()
        
        var param: [String:AnyObject] = [
            "source" : "ios" as AnyObject,
            "version" : "2.0" as AnyObject,
            "type_region" : 0 as AnyObject,
            "country" : "Indonesia" as AnyObject,
            "provinsi" : currentProv as AnyObject? ?? "DAERAH KHUSUS IBUKOTA JAKARTA" as AnyObject
        ]
        
        if let uid = user?.userId{
            param["uid"] = uid as AnyObject?
        }
        if let key = user?.randkey{
            param["key"] = key as AnyObject?
        }
        
        return param
    }
    
    
    // MARK: - APIs
    func checkProvinsi(_ lat: Double, lng: Double, completion: @escaping CompletionHandler){
        let parameter : [String: Any] = [
            "lat" : lat,
            "lng" : lng
        ]
        let url = "\(BASE_URL)\(GET_PROVINSI)"
        
        Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString)).responseString { (response) in
            
            if response.result.isSuccess {
                if let result = response.result.value {
                    let data = JSON(parseJSON: result)
                    print(data)
                    if let prov = data["provinsi"].string{
                        self.currentProv = prov
                        completion(prov, nil)
                    }
                    else {
                        completion("", nil)
                    }
                    
                }
            }
        }
    }
    
    // MARK: - User Authorization
    func loginWithEmail(_ uid: String, pwd: String, completion: @escaping CompletionHandler){
        let parameter : [String:AnyObject] = [
            "uid" : uid as AnyObject,
            "pwd" : pwd as AnyObject,
            "source" : "ios" as AnyObject
        ]
        let url = "\(BASE_URL)\(LOGIN_URL)"
        
        Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString)).responseString { (res) in
            if res.result.isSuccess{
                if  let dataString = res.result.value{
                    
                    
                    print(dataString)
                    let data = JSON(parseJSON: dataString)
                    
                    if data["success"].string == "true"{
                    print(data)
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let moc = appDelegate.managedObjectContext
                        
                        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "User")
                        do{
                            let items = try moc.fetch(fetchRequest)as! [NSManagedObject]
                            for item in items{
                                moc.delete(item)
                            }
                            
                            appDelegate.saveContext()
                        }catch let error as NSError{
                            print("error: \(error.localizedDescription)")
                        }
                        
                        _ = User.userWithData(data, inManagedObjectContext: moc)
                    
                        appDelegate.saveContext()
                    }
                    
                }
                
                completion(res.result.value, nil)
            } else if let error = res.result.error {
                completion(nil, error)
            } else{
                completion(nil, nil)
            }
        }
    }
    func forgotPassword(_ email: String, completion: @escaping CompletionHandler){
        let parameter : [String:AnyObject] = [
            "email" : email as AnyObject
        ]
        let url = "\(BASE_URL)\(FORGOT_PWD_URL)"
        
        Alamofire.request(url, method: HTTPMethod.get, parameters: parameter, encoding: URLEncoding(destination: .queryString)).responseString { (res) in
            if res.result.isSuccess {
                completion(res.result.value, nil)
            } else if let error = res.result.error{
                completion(nil, error)
            } else {
                completion(nil, nil)
            }
        }
    }
    
    func registration(_ user: QlueRegistration) -> DataRequest {
        
        var parameter: [String: Any] = user.getParameter()
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(REGISTRATION_URL)"
        
        print(" param \(parameter)")
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }

    
    func signUp(_ uid: String, phone: Int = 0, pwd: String, email: String, gender: Gender, fullname: String, regis: Int, dob: Date?, idSocmed: String?, socialData: [String: AnyObject]?, source: LoginType) -> DataRequest{
        
        var parameter : [String: Any] = [
            "uid" : uid,
            "phone" : phone,
            "pwd" : pwd,
            "email" : email,
            "gender" : gender.short,
            "full_name" : fullname,
            "regis" : regis,
            "dob" : dob?.formatToAPI() ?? "0000-00-00",
            "source" : source.description,
            "provinsi" : "JAWA BARAT",
            "version" : "2.0",
            "type_region" : 0,
            "country" : "Indonesia"
            
        ]
        
        if let idSocmed = idSocmed{
            parameter["id_socmed"] = idSocmed as AnyObject?
        }
        
        if let socialData = socialData{
            parameter["social_data"] = socialData as AnyObject?
        }
        let url = "\(BASE_URL)\(SIGN_UP_URL)"
        
        print(" param \(parameter)")
        
        return Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }
    
    // MARK: - Registration
    func registration(_ reg: QlueRegistration, pwd: String) -> DataRequest{
        
        let dateOfBirth = reg.dob?.formatToAPI() ?? "0000-00-00"
        
        var parameter : [String: Any] = [
            "uid" : reg.username,
            "phone" : reg.phone,
            "pwd" : pwd,
            "email" : reg.email,
            "gender" : reg.gender.short,
            "full_name" : reg.fullname,
            "regis" : reg.regis,
            "dob" : dateOfBirth,
            "source" : reg.source,
            "provinsi" : "JAWA BARAT",
            "version" : "2.0",
            "type_region" : 0,
            "country" : "Indonesia"
            
        ]
        
        if let idSocmed = reg.idSocmed{
            parameter["id_socmed"] = idSocmed as AnyObject?
        }
        
        if let socialData = reg.socialData{
            parameter["social_data"] = socialData as AnyObject?
        }
        let url = "\(BASE_URL)\(SIGN_UP_URL)"
        
        print(" param \(parameter)")
        
        return Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }

    func getCountryList() -> DataRequest{
        let url = "\(BASE_URL)\(GET_LIST_COUNTRY_URL)"
        
        return Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(destination: .queryString))
    }
    
    func getCityList(_ cdNegara: String) -> DataRequest{
        
        var parameter : [String:AnyObject] = [
            "cd_negara" : cdNegara as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(GET_LIST_CITY_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    func getKelurahanListReg(_ cdKota: String) -> DataRequest{
        
        var parameter : [String:AnyObject] = [
            "cd_kota" : cdKota as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(GET_LIST_KELURAHAN_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    // MARK: - Feed
    func getFeed(_ codeKel: String?, page: Int, lat: Double?, lng: Double?) -> DataRequest{
        
        var parameter : [String: AnyObject] = [

            "page" : page as AnyObject
        ]
        
        if let codeKel = codeKel{
            parameter["code_kel"] = codeKel as AnyObject?
        }
        if let lat = lat{
            parameter["lat"] = lat as AnyObject?
        }
        
        if let lng = lng{
            parameter["lng"] = lng as AnyObject?
        }
        
        parameter.update(defaultParam)
        
        print("param: \(parameter)")
        let url = "\(BASE_URL)\(FEED_FUSHION_PATH_URL)"
        
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }
    
    func getFeedByCategory(category search: String, codeKel: String, start: Int, finish: Int, lat: Double, lng: Double) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            
            "search" : search as AnyObject,
            "code_kel" : codeKel as AnyObject,
            "start" : start as AnyObject,
            "finish" : finish as AnyObject,
            "lat" : lat as AnyObject,
            "lng" : lng as AnyObject
            
        ]
        
        parameter.update(defaultParam)
        
        print("param: \(parameter)")
        let url = "\(BASE_URL)\(FEED_BY_CATEGORY)"
        
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }

    func getFeedByHashtag(hashtag search: String, codeKel: String, start: Int, finish: Int, lat: Double, lng: Double) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            
            "search" : search as AnyObject,
            "code_kel" : codeKel as AnyObject,
            "start" : start as AnyObject,
            "finish" : finish as AnyObject,
            "lat" : lat as AnyObject,
            "lng" : lng as AnyObject
            
        ]
        
        parameter.update(defaultParam)
        
        print("param: \(parameter)")
        let url = "\(BASE_URL)\(FEED_BY_HASHTAG_URL)"
        
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }

    func getFeedDetail(feedId id: Int) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "id" : id as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(FEED_DETAIL_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }
    
    func getFeedProgressDetail(_ postId: Int, type: String) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "post_id" : postId as AnyObject,
            "type" : type as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(FEED_PROGRESS_DETAIL_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }

    func getFeedProgressRating(_ postId: Int, progressId: String) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "post_id" : postId as AnyObject,
            "progress_id" : progressId as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(FEED_PROGRESS_GET_RATING_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }

    func postFeedProgressRating(_ postId: Int, progressId: String, rate: Double) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "review" : rate as AnyObject,
            "description" : "" as AnyObject,
            "post_id" : postId as AnyObject,
            "progress_id" : progressId as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(FEED_PROGRESS_POST_RATING_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }
    
    func postLike(withFeedId postId: String) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "post_id" :postId as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(POST_LIKE_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    func postFavorite(withFeedId feedId: String) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "pid" :feedId as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(POST_FAVORITE_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func getWeather(_ lat: Double, lng: Double) -> DataRequest{
        let parameter : [String: AnyObject] = [
            "lat" : lat as AnyObject,
            "lng" : lng as AnyObject
        ]
        
        let url = "\(BASE_URL)\(GET_WEATHER_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    
    // MARK: - Report
    func postReport(_ report: QlueReport, completion: @escaping CompletionHandler) {
        
        QlueReport().createParameterWithReport(report) { (param) in
            var parameter : [String:AnyObject] = param as [String : AnyObject]
            parameter["id"] = User.currentUser()?.userId as AnyObject?? ?? "" as AnyObject?
            parameter["target_fund"] = 0 as AnyObject?
            parameter.update(self.defaultParam)
            //          print("param: \(parameter)")
            
            let url = "\(BASE_URL)\(POST_REPORT_URL)"
            print(url)
            let req =  Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding(destination: .httpBody)).responseString { (res) in
                
                if res.result.isSuccess{
                    if let result = res.result.value{
                        let data = JSON(parseJSON: result)
                        print(data)
                        if data["success"].string == "false"{
                            completion(data["message"].stringValue, nil)
                        } else if data["success"].string == "key invalid"{
                            Engine.shared.logout()
                        }
                        else{
                            completion(true, nil)
                        }
                    }
                } else if let error = res.result.error{
                    completion(nil, error)
                } else{
                    
                }
            }

            
        
        }
        
        
    }
    
    
    func getPlaceListSwasta(_ radius: Int, lat: Double, long: Double) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "radius": radius as AnyObject,
            "lat" : lat as AnyObject,
            "lng" : long as AnyObject
        ]
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(GET_LIST_PLACE_SWASTA)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }
    
    func getTopicListSwasta(_ bahasa: String = "ind", lat: Double, long: Double) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "bahasa": bahasa as AnyObject,
            "lat" : lat as AnyObject,
            "lng" : long as AnyObject
        ]
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(GET_LIST_TOPIC_SWASTA)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }
    
    func getTopicListGoverment(_ bahasa: String = "ind", lat: Double, long: Double) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "bahasa": bahasa as AnyObject,
            "lat" : lat as AnyObject,
            "lng" : long as AnyObject
        ]
        parameter.update(defaultParam)
        
        print("param \(parameter)")
        let url = "\(BASE_URL)\(GET_LIST_TOPIC_GOVERMENT)"
        print(url)
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }
    
     func getCategoryPlace(_ bahasa: String = "eng") -> DataRequest{
          var parameter : [String: AnyObject] = [
               "bahasa": bahasa as AnyObject
          ]
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(GET_CATEGORY_PLACE)"
         
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
          
     }
     
     func postPlaceSwasta(_ lat: Double, lng: Double, title: String, category: String, address: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "lat": lat as AnyObject,
               "lng": lng as AnyObject,
               "title": title as AnyObject,
               "category": category as AnyObject,
               "address": address as AnyObject
          ]
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(POST_PLACE_SWASTA)"
          
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
    
    // MARK: - Profile
    
    func getDetailProfile(userId fid: String) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "fid": fid as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(GET_PROFILE_DETAIL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func listReportUsert(userId fid: String, start: Int, finish: Int) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "fid": fid as AnyObject,
            "start":  start as AnyObject,
            "finish": finish as AnyObject
        ]
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(LIST_REPORT_USER)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func listTrophyUser(userId fid: String) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "fid": fid as AnyObject,
            "limit":  0 as AnyObject
        ]
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(LIST_TROPHY_USER)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func listForumUser(userId fid: String, start: Int, finish: Int) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "fid": fid as AnyObject,
            "start":  start as AnyObject,
            "finish" : finish as AnyObject,
            "param" : 0 as AnyObject
        ]
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(LIST_FORUM_USER)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func listFollowerUser(userId fid: String, start: Int = 0, finish: Int = 25) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "fid": fid as AnyObject,
            "start" : start as AnyObject,
            "finish" : finish as AnyObject
        ]
        
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(GET_PROFILE_FOLLOWERS)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func listAvatarUser(userId fid: String) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "fid": fid as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(LIST_AVATAR_USER)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func listAvatarCurrentUser(avatar ava: String) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "avatar": ava as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(LIST_AVATAR_CURRENT_USER)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func blockUser(userId fid: String) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "fid": fid as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(BLOCK_USER_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func listChooseReportUser() -> DataRequest{
        
        let url = "\(BASE_URL)\(LIST_CHOOSE_REPORT_URL)"
        
        return Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(destination: .queryString))
    }
    
    func postChooseReportUser(userId pid: String, reportId: String) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "pid": pid as AnyObject,
            "reportId": reportId as AnyObject,
            "param": 1 as AnyObject
        ]
        
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(POST_CHOOSE_REPORT_URL)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
     // MARK: - Chat
     func getListChat() -> DataRequest{
          var parameter : [String: AnyObject] = [
               "limit": 0 as AnyObject
          ]
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(GET_LIST_CHAT)"
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func getChatDetail(withGroupId groupId: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "groupid": groupId as AnyObject,
               "limit" : 0 as AnyObject
          ]
          
          parameter.update(defaultParam)
          
          let url = "\(BASE_URL)\(GET_CHAT_DETAIL)"
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func chatRead(withGroupId groupId: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "groupid": groupId as AnyObject
          ]
          parameter.update(defaultParam)
          
          let url = "\(BASE_URL)\(CHAT_READ)"
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
    
    func postChat(withGroupId groupId: String, rid: String, message: String, image: UIImage? = nil) -> DataRequest {
        
        var parameter : [String: Any] = [
            "groupid": groupId,
            "rid": rid,
            "message": message.convertEmojiToUnicode()
        ]
        
        if let image = image{
            let imageData = UIImageJPEGRepresentation(image, 0.5)
            
            if let data = imageData?.base64EncodedString(options: .lineLength64Characters){
                parameter["file"] = data
            }
        }
        parameter.update(defaultParam)
        
        
        let url = "\(BASE_URL)\(POST_CHAT)"
        return Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }

    
    // MARK: - Notif
    func getNotif(from start: Int, id: String, city: String, deviceName: String) -> DataRequest {
        
        var parameter : [String: AnyObject] = [
            "start": start as AnyObject,
            "id": id as AnyObject,
            "city": city as AnyObject,
            "device_name": deviceName as AnyObject
        ]
        
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(NOTIFICATION_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func readNotif(with id: String, type: String) -> DataRequest {
        
        var parameter : [String: AnyObject] = [
            "pid": id as AnyObject,
            "type": type as AnyObject,
            ]
        
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(READ_NOTIFICATION_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func registerNotif(_ token: String) -> DataRequest {
        
        var parameter : [String: AnyObject] = [
            "gcm": token as AnyObject
        ]
        
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(REGISTER_NOTIFICATION_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func unregisterNotif(_ token: String) -> DataRequest {
        
        var parameter : [String: AnyObject] = [
            "gcm": token as AnyObject
        ]
        
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(UNREGISTER_NOTIFICATION_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    // MARK: - Comment
    func getCommentList(_ id: String, sortBy: String = "old", page: Int) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "id": id as AnyObject,
            "sort_by" : sortBy as AnyObject,
            "start" : page as AnyObject
        ]
        parameter.update(defaultParam)
        print("param \(parameter)")
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(GET_LIST_COMMENT)"
        //        let url = "http://services.qlue.id/api/V2.5/mobile_clusterDetail_comment.php?provinsi=DAERAH+KHUSUS+IBUKOTA+JAKARTA&uid=40515&country=Indonesia&start=0&id=478509&sort_by=old&source=ios&version=2.4.5&key=CihJjOvIQ4wdzTPIezXBVDQS3r4Xsr9g&type_region=0"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }
    
    
    
    
    
    func postComment(_ postId: String, comment: String) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "post_id": postId as AnyObject,
            "comment" : comment as AnyObject
        ]
        parameter.update(defaultParam)
        print("param \(parameter)")
        let url = "\(BASE_URL)\(POST_COMMENT)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func getCommentDiskusiList(_ id: String, sortBy: String = "old", page: Int) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "id": id as AnyObject,
            "sort_by" : sortBy as AnyObject,
            "start" : page as AnyObject,
            "finish" : 5 as AnyObject
        ]
        parameter.update(defaultParam)
        print("param \(parameter)")
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(LIST_COMMENT_FORUM)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }
    
    func postCommentForum(withId id: String, comment: String, file: [UIImage]) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "wid": id as AnyObject,
            "comment" : comment.convertEmojiToUnicode() as AnyObject
        ]
        parameter.update(defaultParam)
        var images : [String] = []
        for image in file{
            let imageData = UIImagePNGRepresentation(image)
            if let data = imageData?.base64EncodedString(options: .lineLength76Characters){
                images.append(data)
            }
        }
        parameter["file"] = "\(images)" as AnyObject?
        print(parameter)
        let url = "\(BASE_URL)\(POST_FORUM_COMMENT)"
        
        return Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding(destination: .queryString))
        
    }
    func deleteComment(withCommentId commentId: String, type: String) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "comment_id": commentId as AnyObject,
            "type" : type as AnyObject,
            "action" : "delete" as AnyObject
        ]
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(DELETE_COMMENT)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    
    
    // MARK: - Avatar
    
    func getAvatarList(from start: Int, sex: String) -> DataRequest {
        
        var parameter : [String: AnyObject] = [
            "sex": sex as AnyObject,
            "limit": start as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(AVATAR_LIST_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func changeAvatar(_ avatarUrl: String) -> DataRequest {
        
        var parameter : [String: AnyObject] = [
            "avatar": avatarUrl as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(CHANGE_AVATAR_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func buyAvatar(_ avatarUrl: String) -> DataRequest {
        
        var parameter : [String: AnyObject] = [
            "avatar": avatarUrl as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(BUY_AVATAR_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
     
         // MARK: - Geocoding google
     func reverseGeocodingForCoordinate(_ coordinate: CLLocationCoordinate2D, completion: @escaping CompletionHandler) -> DataRequest {
          
          let latitude = NSString(format: "%+.6f", coordinate.latitude)
          let longitude = NSString(format: "%+.6f", coordinate.longitude)
          
          let urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=\(kGoogleMaps)"
          
          let request = Alamofire.request(urlString, method: .get, parameters: nil).responseJSON { (response) -> Void in
               
               if let uJson = response.result.value {
                    let json = JSON(uJson)
                    print(json)
                    completion(uJson, nil)
               }
               else if let error = response.result.error {
                    completion(nil, error)
               }
               else {
                    completion(nil, nil)
               }
          }
          
          return request
     }
    
    // MARK: - List Favorite
    func getListFavorite(_ start: Int, finish: Int) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "start": start as AnyObject,
            "finish" : finish as AnyObject,
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(LIST_FAVORITE_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
     // MARK: - Kelurahan
     func getTrophy(_ codeKel: String, district: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "kd_kel": codeKel as AnyObject,
               "district" : district as AnyObject,
               "limit" : "0" as AnyObject
          ]
          parameter.update(defaultParam)
          
          let url = "\(BASE_URL)\(LIST_TROPHY_KELURAHAN_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func joinKelurahan(_ codeKel: String, district: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "code_kel": codeKel as AnyObject,
               "district" : district as AnyObject
          ]
          parameter.update(defaultParam)
          print("param:\(parameter)")
          
          let url = "\(BASE_URL)\(JOIN_KELURAHAN_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func checkRatingKelurahan(_ codeKel: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "code": codeKel as AnyObject
          ]
          parameter.update(defaultParam)
          print("param:\(parameter)")
          
          let url = "\(BASE_URL)\(CHECK_KELURAHAN_RATING_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func profileKelurahan(_ codeKel: String, district: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "code": codeKel as AnyObject,
               "district" : district as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(PROFILE_KELURAHAN_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func postRatingKelurahan(_ codeKel: String, rate: Double, district: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "code": codeKel as AnyObject,
               "rate" : rate as AnyObject,
               "district" : district as AnyObject
          ]
          parameter.update(defaultParam)
          print("param:\(parameter)")
          
          let url = "\(BASE_URL)\(POST_RATING_KELURAHAN)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     func listMemberKelurahan(_ codeKel: String, district: String, start: Int = 0, finish: Int = 25) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "code_kel": codeKel as AnyObject,
               "district" : district as AnyObject,
               "start" : start as AnyObject,
               "finish" : finish as AnyObject
          ]
          
          parameter.update(defaultParam)
          print("pram : \(parameter)")
          let url = "\(BASE_URL)\(LIST_MEMBER_KELURAHAN_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func followMember(withId id: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "fid": id as AnyObject
          ]
          
          parameter.update(defaultParam)
          print("pram : \(parameter)")
          let url = "\(BASE_URL)\(FOLLOW_MEMBER_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func listLaporanKelurahan(withCodeKel id: String, district: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "kelurahan": id as AnyObject,
               "district" : district as AnyObject
          ]
          
          parameter.update(defaultParam)
          print("pram : \(parameter)")
          let url = "\(BASE_URL)\(LIST_LAPORAN_KELURAHAN_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     func gridLaporanKelurahan(withCodeKel id: String, district: String, category: String, start: Int = 0, finish: Int = 25) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "kelurahan": id as AnyObject,
               "district" : district as AnyObject,
               "category" : category as AnyObject,
               "start" : start as AnyObject,
               "finish" : finish as AnyObject
          ]
          
          parameter.update(defaultParam)
          print("pram : \(parameter)")
          let url = "\(BASE_URL)\(LAPORAN_GRID_KELURAHAN)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }

     
     // MARK: - Diskusi
     
     func favoriteDiskusi(withId id: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "wid": id as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(FAVORITE_FORUM_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func getListSeen(withId id: String, start: Int = 0, type: Int = 1) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "pid": id as AnyObject,
               "start" : start as AnyObject,
               "type" : type as AnyObject
          ]
          
          parameter.update(defaultParam)
          print(parameter)
          let url = "\(BASE_URL)\(LIST_SEEN_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func listDiskusiKelurahan(_ codeKel: String, start: Int = 0) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "code_kel": codeKel as AnyObject,
               "start" : start as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(LIST_DISKUSI_KELURAHAN_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func listForumGeneral(_ start: Int = 0) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "start" : start as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(LIST_FORUM)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func getDetailForum(withId id: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "wid": id as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(DETAIL_FORUM_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
    func getTopicForum(isKelurahan isKel: Bool) -> DataRequest{
        
        var parameter: [String: Any] = [:]
        if isKel {
            parameter["type"] = "kelurahan"
        }
        else {
            parameter["type"] = "kelurahan"
        }
        
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(GET_TOPIC_FORUM)"
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
     func postVoteForum(_ forumId: String, voteId: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "id" : voteId as AnyObject,
               "wid" : forumId as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(POST_VOTE_FORUM)"
          
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func postForum(_ title: String, desc: String, categoryId: String, lat: Double, lng: Double, isVote: Bool, listVote: String, file: [UIImage]) -> DataRequest{
          var parameter : [String: Any] = [
               "title" : title,
               "desc" :  desc,
               "uid" : User.currentUser()?.userId ?? "",
               "category_id" : categoryId,
               "lat" : lat,
               "lng" : lng,
               "isVote" : isVote,
               "list_vote" : listVote,
               "provinsi": User.currentUser()?.provinsi ?? "",
               "type_region": "0",
               "country": "Indonesia",
               "version": "2.0",
               "key": User.currentUser()?.randkey ?? "",
               "source": "ios",
          ]
          
          var images : [String] = []
          for image in file{
               let imageData = UIImagePNGRepresentation(image)
            if let data = imageData?.base64EncodedString(options: .lineLength76Characters) {
                    images.append(data)
               }
          }
          parameter["file"] = "\(images)" as AnyObject?
          
          let url = "\(BASE_URL)\(POST_FORUM_URL)"
//          print(parameter)
          
          return Alamofire.request(url, method: .post, parameters: parameter, encoding: URLEncoding(destination: .httpBody))
     }
     
     func editForum(withForumId forumId: String, desc: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "desc" :  desc as AnyObject,
               "param" : 1 as AnyObject,
               "wid" : forumId as AnyObject
          ]
          
          parameter.update(defaultParam)
          
          let url = "\(BASE_URL)\(EDIT_FORUM_URL)"
          print(parameter)
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func deleteForum(withForumId forumId: String, categoryId: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "category_id" :  categoryId as AnyObject,
               "param" : 0 as AnyObject,
               "wid" : forumId as AnyObject
          ]
          
          parameter.update(defaultParam)
          
          let url = "\(BASE_URL)\(EDIT_FORUM_URL)"
          print(parameter)
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     // MARK: - Ranking
     func listRankingUser(withType type: String, start: Int = 0, finish: Int = 25, limit: Int = 25) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "type" : type as AnyObject,
               "start" : start as AnyObject,
               "finish" : finish as AnyObject,
               "limit" : limit as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(LIST_RANKING_USER_URL)"
          
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }

     func listRankingSwasta(withType type: String, start: Int = 0, finish: Int = 25, limit: Int = 25) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "type" : type as AnyObject,
               "start" : start as AnyObject,
               "finish" : finish as AnyObject,
               "limit" : limit as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(LIST_RANKING_SWASTA_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func listRankingKelurahan(withType type: String = "top",lat: Double, lng: Double,  limit: Int = 25) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "type" : type as AnyObject,
               "lat" : lat as AnyObject,
               "lng" : lng as AnyObject,
               "limit" : limit as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(LIST_RANKING_GOV_URL)"
          
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }

     func listRankingKecamatan(withType type: String = "top",lat: Double, lng: Double,  limit: Int = 25) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "type" : type as AnyObject,
               "lat" : lat as AnyObject,
               "lng" : lng as AnyObject,
               "limit" : limit as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(LIST_RANKING_GOV_KECAMATAN_URL)"
          
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }

     func listRankingKotamadya(withType type: String = "top",lat: Double, lng: Double,  limit: Int = 25) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "type" : type as AnyObject,
               "lat" : lat as AnyObject,
               "lng" : lng as AnyObject,
               "limit" : limit as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(LIST_RANKING_GOV_KOTAMADYA_URL)"
          
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func listRankingDinas(withType type: String = "top",lat: Double, lng: Double,  limit: Int = 25) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "type" : type as AnyObject,
               "lat" : lat as AnyObject,
               "lng" : lng as AnyObject,
               "limit" : limit as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(LIST_RANKING_GOV_DINAS_URL)"
          
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }

     func listRankingStaff(withType type: String = "top",lat: Double, lng: Double,  limit: Int = 25) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "type" : type as AnyObject,
               "lat" : lat as AnyObject,
               "lng" : lng as AnyObject,
               "limit" : limit as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(LIST_RANKING_GOV_STAFF_URL)"
          
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
    
    func listRankingKecamatanByKota(_ kota: String, type: String = "top", lat: Double, lng: Double,  limit: Int = 25) -> DataRequest{
        var parameter : [String: Any] = [
            "kota" : kota,
            "type" : type,
            "lat" : lat,
            "lng" : lng,
            "limit" : limit
        ]
        
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(LIST_RANKING_KECAMATAN_BY_KOTAMADYA_URL)"
        
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    func listRankingKelurahanByKecamatan(_ kecamatan: String, type: String = "top", lat: Double, lng: Double,  limit: Int = 25) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "kecamatan" : kecamatan as AnyObject,
            "type" : type as AnyObject,
            "lat" : lat as AnyObject,
            "lng" : lng as AnyObject,
            "limit" : limit as AnyObject
        ]
        
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(LIST_RANKING_KELURAHAN_BY_KECAMATAN_URL)"
        
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    func listRankingDinasDetail(dinasName name: String, type: String = "top", lat: Double, lng: Double,  limit: Int = 25) -> DataRequest{
        var parameter : [String: Any] = [
            "name" : name,
            "type" : type,
            "lat" : lat,
            "lng" : lng,
            "limit" : limit
        ]
        
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(LIST_RANKING_DINAS_DETAIL)"
        
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func listRankingSwastaDetail(swastaName name: String, type: String = "top", lat: Double, lng: Double,  limit: Int = 25) -> DataRequest{
        var parameter : [String: AnyObject] = [
            "name" : name as AnyObject,
            "type" : type as AnyObject,
            "lat" : lat as AnyObject,
            "lng" : lng as AnyObject,
            "limit" : limit as AnyObject
        ]
        
        parameter.update(defaultParam)
        let url = "\(BASE_URL)\(LIST_RANKING_SWASTA_DETAIL_URL)"
        
        
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    


     // MARK: - Search
     func searchAll(withQuery query: String) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "search" : query as AnyObject
          ]
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(SEARCH_ALL_URL)"
          
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func searchKelurahan(_ start: Int = 0, finish: Int = 15, query: String, lat: Double, lng: Double ) -> DataRequest{
          var parameter : [String: AnyObject] = [
               "start": start as AnyObject,
               "finish" : finish as AnyObject,
               "kelurahan" : query as AnyObject,
               "lat" : lat as AnyObject,
               "lng" : lng as AnyObject
          ]
          parameter.update(defaultParam)
          
          parameter.update(defaultParam)
          let url = "\(BASE_URL)\(SEARCH_KELURAHAN_URL)"
          
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func searchUsername(_ username: String, start: Int = 0, finish: Int = 15) -> DataRequest{
          
          var parameter : [String: AnyObject] = [
               "username": username as AnyObject,
               "start": start as AnyObject,
               "finish" : finish as AnyObject,
          ]
          parameter.update(defaultParam)
          
          let url = "\(BASE_URL)\(SEARCH_USERNAME)"
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func searchForum(_ query: String, start: Int = 0, finish: Int = 15) -> DataRequest{
          
          var parameter : [String: AnyObject] = [
               "search": query as AnyObject,
               "start": start as AnyObject,
               "finish" : finish as AnyObject,
          ]
          parameter.update(defaultParam)
          
          let url = "\(BASE_URL)\(SEARCH_FORUM_URL)"
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
     
     func searchQlue(_ query: String, start: Int = 0, finish: Int = 15) -> DataRequest{
          
          var parameter : [String: AnyObject] = [
               "search": query as AnyObject,
               "start": start as AnyObject,
               "finish" : finish as AnyObject,
          ]
          parameter.update(defaultParam)
          
          let url = "\(BASE_URL)\(SEARCH_QLUE_URL)"
          return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
     }
    
    func searchLive(_ query: String, start: Int = 0, finish: Int = 15, lat: Double, lng: Double, filterId: Int, subfilterId: Int) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "search": query as AnyObject,
            "start": start as AnyObject,
            "finish" : finish as AnyObject,
            "lat" : lat as AnyObject,
            "lng" : lng as AnyObject,
            "filter_id" : 0 as AnyObject,
            "subfilter_id" : "" as AnyObject
            ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(SEARCH_LIVE_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func searchPop(_ query: String, start: Int = 0, finish: Int = 15, lat: Double, lng: Double, filterId: Int, subfilterId: Int) -> DataRequest{
        
        var parameter : [String: AnyObject] = [
            "search": query as AnyObject,
            "start": start as AnyObject,
            "finish" : finish as AnyObject,
            "lat" : lat as AnyObject,
            "lng" : lng as AnyObject,
            "filter_id" : 0 as AnyObject,
            "subfilter_id" : "" as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(SEARCH_POPULAR_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    // MARK: - Helper
    func logout(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let moc = appDelegate.managedObjectContext
        
        
        if let deviceToken = FIRInstanceID.instanceID().token() {
            Engine.shared.unregisterNotif(deviceToken).responseString { (response) in
                
                if response.result.isSuccess {
                    if let result = response.result.value {
                        print(result)
                    }
                }
                else if let error = response.result.error {
                    print("error: \(error.localizedDescription)")
                }
                else {
                    
                }
            }
        }
        
        
        let entities = [
            "User", "SearchKey"
        ]
        
        for entity in entities{
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: entity)
            do{
                let items = try moc.fetch(fetchRequest)as! [NSManagedObject]
                for item in items{
                    moc.delete(item)
                }
                
                appDelegate.saveContext()
            }catch let error as NSError{
                print("error: \(error.localizedDescription)")
            }
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "Homescreen")
        
        Util.setRootViewController(viewController)
        
        
    }
    
    func parseJsonWithReq(_ request : DataRequest, controller: UIViewController, loadingStop: @escaping () -> Void,completion: @escaping (_ data: JSON, _ success: Bool, _ fail: Bool) -> Void, falseCompletion: (() -> Void)? = nil){
        request.responseString { (res) in
            loadingStop()
            print(res.request)
            if res.result.isSuccess{
                if let result = res.result.value{
                    let data = JSON.parse(result)
                    print(data)
                    if data["success"].string == "false"{
                        
                        if let falseCompletion = falseCompletion{
                            falseCompletion()
                        } else{
                            
                            Util.showSimpleAlert(data["message"].string, vc: controller, completion: nil)
                        }
                        completion(data, false, true)
                    } else if data["success"].string == "key invalid"{
                        Engine.shared.logout()
                    }
                    else{
                        completion(data, true, false)
                    }
                }
            } else if let error = res.result.error{
                print("error: \(error.localizedDescription)")
            } else{
                
            }
        }
    }
    
    // MARK: - Maps
    func listPOI(_ lat: Double, lng: Double, categoryId: Int = 1) -> DataRequest {
        
        var parameter : [String: AnyObject] = [
            "date": 3 as AnyObject,
            "lat": lat as AnyObject,
            "lng" : lng as AnyObject,
            "category_id": categoryId as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(POI_LIST_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
    
    func filterPOI(_ lat: Double, lng: Double, categoryId: Int = 1) -> DataRequest {
        
        var parameter : [String: AnyObject] = [
            "date": 3 as AnyObject,
            "lat": lat as AnyObject,
            "lng" : lng as AnyObject,
            "category_id": categoryId as AnyObject
        ]
        parameter.update(defaultParam)
        
        let url = "\(BASE_URL)\(POI_FILTER_URL)"
        return Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding(destination: .queryString))
    }
}
