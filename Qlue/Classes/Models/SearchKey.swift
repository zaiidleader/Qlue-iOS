//
//  SearchKey.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/16/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import CoreData


class SearchKey: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
     static func searchKeyWithData(_ keyString: String, inManagedObjectContext moc: NSManagedObjectContext)-> SearchKey?{
          
          var key: SearchKey?
          
          let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SearchKey")
          fetchRequest.predicate = NSPredicate(format: "searchKey == %@", keyString)
          
          do {
               let keys = try moc.fetch(fetchRequest) as! [SearchKey]
               if keys.count > 0 {
                    key = keys.first
               }
               else {
                    key = NSEntityDescription.insertNewObject(forEntityName: "SearchKey", into: moc) as? SearchKey
                    key?.searchKey = keyString
                    
               }
          }
          catch let error as NSError{
               print(error.localizedDescription)
          }
          return key
          
     }
     
     static func getKeys(inManagedObjectContext moc: NSManagedObjectContext) -> [SearchKey]{
               
               var keys = [SearchKey]()
               
               let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SearchKey")
               
               do {
                    keys = try moc.fetch(request) as! [SearchKey]
               }
               catch {
                    
               }
               
               return keys
          
     }

}
