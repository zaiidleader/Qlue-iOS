
//
//  QlueMember.swift
//  Qlue
//
//  Created by Nurul on 7/25/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueMember: NSObject {
     var avatar : String?
     var follow : Bool?
     var userId : String?
     var username : String?
     var isBlocked : Bool?
     var currLevel : String?
     
     /**
      * Instantiate the instance using the passed json values to set the properties values
      */
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          avatar = json["avatar"].string
          follow = json["follow"].bool
          if let userId = json["user_id"].string{
                self.userId = userId
          } else if let userId = json["idno"].string{
          self.userId = userId
          }
          username = json["username"].string
          isBlocked = json["isBlocked"].bool
          currLevel = json["curr_level"].string
     }
}