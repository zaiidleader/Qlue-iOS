//
//  QlueChat.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/11/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueChat {
     var countUnread : String?
     var file : String?
     var groupId : String?
     var isGroup : Bool?
     var listMember : String?
     var members : [QlueMember] = []
     var message : String?
     var messageId : String?
     var name : String?
     var status : String?
     var timestamp : String?
     
     
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          countUnread = json["count_unread"].string
          file = json["file"].string
          groupId = json["group_id"].string
          isGroup = json["isGroup"].bool
          listMember = json["list_member"].string
          let memberArray = json["member"].arrayValue
          for memberJson in memberArray{
               let value = QlueMember(fromJson: memberJson)
               members.append(value)
          }
          message = json["message"].string
          messageId = json["message_id"].string
          name = json["name"].string
          status = json["status"].string
          timestamp = json["timestamp"].string
     }

}