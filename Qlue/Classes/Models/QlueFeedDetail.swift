//
//  QlueFeedDetail.swift
//  Qlue
//
//  Created by Nurul on 7/11/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueFeedDetail: NSObject {
    var feedId: String?
     var thumb: String?
     var file: String?
    var title: String?
    var address: String?
    var lat: Double?
    var long: Double?
    var label2: String?
    var viewCount: String?
    var commentCount: String?
    var username: String?
    var avatar: String?
    var timestamp: Date?
    var desc: String?
    var progress: feedState?
    var kecamatan: String?
     var kelurahan: String?
     var kdKel: String?
     var district: String?
     var reportType: QlueReportType?
     var format: String?
     var like: Bool?
     var likeCount: String?
     

     var history: Bool?
    
    static func feedDetailWithData(_ data: JSON) -> QlueFeedDetail{
        
        let feed = QlueFeedDetail()
        
        feed.feedId = data["id"].string
     feed.file = data["file"].string
     feed.thumb = data["thumbnail"].string
        feed.title = data["title"].string
        feed.address = data["poi"]["address"].string
        if let latStr = data["lat"].string{
            feed.lat = Double.init(latStr)
        }
        if let lngStr = data["lng"].string{
            feed.long = Double.init(lngStr)
        }
        feed.label2 = data["label2"].string
        feed.viewCount = data["view_count"].string
        feed.commentCount = data["comment_count"].string
        feed.username = data["username"].string
        feed.avatar = data["avatar"].string
        feed.timestamp = Date().getDateFromTimestamp(data["timestamp"].string)
        feed.desc = data["description"].string
     feed.format = data["format"].string
        if let progressStr = data["progress"].string{
            feed.progress = feedState(rawValue: progressStr)
        }
        feed.kecamatan = data["kecamatan"].string
     feed.kelurahan = data["kelurahan"].string
     feed.kdKel = data["kd_kel"].string
     feed.district = data["district"].string
     
     if let reportStr = data["tags"].array?.first?.string{
          feed.reportType = QlueReportType(rawValue: reportStr)
     }
     
     feed.like = data["like"].bool
     feed.likeCount = data["like_count"].string
     
     
     feed.history = data["history"].bool
     
        return feed
    
    }
    
}

