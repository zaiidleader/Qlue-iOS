//
//  SearchKey+CoreDataProperties.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/16/16.
//  Copyright © 2016 Qlue. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SearchKey {

    @NSManaged var searchKey: String?

}
