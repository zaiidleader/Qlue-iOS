//
//  QlueKelurahanLaporan.swift
//  Qlue
//
//  Created by Nurul on 7/31/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON

class QlueKelurahanLaporan: NSObject {

     var label: String?
     var data: [QlueFeed] = []
     
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          label = json["label"].string
          
          for d in json["data"].arrayValue{
               data.append(QlueFeed.feedWithData(d))
          }
          
     }

     
}
