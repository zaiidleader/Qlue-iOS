//
//  QlueRegistration.swift
//  Qlue
//
//  Created by Nurul on 8/17/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation

enum LoginType{
     case google
     case facebook
     case twitter
     case qlue
     
     var description: String{
          switch self{
          case .google :
               return "GooglePlus"
          case .facebook:
               return "Facebook"
          case .twitter:
               return "Twitter"
          case .qlue:
               return "Qlue"
          }
     }
     
     var regis: Int{
          switch self{
          case .qlue :
               return 0
          default:
               return 1
          }
     }
}

class QlueRegistration {
     var email: String! = ""
     var username: String! = ""
     var phone: Int! = 0
     var gender: Gender! = .Male
     var fullname: String! = ""
     var regis: Int = 0
     var dob: Date?
     var idSocmed: String?
     var socialData: [String: Any]?
     var source: String! = "ios"
     var password: String = ""
     var kdKel: String = ""
     
     init(){
     
     }
     
     init(username: String, phone: Int, email: String, gender: Gender, fullname: String, dob: Date?, idSocmed: String, socialData: [String: Any]?, type: LoginType){
          self.email = email
          self.username = username
          self.phone = phone
          self.gender = gender
          self.fullname = fullname
          self.regis = type.regis
          self.dob = dob
          self.idSocmed = idSocmed
          self.socialData = socialData
          self.source = type.description
     }
     
     func getParameter() -> [String: Any] {
     
          let parameter:  [String: Any] = [
               "gender": self.gender.short,
               "code_kel": self.kdKel,
               "regis" : 0,
               "uid" : self.username,
               "full_name" : self.username,
               "dob" : "1991-04-26",
               "pwd" : self.password,
               "email" : self.email
          ]
          
          
          return parameter
     }
}
