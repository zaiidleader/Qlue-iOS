//
//  QlueChatDetail.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/12/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

enum ChatDetailType{
     case rightText
     case leftText
     case rightImage
     case leftImage
     
     case groupRightText
     case groupLeftText
     case groupRightImage
     case groupLeftImage
}

class QlueChatDetail {
     var id : String?
     var avatar : String?
     var file : String?
     var groupId : String?
     var message : String?
     var status : String?
     var timestamp : String?
     var userId : String?
     var username : String?
     var type : ChatDetailType = .rightText
     
     
     var isSend: Bool = true
     var imageSend: UIImage?
     
     
     /**
      * Instantiate the instance using the passed json values to set the properties values
      */
     init(fromJson json: JSON!, isGroup: Bool){
          if json == nil{
               return
          }
          id = json["Id"].string
          avatar = json["avatar"].string
          file = json["file"].string
          groupId = json["group_id"].string
          message = json["message"].string
          status = json["status"].string
          timestamp = json["timestamp"].string
          userId = json["user_id"].string
          username = json["username"].string
          
          if json["user_id"].string == User.currentUser()?.userId{
               if let file = json["file"].string, file != ""{
                    if isGroup == true{
                         type = .groupRightImage
                    } else{
                         type = .rightImage
                    }
               } else{
                    if isGroup == true{
                         type = .groupRightText
                    } else{
                         type = .rightText
                    }
               }
          } else{
               if let file = json["file"].string, file != ""{
                    if isGroup == true{
                         type = .groupLeftImage
                    } else{
                         type = .leftImage
                    }
               } else{
                    if isGroup == true{
                         type = .groupLeftText
                    } else{
                         type = .leftText
                    }
               }
          }
          
     }

}
