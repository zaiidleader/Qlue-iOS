//
//  QlueTopic.swift
//  Qlue
//
//  Created by Nurul on 7/13/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueParentTopic: NSObject{
     var suggestId: String?
     var suggestName: String?
     var suggestIcon: String?
     var groupTopic: [QlueTopic] = []
     
     static func parentTopicWithData(_ data: JSON) -> QlueParentTopic{
          let topic = QlueParentTopic()
          
          topic.suggestId = data["suggest_id"].string
          topic.suggestName = data["suggest_name"].string
          topic.suggestIcon = data["suggest_icon"].string
          
          for t in data["group_topic"].arrayValue{
               topic.groupTopic.append(QlueTopic.topicWithData(t))
          }
          
          return topic
     }
     
}
class QlueTopic: NSObject{
    var suggestId: String?
    var suggestName: String?
    var suggestIcon: String?
    
    
    static func topicWithData(_ data: JSON) -> QlueTopic{
        let topic = QlueTopic()
        
        topic.suggestId = data["suggest_id"].string
        topic.suggestName = data["suggest_name"].string
        topic.suggestIcon = data["suggest_icon"].string
        
        return topic
    }
}
