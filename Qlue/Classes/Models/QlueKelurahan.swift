//
//  QlueKelurahan.swift
//  Qlue
//
//  Created by Nurul on 7/23/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueKelurahan: NSObject {
     
     var codeKel: String?
     var name: String?
     var avatar: String?
     var district: String?
     var lurahId: String?
     var fullName: String?
     var provinsi: String?
     var jumlahpenduduk: String?
     var banner: String?
     var lat: Double?
     var lng: Double?
     var desc: String?
     var noTelp: String?
     var rating: Double?
     var ratingCount: String?
     var jumlahlaporan: Int?
     var jumlahdiskusi: String?
     var laporanterakhirGov: String?
     var laporanterakhirBuss: String?
     var diskusiterakhir: String?
     var memberCount: Int?
     var myKel: Bool?
     var join: Bool?
     var joinKelOther: Bool?
     var joinKel: Bool?
     var rank: String?
     var wait: String?
     var process: String?
     var complete: String?
     var code: String?
     
     
     var ratingForUser: Double = 0
     
//     static func kelurahanWithData(json: JSON) -> QlueKelurahan{
//          
//          let kelurahan = QlueKelurahan()
//          
//          
//          kelurahan.avatar = json["avatar"].string
//          kelurahan.banner = json["banner"].string
//          kelurahan.code = json["code"].string
//          kelurahan.complete = json["complete"].string
//          kelurahan.desc = json["description"].string
//          kelurahan.diskusiterakhir = json["diskusiterakhir"].string
//          kelurahan.district = json["district"].string
//          kelurahan.fullName = json["full_name"].string
//          kelurahan.join = json["join"].bool
//          kelurahan.joinKel = json["joinKel"].bool
//          kelurahan.joinKelOther = json["joinKelOther"].bool
//          kelurahan.jumlahdiskusi = json["jumlahdiskusi"].string
//          kelurahan.jumlahlaporan = json["jumlahlaporan"].int
//          kelurahan.jumlahpenduduk = json["jumlahpenduduk"].string
//          kelurahan.codeKel = json["kd_kel"].stringValue
//          kelurahan.laporanterakhirBuss = json["laporanterakhirBuss"].stringValue
//          kelurahan.laporanterakhirGov = json["laporanterakhirGov"].stringValue
//          if let latStr = json["lat"].string{
//               kelurahan.lat = Double.init(latStr)
//          }
//          if let lngStr = json["lng"].string{
//               kelurahan.lng = Double.init(lngStr)
//          }
//          kelurahan.lurahId = json["lurah_id"].string
//          kelurahan.memberCount = json["member_count"].int
//          kelurahan.myKel = json["myKel"].bool
//          kelurahan.name = json["name"].string
//          kelurahan.noTelp = json["no_telp"].string
//          kelurahan.process = json["process"].string
//          kelurahan.provinsi = json["provinsi"].string
//          kelurahan.rank = json["rank"].string
//          
//          if let rating = json["rating"].string{
//               kelurahan.rating = Double.init(rating)
//          }
//          kelurahan.ratingCount = json["rating_count"].string
//          kelurahan.wait = json["wait"].string
//
//          
//          return kelurahan
//          
//          
//     }
     
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          avatar = json["avatar"].string
          banner = json["banner"].string
          code = json["code"].string
          complete = json["complete"].string
          desc = json["description"].string
          diskusiterakhir = json["diskusiterakhir"].string
          district = json["district"].string
          fullName = json["full_name"].string
          join = json["join"].bool
          joinKel = json["joinKel"].bool
          joinKelOther = json["joinKelOther"].bool
          jumlahdiskusi = json["jumlahdiskusi"].string
          jumlahlaporan = json["jumlahlaporan"].int
          jumlahpenduduk = json["jumlahpenduduk"].string
          if let codeKel = json["kd_kel"].string{
               self.codeKel = codeKel
          } else if let  codeKel = json["code"].string{
               self.codeKel = codeKel
          }
          laporanterakhirBuss = json["laporanterakhirBuss"].string
          laporanterakhirGov = json["laporanterakhirGov"].string
          if let latStr = json["lat"].string{
               lat = Double.init(latStr)
          }
          if let lngStr = json["lng"].string{
               lng = Double.init(lngStr)
          }
          lurahId = json["lurah_id"].string
          memberCount = json["member_count"].int
          myKel = json["myKel"].bool
          name = json["name"].string
          noTelp = json["no_telp"].string
          process = json["process"].string
          provinsi = json["provinsi"].string
          rank = json["rank"].string
          if let rating = json["rating"].string{
               self.rating = Double.init(rating)
          }
          ratingCount = json["rating_count"].string
          wait = json["wait"].string
     }
}