//
//  QlueComment.swift
//  Qlue
//
//  Created by Nurul on 7/2/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueComment: NSObject{
     
     var commentId: String?
     var username: String?
     var avatar: String?
     var comment: String?
     var timestamp: Date?
     var userId: String?
     var postId: String?
     var image: String?
     var at: [QlueCommentUser] = []
     
     static func commentWithData(_ data: JSON) -> QlueComment{
          let comment: QlueComment = QlueComment()
          
          if let comment_id = data["comment_id"].string{
               comment.commentId = comment_id
          } else if let id = data["id"].string{
               comment.commentId = id
          }
          comment.username = data["username"].string
          comment.avatar = data["avatar"].string
          comment.comment = data["comment"].string
          comment.timestamp = Date().getDateFromTimestamp(data["timestamp"].string)
          comment.userId = data["user_id"].string
          comment.postId = data["post_id"].string
          comment.image = data["image"].arrayValue.first?.string
          
          comment.at = []
          for at in data["at"].arrayValue{
               comment.at.append(QlueCommentUser.init(fromJson: at))
          }
          
          return comment
     }
     
}

class QlueCommentUser{
     var username: String?
     var apns: String?
     var idno: String?
     var gcm: String?
     
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          username = json["username"].string
          apns = json["apns"].string
          idno = json["idno"].string
          gcm = json["gcm"].string
     }

}
