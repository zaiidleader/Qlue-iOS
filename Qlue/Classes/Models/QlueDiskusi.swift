//
//  QlueDiskusi.swift
//  Qlue
//
//  Created by Nurul on 7/24/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueDiskusi: NSObject {
     var categoryId : String?
     var commentCount : String?
     var desc : String?
     var favable : Bool?
     var file : String?
     var id : String = ""
     var isFav : Bool?
     var isSticky : Bool?
     var isVote : Bool?
     var memberCount : String?
     var thumbUrl : String?
     var timestamp : String?
     var title : String?
     var titleIcon : String?
     var userId : String?
     var username : String?
     var viewCount : String?
     var avatar : String?
     var joined : Bool?
     var isBlocked : Bool?
     var mywall : Bool?
     var isAnnouncement : Bool?
     var totalVote : Int?
     var files : [String] = []
     var votes : [QlueVote] = []
     
     var myVoteId: String = ""
     var myVoteName: String?
     
     
     /**
      * Instantiate the instance using the passed json values to set the properties values
      */
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          categoryId = json["category_id"].string
          
          if let commentCount = json["comment_count"].string{
               self.commentCount = commentCount
          }
          if let commentCount = json["comment_count"].int{
               self.commentCount = commentCount.description
          }
          
          if let viewCount = json["view_count"].string{
               self.viewCount = viewCount
          }
          if let viewCount = json["view_count"].int{
               self.viewCount = viewCount.description
          }
          
          if let memberCount = json["member_count"].string{
               self.memberCount = memberCount
          }
          if let memberCount = json["member_count"].int{
               self.memberCount = memberCount.description
          }

          desc = json["desc"].string
          favable = json["favable"].bool
          file = json["file"].string 
          id = json["id"].stringValue
          isFav = json["isFav"].bool
          isSticky = json["isSticky"].bool
          isVote = json["isVote"].bool
          thumbUrl = json["thumb_url"].string
          timestamp = json["timestamp"].string
          title = json["title"].string
          titleIcon = json["title_icon"].string
          userId = json["user_id"].string 
          username = json["username"].string
          avatar = json["avatar"].string
          joined = json["joined"].bool
          mywall = json["mywall"].bool
          isAnnouncement = json["isAnnouncement"].bool
          isBlocked = json["isBlocked"].bool
          totalVote = json["total_vote"].int
          
          for vote in json["count_vote"].arrayValue{
               votes.append(QlueVote.init(fromJson: vote))
          }
          myVoteId = json["your_vote"]["id"].stringValue
          myVoteName = json["your_vote"]["name"].string
          
          files = []
          for file in json["files"].arrayValue{
               files.append(file.stringValue)
          }

          
          
     }
}