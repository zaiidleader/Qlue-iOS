//
//  QlueForumTopic.swift
//  Qlue
//
//  Created by Andi on 8/21/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueForumTopic{
     var categoryId : String?
     var countWall : String?
     var desc : String?
     var file : String?
     var isShowAds : Bool?
     var isVoteable : Bool?
     var title : String?
     var type : String?
     
     
     /**
      * Instantiate the instance using the passed json values to set the properties values
      */
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          categoryId = json["category_id"].string
          countWall = json["count_wall"].string
          desc = json["desc"].string
          file = json["file"].string
          isShowAds = json["is_show_ads"].bool
          isVoteable = json["is_voteable"].bool
          title = json["title"].string
          type = json["type"].string
     }
     
}