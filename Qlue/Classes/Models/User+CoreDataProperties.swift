//
//  User+CoreDataProperties.swift
//  Qlue
//
//  Created by Nurul on 7/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var avatar: String?
    @NSManaged var banner: String?
    @NSManaged var bio: String?
    @NSManaged var codeKelurahan: String?
    @NSManaged var codeProvinsi: String?
    @NSManaged var coin: String?
    @NSManaged var currLevel: String?
    @NSManaged var currXp: NSNumber?
    @NSManaged var diamond: String?
    @NSManaged var email: String?
    @NSManaged var followerCount: NSNumber?
    @NSManaged var followingCount: NSNumber?
    @NSManaged var fullname: String?
    @NSManaged var kelurahan: String?
    @NSManaged var level: String?
    @NSManaged var nextLevel: String?
    @NSManaged var nextXp: NSNumber?
    @NSManaged var phone: String?
    @NSManaged var provinsi: String?
    @NSManaged var randkey: String?
    @NSManaged var sex: String?
    @NSManaged var timeJoin: Date?
    @NSManaged var userId: String?
    @NSManaged var username: String?
    @NSManaged var point: String?
    @NSManaged var reportWait: NSNumber?
    @NSManaged var reportProcess: NSNumber?
    @NSManaged var reportComplete: NSNumber?

}
