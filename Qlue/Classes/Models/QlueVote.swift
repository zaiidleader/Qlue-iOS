//
//  QlueVote.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/22/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueVote{
     var id: String?
     var name: String?
     var count: Int?
     
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
     
          id = json["id"].string
          name = json["name"].string
          count = json["count"].int
          
     }
}