//
//  QlueFeedProgress.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 8/28/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueFeedProgress {
     var descriptionField : String?
     var file : String?
     var id : String?
     var name : String?
     var period : String?
     var postId : String?
     var review : String?
     var reviewCount : String?
     var timestamp : String?
     var username : String?
     
     
     /**
      * Instantiate the instance using the passed json values to set the properties values
      */
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          descriptionField = json["description"].string
          file = json["file"].string
          id = json["id"].string
          name = json["name"].string
          period = json["period"].string
          postId = json["post_id"].string
          review = json["review"].string
          reviewCount = json["review_count"].string
          timestamp = json["timestamp"].string
          username = json["username"].string
     }
     
}
