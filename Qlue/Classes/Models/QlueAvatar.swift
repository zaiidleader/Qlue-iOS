//
//  QlueAvatar.swift
//  Qlue
//
//  Created by Bayu Yasaputro on 9/25/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import UIKit
import SwiftyJSON

class QlueAvatar: NSObject {
     
     var avatarId: String?
     var avatarName: String?
     var fileURL: String?
     var diamond: String?
     var coin: String?
     var level: String?
     var type: String?
     var desc: String?
     var isBuyed: Bool?
     var isNewAvatar: Bool?
     
     
     var grade: String?
     var isGov: Bool?
     var avatar: String?
     
     static func avatarWithData(_ data: JSON) -> QlueAvatar {
          let avatar: QlueAvatar = QlueAvatar()
          
          avatar.avatarId = data["avatarId"].string
          avatar.avatarName = data["avatarName"].string
          avatar.fileURL = data["file"].string
          avatar.diamond = data["diamond"].string
          avatar.coin = data["coin"].string
          avatar.level = data["level"].string
          avatar.type = data["type"].string
          avatar.desc = data["desc"].string
          avatar.isBuyed = data["isBuyed"].bool
          avatar.isNewAvatar = data["isNew"].bool
          
          
          avatar.grade = data["grade"].string
          avatar.isGov = data["isGov"].bool
          avatar.avatar = data["avatar"].string
          
          return avatar
     }
}
