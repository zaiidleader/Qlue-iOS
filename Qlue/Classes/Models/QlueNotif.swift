//
//  QlueNotif.swift
//  Qlue
//
//  Created by Nurul on 6/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueNotif: NSObject {

    var notifId: String?
    var desc: String?
    var timestamp: String?
    var fileUrl: String?
    var senderId: String?
    var commentCount: String?
    var type: String?
    var tags: String?
    var username: String?
    var labels: String?
    var userId: String?
    var likeCount: String?
    var status: String?
    var count: String?
    
    
    static func notifWithData(_ data: JSON) -> QlueNotif {
        let notif: QlueNotif = QlueNotif()
        
        notif.notifId = data["id"].string
        notif.desc = data["userComment"].string
        notif.timestamp = data["timestamp"].string
        notif.fileUrl = data["file"].string
        notif.senderId = data["senderId"].string
        notif.commentCount = data["comment_count"].string
        notif.type = data["type"].string
        notif.tags = data["tags"].string
        notif.username = data["username"].string
        notif.labels = data["labels"].string
        notif.userId = data["userId"].string
        notif.likeCount = data["like_count"].string
        notif.status = data["status"].string
        notif.count = data["count"].string
        
        return notif
    }
    
}

