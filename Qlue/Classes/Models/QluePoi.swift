//
//  QluePoi.swift
//  Qlue
//
//  Created by Nurul on 7/13/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QluePoi: NSObject{
    var placemarkId: String?
    var lat: Double?
    var lng: Double?
    var title: String?
    var address: String?
    var name: String?
    var calcRate: String?
    var totalReviewer: String?
    var file: String?
    var icon: String?
    var distance: String?
    var categoryId: String?
    
    var tags: [AnyObject] = []
    var thumb: String?
    var progress: String?
    var format: String?
    var videoMetadata: AnyObject?
    var thumbnail: String?
    var username: String?
    var desc: String?
    var timestamp: String?
    var labels: String?
    
    var note: String = ""
    
    static func poiWithData(_ data: JSON) -> QluePoi{
        
        let poi: QluePoi = QluePoi()
        
        if let id = data["id"].int{
            poi.placemarkId = id.description
        } else{
            poi.placemarkId = data["placemarkId"].string
        }
        
        if let latStr = data["lat"].string {
            poi.lat = Double.init(latStr)
        }
        if let lngStr = data["lng"].string {
            poi.lng = Double.init(lngStr)
        }
        poi.title = data["title"].string
        poi.address = data["address"].string
        poi.file = data["file"].string
        poi.name = data["name"].string
        poi.calcRate = data["calcRate"].string
        poi.totalReviewer = data["totalReviewer"].string
        poi.categoryId = data["category_id"].string
        poi.distance = data["distance"].string
        poi.icon = data["icon"].string
        
        if let tags = data["tags"].arrayObject {
            poi.tags = tags as [AnyObject]
        }
        poi.thumb = data["thumb"].string
        poi.progress = data["progress"].string
        poi.format = data["format"].string
        poi.videoMetadata = data["video_metadata"].object as AnyObject?
        poi.thumbnail = data["thumbnail"].string
        poi.username = data["username"].string
        poi.desc = data["desc"].string
        poi.timestamp = data["timestamp"].string
        poi.labels = data["labels"].string
        
        return poi
    }
}

