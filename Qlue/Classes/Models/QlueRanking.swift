//
//  QlueRanking.swift
//  Qlue
//
//  Created by Nurul on 8/14/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueRanking{
     
     var avatar : String?
     var code : String?
     var complete : String?
     var process : String?
     var positif: String?
     var negatif: String?
     var ratioAbandon : String?
     var ratioComplete : String?
     var total : String?
     var wait : String?
     var nama : String?
     var rank : Int?
     var averageTimeComplete : String?
     var totalTimeComplete : String?
     
     var userId: String?
     var username: String?
     var report: String?
     var detil: String?
     var bagian: String?
     var provinsi: String?
     
     /**
      * Instantiate the instance using the passed json values to set the properties values
      */
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          avatar = json["avatar"].string
          code = json["code"].string
          nama = json["nama"].string
          rank = json["rank"].int
          
          complete = json["report"]["complete"].string
          process = json["report"]["process"].string
          wait = json["report"]["wait"].string
          
          positif = json["report"]["positif"].string
          negatif = json["report"]["negatif"].string
          
          ratioAbandon = json["report"]["ratio_abandon"].string
          ratioComplete = json["report"]["ratio_complete"].string
          total = json["report"]["total"].string
          
          averageTimeComplete = json["time"]["average_time_complete"].string
          totalTimeComplete = json["time"]["total_time_complete"].string
          
          bagian = json["bagian"].string
          detil = json["detil"].string
          provinsi = json["provinsi"].string
          report = json["report"].string
          userId = json["user_id"].string
          username = json["username"].string

          
     }
     
}