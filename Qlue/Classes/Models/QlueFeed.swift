//
//  QlueFeed.swift
//  Qlue
//
//  Created by Nurul on 6/16/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueFeed: NSObject{
     var feedId: Int?
     
     var avatar: String?
     var username: String?
     var userId: String?
     var timestamp: String?
     
     var title: String?
     var desc: String?
     var progress: String?
     
     var placeName: String?
     var kelurahan: String?
     var kecamatan: String?
     var district: String?
     var kdKel: String?
     
     var countView: Int?
     var distance: Double?
     var countLike: Int?
     var like: Bool?
     var countComment: Int?
     var countPreviewComment: Int?
     
     var fileUrl: String?
     var thumbUrl: String?
     var thumbnail: String?
     var labelUrl: String?
     var icon: String?
     
     var categoryIcon: String?
     var categoryName: String?
     
     var postType: String?
     var reportType: QlueReportType?
     var progressState: feedState?
     
     var format: String?

     var comments: [QlueComment] = []
     
     var lat: Double?
     var lng: Double?
     var poi: QluePoi?
     
     var forumIsVote: Bool?
     var forumIsFav: Bool?
     var forumViewCount: String?
     var forumCommentCount: String?
     
     
     var label2: String?
     var history: Bool?
     
     var qlueTitle: String?
     
     static func feedWithData(_ data: JSON) -> QlueFeed{
          
          let feed: QlueFeed = QlueFeed()
          
          if let feedId =  data["id"].int{
               feed.feedId = feedId
          } else if let feedId = data["id"].string{
               feed.feedId = Int(feedId)
          }
          
          feed.avatar = data["avatar"].string
          feed.progress = data["progress"].string
          feed.username = data["username"].string
          if let userID = data["user_id"].int{
               feed.userId = userID.description
          } else{
               feed.userId = data["user_id"].string
          }
          if let time_stamp = data["time_stamp"].string{
          feed.timestamp = time_stamp
          } else{
               feed.timestamp = data["timestamp"].string
          }
          
          feed.title = data["tags"].array?.last?.string
          feed.qlueTitle = data["title"].string
          feed.desc = data["description"].string
          feed.placeName = data["kelurahan"].string
          feed.kelurahan = data["kelurahan"].string
          feed.kecamatan = data["kecamatan"].string
          feed.kdKel = data["kd_kel"].string
          feed.district = data["district"].string
          feed.format = data["format"].string
          feed.countView = data["count_view"].int
          feed.distance = data["distance"].double
          if let likeCount = data["count_like"].int{
               feed.countLike = likeCount
          } else if let likeCount = data["like_count"].string{
               feed.countLike = Int(likeCount)
          }
          feed.like = data["like"].bool
          
          feed.countComment = data["count_comment"].int
          if let countComment = data["count_comment"].int, countComment > MAX_COMMENT_FEED_LIST {
               feed.countPreviewComment = MAX_COMMENT_FEED_LIST
          } else{
               feed.countPreviewComment = data["count_comment"].int
          }
          
          feed.fileUrl = data["file"].string
          feed.thumbUrl = data["thumb"].string
          feed.thumbnail = data["thumbnail"].string
          feed.labelUrl = data["icon2"].string
          feed.icon = data["icon"].string
          
          feed.postType = data["post_type"].string
          
          if let tags  = data["tags"].array{
               if tags.count > 0{
               if let reportStr = tags.first?.string{
                    feed.reportType = QlueReportType(rawValue: reportStr)
               }
               if let catName = tags[1].string{
                    
                    feed.categoryName = catName
                    }
               }
          }
          
//          if (feed.reportType == .ReviewNegatif) || (feed.reportType == .ReviewPositif){
               feed.categoryIcon = data["icon2"].stringValue.getLabelTopic() + ".png"
//          } else{
//               feed.categoryIcon = data["icon"].stringValue.getLabelTopic() + ".png"
//          }
          
          for comment in data["comments"].arrayValue{
               
               feed.comments.append(QlueComment.commentWithData(comment))
          }
          
          if let lat = data["lat"].double{
               feed.lat = lat
          } else if let latStr = data["lat"].string{
               feed.lat = Double.init(latStr)
          }

          if let lng = data["lng"].double{
               feed.lng = lng
          } else if let lngStr = data["lng"].string{
               feed.lng = Double.init(lngStr)
          }

          if let poiData = data["poi"].dictionaryObject{
               feed.poi = QluePoi.poiWithData(JSON(poiData))
               
          }
          
          feed.progressState = feedState(rawValue: data["progress"].stringValue)
          
          feed.forumIsFav = data["isFav"].bool
          feed.forumIsVote = data["isVote"].bool
          feed.forumViewCount = data["view_count"].string
          feed.forumCommentCount = data["comment_count"].string
          
          
          feed.label2 = data["label2"].string
          
          feed.history = data["history"].bool
          
          return feed
          
     }
     
     
     
}
