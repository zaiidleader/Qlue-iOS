//
//  User.swift
//  Qlue
//
//  Created by Nurul on 7/1/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON


class User: NSManagedObject {
    
    // Insert code here to add functionality to your managed object subclass
    
    static func userWithData(_ data: JSON, inManagedObjectContext moc: NSManagedObjectContext)-> User?{
        
        var user: User?
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        fetchRequest.predicate = NSPredicate(format: "userId == %@", data["id"].stringValue)
        
        do {
            let users = try moc.fetch(fetchRequest) as! [User]
            if users.count > 0 {
                user = users.first
            }
            else {
                user = NSEntityDescription.insertNewObject(forEntityName: "User", into: moc) as? User
                user?.userId = data["id"].string
            }
        }
        catch let error as NSError{
            print(error.localizedDescription)
        }
        
        user?.avatar = data["avatar"].string
        user?.banner = data["banner"].string
        user?.codeKelurahan = data["kd_kel"].string
        user?.codeProvinsi = data["code_provinsi"].string
        user?.coin = data["coin"].string
        user?.currLevel = data["curr_level"].string
        user?.currXp = data["curr_xp"].number
        user?.diamond = data["diamond"].string
        user?.point = data["point"].string
        user?.email = data["email"].string
        user?.followerCount = data["follower_count"].number
        user?.followingCount = data["following_count"].number
        user?.fullname = data["full_name"].string
        user?.kelurahan = data["kd_kel"].string
        user?.level = data["level"].string
        user?.nextLevel = data["next_level"].string
        user?.nextXp = data["next_xp"].number
        user?.phone = data["phone"].string
        user?.provinsi = data["district"].string
        user?.randkey = data["randkey"].string
        user?.sex = data["sex"].string
        user?.username = data["username"].string
        user?.bio = data["bio"].string
        user?.timeJoin = Date().getDateFromTimestamp(data["time_join"].string)
        user?.reportWait = data["report"]["wait"].number
        user?.reportProcess = data["report"]["process"].number
        user?.reportComplete = data["report"]["complete"].number
        
        return user
        
    }
    
    static func currentUser() -> User? {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let moc = appDelegate.managedObjectContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        do{
            let users = try moc.fetch(request) as! [User]
            if users.count > 0 {
                return users.first
            }
        }catch let error as NSError{ print(error.localizedDescription) }
        
        
        return nil
        
    }
    
}
