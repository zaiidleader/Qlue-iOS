//
//  QlueOtherUser.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueOtherUser{
     
     var avatar : String?
     var bagian : String?
     var banner : String?
     var bio : String?
     var categoryWall : String?
     var codeKelurahan : String?
     var codeProvinsi : String?
     var coin : String?
     var currLevel : String?
     var currXp : Int?
     var detil : String?
     var diamond : String?
     var district : String?
     var dob : String?
     var email : String?
     var follow : Bool?
     var followerCount : String?
     var followingCount : String?
     var fullName : String?
     var id : String?
     var isBlocked : Bool?
     var isShowEvent : Bool?
     var isShowReward : Bool?
     var kdKel : String?
     var kredit : String?
     var level : String?
     var nextLevel : String?
     var nextXp : Int?
     var noktp : String?
     var phone : String?
     var point : String?
     var pointEvent : String?
     var sex : String?
     var timeJoin : Date?
     var type : AnyObject?
     var username : String?
     
     // auth
     var changeProgress : Bool?
     var delComForum : Bool?
     var delComReport : Bool?
     var delForum : Bool?
     var delReport : Bool?

     // report
     var complete : Int?
     var process : Int?
     var wait : Int?

     
     
     /**
      * Instantiate the instance using the passed json values to set the properties values
      */
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          
          avatar = json["avatar"].string
          bagian = json["bagian"].string
          banner = json["banner"].string
          bio = json["bio"].string
          categoryWall = json["category_wall"].string
          codeKelurahan = json["code_kelurahan"].string
          codeProvinsi = json["code_provinsi"].string
          coin = json["coin"].string
          currLevel = json["curr_level"].string
          currXp = json["curr_xp"].int
          detil = json["detil"].string
          diamond = json["diamond"].string
          district = json["district"].string
          dob = json["dob"].string
          email = json["email"].string
          follow = json["follow"].bool
          followerCount = json["follower_count"].string
          followingCount = json["following_count"].string
          fullName = json["full_name"].string
          id = json["id"].string
          isBlocked = json["isBlocked"].bool
          isShowEvent = json["isShowEvent"].bool
          isShowReward = json["isShowReward"].bool
          kdKel = json["kd_kel"].string
          kredit = json["kredit"].string
          level = json["level"].string
          nextLevel = json["next_level"].string
          nextXp = json["next_xp"].int
          noktp = json["noktp"].string
          phone = json["phone"].string
          point = json["point"].string
          pointEvent = json["point_event"].string
          
          sex = json["sex"].string
          timeJoin =  Date().getDateFromTimestamp(json["time_join"].string)
          type = json["type"].string as AnyObject?
          username = json["username"].string
          
          
          // auth
          
          let authJson = JSON.init(json["auth"].dictionaryValue)
          changeProgress = authJson["change_progress"].bool
          delComForum = authJson["del_com_forum"].bool
          delComReport = authJson["del_com_report"].bool
          delForum = authJson["del_forum"].bool
          delReport = authJson["del_report"].bool
          
          // report
          let reportJson = JSON.init(json["report"].dictionaryValue)
          complete = reportJson["complete"].int
          process = reportJson["process"].int
          wait = reportJson["wait"].int
     }
     
}

class ReportUser{
     
     var reportId : String?
     var reportName : String?
     
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
     
          reportId = json["reportId"].string
          reportName = json["reportName"].string
     }
}


