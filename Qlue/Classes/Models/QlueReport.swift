//
//  QlueReport.swift
//  Qlue
//
//  Created by Nurul on 7/16/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import CoreLocation
import AVFoundation
import Photos

class QlueReport: NSObject {
     var poi: QluePoi?
     var qlueType: QlueType?
     var image: UIImage?
     var parentTopic: QlueParentTopic?
     var topic: QlueTopic?
     var videoUrl: URL?
     var currentLocation: CLLocation?
     var desc: String?
     var title: String?
     var type: Int = 0
     var file: String?
     var privacy: String? = ""
     var timestamp: String?
     var locationSource: String = ""
     var format: Media?
     
     func createParameterWithReport(_ report: QlueReport, completion: @escaping (_ param: [String: Any]) -> () ) {
          
          var parameter: [String: Any] = [ : ]
          
          parameter["lat"] = report.currentLocation?.coordinate.latitude
          parameter["lng"] = report.currentLocation?.coordinate.longitude
          parameter["address"] = ""
          parameter["desc"] = report.desc ?? ""
          parameter["type"] = report.type
          
          var labels = ""
          if let parentTopic = report.parentTopic?.suggestIcon,
               let topicLabel = report.topic?.suggestIcon,
               let title = report.title{
               labels += parentTopic.getLabelTopic()
               labels += " " + topicLabel.getLabelTopic() + " " + title
          }
          parameter["labels"] = labels
          if let parentTopic = report.parentTopic?.suggestName,
               let topicLabel = report.topic?.suggestName,
               let title = report.title{
               parameter["tags"] = parentTopic + " " + topicLabel + " " + title
          }
          
          
          
          parameter["privacy"] = ""
          parameter["timestamp"] = Date().getTimestamp()
          let pid = report.poi?.placemarkId ?? ""
          
          parameter["pid"] = "Q_" + pid
          parameter["accuracy"] = report.currentLocation?.horizontalAccuracy ?? 0
          
          if let image = report.image{
               let imageData = UIImagePNGRepresentation(image)
               let data = imageData?.base64EncodedString(options: .lineLength76Characters)
               if let format = report.format {
                    
                    parameter["format"] = format.description
                    
                    switch format {
                    case .photo(_): //break
                         parameter["file"] = data ?? ""
                         completion(parameter)
//                         parameter["file"] = ""
                    default:
                         parameter["thumbnail"] = data ?? ""
                         
                         parameter["video_metadata"] = "{\"width\":\(image.size.width), \"height\": \(image.size.height)}"
                         if let videoUrl = report.videoUrl{
                              
                              let fetchResult = PHAsset.fetchAssets(withALAssetURLs: [videoUrl], options: nil)
                              if let phAsset = fetchResult.firstObject {
                                   PHImageManager.default().requestAVAsset(forVideo: phAsset, options: PHVideoRequestOptions(), resultHandler: { (asset, audioMix, info) -> Void in
                                        if let asset = asset as? AVURLAsset {
                                             let videoData = try? Data(contentsOf: asset.url)
                                             let videoEncode = videoData?.base64EncodedString(options: .lineLength64Characters)
                                             if let file = videoEncode{
                                                  parameter["file"] = file
                                                  completion(parameter)
                                             }else{
                                                  print("Video ga ke encode !!!")
                                                  completion(parameter)
                                             }
                                        } else{
                                             completion(parameter)
                                        }
                                   })
                              }
                              else {
                                   completion(parameter)
                              }
                              
//                              if let path = videoUrl.path{
//                              let videoData = NSFileManager.defaultManager().contentsAtPath(path)
////                              
////                              NSString *path = [pathURL path];
////                              NSData *data = [[NSFileManager defaultManager] contentsAtPath:path];
////                              let videoData = NSData.init(contentsOfURL: videoUrl)
//                              let videoEncode = videoData?.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
//                              if let file = videoEncode{
//                                   parameter["file"] = file
//                              }else{
//                                   print("Video ga ke encode !!!")
//                              }
//                         }
                              
                              
                              
                         }
                         }
                         
               } else{
                    completion(parameter)
               }
          } else{
               completion(parameter)
          }
          
     }
}
