//
//  QlueTrophy.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 9/18/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueTrophy {
     
     var id : String?
     var date : String?
     var icon : String?
     var rank : String?
     var total : String?
     
     
     /**
      * Instantiate the instance using the passed json values to set the properties values
      */
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          id = json["Id"].string
          date = json["date"].string
          icon = json["icon"].string
          rank = json["rank"].string
          total = json["total"].string
     }

}
