//
//  QlueUserAvatar.swift
//  Qlue
//
//  Created by Nurul Rachmawati on 10/9/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import SwiftyJSON

class QlueUserAvatar {
     var avatar : String?
     var desc : String?
     var grade : String?
     var isGov : Bool?
     var level : String?
     var type : String?
     
     init(fromJson json: JSON!){
          if json == nil{
               return
          }
          avatar = json["avatar"].string
          desc = json["desc"].string
          grade = json["grade"].string
          isGov = json["isGov"].bool
          level = json["level"].string
          type = json["type"].string
     }
}
