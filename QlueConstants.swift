//
//  QlueConstants.swift
//  Qlue
//
//  Created by Nurul on 5/25/16.
//  Copyright © 2016 Qlue. All rights reserved.
//

import Foundation
import UIKit

let kDarkColor = UIColor(red: 0.153, green: 0.196, blue: 0.216, alpha: 1.00)
let kQlueBlue = UIColor(red: 0.118, green: 0.616, blue: 0.922, alpha: 1.0)
let kBgNavBar = UIColor(red: 0.149, green: 0.592, blue: 0.925, alpha: 1.0)

let kGreyColor = UIColor(red: 0.875, green: 0.875, blue: 0.875, alpha: 1.0)
let kGreyEEE = UIColor(red: 0.933, green: 0.933, blue: 0.933, alpha: 1.0)
let kGrey708896Color = UIColor.init(red: 0.439, green: 0.533, blue: 0.588, alpha: 1.00)
let kDarkBlue = UIColor(red: 0.094, green: 0.275, blue: 0.561, alpha: 1.0)
let kBgTabBar = UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1.0)

let kInactiveTabBar = UIColor(red: 0.741, green: 0.741, blue: 0.741, alpha: 1.0)
let kActiveTabBar = UIColor(red: 0.129, green: 0.588, blue: 0.953, alpha: 1.0)
let kCreamColor = UIColor(red: 1.000, green: 0.878, blue: 0.718, alpha: 1.00)
let kBgCellBlue = UIColor(red: 0.894, green: 0.949, blue: 0.992, alpha: 1.00)
let kRedColor = UIColor.init(red: 0.875, green: 0.286, blue: 0.224, alpha: 1.00)
let kPinkEA1E63Color = UIColor.init(red: 0.918, green: 0.118, blue: 0.388, alpha: 1.0)
let kBlueE0F5FDColor = UIColor.init(red: 0.878, green: 0.961, blue: 0.992, alpha: 1.0)
let kBlue0988EEColor = UIColor.init(red: 0.035, green: 0.596, blue: 0.933, alpha: 1.00)
let kCommunityForumIndicatorColor = UIColor.init(red: 1.000, green: 0.757, blue: 0.027, alpha: 1.00)


let kWaitRedColor = UIColor.init(red: 0.933, green: 0.325, blue: 0.310, alpha: 1.00)
let kProcessYellowColor =  UIColor.init(red: 1.000, green: 0.761, blue: 0.031, alpha: 1.00)
let kCompleteGreenColor = UIColor.init(red: 0.298, green: 0.690, blue: 0.314, alpha: 1.00)

struct QlueFont {
    static let ProximaNovaSoft = "ProximaNovaSoft-Bold"
    static let HelveticaNeue = "HelveticaNeue"
     static let ProximaNovaRegular = "ProximaNova-Regular"
    
}

let kUrlSchemeGoogle = "org.qlue.qlueios"
//let kUrlSchemeFb = "fb1057347661012668"
let kUrlSchemeFb = "fb1567764003446588"
let kConsumerKeyTwitter = "4wJsx49URdwoarFNkiu86HDc3"
let kConsumerSecretTwitter = "KSKe6Ilk06HbFMOsx9Q7DY3Dslqv7ClJOKWrSYnSXtXdGtKEMl"
let kGoogleMaps = "AIzaSyAW5g7dZAqEtxPFJaLZPr-pK77rYsNDh6I"

let kLastReportGov = "laporanterakhirGov"
let kLastReportBuss = "laporanterakhirBuss"

let kReceiveNotification = "kReceiveNotification"

enum QlueSegueEnum: String{
    case Home = "HomeSegue"
}

let kUserKey = "kUserKey"
let kUserId = "kUserId"

let MAX_COMMENT_FEED_LIST = 3

let kUserDidUpdateNotification = "kUserDidUpdateNotification"


let feedStoryboard = UIStoryboard(name: "Feed", bundle: nil)
let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let reportStoryboard = UIStoryboard(name: "Report", bundle: nil)
let profileStoryboard = UIStoryboard(name: "Profile", bundle: nil)
let kelurahanStoryboard = UIStoryboard(name: "Kelurahan", bundle: nil)
let moreStoryboard = UIStoryboard(name: "More", bundle: nil)
let forumStoryboard = UIStoryboard(name: "Forum", bundle: nil)
let chatStoryboard = UIStoryboard(name: "Chat", bundle: nil)
let menuStoryboard = UIStoryboard(name: "Menu", bundle: nil)
let searchStoryboard = UIStoryboard(name: "Search", bundle: nil)
let settingsStoryboard = UIStoryboard(name: "Settings", bundle: nil)
let fabStoryboard = UIStoryboard(name: "FAB", bundle: nil)

let kDidUpdateLocationNotification: String = "kDidUpdateLocationNotification"

let barButtonStyle : [String:AnyObject]? = [
     NSForegroundColorAttributeName: kBgNavBar,
     NSFontAttributeName : UIFont(name: QlueFont.ProximaNovaSoft, size: 13)!
]

let placeholderImage: UIImage = UIImage.init(named: "ic_placeholder")!

// MARK: - HTML Files without .html
let kAboutHtmlFile = "about_id"
let kEulaHtmlFile = "eula"
let kFaqHtmlFile = "faq_id"


// MARK: - feedState

enum feedState: String{
    case Wait = "wait"
    case Process = "process"
    case Complete = "complete"
    
    
    var colorDescription: String {
        switch self {
        case .Wait : return "Red"
        case .Process : return "Yellow"
        case .Complete : return "Green"
        }
    }
    
     var colorGrid: UIColor{
          switch self {
          case .Wait : return kWaitRedColor
          case .Process : return kProcessYellowColor
          case .Complete : return kCompleteGreenColor
          }
     }
     
    var image: UIImage?{
        switch self {
        case .Wait : return UIImage(named: "ic_progressmeter_waiting")
        case .Process : return UIImage(named: "ic_progress_yellow")
        case .Complete : return UIImage(named: "ic_progress_green")
        }
    }
     
     var labelDesc: String{
          switch self {
          case .Wait : return "Waiting For Process"
          case .Process : return "Click Yellow For Detail Process"
          case .Complete : return "Click Green For Detail Complete"
          }
     }
}

// MARK: - Gender

enum Gender: String{
    case Male = "male"
    case Female = "female"
    
    var short: String{
        switch  self {
        case .Male:
            return "M"
        default:
            return "F"
        }
    }
    
    var description: String{
        switch self {
        case .Male:
            return "Male"
        default:
            return "Female"
        }
    }
}

// MARK: - QlueType 
enum QlueType{
    case goverment
    case business
}


// MARK: - QlueReportType
enum QlueReportType: String{
    case ReviewNegatif = "Review Negatif"
    case ReviewPositif = "Review Positif"
    case Reporting = "Reporting"
    case Keluhan = "Keluhan Masyarakat"
     
    
    var likeImageActive: UIImage?{
        switch self {
        case .ReviewPositif:
            return UIImage.init(named: "btn_like_clicked")
        default:
            return UIImage.init(named: "btn_support_clicked")
        }
    }
    var likeImageDeactive: UIImage?{
        switch self {
        case .ReviewPositif:
            return UIImage.init(named: "btn_like_normal")
        default:
            return UIImage.init(named: "btn_support_normal")
        }
    }
    var likeImageActive2: UIImage?{
        switch self {
        case .ReviewPositif:
            return UIImage.init(named: "ic_love_on")
        default:
            return UIImage.init(named: "ic_support_on")
        }
     }
     var likeImageDeactive2: UIImage?{
        switch self {
        case .ReviewPositif:
            return UIImage.init(named: "ic_love_off")
        default:
            return UIImage.init(named: "ic_support_off")
        }
     }
     var stateProgress: Bool{
          switch self {
          case .Reporting:
               return true
          default:
               return false
          }
     }
}

// MARK: - Place Mark
enum PlaceMark: String{
     case School = "ic_2schl.png"
     case Restaurant = "ic_2resto.png"
     case Bank = "ic_2pbkn.png"
     case Company = "ic_2prshn.png"
     case Health = "ic_2kshtn.png"
     case WorshipPlace = "ic_2pray.png"
     case EntertainmentArea = "ic_2entr.png"
     case PublicFacilities = "ic_2fsum.png"
     
     var imageNamed: String{
          switch self {
          case .School:
               return "ic_mark_schl"
          case .Restaurant:
               return "ic_mark_resto"
          case .Bank:
               return "ic_mark_pbkn"
          case .Company:
               return "ic_mark_prshn"
          case .Health:
               return "ic_mark_kshtn"
          case .WorshipPlace:
               return "ic_mark_pray"
          case .EntertainmentArea:
               return "ic_mark_entr"
          case .PublicFacilities:
               return "ic_mark_fsum"
          }
     }
     
     var image: UIImage?{
          return UIImage.init(named: self.imageNamed)
     }
}

// MARK: - Push Notification Types
enum NotificationType: String {
     case Message = "16"
     case Forum = "7"
     case ForumDetail = "8"
     case Follow = "12"
     case Admin = "24"
     case MentionPost = "21"
     case MentionForum = "22"
     case EditKelurahan = "23"
     case ApproveAvatar = "10"
     case Like = "0"
     case ProgressUpdate = "25"
     case FAQReject = "27"
     case ForumKelurahan = "28"
     case UserComment = "1"
     case Conversation = "3"
     case ReportInNeighborhood = "5"
     case PromoCode = "30"
}
